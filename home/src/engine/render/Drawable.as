package engine.render 
{
	import engine.render.scene.SceneLayer;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Drawable
	{				
		private var _Image:Image;
		
		public var Display:Sprite;
		
		private var _Rotation:int;
		
		private var _RawData:BitmapData;
		
		private var _Normal:Point;
		
		private var _Layer:SceneLayer;
		
		private var _RotationDirty:Boolean = false;
		
		public function Drawable() 
		{
			_Normal = new Point( 0, 1 );
			
			Display = new Sprite();
		}
		
		public function reset():void
		{
			Display.rotation = 0;
			Display.x = 0;
			Display.y = 0;
			Display.scaleX = 1;
			Display.scaleY = 1;
		}
		
		public function rotate( amount:int ):void
		{
			if ( amount == 0 )
				return;
				
			_RotationDirty = true;
				
			const radians:Number = amount * ( Math.PI / 180 );
			
			Display.rotation += radians;
		}
		
		public function setCenter( x:int, y:int ):void
		{
			Display.pivotX = x;
			Display.pivotY = y;
		}
		
		public function addChild( child:DisplayObject ):void
		{
			Display.addChild( child );
		}
		
		public function removeChild( child:DisplayObject ):void
		{
			Display.removeChild( child );
		}
		
		// Mutators
		public function set RawData( value:BitmapData ):void
		{ 
			_RawData = value;
			
			// @TODO: optimize this?
			var texture:Texture = Texture.fromBitmapData( value );
			
			_Image = new Image( texture );
			
			Display.addChild( _Image );
			
			Display.pivotX = _RawData.width / 2;
			Display.pivotY = _RawData.height / 2;
		}
		
		public function set worldX( value:Number ):void
		{
			Display.x = value;
		}
		
		public function set worldY( value:Number ):void
		{
			Display.y = -value;	// Inverted world y
		}
		
		// Accessors
		public function get sourceWidth():int { return _RawData.width; }
		public function get sourceHeight():int { return _RawData.height; }
		
		public function get worldX():Number
		{			
			return Display.x;
		}
		
		public function get worldY():Number
		{
			return -Display.y;
		}
		
		public function transformPoint( point:Point ):Point
		{
			if ( !_Layer )
				return point;
				
			return Display.transformationMatrix.transformPoint( point );
		}
		
		public function get normal():Point
		{
			if ( _RotationDirty )
			{
				var matrix:Matrix = Display.transformationMatrix.clone();
				matrix.tx = 0;
				matrix.ty = 0;
				
				// We're just concerned with rotation, not translation, for generating a normal
				_Normal.x = 0;
				_Normal.y = 1;
				_Normal = matrix.transformPoint( _Normal );
				
				_Normal.x *= -1;
			}
			
			_RotationDirty = false;
			
			return _Normal;
		}
		
		public function get matrix():Matrix { return Display.transformationMatrix; }
		
		public function get cachedWidth():int { return _RawData.width; }
		public function get cachedHeight():int { return _RawData.height; }
		
		public function get width():int { return Display.width; }
		public function get height():int { return Display.height; }
		
		public function get layer():SceneLayer { return _Layer; }
		public function set layer( value:SceneLayer ):void { _Layer = value; }
		
		public function set alpha( value:Number ):void
		{
			Display.alpha = value;
		}
		
		public function get alpha():Number { return Display.alpha; }
		
		public function destroy():void
		{
			
		}
	}
}