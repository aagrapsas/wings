package engine.render.animation 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.GenericPool;
	import engine.misc.IPoolable;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Animation extends Sprite implements IAnimated
	{	
		private var _Display:Image;
		
		private var _Frames:Vector.<Texture>;
		
		private var _Duration:Number;
		private var _AnimationAccumulator:Number = 0;
		private var _CurrentFrame:int = -1;
		private var _IsLooping:Boolean;
		
		private var _FrameCallbacks:Dictionary;
		
		private var _FinishCallback:Function;
		
		private var _IsPaused:Boolean = false;
		
		private var _Pool:GenericPool;
		
		public function Animation( frames:Vector.<Texture>, duration:Number, isLooping:Boolean ) 
		{
			_FrameCallbacks = new Dictionary;
			_Frames = frames
			_Duration = duration;
			
			_IsLooping = isLooping;
			
			_Display = new Image( _Frames[ 0 ] );
			_Display.pivotX = _Display.width / 2;
			_Display.pivotY = _Display.height / 2;
			
			this.addChild( _Display );
			
			_Display.visible = false;
			
			setupImages();
		}
		
		private function setupImages():void
		{

		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _IsPaused )
				return;
			
			const frame:int = Math.min( ( _AnimationAccumulator / _Duration ) *  _Frames.length, _Frames.length - 1 );
			
			showFrame( frame );
			
			// Debug.debugLog( EngineLogChannels.ANIMATION, "Frame: " + frame );
			
			_AnimationAccumulator += deltaTime;
			
			if ( _AnimationAccumulator >= _Duration )
			{
				if ( _FinishCallback != null )
				{
					_FinishCallback.apply( null, [this] );
				}
				
				if ( _IsLooping )
				{
					_AnimationAccumulator = 0;
				}
				else
				{
					_Display.visible = false;
					_IsPaused = true;
				}
			}
		}
		
		private function showFrame( frame:int ):void
		{
			if ( _CurrentFrame != frame )
			{
				if ( _FrameCallbacks[ frame ] )
				{
					// pass reference back
					_FrameCallbacks[ frame ].apply( null, [this] );
				}
				
				_CurrentFrame = frame;
				
				_Display.texture = _Frames[ _CurrentFrame ];
			}
		}
		
		public function registerFinishCallback( callback:Function ):void
		{
			_FinishCallback = callback;
		}
		
		public function registerCallback( frame:int, callback:Function ):void
		{
			_FrameCallbacks[ frame ] = callback;
		}
		
		public function goToFrame( frame:int ):void
		{
			if ( frame > _Frames.length )
				return;
			
			var timePerFrame:Number = _Duration / _Frames.length;
			
			_AnimationAccumulator = timePerFrame * frame - timePerFrame;
		}
		
		public function pause():void
		{
			_IsPaused = true;
		}
		
		public function playOnce( shouldInterrupt:Boolean ):void
		{
			_IsPaused = false;
			_IsLooping = false;
			
			_Display.visible = true;
			
			if ( shouldInterrupt || _AnimationAccumulator >= _Duration )
			{
				_AnimationAccumulator = 0;
				_CurrentFrame = -1;
			}
		}
		
		public function resume():void
		{
			_IsPaused = false;
		}

		public function set IsLooping( value:Boolean ):void { _IsLooping = value; }
		public function set Duration( value:Number ):void { _Duration = value; }
		public function get Frames():Vector.<Texture> { return _Frames; }
		public function get Duration():Number { return _Duration; }
		public function get IsLooping():Boolean { return _IsLooping; }
		
		public function set pool( value:GenericPool ):void { _Pool = value; }
		
		public function get Width():int { return _Frames[ 0 ].width; }
		public function get Height():int { return _Frames[ 0 ].height; }
		
		public function destroy():void
		{			
			if ( _Pool )
			{
				_Pool.push( this );
			}
		}
		
		public function suspend():void
		{
			
		}
		
		public function resurrect():void
		{
			_FrameCallbacks = new Dictionary();
		}
	}
}