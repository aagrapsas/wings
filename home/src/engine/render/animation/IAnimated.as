package engine.render.animation 
{
	import engine.interfaces.ITickable;
	import engine.misc.IPoolable;
	import flash.display.IBitmapDrawable;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IAnimated extends IBitmapDrawable, ITickable, IPoolable
	{
		function registerFinishCallback( callback:Function ):void;
		function playOnce( shouldInterrupt:Boolean ):void;
	}
}