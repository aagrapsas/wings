package engine.render.scene 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneLayer implements ITickable, IDestroyable
	{
		public var MovementPercent:Number;
		public var Name:String;
		
		private var _X:Number;
		private var _Y:Number;
		
		public var Display:Sprite;
		
		public function SceneLayer( name:String, movementPercent:Number ) 
		{
			Name = name;
			MovementPercent = movementPercent;
			
			Display = new Sprite();
		}
		
		public function get X():Number { return _X; }
		public function set X( value:Number ):void
		{
			_X = value;
			
			Display.x = value;
		}
		
		public function get Y():Number { return _Y; }
		public function set Y( value:Number ):void
		{
			_Y = value;
			
			Display.y = value;
		}
		
		public function tick( deltaTime:Number ):void { }
		
		public function get width():int 
		{ 
			return Display.width;
		}
		
		public function get height():int
		{			
			return Display.height;
		}
		
		public function destroy():void
		{

		}
	}
}