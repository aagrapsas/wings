package engine.render.scene 
{
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneFade extends Sprite implements ITickable
	{	
		private var _Direction:int;
		private var _Duration:Number;
		private var _Alpha:Number;
		
		private var _Scene:SceneManager;
		private var _Stage:Stage;
		
		private var _Accumulator:Number;
		
		public function SceneFade( stage:Stage, scene:SceneManager ) 
		{
			_Stage = stage;
			_Scene = scene;
			
			drawRectangle();
		}
		
		private function drawRectangle():void
		{
			var graphics:flash.display.Sprite = new flash.display.Sprite();
			
			graphics.graphics.clear();
			graphics.graphics.beginFill( 0x000000, 1 );
			graphics.graphics.drawRect( 0, 0, _Stage.stageWidth, _Stage.stageHeight );
			graphics.graphics.endFill();
			
			graphics.cacheAsBitmap = true;
			
			var data:BitmapData = new BitmapData( graphics.width, graphics.height );
			data.draw( graphics );
			
			this.addChild( new Image( Texture.fromBitmapData( data ) ) );
			
			this.alpha = 0;
		}
		
		public function startFade( direction:int, duration:Number ):void
		{
			_Direction = direction > 0 ? 1 : -1;
			_Alpha = _Direction > 0 ? 0 : 1;
			_Accumulator = 0;
			
			_Duration = duration;
			
			_Scene.addTickable( this );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _Accumulator >= _Duration )
			{
				_Scene.removeTickable( this );
			}
			
			this.alpha = _Direction > 0 ? _Accumulator / _Duration : 1 - ( _Accumulator / _Duration );
			
			_Accumulator += deltaTime;
		}
		
		public function get IsBlackedOut():Boolean
		{
			return ( _Alpha >= 1 );
		}
	}
}