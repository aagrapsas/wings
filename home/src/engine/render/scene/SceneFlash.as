package engine.render.scene 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.display.BitmapData;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneFlash extends Sprite implements IDestroyable, ITickable
	{
		private var _Stage:Stage;
		private var _Dir:int;
		private var _Manager:SceneManager;
		
		private var _TransitionLength:Number = 0;
		private var _TransitionStart:Number = 0;
		
		public function SceneFlash( stage:Stage, manager:SceneManager ) 
		{
			_Stage = stage;
			_Manager = manager;
			
			initialize();
		}
		
		private function initialize():void
		{
			var graphics:flash.display.Sprite = new flash.display.Sprite();
			
			graphics.graphics.clear();
			graphics.graphics.beginFill( 0x000000, 1 );
			graphics.graphics.drawRect( 0, 0, _Stage.stageWidth, _Stage.stageHeight );
			graphics.graphics.endFill();
			
			graphics.cacheAsBitmap = true;
			
			var data:BitmapData = new BitmapData( graphics.width, graphics.height );
			data.draw( graphics );
			
			this.addChild( new Image( Texture.fromBitmapData( data ) ) );
			
			this.alpha = 0;
			
			_Stage.addChild( this );
		}
		
		public function flash( duration:Number ):void
		{
			_TransitionLength = duration / 2;
			
			_Dir = 1;
			
			_TransitionStart = _Manager.GameTime ;
			
			_Manager.addTickable( this );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _Dir > 0 )
			{
				var alpha:Number = ( _Manager.GameTime - _TransitionStart ) / _TransitionLength;
				alpha = Math.min( MathUtility.getEaseOutAlpha( alpha ), 1 );
				
				this.alpha = alpha;
				
				if ( alpha == 1 )
				{
					_Dir = -1;
					_TransitionStart = _Manager.GameTime;
				}
			}
			else
			{
				var alphaN:Number = Math.max( 1 - ( _Manager.GameTime - _TransitionStart ) / _TransitionLength, 0 );
				alphaN = MathUtility.getEaseInAlpha( alphaN );
				
				this.alpha = alphaN;
				
				if ( alphaN == 0 )
				{
					_Manager.removeTickable( this );
				}
			}
		}
		
		public function destroy():void
		{
			_Stage.removeChild( this );
		}
	}
}