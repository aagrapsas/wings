package engine.render.scene 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.render.Camera;
	import engine.render.Drawable;
	import engine.ui.IPopup;
	import flash.display.Bitmap;
	import flash.display.Graphics;
	import flash.display.StageDisplayState;
	import starling.events.Event;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	import game.misc.FontSystem;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneManager implements IDestroyable
	{
		private var _Root:QuadTreeNode;
		
		private var _Stage:Stage;
		private var _CachedCollisionSceneNode:SceneNode;
		private var _Actors:Vector.<Actor>;
		private var _Drawables:Vector.<Drawable>;
		private var _InternalSprite:Sprite;
		private var _UILayer:Sprite;
		private var _TickList:Vector.<ITickable>;
		private var _UnpausableTickList:Vector.<ITickable>;
		private var _LastFrameTime:Number = 0;
		
		private var _IsFullScreen:Boolean;
		
		private var _WorldUILayer:SceneLayer;
		
		public var Layers:Dictionary;
		
		public var isGameReady:Boolean = false;
		
		private var _FadeToBlack:SceneFade;
		private var _SceneFlash:SceneFlash;
		
		// Cutscene Blinds
		private var _TopBlind:SceneBlind;
		private var _BottomBlind:SceneBlind;
		private const BLIND_SIZE:uint = 40;
		
		// Caption text
		private var _PendingText:String;							// Text to next swap in
		private var _PendingDuration:Number					// Duration to use after swap
		private var _CaptionTextBox:TextField;					// TextField itself
		private var _TextTransitionStamp:Number;			// When the previous transition started
		private var _TextDisplayStamp:Number;				// When the text was first posted
		private var _TransitionDuration:Number = 0.25;	// How long a transition should take
		private var _TextRemainDuration:Number;			// How long the text should remain posted
		private var _CaptionAlphaDirection:int;										// Direction of the transition
		
		public var _GameTime:Number = 0;
		
		private var _IsPaused:Boolean = false;
		
		// Pop-up management, for now
		private var _ActivePopup:IPopup;
		
		public function SceneManager( stage:Stage ) 
		{
			_Stage = stage;
			
			_CachedCollisionSceneNode = new SceneNode( new RectCollider( 0, 0, 10, 10, CollisionFlags.ALL ) );
			_Actors = new Vector.<Actor>;
			_TickList = new Vector.<ITickable>;
			_UnpausableTickList = new Vector.<ITickable>();
			_Drawables = new Vector.<Drawable>;
			
			_FadeToBlack = new SceneFade( stage, this );
			
			populateLayers();
			
			// Loop
			_Stage.addEventListener( Event.ENTER_FRAME, update );
			
			setupBlinds();
			setupCaptions();
			
			_UILayer = new Sprite();
			_Stage.addChild( _UILayer );
			
			_Stage.addChild( _FadeToBlack );
			
			_SceneFlash = new SceneFlash( _Stage, this );
		}
		
		public function setupQuadTree( worldWidth:uint, worldHeight:uint, poolSize:uint ):void
		{
			if ( _Root )
			{
				_Root.freeRecursive();
			}
			
			_Root = new QuadTreeNode( new RectCollider( 0, 0, worldWidth, worldHeight, CollisionFlags.ALL ), null, 0, new QuadTreeNodePool( poolSize ) );
		}
		
		private function setupBlinds():void
		{
			// Blinds
			//_TopBlind = new SceneBlind( -BLIND_SIZE, 0, BLIND_SIZE, _Stage.stageWidth );
			//_BottomBlind = new SceneBlind( _Stage.stageHeight, _Stage.stageHeight - BLIND_SIZE, BLIND_SIZE, _Stage.stageWidth );
			//_Stage.addChild( _TopBlind.Display );
			//_Stage.addChild( _BottomBlind.Display );
		}
		
		private function setupCaptions():void
		{
			// Captions
			_CaptionTextBox = new TextField( _Stage.stageWidth, 150, "", FontSystem.PIRULEN, 18, 0xFFFFFF );
			_CaptionTextBox.alpha = 0.5;
			_CaptionTextBox.vAlign = VAlign.CENTER;
			_CaptionTextBox.hAlign = HAlign.CENTER;
			
			_Stage.addChild( _CaptionTextBox );
		}
		
		private function populateLayers():void
		{
			_InternalSprite = new Sprite;
			Layers = new Dictionary;			
			_Stage.addChild( _InternalSprite );
			
			setupSpecialLayers();
		}
		
		public function addLayer( layer:SceneLayer ):void
		{			
			Layers[ layer.Name ] = layer;
			
			_InternalSprite.addChild( layer.Display );
			
			_InternalSprite.setChildIndex( _WorldUILayer.Display, _InternalSprite.numChildren - 1 );
		}
		
		public function removeLayer( layer:SceneLayer ):void
		{
			_InternalSprite.removeChild( layer.Display );
			
			delete Layers[ layer.Name ];
		}
		
		public function clearLayers():void
		{
			for each ( var layer:SceneLayer in Layers )
			{					
				_InternalSprite.removeChild( layer.Display );				
				
				layer.destroy();
			}
			
			Layers = new Dictionary;
			
			setupSpecialLayers();
		}
		
		private function setupSpecialLayers():void
		{
			_WorldUILayer = new SceneLayer( "world_ui", 1.0 );
			
			Layers[ _WorldUILayer.Name ] = _WorldUILayer;
			_InternalSprite.addChild( _WorldUILayer.Display );
		}
		
		public function addPopup( popup:IPopup ):void 
		{
			clearPopup();
			
			_ActivePopup = popup;
			
			_UILayer.addChild( _ActivePopup.display );
		}
		
		public function clearPopup():void
		{
			if ( _ActivePopup )
			{
				_ActivePopup.destroy();
				_UILayer.removeChild( _ActivePopup.display );
				_ActivePopup = null;
				
				//_Stage.focus = null;
			}
		}
		
		public function toggleBlinds( shouldShow:Boolean ):void
		{			
			if ( shouldShow )
			{
				_TopBlind.showBlind( 1 );
				_BottomBlind.showBlind( 1 );
			}
			else
			{
				_TopBlind.hideBlind( 1 );
				_BottomBlind.hideBlind( 1 );
			}
		}
		
		private function update( e:Event ):void
		{
			var elapsedTime:Number = 1 / 30;// ( getTimer() / 1000 ) - _LastFrameTime;
			
			// Debug.perfMon.frameStart();
			
			if ( _ActivePopup )	// In-case something needs to animate
			{
				_ActivePopup.tick( elapsedTime );
			}
			
			// Audio manager, etc. fit in this category
			for each ( var unpausable:ITickable in _UnpausableTickList )
			{
				unpausable.tick( elapsedTime );
			}
			
			if ( _IsPaused == false )
			{
				// _Stage.focus = null;
				
				_GameTime += elapsedTime;
			
				for each ( var tickable:ITickable in _TickList )
				{				
					tickable.tick( elapsedTime );
				}
				
				updateCaption();
				
				updateDisplayState();
			}
			
			// Debug.perfMon.frameEnd();
			
			// _LastFrameTime = getTimer() / 1000;
		}
		
		private function updateCaption():void
		{
			if ( _CaptionTextBox.text != "" )
			{
				if ( _CaptionAlphaDirection != 0 )
				{
					var elapsedTime:Number = _GameTime - _TextTransitionStamp;
					var alpha:Number = _CaptionAlphaDirection > 0 ? Math.min( elapsedTime / _TransitionDuration, 1 ) : Math.max( 0, 1 - ( elapsedTime / _TransitionDuration ) );
					_CaptionTextBox.alpha = alpha;
					
					if ( elapsedTime >= _TransitionDuration )
					{
						if ( _CaptionAlphaDirection == -1 )
						{
							// Handle situation with pending text
							if ( _PendingText != null && _PendingText != "" )
							{
								_CaptionTextBox.text = _PendingText;
								_TextRemainDuration = _PendingDuration;
								_PendingText = "";
								_CaptionAlphaDirection = 1;
								_TextTransitionStamp = _GameTime;
							}
							else	// Otherwise, clear
							{
								_CaptionTextBox.text = "";
								_CaptionAlphaDirection = 0;
							}
						}
						else	// Otherwise, it's been shown, now mark it so we can eventuall transition out
						{
							_TextDisplayStamp = _GameTime;
							_CaptionAlphaDirection = 0;
						}
					}
				}
				else
				{
					var totalElapsedTime:Number = _GameTime - _TextDisplayStamp;
					
					if ( totalElapsedTime >= _TextRemainDuration )
					{
						_CaptionAlphaDirection = -1;
						_TextTransitionStamp = _GameTime;
					}
				}
			}
		}
		
		private function updateDisplayState():void
		{		
			/*
			if ( _PreviousDisplayState != _Stage.displayState )
			{
				const stageWidth:int = 730;
				const stageHeight:int = 600;
				
				var shouldShowBlind:Boolean = _TopBlind.IsVisible && _BottomBlind.IsVisible;
				
				_PreviousDisplayState = _Stage.displayState;
				
				const isFullScreen:Boolean = _Stage.displayState == StageDisplayState.FULL_SCREEN;
				const height:int = isFullScreen ? _Stage.fullScreenHeight : stageHeight;
				const left:int = isFullScreen ? stageWidth - _Stage.fullScreenWidth : 0;
				const width:int = isFullScreen ? _Stage.fullScreenWidth + Math.abs( left ) : _Stage.stageWidth;
				const verticalDiff:int = isFullScreen ? _Stage.fullScreenHeight - stageHeight : 0;
				
				_TopBlind.reDraw( left, -BLIND_SIZE - verticalDiff/2, -verticalDiff/2, BLIND_SIZE, width );
				_BottomBlind.reDraw( left, height - verticalDiff/2, height - verticalDiff/2 - BLIND_SIZE, BLIND_SIZE, width );
				
				if ( shouldShowBlind )
				{
					_TopBlind.showBlind(BLIND_SIZE);
					_BottomBlind.showBlind(BLIND_SIZE);
				}
			}
			*/
		}
		
		public function addActor( actor:Actor ):void
		{
			_Actors.push( actor );
			
			actor.ParentLayer = Layers[ "objects" ];
			
			if ( actor.Display )
			{
				actor.ParentLayer.Display.addChild( actor.Display.Display );
			}
			
			if ( actor.Node )
				_Root.add( actor.Node );
		}
		
		public function removeActor( actor:Actor ):void
		{
			var index:int = _Actors.indexOf( actor );
			
			if ( index > -1 )
			{
				if ( Actors[ index ].Node )
					Actors[ index ].Node.remove();
				
				_Actors.splice( index, 1 );
				
				actor.ParentLayer.Display.removeChild( actor.Display.Display );
			}
		}
		
		public function addWorldObject( object:DisplayObject ):void
		{			
			Layers[ "objects" ].Display.addChild( object );
		}
		
		public function removeWorldObject( object:DisplayObject ):void
		{
			Layers[ "objects" ].Display.removeChild( object );
		}
		
		public function addTickable( tickable:ITickable, isPausable:Boolean = true ):void
		{
			if ( isPausable )
			{
				_TickList.push( tickable );
			}
			else
			{
				_UnpausableTickList.push( tickable );
			}
		}
		
		public function removeTickable( tickable:ITickable ):void
		{
			var index:int = _TickList.indexOf( tickable );
			
			if ( index > - 1 )
			{
				_TickList.splice( index, 1 );
			}
		}
		
		public function lineCheck( x1:int, y1:int, x2:int, y2:int, collisionFlag:uint, excludeNode:SceneNode ):Vector.<SceneNode>
		{
			return _Root.getLineColliders( x1, y1, x2, y2, collisionFlag, excludeNode );
		}
		
		public function rectCheck( x:int, y:int, width:uint, height:uint, collisionFlag:uint, debugGraphics:Graphics = null, debugColor:uint = 0 ):Vector.<SceneNode>
		{
			_CachedCollisionSceneNode.CollisionComponent.X = x;
			_CachedCollisionSceneNode.CollisionComponent.Y = y;
			_CachedCollisionSceneNode.CollisionComponent.Width = width;
			_CachedCollisionSceneNode.CollisionComponent.Height = height;
			_CachedCollisionSceneNode.CollisionComponent.CollisionFlag = collisionFlag;
			
			if ( debugGraphics )
			{
				_CachedCollisionSceneNode.CollisionComponent.drawDebug( debugGraphics, new Point( 1, -1 ), debugColor );
			}
			
			return _Root.getColliders( _CachedCollisionSceneNode, false );
		}
		
		public function collisionCheck( node:SceneNode, isAccurate:Boolean = false ):Vector.<SceneNode>
		{
			return _Root.getColliders( node, isAccurate );
		}
		
		public function pointCheck( x:int, y:int, collisionFlag:uint ):Vector.<SceneNode>
		{
			return _Root.getPointColliders( x, y, collisionFlag );
		}
		
		public function showText( text:String, duration:Number = 5.0 ):void
		{
			if ( _CaptionTextBox.text != "" )
			{
				_CaptionAlphaDirection = -1;
				_PendingText = text;
				_PendingDuration = duration;
			}
			else
			{
				_CaptionAlphaDirection = 1;
				_CaptionTextBox.text = text;
				_TextRemainDuration = duration;
			}
			
			_TextTransitionStamp = _GameTime;
		}
		
		public function fadeToBlack( direction:int, duration:Number ):void
		{
			_FadeToBlack.startFade( direction, duration );
		}
		
		public function setFullScreen( value:Boolean ):void
		{
			// _Stage.displayState = value ? StageDisplayState.FULL_SCREEN : StageDisplayState.NORMAL;
			// @TODO: may want to dispatch an event at this point
		}
		
		public function notifyNewScene():void
		{
			var toNotify:Vector.<ISceneNotifiable> = new Vector.<ISceneNotifiable>();
			
			for each ( var tickable:ITickable in _TickList )
			{
				if ( tickable is ISceneNotifiable )
				{
					var notifiable:ISceneNotifiable = tickable as ISceneNotifiable;
					
					toNotify.push(notifiable);
				}
			}
			
			// Has to be done this way, as it may remove itself from tick list
			for each ( var needingNotify:ISceneNotifiable in toNotify )
			{
				needingNotify.handleSceneTransition();
			}
		}
		
		public function set IsPaused( value:Boolean ):void { _IsPaused = value; }
		
		// Accessors
		public function get Actors():Vector.<Actor> { return _Actors; }
		public function get Tickables():Vector.<ITickable> { return _TickList; }
		public function get CollisionRoot():QuadTreeNode { return _Root; }
		//public function get IsFullScreen():Boolean { return _Stage.displayState == StageDisplayState.FULL_SCREEN; }
		public function get WorldWidth():int { return _Root.CollisionComponent.Width; }
		public function get WorldHeight():int { return _Root.CollisionComponent.Height; }
		public function get Display():Sprite { return _InternalSprite; }
		public function get Flash():SceneFlash { return _SceneFlash; }
		public function get GameStage():Stage { return _Stage; }
		public function get UILayer():Sprite { return _UILayer; }
		public function get GameTime():Number { return _GameTime; }
		public function get IsPaused():Boolean { return _IsPaused; }
		public function get ActivePopup():IPopup { return _ActivePopup; }
		
		// Mutators
		
		public function destroy():void
		{
			_Stage.removeEventListener( Event.ENTER_FRAME, update );
			
			// destroy everything in root
			_Root = null;
			
			_TickList = null;
			_InternalSprite = null;
			_Stage = null;
			_CachedCollisionSceneNode = null;
			_Actors = null;
			_Drawables = null;
		}
	}
}