package engine.collision 
{
	import adobe.utils.CustomActions;
	import flash.display.Graphics;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class RectCollider 
	{	
		public var X:int;
		public var Y:int;
		public var Width:uint;
		public var Height:uint;
		public var CollisionFlag:uint;
		
		public var NonAxisX:Vector.<int>;
		public var NonAxisY:Vector.<int>;
		
		public static const TOP_LEFT:int = 0;
		public static const TOP_RIGHT:int = 1;
		public static const BOTTOM_LEFT:int = 2;
		public static const BOTTOM_RIGHT:int = 3;
		
		public function RectCollider( x:int, y:int, width:uint, height:uint, collisionFlag:uint ) 
		{
			X = x;
			Y = y;
			Width = width;
			Height = height;
			CollisionFlag = collisionFlag;
			
			NonAxisX = new Vector.<int>;
			NonAxisY = new Vector.<int>;
			NonAxisX.length = 4;
			NonAxisY.length = 4;
		}
		
		public function doesAccuratelyCollide( collider:RectCollider ):Boolean
		{
			if ( ( collider.CollisionFlag & this.CollisionFlag ) == 0 )
				return false;
			
			// this is going to be ugly code
			const topLeftX1:int = NonAxisX[ TOP_LEFT ];
			const topLeftY1:int = NonAxisY[ TOP_LEFT ];
			const topLeftX2:int = collider.NonAxisX[ TOP_LEFT ];
			const topLeftY2:int = collider.NonAxisY[ TOP_LEFT ];
			
			const topRightX1:int = NonAxisX[ TOP_RIGHT ];
			const topRightY1:int = NonAxisY[ TOP_RIGHT ];
			const topRightX2:int = collider.NonAxisX[ TOP_RIGHT ];
			const topRightY2:int = collider.NonAxisY[ TOP_RIGHT ];
			
			const bottomLeftX1:int = NonAxisX[ BOTTOM_LEFT ];
			const bottomLeftY1:int = NonAxisY[ BOTTOM_LEFT ];
			const bottomLeftX2:int = collider.NonAxisX[ BOTTOM_LEFT ];
			const bottomLeftY2:int = collider.NonAxisY[ BOTTOM_LEFT ];
			
			const bottomRightX1:int = NonAxisX[ BOTTOM_RIGHT ];
			const bottomRightY1:int = NonAxisY[ BOTTOM_RIGHT ];
			const bottomRightX2:int = collider.NonAxisX[ BOTTOM_RIGHT ];
			const bottomRightY2:int = collider.NonAxisY[ BOTTOM_RIGHT ];
			
			// @TODO: need to account for if they're within us, or we're within them
			
			// and now for the magic...
			var myLines:Vector.<LineSegment> = new Vector.<LineSegment>;
			
			myLines.push( new LineSegment( topLeftX1, topLeftY1, topRightX1, topRightY1 ) );
			myLines.push( new LineSegment( topRightX1, topRightY1, bottomRightX1, bottomRightY1 ) );
			myLines.push( new LineSegment( bottomRightX1, bottomRightY1, bottomLeftX1, bottomLeftY1 ) );
			myLines.push( new LineSegment( bottomLeftX1, bottomLeftY1, topLeftX1, topLeftY1 ) );
			
			var theirLines:Vector.<LineSegment> = new Vector.<LineSegment>;
			
			theirLines.push( new LineSegment( topLeftX2, topLeftY2, topRightX2, topRightY2 ) );
			theirLines.push( new LineSegment( topRightX2, topRightY2, bottomRightX2, bottomRightY2 ) );
			theirLines.push( new LineSegment( bottomRightX2, bottomRightY2, bottomLeftX2, bottomLeftY2 ) );
			theirLines.push( new LineSegment( bottomLeftX2, bottomLeftY2, topLeftX2, topLeftY2 ) );
			
			for each ( var line1:LineSegment in myLines )
			{
				for each ( var line2:LineSegment in theirLines )
				{
					if ( doLinesIntersect( line1.X1, line1.Y1, line1.X2, line1.Y2, line2.X1, line2.Y1, line2.X2, line2.Y2 ) )
					{
						return true;
					}
				}
			}
			
			return false;
		}
		
		public function doesCollide( collider:RectCollider ):Boolean
		{
			if ( ( collider.CollisionFlag & this.CollisionFlag ) == 0 )
				return false;
			
			if ( collider.Bottom > this.Top || collider.Top < this.Bottom || collider.Right < this.Left || collider.Left > this.Right )
			{
				return false;
			}
			
			return true;
		}
		
		/**
		 * Does this rectangle fit within the collider rectangle
		 * @param	collider	rectangle that this should fit in to return true
		 * @return	true if this fits into the collider
		 */
		public function doesFitIn( collider:RectCollider ):Boolean
		{
			if ( ( collider.CollisionFlag & this.CollisionFlag ) == 0 )
				return false;
				
			return ( this.Left >= collider.Left && this.Right <= collider.Right && this.Top <= collider.Top && this.Bottom >= collider.Bottom );
		}
		
		/*
		 * var ip:Point = new Point();
 
function intersection(p1:Sprite, p2:Sprite, p3:Sprite, p4:Sprite):Point {
    var nx:Number, ny:Number, dn:Number;
    var x4_x3:Number = p4.x - p3.x;
    var pre2:Number = p4.y - p3.y;
    var pre3:Number = p2.x - p1.x;
    var pre4:Number = p2.y - p1.y;
    var pre5:Number = p1.y - p3.y;
    var pre6:Number = p1.x - p3.x;
    nx = x4_x3 * pre5 - pre2 * pre6;
    ny = pre3 * pre5 - pre4 * pre6;
    dn = pre2 * pre3 - x4_x3 * pre4;
    nx /= dn;
    ny /= dn;
    // has intersection
    if(nx>= 0 && nx <= 1 && ny>= 0 && ny <= 1){
        ny = p1.y + nx * pre4;
        nx = p1.x + nx * pre3;
        ip.x = nx;
        ip.y = ny;
    }else{
        // no intersection
        ip.x = ip.y = -1000;
    }
    return ip
}
*/
		
		private function doLinesIntersect( s1X:int, s1Y:int, e1X:int, e1Y:int, s2X:int, s2Y:int, e2X:int, e2Y:int ):Boolean
		{
			const dSE2X:Number = e2X - s2X;
		    const dSE2Y:Number = e2Y - s2Y;
		    const dSE1X:Number = e1X - s1X;
		    const dSE1Y:Number = e1Y - s1Y;
		    const dSSX:Number = s1Y - s2Y;
		    const dSSY:Number = s1X - s2X;
		
			const D:Number = dSE2Y * dSE1X - dSE2X * dSE1Y;
			const X:Number = ( dSE2X * dSSX - dSE2Y * dSSY ) / D;
		    const Y:Number = ( dSE1X * dSSX - dSE1Y * dSSY ) / D;
		
		    if ( X >= 0 && X <= 1 && Y >= 0 && Y <= 1 )
				return true;
				
			return false
		}
		
		public function doesLineIntersect( x1:int, y1:int, x2:int, y2:int, collisionFlag:uint ):Boolean
		{
			if ( ( collisionFlag & this.CollisionFlag ) == 0 )
				return false;
			
			return ( 	doLinesIntersect( x1, y1, x2, y2, Left, Top, Left, Bottom )		||
						doLinesIntersect( x1, y1, x2, y2, Left, Top, Right, Top )		||
						doLinesIntersect( x1, y1, x2, y2, Left, Bottom, Right, Bottom ) ||
						doLinesIntersect( x1, y1, x2, y2, Right, Top, Right, Bottom ) );
		}
		
		public function doesLineAccuratelyIntersect( x1:int, y1:int, x2:int, y2:int, collisionFlag:uint ):Boolean
		{
			if ( ( collisionFlag & this.CollisionFlag ) == 0 )
				return false;
				
			// this is going to be ugly code
			const topLeftX:int = NonAxisX[ TOP_LEFT ];
			const topLeftY:int = NonAxisY[ TOP_LEFT ];
			
			const topRightX:int = NonAxisX[ TOP_RIGHT ];
			const topRightY:int = NonAxisY[ TOP_RIGHT ];
			
			const bottomLeftX:int = NonAxisX[ BOTTOM_LEFT ];
			const bottomLeftY:int = NonAxisY[ BOTTOM_LEFT ];
			
			const bottomRightX:int = NonAxisX[ BOTTOM_RIGHT ];
			const bottomRightY:int = NonAxisY[ BOTTOM_RIGHT ];
			
			if ( doLinesIntersect( x1, y1, x2, y2, topLeftX, topLeftY, topRightX, topRightY ) ||
				 doLinesIntersect( x1, y1, x2, y2, topRightX, topRightY, bottomRightX, bottomRightY ) ||
				 doLinesIntersect( x1, y1, x2, y2, bottomRightX, bottomRightY, bottomLeftX, bottomRightY ) ||
				 doLinesIntersect( x1, y1, x2, y2, bottomLeftX, bottomLeftY, topLeftX, topLeftY ) )
			 {
				 return true;
			 }
				 
			 return false;
		}
		
		public function containsPoint( x:int, y:int, collisionFlag:uint ):Boolean
		{
			if ( ( collisionFlag & this.CollisionFlag ) == 0 )
				return false;
			
			return ( x >= Left && x <= Right && y <= Top && y >= Bottom );
		}
		
		public function drawDebug( graphics:Graphics, transform:Point, color:uint ):void
		{
			const halfWidth:uint = Width / 2;
			const halfHeight:uint = Height / 2;
			const TransX:int = ( X * transform.x ) - halfWidth;
			const TransY:int = ( Y * transform.y ) - halfHeight;
			
			graphics.lineStyle( 1, color );
			graphics.drawRect( TransX, TransY, Width, Height );
			
			// accurate bounds 0xFFFF00			
			graphics.lineStyle( 1, 0x33FF00 );
			graphics.moveTo( NonAxisX[ TOP_LEFT ], -NonAxisY[ TOP_LEFT ] );
			graphics.lineTo( NonAxisX[ TOP_RIGHT ], -NonAxisY[ TOP_RIGHT ] );
			graphics.lineTo( NonAxisX[ BOTTOM_RIGHT ], -NonAxisY[ BOTTOM_RIGHT ] );
			graphics.lineTo( NonAxisX[ BOTTOM_LEFT ], -NonAxisY[ BOTTOM_LEFT ] );
			graphics.lineTo( NonAxisX[ TOP_LEFT ], -NonAxisY[ TOP_LEFT ] );
		}
		
		public function get Left():int { return X - Width / 2; }
		public function get Right():int { return X + Width / 2; }
		public function get Top():int { return Y + Height / 2; }
		public function get Bottom():int { return Y - Height / 2; }
	}
}

class LineSegment
{
	public var X1:int;
	public var Y1:int;
	public var X2:int;
	public var Y2:int;
	
	public function LineSegment( x1:int, y1:int, x2:int, y2:int )
	{
		X1 = x1;
		Y1 = y1;
		X2 = x2;
		Y2 = y2;
	}
}