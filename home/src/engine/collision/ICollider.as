package engine.collision 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface ICollider 
	{
		function get Location():Point;
		function set Location( value:Point ):void;
	}	
}