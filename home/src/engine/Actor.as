package engine 
{
	import engine.collision.ICollider;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import engine.render.Drawable;
	import engine.render.scene.SceneLayer;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import starling.events.EventDispatcher;
	import flash.geom.Matrix;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Actor extends EventDispatcher implements IDestroyable
	{
		// Collision component to use
		// It's important to note that the collision component is in world space
		// wherein positive X is to the right and positive Y is up
		private var _SceneNode:SceneNode;
		
		private var _Drawable:Drawable;
		
		private var _GameData:IGameData;

		private var _ParentLayer:SceneLayer;
		
		private var _Dir:Point;
		
		private var _CachedDeltaX:Number;
		private var _CachedDeltaY:Number;
		
		private var _IsDisposing:Boolean = false;
		
		public function Actor()
		{
			_Dir = new Point( 0, 1 );
		}
		
		public function initialize():void
		{
			
		}
		
		/**
		 * Rotate a specific angle around the center of the display object
		 * @param	amount to rotate (positive is clock-wise) in degrees
		 */
		public function rotate( amount:Number ):void
		{
			_Drawable.rotate( amount );
			
			_Dir = _Drawable.normal;
			
			if ( _SceneNode && _SceneNode.CollisionComponent )
			{
				_SceneNode.CollisionComponent.Width = _Drawable.width;
				_SceneNode.CollisionComponent.Height = _Drawable.height;
				_SceneNode.updatePosition();
			}
			
			// Debug.debugLog( EngineLogChannels.DEBUG, "Normal: " + _Dir );
		}
		
		public function angleBetween( x1:Number, y1:Number, x2:Number, y2:Number ):Number
		{
			var angle:Number = Math.atan2( x1, y1 ) - Math.atan2( x2, y2 );
			
			return angle;
		}
		
		public function faceToward( x:Number, y:Number, clamp:Number = 360 ):void
		{
			// Only calculate these values once (const)
			var angle:Number = MathUtility.getAngle( x, y, _Dir.x, _Dir.y );
			
			// Clamp
			if ( Math.abs( angle ) > clamp )
			{
				angle = angle > 0 ? clamp : -clamp;
			}
			
			// Rotate
			rotate( angle );
		}
		
		public function clearDeltaMovement():void
		{
			_CachedDeltaX = 0;
			_CachedDeltaY = 0;
		}
		
		/* MUTATORS */
		public function set X( value:Number ):void
		{ 
			if ( !_Drawable )
				return;
				
			_CachedDeltaX = value - _Drawable.worldX;
			
			_Drawable.worldX = value;
			
			if ( _SceneNode )
			{
				_SceneNode.CollisionComponent.X = _Drawable.worldX;
				_SceneNode.updatePosition();
			}
		}
		
		public function set Y( value:Number ):void
		{ 
			if ( !_Drawable )
				return;
			
			_CachedDeltaY = -value + _Drawable.worldY;
				
			_Drawable.worldY = value;
			
			if ( _SceneNode )
			{
				_SceneNode.CollisionComponent.Y = _Drawable.worldY;
				_SceneNode.updatePosition();
			}
		}
		
		public function set Data( value:IGameData ):void { _GameData = value; }
		
		public function set Node( value:SceneNode ):void
		{ 
			_SceneNode = value;
			_SceneNode.InternalObject = this;
		}
		
		public function setDrawingData( value:BitmapData ):void
		{
			_Drawable = new Drawable();
			_Drawable.RawData = value;
		}
		
		public function set ParentLayer( value:SceneLayer ):void
		{ 
			_ParentLayer = value;
			
			if ( _Drawable )
			{
				_Drawable.layer = value;
			}
		}

		/* ACCESSORS */
		public function get X():Number { return _Drawable.worldX; }
		public function get Y():Number { return _Drawable.worldY; }
		
		public function get Width():int { return _Drawable.sourceWidth; }
		public function get Height():int { return _Drawable.sourceHeight; }
		
		public function get Data():IGameData{ return _GameData; }
		
		public function get Node():SceneNode { return _SceneNode; }
		
		public function get Dir():Point { return _Dir; }
		
		public function get Display():Drawable { return _Drawable; }
		
		public function get ParentLayer():SceneLayer{ return _ParentLayer; }
		
		public function get deltaX():Number { return _CachedDeltaX; }
		public function get deltaY():Number { return _CachedDeltaY; }
		
		public function get isDisposing():Boolean { return _IsDisposing; }
		
		/* DESTRUCTOR */
		public function destroy():void
		{
			_IsDisposing = true;
			
			_SceneNode = null;
		}
	}
}