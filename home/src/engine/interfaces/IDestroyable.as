package engine.interfaces 
{
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IDestroyable 
	{
		function destroy():void;
	}
	
}