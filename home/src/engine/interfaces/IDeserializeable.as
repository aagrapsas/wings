package engine.interfaces 
{
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IDeserializeable 
	{
		function deserialize( xml:XML ):void;
	}	
}