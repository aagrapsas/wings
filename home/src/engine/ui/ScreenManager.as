package engine.ui 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ScreenManager implements ITickable, IDestroyable
	{		
		public var Display:Sprite;
		
		private var _Elements:Vector.<IUIElement>;
		private var _ElementsToRemove:Vector.<IUIElement>;
		
		private var _GameData:IGameData;
		
		public function ScreenManager( gameData:IGameData ) 
		{
			_Elements = new Vector.<IUIElement>();
			_ElementsToRemove = new Vector.<IUIElement>();
			Display = new Sprite();
			
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_GameData.Scene.GameStage.addChild( Display );
		}
		
		public function addElement( element:IUIElement ):void
		{
			_Elements.push( element );
			
			Display.addChild( element.container );
		}
		
		public function removeElement( element:IUIElement ):void
		{
			var index:int = _Elements.indexOf( element );
			
			if ( index > -1 )
			{
				_Elements.splice( index, 1 );
				
				Display.removeChild( element.container );
			}
		}
		
		public function update( deltaTime:Number ):void
		{	
			// Iterate all elements, update, & determine if stale
			for each ( var element:IUIElement in _Elements )
			{
				element.update( deltaTime );
				
				if ( element.isDisposable )
				{
					_ElementsToRemove.push( element );
				}
			}
			
			// Remove any stale elements
			for each ( var elementToRemove:IUIElement in _ElementsToRemove )
			{
				removeElement( elementToRemove );
			}
			
			// Clear elements to remove if all have been removed
			if ( _ElementsToRemove.length > 0 )
			{
				_ElementsToRemove.splice( 0, _ElementsToRemove.length - 1 );
			}
		}
		
		public function destroy():void
		{
			for each ( var element:IUIElement in _Elements )
			{
				element.destroy();
			}
			
			_GameData.Scene.GameStage.removeChild( Display );
		}
	}
}