package engine.ui 
{
	import engine.misc.MathUtility;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIElement implements IUIElement
	{
		private var _Display:Sprite;		
		
		// Fading
		private var _IsFading:Boolean = false;
		private var _AlphaTarget:Number;
		private var _AlphaStart:Number;
		private var _AlphaDuration:Number;
		private var _AlphaAccumulator:Number;
		protected var _AlphaStep:Number = 0.10;
		
		// Movement
		private var _StartX:int;
		private var _StartY:int;
		private var _EndX:int;
		private var _EndY:int;
		private var _MoveDuration:Number;
		private var _MoveAccumulator:Number = 0;
		private var _IsMoving:Boolean = false;
		
		private var _IsDisposable:Boolean = false;
		
		public function UIElement() 
		{
			
		}
		
		public function fadeIn( duration:Number ):void
		{
			_IsFading = true;
			_AlphaTarget = 1.0;
			this.alpha = 0;
			_AlphaStart = 0;
			_AlphaAccumulator = 0;
			_AlphaDuration = duration;
		}
		
		public function fadeOut( duration:Number ):void
		{
			_IsFading = true;
			_AlphaTarget = 0;
			_AlphaStart = 1.0;
			_AlphaAccumulator = 0;
			_AlphaDuration = duration;
		}
		
		public function moveTo( x:int, y:int, duration:Number ):void
		{
			_StartX = _Display.x;
			_StartY = _Display.y;
			
			_EndX = x;
			_EndY = y;
			
			_MoveDuration = duration;
			
			_MoveAccumulator = 0;
			
			_IsMoving = true;
		}
		
		public function update( deltaTime:Number ):void
		{
			if ( _IsFading )
			{
				_AlphaAccumulator += deltaTime;
				
				this.alpha = MathUtility.lerp( _AlphaStart, _AlphaTarget, Math.min( _AlphaAccumulator / _AlphaDuration, 1.0 ) );
				
				if ( this.alpha == 1 || this.alpha == 0 )
				{
					_IsFading = false;
					
					if ( this.alpha == 0 )
					{
						_IsDisposable = true;	// Mark for removal
					}
				}
			}
			
			if ( _IsMoving )
			{
				var alpha:Number = Math.min( _MoveAccumulator / _MoveDuration, 1.0 );
				_Display.x = MathUtility.easeInInt( _StartX, _EndX, alpha );
				_Display.y = MathUtility.easeInInt( _StartY, _EndY, alpha );
				
				if ( _Display.x == _EndX && _Display.y == _EndY )
				{
					_IsMoving = false;
				}
			}
		}
		
		public function setup():void
		{
			
		}
		
		public function breakDown():void
		{
			
		}
		
		public function get ID():String { return "UIElement"; }
		public function get isModal():Boolean { return false; }
		
		public function get container():Sprite { return _Display; }
		
		public function get isDisposable():Boolean { return _IsDisposable; }
		
		public function destroy():void
		{
			
		}
	}
}