package engine.ui 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import starling.display.Sprite;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IPopup extends ITickable, IDestroyable
	{
		function get display():Sprite;
		function get name():String;
	}
}