package engine.ui 
{
	import engine.interfaces.IDestroyable;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IUIElement extends IDestroyable
	{
		function setup():void;
		function update( deltaTime:Number ):void;
		function breakDown():void;
		function get ID():String;
		function get isModal():Boolean;
		function get container():Sprite;
		function get isDisposable():Boolean;
	}
}