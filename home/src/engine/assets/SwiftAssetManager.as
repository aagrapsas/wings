package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import flash.display.Bitmap;
	import flash.media.Sound;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SwiftAssetManager 
	{
		private var _PackedAssets:Dictionary;	// XML for packed assets
		
		private var _Loaders:Dictionary;
		
		private var _Images:Dictionary;
		private var _XML:Dictionary;
		private var _Audio:Dictionary;
		private var _Animations:Dictionary;
		private var _SWFs:Dictionary;
		
		private var _GeneratedAssetLocation:String;
		
		public function SwiftAssetManager( resourceLocation:String, manifests:XML ) 
		{
			if ( !resourceLocation )
			{
				_GeneratedAssetLocation = "data/generated";
			}
			else
			{
				_GeneratedAssetLocation = resourceLocation + "/data/generated";
			}
			
			for each ( var manifestXML:XML in manifests.manifest )
			{
				_PackedAssets[ manifestXML.@name ] = manifestXML;
			}
		}
		
		public function isPackageLoaded( name:String ):Boolean
		{
			if ( !_Loaders[ name ] )
			{
				return false;
			}
			
			var loader:SwiftAssetLoader = _Loaders[ name ];
			
			return loader.isLoaded;
		}
		
		public function loadPackage( name:String ):void
		{
			if ( !_PackedAssets[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to load package " + name + " package doesn't exist in manifest!" );
				return;
			}
			
			if ( _Loaders[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to load already loaded package with name " + name );
				return;
			}
			
			var xml:XML = _PackedAssets[ name ];
			
			Debug.debugLog( EngineLogChannels.ASSETS, "Loading package " + name );
			
			var loader:SwiftAssetLoader = new SwiftAssetLoader( xml.@name, _GeneratedAssetLocation + "/" + xml.@name + ".swf", xml );
			loader.loadSwiftRiverSWF( onPackageLoaded );
			
			_Loaders[ name ] = loader;
		}
		
		private function onPackageLoaded( loaded:SwiftAssetLoader ):void
		{
			Debug.debugLog( EngineLogChannels.ASSETS, "Loaded package " + loaded.name );
			
			var images:Dictionary = loaded.allImages;
			var animations:Dictionary = loaded.allAnimations;
			var xml:Dictionary = loaded.allXML;
			var audio:Dictionary = loaded.allAudio;
			
			for ( var imageKey:String in images )
			{
				if ( _Images[ imageKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Conflict found with image asset name " + imageKey );
					continue;
				}
				
				_Images[ imageKey ] = images[ imageKey ];
			}
			
			for ( var animKey:String in animations )
			{
				if ( _Animations[ animKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Conflict found with animation asset name " + animKey );
					continue;
				}
				
				_Animations[ animKey ] = animations[ animKey ];
			}
			
			for ( var audioKey:String in audio )
			{
				if ( _Audio[ audioKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Conflict found with audio asset name " + audioKey );
					continue;
				}
				
				_Audio[ audioKey ] = audio[ audioKey ];
			}
			
			for ( var xmlKey:String in xml )
			{
				if ( _XML[ xmlKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Conflict found with XML asset name " + xmlKey );
					continue;
				}
				
				_XML[ xmlKey ] = xml[ xmlKey ];
			}
		}
		
		public function getXML( name:String ):XML
		{
			if ( !_XML[ name )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access xml that doesn't exist! Name: " + name );
				return;
			}
			
			return _XML[ name ];
		}
		
		public function getImage( name:String ):Bitmap
		{
			if ( !_Images[ name )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access image that doesn't exist! Name: " + name );
				return;
			}
			
			return _Images[ name ];
		}
		
		public function getAnimation( name:String ):Vector.<Bitmap>
		{
			if ( !_Animations[ name )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access animation that doesn't exist! Name: " + name );
				return;
			}
			
			return _Animations[ name ];
		}
		
		public function getAudio( name:String ):Sound
		{
			if ( !_Audio[ name )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access audio that doesn't exist! Name: " + name );
				return;
			}
			
			return _Audio[ name ];
		}
	}
}