package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NetStatusEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Animatic extends Sprite
	{		
		private var _Connection:NetConnection;
		private var _Stream:NetStream;
		private var _Loc:String;
		private var _Width:int;
		private var _Height:int;
		private var _ShouldPlayOnStart:Boolean;
		private var _Video:Video;
		
		public function Animatic( loc:String, width:int, height:int, shouldPlayOnStart:Boolean ) 
		{
			_Loc = loc;
			_Width = width;
			_Height = height;
			_ShouldPlayOnStart = shouldPlayOnStart;
			
			loadAnimatic();
		}	
		
		private function loadAnimatic():void
		{
			_Connection = new NetConnection();
			_Connection.addEventListener( NetStatusEvent.NET_STATUS, onNetStatusReturned );
			_Connection.connect( null );
		}
		
		private function onNetStatusReturned( e:NetStatusEvent ):void
		{
			_Connection.removeEventListener( NetStatusEvent.NET_STATUS, onNetStatusReturned );
			
			if ( e.info.code == "NetConnection.Connect.Success" )
			{
				_Video = new Video( _Width, _Height );
				_Stream = new NetStream( _Connection );
				_Stream.addEventListener( AsyncErrorEvent.ASYNC_ERROR, onAsyncError );
				_Stream.client = new Object;
				_Video.attachNetStream( _Stream );
				this.addChild( _Video );
				
				if ( _ShouldPlayOnStart )
				{
					this.play();
				}
			}
			else
			{
				Debug.errorLog( EngineLogChannels.ERROR, "Could not find " + _Loc );
			}
			
			this.dispatchEvent( new Event( Event.COMPLETE ) );
		}
		
		private function onAsyncError( e:AsyncErrorEvent ):void
		{
			Debug.errorLog( EngineLogChannels.ERROR, e.text );
		}
		
		public function play():void
		{
			_Stream.play( _Loc );
		}
		
		public function pause():void
		{
			_Stream.pause();
		}
		
		public function resume():void
		{
			_Stream.resume();
		}
	}
}