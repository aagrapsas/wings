package engine.assets 
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class XMLAsset implements IAsset
	{
		private var _Name:String;
		private var _Location:String;
		private var _Loader:URLLoader;
		private var _XML:XML;
		private var _Callback:Function;
		
		public function XMLAsset( name:String, location:String ) 
		{
			_Name = name;
			_Location = location;
		}
		
		public function load( callback:Function ):void
		{
			_Callback = callback;
			
			_Loader = new URLLoader();
			_Loader.addEventListener( Event.COMPLETE, onComplete );
			_Loader.load( new URLRequest( _Location ) );
		}
		
		private function onComplete( e:Event ):void
		{
			_Loader.removeEventListener( Event.COMPLETE, onComplete );
			
			_XML = new XML( _Loader.data );
			
			_Callback.apply( null, [ this ] );
		}
		
		public function get data():XML { return _XML; }
		public function get name():String { return _Name; }
		public function get location():String { return _Location; }
		public function get isDoneLoading():Boolean { return _XML != null; }
	}
}