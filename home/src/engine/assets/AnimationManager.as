package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IGameData;
	import engine.misc.EngineProperties;
	import engine.render.animation.Animation;
	import flash.display.Bitmap;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import flash.utils.Dictionary;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AnimationManager extends EventDispatcher
	{
		private var _Animations:Dictionary;
		private var _GameData:IGameData;
		
		public function AnimationManager( gameData:IGameData ) 
		{
			_GameData = gameData;
			
			_Animations = new Dictionary;
		}
		
		public function deserialize( xml:XML ):void
		{			
			Debug.debugLog( EngineLogChannels.ASSETS, "Deserializing animation configuration..." );
			
			for each ( var subXML:XML in xml.animation )
			{
				var name:String = subXML.@name;
				var duration:Number = Number( subXML.@duration );
				var isLooping:Boolean = String( subXML.@looping ) == "true";
				var frames:Vector.<Bitmap> = _GameData.AssetManager.getAnimation( name );
				
				if ( !frames )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Unable to manufacture " + name + " animation, frames are null!" );
					continue;
				}
				
				var textureFrames:Vector.<Texture> = new Vector.<Texture>();
				
				for each ( var bitmap:Bitmap in frames )
				{
					textureFrames.push( Texture.fromBitmap( bitmap ) );
				}
				
				_Animations[ name ] = new Animation( textureFrames, duration, isLooping );
			}
			
			Debug.debugLog( EngineLogChannels.ASSETS, "Animation configuration parsed" );
		}
		
		public function getAnimation( name:String ):Animation
		{
			var animation:Animation = _Animations[ name ];
			
			if ( !animation )
				return null;
				
			var newAnimation:Animation = new Animation( animation.Frames, animation.Duration, animation.IsLooping );
			
			return newAnimation;
		}
	}
}