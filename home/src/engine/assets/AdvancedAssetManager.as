package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import starling.events.EventDispatcher;
	import flash.media.Sound;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import starling.display.Image;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AdvancedAssetManager extends EventDispatcher implements ITickable
	{
		private const GENERATED_MANIFEST:String = "data/generated/manifest.xml";
		private const MANUAL_MANIFEST:String = "data/manual/manifest.xml";
		private const PRELOAD_FILE:String = "data/manual/preload.xml";
		
		private var _PreloadManifest:XMLAsset;
		private var _GeneratedManifest:XMLAsset;
		private var _ManualManifest:XMLAsset;
		private var _IsFirstLoad:Boolean = true;
		
		private var _GameData:IGameData;

		private var _Assets:Dictionary;
		
		private var _LoadingQueue:Vector.<IAsset>;
		private var _Loading:Vector.<IAsset>;
		
		private static const MAX_CONNECTIONS:int = 6;
		
		private var _IsPreloading:Boolean = false;
		
		private var _ProcessingAssetsCount:int = 0;
		
		private var _PreloadCount:int = 0;
		
		private var _SwiftRiverCallback:Function;
		
		public function AdvancedAssetManager( gameData:IGameData, preloadedAssets:Dictionary = null ) 
		{
			// Assignment
			_GameData = gameData;
			
			// Allocation
			_LoadingQueue = new Vector.<IAsset>();
			_Loading = new Vector.<IAsset>();
			_Assets = new Dictionary();
			
			Debug.debugLog( EngineLogChannels.DEBUG, "Initializing asset manager" );
			
			if ( _GameData.Properties.resourcePath )
			{
				Debug.debugLog( EngineLogChannels.DEBUG, "Resource path: " + _GameData.Properties.resourcePath );
			}
			
			if ( preloadedAssets )
			{
				for ( var key:String in preloadedAssets )
				{
					_Assets[ key ] = preloadedAssets[ key ];
				}
			}
		}
		
		public function loadPreloadAssets():void
		{
			// Load manifests and initial files
			loadManifests();
		}
		
		public function registerSwiftRiverCallback( callback:Function ):void
		{
			_SwiftRiverCallback = callback;
		}
		
		private function conditionURL( path:String ):String
		{			
			var conditioned:String = path;
			
			if ( _GameData.Properties.resourcePath )
			{
				conditioned = _GameData.Properties.resourcePath + "/" + path;
			}
			
			if ( _GameData.Properties.buildNumber )
			{
				conditioned = conditioned + "?=" + _GameData.Properties.buildNumber;
			}
			
			return conditioned;
		}
		
		private function loadManifests():void
		{
			var generatedURL:String = conditionURL( GENERATED_MANIFEST );
			var manualURL:String = conditionURL( MANUAL_MANIFEST );
			var preloadURL:String = conditionURL( PRELOAD_FILE );
			
			Debug.debugLog( EngineLogChannels.ASSETS, "Loading preload manifest from URL: " + preloadURL );
			Debug.debugLog( EngineLogChannels.ASSETS, "Loading manual manifest from URL: " + manualURL );
			Debug.debugLog( EngineLogChannels.ASSETS, "Loading generated manifest from URL: " + preloadURL );
			
			_PreloadManifest = new XMLAsset( "preload", preloadURL );
			_GeneratedManifest = new XMLAsset( "generated", generatedURL );
			_ManualManifest = new XMLAsset( "manual", manualURL );
			
			_PreloadManifest.load( onManifestLoaded );			
			_GeneratedManifest.load( onManifestLoaded );
			_ManualManifest.load( onManifestLoaded );
			
			Debug.debugLog( EngineLogChannels.ASSETS, "Loading all manifests..." );
		}
		
		private function onManifestLoaded( manifest:XMLAsset ):void
		{
			Debug.debugLog( EngineLogChannels.ASSETS, "Manifest loaded." );
			
			if ( _IsFirstLoad && _PreloadManifest.isDoneLoading && _GeneratedManifest.isDoneLoading && _ManualManifest.isDoneLoading )
			{
				Debug.debugLog( EngineLogChannels.ASSETS, "All manifests loaded." );
				
				_IsFirstLoad = false;
				
				// All manifests are loaded, create asset entries
				setupGeneratedManifest();
				setupManualManifest();
				
				// Load assets that are necessary for game
				setupPreloadAssets();
				
				_IsPreloading = true;
			}
		}
		
		private function setupPreloadAssets():void
		{
			var xml:XML = _PreloadManifest.data;
			
			// Load actual files
			for each ( var asset:XML in xml.asset )
			{
				var name:String = asset.@name;
				
				if ( !_Assets[ name ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to load asset " + name + " that doesn't exist!" );
					continue;
				}
				
				// Push to loading queue for eventual loading
				_LoadingQueue.push( _Assets[ name ] );
			}
			
			_PreloadCount = _LoadingQueue.length;
		}
		
		private function setupGeneratedManifest():void
		{
			var xml:XML = _GeneratedManifest.data;
			
			// Cache swf's for eventual loading
			for each ( var manifest:XML in xml.manifest )
			{
				var name:String = manifest.@name;
				var location:String = "data/generated/" + name + ".swf";
				
				location = conditionURL( location );
				
				Debug.debugLog( EngineLogChannels.ASSETS, "Loading swift river swf " + name + " from " + location );
				
				var swift:SwiftAssetLoader = new SwiftAssetLoader( name, location, manifest );
				swift.registerExtractingCallback( _SwiftRiverCallback );
				
				_Assets[ name ] = swift;
			}
		}
		
		private function setupManualManifest():void
		{
			var xml:XML = _ManualManifest.data;
			
			// Mark files as available
			for each ( var asset:XML in xml.asset )
			{
				var name:String = asset.@name;
				var type:String = asset.@type;
				var location:String = asset.@location;
				
				location = conditionURL( location );
				
				Debug.debugLog( EngineLogChannels.ASSETS, "Loading asset" + name + " of type " + type + " from " + location );
				
				// @TODO: handle other file types here
				if ( type == "xml" )
				{
					var xmlAsset:XMLAsset = new XMLAsset( name, location );
					
					_Assets[ name ] = xmlAsset;
				}
				else if ( type == "swf" )
				{
					var swfAsset:SwfLoader = new SwfLoader( name, location, asset );
					
					_Assets[ name ] = swfAsset;
				}
			}
		}
		
		private function onXMLLoaded( xml:XMLAsset ):void
		{
			_Assets[ xml.name ] = xml.data;
			
			_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( xml.name, AssetEvent.XML_TYPE ) );
			
			_ProcessingAssetsCount--;
		}
		
		private function onAnimationLoaded( animation:AnimationAsset ):void
		{
			_Assets[ animation.name ] = animation.data;
			
			_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( animation.name, AssetEvent.ANIMATION_TYPE ) );
			
			_ProcessingAssetsCount--;
		}
		
		private function onImageLoaded( image:ImageAsset ):void
		{
			_Assets[ image.name ] = image.data;
			
			_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( image.name, AssetEvent.IMAGE_TYPE ) );
			
			_ProcessingAssetsCount--;
		}
		
		private function onSwfLoaded( swf:SwfLoader ):void
		{
			var classes:Dictionary = swf.classMap;
			
			for ( var classKey:String in classes )
			{
				if ( _Assets[ classKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Asset collision in namespace (one is class) " + classKey );
					continue;
				}
				
				_Assets[ classKey ] = classes[ classKey ];
				
				_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( classKey, AssetEvent.SWF_TYPE ) );
			}
			
			_ProcessingAssetsCount--;
		}
		
		private function onSwiftAssetLoaded( swift:SwiftAssetLoader ):void
		{
			var allXML:Dictionary = swift.allXML;
			var allAudio:Dictionary = swift.allAudio;
			var allImages:Dictionary = swift.allImages;
			var allAnimations:Dictionary = swift.allAnimations;
			
			// All xml files in the swift
			for ( var xmlKey:String in allXML )
			{
				if ( _Assets[ xmlKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Asset collision in namespace (one is xml) " + xmlKey );
					continue;
				}
				
				_Assets[ xmlKey ] = allXML[ xmlKey ];
				
				_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( xmlKey, AssetEvent.XML_TYPE ) );
			}
			
			// All audio files in the swift
			for ( var audioKey:String in allAudio )
			{
				if ( _Assets[ audioKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Asset collision in namespace (one is audio) " + audioKey );
					continue;
				}
				
				_Assets[ audioKey ] = allAudio[ audioKey ];
				
				_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( audioKey, AssetEvent.AUDIO_TYPE ) );
			}
			
			// All images in the swift
			for ( var imageKey:String in allImages )
			{
				if ( _Assets[ imageKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Asset collision in namespace (one is image) " + imageKey );
					continue;
				}
				
				_Assets[ imageKey ] = allImages[ imageKey ];
				
				_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( imageKey, AssetEvent.IMAGE_TYPE ) );
			}
			
			// All animations in the swift
			for ( var animKey:String in allAnimations )
			{
				if ( _Assets[ animKey ] )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Asset collision in namespace (one is animation) " + animKey );
					continue;
				}
				
				_Assets[ animKey ] = allAnimations[ animKey ];
				
				_GameData.EngineEvents.dispatchEvent( AssetEvent.ASSET_EVENT_NAME, new AssetEvent( animKey, AssetEvent.ANIMATION_TYPE ) );
			}
			
			_ProcessingAssetsCount--;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _Loading.length > 0 )
			{
				var toRemove:Vector.<int> = new Vector.<int>();
				
				for each ( var asset:IAsset in _Loading )
				{
					if ( asset.isDoneLoading )
					{						
						// Mark for removal for currently loading list
						toRemove.push( asset );
					}
				}
				
				// Remove each stale asset loader
				for each ( var assetToRemove:IAsset in _Loading )
				{					
					Debug.debugLog( EngineLogChannels.ASSETS, "Asset " + assetToRemove.name + " finished loading" );
					
					_Loading.splice( _Loading.indexOf( assetToRemove ), 1 );
				}
			}
			
			// If more objects exist and we're not at max, add until at max
			if ( _LoadingQueue.length > 0 && _Loading.length < MAX_CONNECTIONS )
			{
				var amountToAdd:int = Math.min( _LoadingQueue.length, MAX_CONNECTIONS );
				
				// Pop elements from queue and add to currently loading
				for ( var i:int = 0; i < amountToAdd; i++ )
				{
					var toActive:IAsset = _LoadingQueue.pop();
					
					// @TODO: make this cleaner, it's kinda' ick
					if ( toActive is SwiftAssetLoader )
					{
						toActive.load( onSwiftAssetLoaded );	// Most likely case
					}
					else if ( toActive is XMLAsset )
					{
						toActive.load( onXMLLoaded );
					}
					else if ( toActive is SwfLoader )
					{
						toActive.load( onSwfLoaded );
					}
					else if ( toActive is AnimationAsset )
					{
						toActive.load( onAnimationLoaded );
					}
					else if ( toActive is ImageAsset )
					{
						toActive.load( onImageLoaded );
					}
					
					Debug.debugLog( EngineLogChannels.ASSETS, "Moving " + toActive.name + " to active loading list" );
					
					_Loading.push( toActive );
					
					_ProcessingAssetsCount++;
				}
			}
			
			if ( _IsPreloading && _Loading.length == 0 && _LoadingQueue.length == 0 && _ProcessingAssetsCount == 0 )
			{
				Debug.debugLog( EngineLogChannels.ASSETS, "Asset manager is done loading preload assets!" );
				
				this.dispatchEvent( new starling.events.Event( starling.events.Event.COMPLETE ) );
				_IsPreloading = false;
			}
		}
		
		public function getPreloadPercentage():Number
		{
			return 1 - ( ( _LoadingQueue.length + _Loading.length ) / _PreloadCount );
		}
		
		public function getLoadingAssetNames():String
		{
			var returnValue:String = "";
			
			if ( _Loading.length > 0 )
			{
				returnValue = _Loading[ 0 ].name;
			}
			
			return returnValue;
		}
		
		public function getRemainingItemCount():int
		{			
			return ( _Loading.length + _LoadingQueue.length );
		}
		
		public function getImage( name:String ):Bitmap
		{
			if ( !_Assets[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access image with invalid name: " + name );
				return null;
			}
			else if ( _Assets[ name ] is IAsset )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access image not yet loaded: " + name );
				return null;
			}
			
			return _Assets[ name ];
		}
		
		public function getImageCopy( name:String ):Image
		{
			var bitmap:Bitmap = getImage( name );
			
			var image:Image = Image.fromBitmap( bitmap );
			
			return image;
		}
		
		public function getXML( name:String ):XML
		{
			if ( !_Assets[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access xml with invalid name: " + name );
				return null;
			}
			else if ( _Assets[ name ] is IAsset )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access xml not yet loaded: " + name );
				return null;
			}
			
			return new XML( _Assets[ name ] );
		}
		
		public function getSound( name:String ):Sound
		{
			if ( !_Assets[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access sound with invalid name: " + name );
				return null;
			}
			
			return _Assets[ name ];
		}
		
		public function getAnimation( name:String ):Vector.<Bitmap>
		{
			if ( !_Assets[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access animation with invalid name: " + name );
				return null;
			}
			
			return _Assets[ name ];
		}
		
		public function getClass( name:String ):Class
		{
			if ( !_Assets[ name ] )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access class with invalid name: " + name );
				return null;
			}
			else if ( _Assets[ name ] is IAsset )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to access class not yet loaded: " + name );
				return null;
			}
			
			return _Assets[ name ];
		}
		
		public function getBitmapFromClass( name:String ):Bitmap
		{
			var bitmapClass:Class = getClass( name );
			
			if ( !bitmapClass )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Attempted to load bitmap from invalid class name." );
				return null;
			}
			
			var bitmapData:BitmapData = new bitmapClass() as BitmapData;
			
			if ( !bitmapData )
			{
				Debug.errorLog( EngineLogChannels.ASSETS, "Unable to cast class " + name + " to BitmapData. Check type!" );
				return null;
			}
			
			var bitmap:Bitmap = new Bitmap( bitmapData, "auto", true );
			
			return bitmap;
		}
	}
}