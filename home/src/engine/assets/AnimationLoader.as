package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.render.animation.Animation;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AnimationLoader extends EventDispatcher
	{		
		private var _Animation:Animation;
		private var _Frames:Vector.<Bitmap>;
		private var _ImageLoader:Loader;
		private var _AssetsToLoad:Vector.<String>;
		private var _Duration:Number;
		private var _IsLooping:Boolean;
		
		public function AnimationLoader() 
		{
			
		}
		
		private function getResourcePath( resourcePath:String, location:String ):String
		{
			if ( location.substr( 0, 4 ) == "http" )
			{
				return location;
			}
			
			if ( resourcePath.length > 1 )
				return resourcePath + location;
			return location;
		}
		
		public function load( resourcePath:String, xml:XML ):void
		{
			_AssetsToLoad = new Vector.<String>;
			_Frames = new Vector.<Bitmap>;
			
			_Duration = Number( xml.@duration );
			_IsLooping = String( xml.@looping ) == "true";
			
			for each ( var subXML:XML in xml.frame )
			{
				if ( subXML.@rangeStart != undefined )
				{
					const rangeStart:int = int( subXML.@rangeStart );
					const rangeEnd:int = int( subXML.@rangeEnd );
					const loc:String = String( subXML.@asset );
					const extension:String = String( subXML.@extension );
					const zeroPadding:int = int( subXML.@zeroPadding );
					
					for ( var i:int = rangeStart; i <= rangeEnd; i++ )
					{
						var frameI:String = i.toString();
						var neededZeros:int = zeroPadding - frameI.length;
						
						for ( var iPadding:int = 0; iPadding < neededZeros; iPadding++ )
						{
							frameI = "0" + frameI;
						}
						
						var path:String = loc + frameI + "." + extension;
						
						path = getResourcePath( resourcePath, path );
						
						//Debug.debugLog( EngineLogChannels.ASSETS, "Loading range animation with path: " + path );
						
						_AssetsToLoad.push( path );
					}
				}
				else
				{
					_AssetsToLoad.push( String( subXML.@asset ) );
				}
			}
			
			_AssetsToLoad.reverse();
			
			loadNextAsset();
		}
		
		private function loadNextAsset():void
		{
			// This condition should only be hit if there are no animations
			if ( _AssetsToLoad.length == 0 )
			{
				this.dispatchEvent( new Event( Event.COMPLETE ) );
				return;
			}
			
			var asset:String = _AssetsToLoad.pop();
			_ImageLoader = new Loader;
			_ImageLoader.contentLoaderInfo.addEventListener( Event.COMPLETE, onAssetLoaded );
			_ImageLoader.load( new URLRequest( asset ) );
		}
		
		private function onAssetLoaded( e:Event ):void
		{
			_ImageLoader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onAssetLoaded );
			_Frames.push( _ImageLoader.content as Bitmap );
			
			if ( _AssetsToLoad.length == 0 )
			{
				_Animation = new Animation( _Frames, _Duration, _IsLooping );
				
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
			else
			{
				loadNextAsset();
			}
		}
		
		public function getAnimation():Animation
		{
			return _Animation;
		}
	}
}