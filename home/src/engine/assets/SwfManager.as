package engine.assets 
{
	import engine.assets.SwfLoader;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.misc.EngineProperties;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SwfManager extends EventDispatcher
	{
		private var swfs:Dictionary;
		private var classMap:Dictionary;
		
		private var _SwfsToLoad:int = 0;
		
		private var _EngineProperties:EngineProperties;
		private var _ResourcePath:String;
		
		public function SwfManager( engineProperties:EngineProperties ) 
		{
			swfs = new Dictionary();
			classMap = new Dictionary();
			
			_EngineProperties = engineProperties;
			
			_ResourcePath = _EngineProperties.resourcePath + "/";
		}
		
		private function getResourcePath( location:String ):String
		{
			if ( location.substr( 0, 4 ) == "http" )
			{
				return location;
			}
			
			if ( _ResourcePath.length > 1 )
				return _ResourcePath + location;
			return location;
		}
		
		public function deserialize( xml:XML ):void
		{
			for each ( var swfXML:XML in xml.swf )
			{
				const swfName:String = swfXML.@name;
				const swfLoc:String = getResourcePath( String( swfXML.@loc ) );

				swfs[ swfName ] = new SwfLoader( swfName, swfLoc, swfXML );;
				
				_SwfsToLoad++;
			}
			
			if ( _SwfsToLoad > 0 )
			{
				for each ( var swfLoader:SwfLoader in swfs )
				{
					swfLoader.load( onSwfLoaded );
				}
			}
			else
			{
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
		}
		
		private function onSwfLoaded( swf:SwfLoader ):void
		{
			_SwfsToLoad--;
			
			if ( _SwfsToLoad == 0 )
			{
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
			
			for ( var key:String in swf.classMap )
			{
				if ( classMap[ key ] != null )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Collision in class namespace, a class loaded in swf " + swf.name + " collides with a previously loaded class of name " + key );
					continue;
				}
				
				classMap[ key ] = swf.classMap[ key ];
			}
		}
		
		public function getSwf( name:String ):SwfLoader
		{
			return swfs[ name ];
		}
		
		public function getClass( name:String ):Class
		{
			return classMap[ name ];
		}
	}
}