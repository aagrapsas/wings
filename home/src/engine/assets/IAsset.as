package engine.assets 
{
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IAsset 
	{
		function load( callback:Function ):void;
		function get name():String;
		function get isDoneLoading():Boolean;
	}	
}