package engine.debug 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Debug 
	{
		private static var _Instance:Debug;
		
		private var _DebugReporter:DebugReporter;
		private var _PerfMonitor:PerformanceMonitor;
		private var _PerfHud:PerformanceHUD;
		
		public function Debug()
		{
			initialize();
		}
		
		private function initialize():void
		{
			_DebugReporter = new DebugReporter();
			
			var debugLogger:DebugLogger = new DebugLogger();
			
			_DebugReporter.addDebugChannel( debugLogger );
			_DebugReporter.addErrorChannel( debugLogger );
			// _DebugReporter.addErrorChannel( new ErrorScreen( _Stage ) );
		}
		
		public static function setup():void
		{
			if ( _Instance == null )
			{
				create();
			}
		}
		
		public static function tearDown():void
		{
			
		}
		
		private static function create():void
		{
			_Instance = new Debug();	
		}
		
		public static function debugLog( type:String, message:String ):void
		{
			_Instance._DebugReporter.debugLog( type, message );
		}
		
		public static function errorLog( type:String, message:String ):void
		{
			_Instance._DebugReporter.errorLog( type, message );
		}
		
		public static function get debugReporter():DebugReporter { return _Instance._DebugReporter; }
		
		//public static function get perfMon():PerformanceMonitor { return _Instance._PerfMonitor; }
		//public static function set perfMon( value:PerformanceMonitor ):void { _Instance._PerfMonitor = value; }
		
		//public static function get perfHud():PerformanceHUD { return _Instance._PerfHud; }
		//public static function set perfHud( value:PerformanceHUD ):void { _Instance._PerfHud = value; }
	}
}