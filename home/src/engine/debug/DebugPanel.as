package engine.debug 
{
	import engine.interfaces.IDestroyable;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DebugPanel extends Sprite implements IDestroyable, IDebugChannel
	{		
		private const BACKGROUND_COLOR:uint = 0x330033;
		private const BACKGROUND_TEXT_COLOR:uint = 0x000000;
		private const FOREGROUND_COLOR_HISTORY:uint = 0xFFFFFF;
		private const FOREGROUND_COLOR_ENTRY:uint = 0xCCFF00;
		private const TEXT_HEIGHT:uint = 20;
		private const WIDTH:uint = 400;
		private const VISIBLE_REPLACE_TIME:Number = 0.025;
		
		private var _TextInput:TextField;
		private var _TextHistory:TextField;
		private var _Stage:Stage;
		
		private var _AccumulatedTime:Number;
		private var _LastUpdateTime:Number;
		
		private var _Height:uint;
		
		private var _DebugFunctions:Dictionary;
		
		public function DebugPanel( stage:Stage, height:uint = 400 ) 
		{
			_TextInput = new TextField;
			_TextInput.type = TextFieldType.INPUT;
			_TextHistory = new TextField;
			_DebugFunctions = new Dictionary;
			
			_Stage = stage;
			
			_Height = height - 150;
			
			this.graphics.beginFill( BACKGROUND_COLOR, 0.85 );
			this.graphics.drawRect( 0, 0, WIDTH, _Height );
			this.graphics.endFill();
			
			this.graphics.beginFill( BACKGROUND_TEXT_COLOR, 0.85 );
			this.graphics.drawRect( 0, _Height - TEXT_HEIGHT, WIDTH + 1, TEXT_HEIGHT );
			this.graphics.endFill();
			
			addChild( _TextInput );
			addChild( _TextHistory );
			
			draw();
			
			if ( CONFIG::ship == false )
			{
				_Stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true );
			}
		}
		
		private function draw():void
		{			
			_TextInput.width = WIDTH;
			_TextHistory.width = WIDTH;
			_TextHistory.height = _Height - 20;
			_TextInput.y = _TextHistory.height;
			
			_TextInput.textColor = FOREGROUND_COLOR_ENTRY;
			
			_TextHistory.textColor = FOREGROUND_COLOR_HISTORY;
			_TextHistory.multiline = true;
			_TextHistory.wordWrap = true;
			
			_TextHistory.border = true;
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			if ( e.keyCode == Keyboard.ENTER )
			{
				processLine();
			}
			else if ( e.keyCode == 192 )
			{
				toggleVisible();
			}
		}
		
		private function processLine():void
		{
			const line:String = _TextInput.text;
			
			_TextInput.text = "";
			_TextHistory.appendText( "\n" + line );
			
			if ( line == "?" )
			{
				_TextHistory.appendText( "\nKnown functions: ");
				for ( var key:String in _DebugFunctions )
				{
					_TextHistory.appendText( "\n" + key );
				}
			}
			else
			{				
				const params:Array = line.split( " " );
				const command:String = String( params[0] ).toLowerCase();
				params.splice( 0, 1 );
				
				if ( _DebugFunctions[ command ] )
				{
					var debugFunction:Function = _DebugFunctions[ command ];
					
					debugFunction.apply( null, params );
				}
				else
				{
					_TextHistory.appendText( "\nCommand " + command + " not found." );
				}
			}
		}
		
		public function addFunction( name:String, debugFunction:Function ):void
		{
			_DebugFunctions[ name.toLowerCase() ] = debugFunction;
		}
		
		public function clearFunctions():void
		{
			_DebugFunctions = new Dictionary;
		}
		
		public function toggleVisible():void
		{
			this.visible = !this.visible;
			_TextHistory.visible = !_TextHistory.visible;
			_TextInput.visible = !_TextInput.visible;
			
			if ( this.visible )
			{
				_Stage.focus = _TextInput;
				_AccumulatedTime = 0;
				_LastUpdateTime = 0;
				this.addEventListener( Event.ENTER_FRAME, update );
			}
		}
		
		private function update( e:Event ):void
		{
			if ( _AccumulatedTime >= VISIBLE_REPLACE_TIME )
			{
				this.removeEventListener( Event.ENTER_FRAME, update );
				
				_TextInput.text = "";
			}
			
			_AccumulatedTime += ( getTimer() / 1000 ) - _LastUpdateTime;
			_LastUpdateTime = getTimer() / 1000;
		}
		
		public function destroy():void
		{
			if ( _Stage )
				_Stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			
			this.removeEventListener( Event.ENTER_FRAME, update );
				
			_Stage = null;
			_TextHistory = null;
			_TextInput = null;
			_DebugFunctions = null;
		}
		
		/* INTERFACE engine.debug.IDebugChannel */
		
		public function report( type:String, message:String ):void 
		{
			const time:Number = getTimer() / 1000;
			
			_TextHistory.appendText( "\n[" + time + "] " + type + ": " + message );
			_TextHistory.scrollV = _TextHistory.maxScrollV;
		}
	}
}