package engine.debug 
{
	import engine.interfaces.IDestroyable;
	import starling.display.Sprite;
	import starling.display.Stage;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DebugManager implements IDestroyable
	{
		private var _DebugPanel:DebugPanel;
		private var _PerformanceHUD:PerformanceHUD;
		private var _PerformanceMon:PerformanceMonitor;
		private var _DebugConfig:DebugConfig;
		private var _DebugSprite:Sprite;
		private var _XMLErrorWindow:XMLErrorWindow;
		
		public function DebugManager() 
		{
			_DebugSprite = new Sprite;
		}
		/*
		public function initialize( stage:Stage ):void
		{
			_DebugPanel = new DebugPanel( stage );
			_PerformanceHUD = new PerformanceHUD( stage );
			_PerformanceMon = new PerformanceMonitor();
			_XMLErrorWindow = new XMLErrorWindow( stage );
			
			Debug.perfHud = _PerformanceHUD;
			
			Debug.debugReporter.addDebugChannel( _DebugPanel );
			Debug.debugReporter.addErrorChannel( _DebugPanel );
			
			stage.addChild( _DebugPanel );
			stage.addChild( _PerformanceHUD.Display );
			stage.addChild( _DebugSprite );
			stage.addChild( _PerformanceMon.display );
		}
		
		public function toggleDebugPanel():void
		{
			_DebugPanel.toggleVisible();
		}
		
		public function togglePerfHud():void
		{
			_PerformanceHUD.toggleVisible();
		}
		
		public function addDebugFunction( name:String, debugFunction:Function ):void
		{
			_DebugPanel.addFunction( name, debugFunction );
		}
		
		public function clearDebugFunctions():void
		{
			_DebugPanel.clearFunctions();
		}
		
		public function debugLog( type:String, message:String ):void
		{
			Debug.debugLog( type, message );
		}
		
		public function errorLog( type:String, message:String ):void
		{
			Debug.errorLog( type, message );
		}
		
		public function set Config( value:DebugConfig ):void
		{
			if ( _DebugConfig )
				_DebugConfig.destroy();
			
			_DebugPanel.clearFunctions();
			_DebugConfig = value;
			
			_DebugConfig.initialize();
		}
		
		public function sortDebugSpriteToTop():void
		{
			_DebugSprite.parent.setChildIndex(_DebugSprite, _DebugSprite.parent.numChildren - 1);
		}
		
		public function get DebugSprite():Sprite { return _DebugSprite; }
		public function get PerfHUD():PerformanceHUD { return _PerformanceHUD; }
		public function get PerfMon():PerformanceMonitor { return _PerformanceMon; }
		public function get IsDebugPanelVisible():Boolean { return _DebugPanel.visible; }
		public function get XMLError():XMLErrorWindow { return _XMLErrorWindow; }
		*/
		public function destroy():void
		{
			_DebugPanel.destroy();
			_DebugPanel = null;
		}
	}
}