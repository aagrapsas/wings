package engine.debug 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import starling.events.KeyboardEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ErrorScreen extends Sprite implements IDebugChannel
	{
		private var _Text:TextField;
		
		private var _Stage:Stage;
		
		private var _IsShown:Boolean = false;
		
		private const X:int = 100;
		private const Y:int = 100;
		private const WIDTH:int = 400;
		private const HEIGHT:int = 200;
		
		private const BACKGROUND_COLOR:uint = 0x330033;
		private const FOREGROUND_COLOR:uint = 0xFF0033;
		
		public function ErrorScreen( stage:Stage ) 
		{
			_Stage = stage;
			
			this.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{
			if ( e.keyCode == Keyboard.ESCAPE )
			{
				this.visible = false;
			}
		}
	
		public function handleError( e:UncaughtErrorEvent ):void
		{
			
		}
		
		private function render():void
		{
			if ( this.visible == false )
				this.visible = true;
			
			if ( _IsShown )
				return;
				
			_IsShown = true;
			
			_Stage.addChild( this );
			
			this.x = X;
			this.y = Y;
			
			this.graphics.beginFill( BACKGROUND_COLOR, 0.85 );
			this.graphics.drawRect( 0, 0, WIDTH, HEIGHT );
			this.graphics.endFill();
			
			_Text = new TextField();
			_Text.border = true;
			_Text.width = WIDTH;
			_Text.height = HEIGHT;
			_Text.textColor = FOREGROUND_COLOR;
			_Text.multiline = true;
			_Text.wordWrap = true;
			_Text.text = "AN ERROR OCCURRED IN THE ENGINE";
			_Text.appendText( "\n---------------------------" );
			
			this.addChild( _Text );
		}
		
		public function report( type:String, message:String ):void
		{
			render();
			
			var time:Number = getTimer() / 1000;
			
			_Text.appendText( "\n(" + time + ") " + type + ": " + message );
		}
	}
}