package engine.debug 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.text.TextField;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class XMLErrorWindow extends Sprite
	{
		private var _IsShown:Boolean = false;
		private var _Text:TextField;
		
		private const BACKGROUND_COLOR:uint = 0x330033;
		private const BACKGROUND_TEXT_COLOR:uint = 0x000000;
		private const FOREGROUND_COLOR_HISTORY:uint = 0xFF0033;
		private const TEXT_HEIGHT:uint = 20;
		private const HEIGHT:int = 400;
		private const WIDTH:uint = 400;
		
		private var _Stage:Stage;
		
		public function XMLErrorWindow( stage:Stage ) 
		{
			_Stage = stage;
		}
		
		private function show():void
		{
			if (_IsShown)
				return;
				
			_Stage.addChild( this );
			
			_Text = new TextField();
				
			this.addChild( _Text );
			
			this.graphics.beginFill( BACKGROUND_COLOR, 0.85 );
			this.graphics.drawRect( 0, 0, WIDTH, HEIGHT );
			this.graphics.endFill();
			
			_Text.width = WIDTH;
			_Text.height = HEIGHT;
			_Text.textColor = FOREGROUND_COLOR_HISTORY;
			_Text.multiline = true;
			_Text.wordWrap = true;
			_Text.border = true;
			
			_IsShown = true;
			
			this.visible = true;
		}
		
		public function report( message:String ):void
		{
			show();
			
			_Text.appendText( "\n" + message );
		}
	}
}