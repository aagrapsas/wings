package engine.debug 
{
	import engine.Actor;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import flash.display.Graphics;
	import starling.events.Event;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DebugConfig implements IDestroyable
	{
		protected var _GameData:IGameData;
		
		private var _AreBlindsVisible:Boolean = false;
		
		public function DebugConfig( gameData:IGameData ) 
		{
			_GameData = gameData;
		}
		
		public function initialize():void
		{
			/*
			_GameData.DebugSystem.addDebugFunction( "showcollision", showCollision );
			_GameData.DebugSystem.addDebugFunction( "hidecollision", hideCollision );
			_GameData.DebugSystem.addDebugFunction( "toggleblinds", toggleBlinds );
			_GameData.DebugSystem.addDebugFunction( "perfhud", perfHud );
			_GameData.DebugSystem.addDebugFunction( "togglefullscreen", toggleFullScreen );
			_GameData.DebugSystem.addDebugFunction( "perfmon", perfMon );
			*/
		}
		/*
		private function updateDrawCollision( e:Event ):void
		{
			var graphics:Graphics = _GameData.DebugSystem.DebugSprite.graphics;
			
			graphics.clear();
			
			_GameData.Scene.CollisionRoot.drawDebug( graphics, 0x660099 );
			
			var transform:Point = new Point( 1, -1 );
			
			for each ( var actor:Actor in _GameData.Scene.Actors )
			{
				if ( actor.Node )
					actor.Node.CollisionComponent.drawDebug( graphics, transform, 0xCCFF00 );
			}
		}
		
		private function showCollision():void
		{
			_GameData.Scene.GameStage.addEventListener( Event.ENTER_FRAME, updateDrawCollision, false, 0, true );
		}
		
		private function hideCollision():void
		{
			_GameData.Scene.GameStage.removeEventListener( Event.ENTER_FRAME, updateDrawCollision );
			_GameData.DebugSystem.DebugSprite.graphics.clear();
		}
		
		private function toggleBlinds():void
		{
			_AreBlindsVisible = !_AreBlindsVisible;
			_GameData.Scene.toggleBlinds( _AreBlindsVisible );
		}
		
		private function perfHud():void
		{
			_GameData.DebugSystem.togglePerfHud();
		}
		
		private function perfMon():void
		{
			_GameData.DebugSystem.PerfMon.toggleVisible();
		}
		
		private function toggleFullScreen():void
		{
			_GameData.Scene.setFullScreen(!_GameData.Scene.IsFullScreen);
		}
		*/
		
		public function destroy():void
		{
			
		}
	}
}