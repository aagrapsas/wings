package engine.audio 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import starling.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SoundManager implements IDestroyable, ITickable
	{
		private var _Sounds:Dictionary;
		
		private var _SoundDescriptions:Dictionary;
		
		
		
		private var _SoundsToLoad:Array;
		private var _CurrentlyLoading:String;
		private var _AttenuationDistance:int;
		private var _AttenuationDistanceSquared:int;
		private var _GameData:IGameData;
		
		public function SoundManager( gameData:IGameData ) 
		{
			_Sounds = new Dictionary();
			_SoundDescriptions = new Dictionary();
			_GameData = gameData;
		}
		
		public function deserialize( xml:XML ):void
		{
			Debug.debugLog( EngineLogChannels.ASSETS, "Deserializing sound configuration..." );
			
			for each ( var soundXML:XML in xml.sound )
			{
				var name:String = soundXML.@name;
				var instances:int = int( soundXML.@instances );
				var isLooping:Boolean = String( soundXML.@loop ) == "true";
				var volume:Number = soundXML.@volume == undefined ? 1 : Number( soundXML.@volume );
				var sound:Sound = _GameData.AssetManager.getSound( name );
				
				if ( !sound )
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Unable to load sound " + name );
					continue;
				}
				
				_Sounds[ name ] = new SoundFile( _GameData, name, sound, isLooping, instances, volume );
			}
			
			Debug.debugLog( EngineLogChannels.ASSETS, "Sound configuration parsed" );
		}
		
		public function tick( deltaTime:Number ):void
		{
			for each ( var soundFile:SoundFile in _Sounds )
			{
				soundFile.tick( deltaTime );
			}
		}
		
		public function playSound( sound:String, start:Number = 0, attenuationActor:Actor = null ):SoundObject
		{
			if ( _Sounds[ sound ] )
				return _Sounds[ sound ].play( start, attenuationActor );
			return null;
		}
		
		public function stopSound( sound:String ):void
		{
			if ( _Sounds[ sound ] )
				_Sounds[ sound ].stop();
		}
		
		public function stopAllSounds():void
		{
			for each ( var sound:SoundFile in _Sounds )
			{
				sound.stop();
			}
		}
		
		public function getSound( sound:String ):SoundFile
		{
			return _Sounds[ sound ];
		}
		
		public function get CurrentlyLoadingName():String { return _CurrentlyLoading; }
		
		public function setAttenuationDistance( distance:int ):void
		{
			_AttenuationDistance = distance;
			_AttenuationDistanceSquared = distance * distance;
			
			for each ( var sound:SoundFile in _Sounds )
			{
				sound.AttenuationDistance = _AttenuationDistance;
				sound.AttenuationDistanceSquared = _AttenuationDistanceSquared;
			}
		}
		
		public function destroy():void
		{
			for each ( var sound:SoundFile in _Sounds )
			{
				sound.destroy();
			}
			
			_Sounds = null;
		}
	}
}