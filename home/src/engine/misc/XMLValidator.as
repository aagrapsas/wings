package engine.misc 
{
	import engine.debug.Debug;
	import engine.debug.DebugManager;
	import engine.debug.EngineLogChannels;
	import engine.debug.XMLErrorWindow;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class XMLValidator 
	{		
		private var _Required:Dictionary;
		private var _Optional:Dictionary;
		private var _Debug:DebugManager;
		
		public function XMLValidator( debugManager:DebugManager ) 
		{
			_Debug = debugManager;
			
			_Required = new Dictionary();
			_Optional = new Dictionary();
		}
		
		public function addRequired( required:Array ):void
		{
			for each ( var field:String in required )
			{
				_Required[ field ] = true;
			}
		}
		
		public function addOptional( optional:Array ):void
		{
			for each ( var field:String in optional )
			{
				_Optional[ field ] = true;
			}
		}
		
		private function parseError( message:String ):void
		{
			//if ( _Debug )
			//	_Debug.XMLError.report( message );
				
			Debug.errorLog( EngineLogChannels.ERROR, "XMLValidator: " + message );
		}
		
		public function isValid( name:String, xml:XML ):Boolean
		{
			var attributes:XMLList = xml.attributes();
			
			// Validate that all fields that are there are valid
			for ( var i:int = 0; i < attributes.length(); i++)
			{
				var attributeName:String = attributes[ i ].name();
				
				// Continue if in required
				if ( _Required[ attributeName ] )
					continue;
					
				// Continue if in optional
				if ( _Optional[ attributeName ] )
					continue;
				
				// Error if in none
				parseError( "Field " + attributeName + " in xml " + xml + " is unexpected for " + name );
				
				return false;
			}
			
			// Make sure all required fields are there
			for ( var required:String in _Required )
			{
				var value:String = xml.attribute( required );
				
				if ( !value )
				{
					parseError( "Required field " + required + " for " + name + " was not found in xml " + xml );
					
					return false;
				}
			}
			
			return true;
		}
		
		public function set required( value:Dictionary ):void { _Required = value; }
		public function set optional( value:Dictionary ):void { _Optional = value; }
	}
}