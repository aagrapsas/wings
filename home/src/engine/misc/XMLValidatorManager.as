package engine.misc 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IGameData;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class XMLValidatorManager 
	{
		private var _Map:Dictionary;
		
		public function XMLValidatorManager() 
		{
			_Map = new Dictionary();
		}
		
		public function deserialize( xml:XML, gameData:IGameData ):void
		{
			for each ( var validatorXML:XML in xml.validator )
			{
				var validator:XMLValidator = new XMLValidator( null );
				var name:String = String( validatorXML.@name );
				var required:Dictionary = new Dictionary();
				var optional:Dictionary = new Dictionary();
				
				for each ( var requiredXML:XML in validatorXML.required )
				{
					required[ String( requiredXML.@name ) ] = true;
				}
				
				for each ( var optionalXML:XML in validatorXML.optional )
				{
					optional[ String( optionalXML.@name ) ] = true;
				}
				
				validator.optional = optional;
				validator.required = required;
				
				_Map[ name ] = validator;
			}
		}
		
		public function getValidator( name:String ):XMLValidator
		{
			return _Map[ name ];
		}
		
		public function isValidXML( name:String, xml:XML ):Boolean
		{
			var validator:XMLValidator = _Map[ name ];
			
			if ( !validator )
			{
				Debug.errorLog( EngineLogChannels.ERROR, "XMLValidator: attempted to validate data that does not have a validator for type " + name );
				return false;
			}
			
			return validator.isValid( name, xml );
		}
	}

}