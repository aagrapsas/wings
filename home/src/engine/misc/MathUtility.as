package engine.misc 
{
	import engine.collision.CollisionInfo;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class MathUtility 
	{		
		public static function getAngle( x1:Number, y1:Number, x2:Number, y2:Number ):Number
		{
			const first:Number = Math.atan2( x1, y1 );
			const second:Number = Math.atan2( x2, y2 );
			var angle:Number = first - second;
			var angle2:Number;
			
			if ( angle > Math.PI )
			{
				angle2 = angle - Math.PI * 2;
			}
			else if ( angle < Math.PI )
			{
				angle2 = angle + Math.PI * 2;
			}
			
			if ( Math.abs( angle ) > Math.abs( angle2 ) )
			{
				angle = angle2;
			}
			
			angle *= 180 / Math.PI;
			
			return angle;
		}
		
		public static function lerpInt( start:int, end:int, alpha:Number ):int
		{
			return start + alpha * ( end - start );
		}
		
		public static function lerp( start:Number, end:Number, alpha:Number ):Number
		{
			return start + alpha * ( end - start );
		}
		
		public static function easeIn( start:Number, end:Number, alpha:Number ):Number
		{
			return lerp( start, end, getEaseInAlpha( alpha ) );
		}
		
		public static function easeOut( start:Number, end:Number, alpha:Number ):Number
		{
			return lerp( start, end, getEaseOutAlpha( alpha ) );
		}
		
		public static function getEaseInOutAlpha( alpha:Number ):Number
		{
			return ( 1 - Math.sin( Math.PI / 2 + alpha * Math.PI ) ) / 2;
		}
		
		public static function getEaseInAlpha( alpha:Number ):Number
		{
			return Math.sin( Math.PI * alpha / 2 );
		}
		
		public static function getEaseOutAlpha( alpha:Number ):Number
		{
			return 1 - Math.cos( Math.PI * alpha / 2 );
		}
		
		public static function easeInInt( start:int, end:int, alpha:Number ):int
		{
			return lerpInt( start, end, getEaseInAlpha( alpha ) );
		}
		
		public static function easeOutInt( start:int, end:int, alpha:Number ):int
		{
			return lerpInt( start, end, getEaseOutAlpha( alpha ) );
		}
		
		public static function easeInOutInt( start:int, end:int, alpha:Number ):int
		{
			return lerpInt( start, end, getEaseInOutAlpha( alpha ) );
		}
		
		public static function getLineIntersection( s1X:int, s1Y:int, e1X:int, e1Y:int, s2X:int, s2Y:int, e2X:int, e2Y:int ):CollisionInfo
		{
			const dSE2X:Number = e2X - s2X;
		    const dSE2Y:Number = e2Y - s2Y;
		    const dSE1X:Number = e1X - s1X;
		    const dSE1Y:Number = e1Y - s1Y;
		    const dSSX:Number = s1Y - s2Y;
		    const dSSY:Number = s1X - s2X;
		
			const D:Number = dSE2Y * dSE1X - dSE2X * dSE1Y;
			const X:Number = ( dSE2X * dSSX - dSE2Y * dSSY ) / D;
		    const Y:Number = ( dSE1X * dSSX - dSE1Y * dSSY ) / D;
		
		    if ( X >= 0 && X <= 1 && Y >= 0 && Y <= 1 )
			{
				const intX:Number = s1X + X * dSE1X;
				const intY:Number = s1Y + X * dSE1Y;
				
				return new CollisionInfo( intX, intY, true );
			}
				
			return new CollisionInfo( 0, 0, false );
		}
		
		public static function getTwip( value:Number ):Number
		{
			return value - ( value % 0.05 ) + Math.round( ( value % 0.05 ) * 100 ) / 100;
		}
		
		/**
		 * Returns a clock-wise perpendicular with passed in values
		 * @param	x
		 * @param	y
		 */
		public static function getPerpendicular( x:Number, y:Number ):Point
		{
			return new Point( y, -x );
		}
	}
}