package engine.misc 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IGameData;
	public class GenericPool 
	{
		private var _Type:String;
		private var _GameData:IGameData;
		private var _StartSize:int = 0;
		private var _MinimumAllocation:int = 5;
		
		private var _Free:Vector.<IPoolable>;
		private var _InUse:Vector.<IPoolable>;
		private var _CreationMethod:Function;
		private var _Cap:int;
		
		public function GenericPool( creationMethod:Function, type:String, gameData:IGameData, size:int, cap:int ) 
		{
			_Free = new Vector.<IPoolable>();
			_InUse = new Vector.<IPoolable>();
			
			_Type = type;
			_CreationMethod = creationMethod;
			_GameData = gameData;
			_StartSize = size;
			_Cap = cap;
			
			if ( _StartSize > 0 )
			{
				createObject( _StartSize );
			}
		}
		
		private function createObject( amount:int ):void
		{
			for ( var i:int = 0; i < amount; i++ )
			{				
				var poolable:IPoolable = _CreationMethod.apply( null, [_Type] );
				
				poolable.pool = this;
				poolable.suspend();
				_Free.push( poolable );
			}
		}
		
		public function pop():IPoolable
		{
			if ( _Free == null || _InUse == null )
				return null;
				
			if ( _Cap > 0 && _InUse.length >= _Cap )
				return null;
			
			var poolable:IPoolable;
			
			if ( _Free.length == 0 )
				createObject( _MinimumAllocation );
				
			poolable = _Free.pop();
			
			poolable.resurrect();
			
			_InUse.push( poolable );
			
			//_GameData.DebugSystem.PerfHUD.setValue( "Pool " + _Type, "F: " + _Free.length + " / U: " + _InUse.length );
			
			return poolable;
		}
		
		public function push( poolable:IPoolable ):void
		{
			if ( _Free == null || _InUse == null )
				return;
			
			const index:int = _InUse.indexOf( poolable );
			
			if ( index == -1 )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Attempted to add poolable to " + _Type + " pool, was not found in in-use collection" );
				return;
			}
			
			_InUse.splice( index, 1 );
			
			_Free.push( poolable );
			
			//_GameData.DebugSystem.PerfHUD.setValue( "Pool " + _Type, "F: " + _Free.length + " / U: " + _InUse.length );
			
			poolable.suspend();
		}
		
		/**
		 * Forces all in-use poolables back into the free pool and suspends them
		 */
		public function freeAll():void
		{
			var toRemove:Vector.<IPoolable> = new Vector.<IPoolable>();
			
			for each ( var poolable:IPoolable in _InUse )
			{
				toRemove.push( poolable );
			}
			
			for each ( var remove:IPoolable in toRemove )
			{
				push( remove );
			}
			
			if ( _InUse.length > 0 )
			{
				Debug.errorLog( EngineLogChannels.ERROR, "Pool wasn't freed correctly, type: " + _Type );
			}
		}
		
		public function destroy():void
		{
			for each ( var poolable:IPoolable in _InUse )
			{
				poolable.destroy();
			}
			
			for each ( var poolable2:IPoolable in _Free )
			{
				poolable2.destroy();
			}
			
			_InUse = null;
			_Free = null;
		}
		
		public function get totalSize():int { return _Free.length + _InUse.length; }
		public function get freeSize():int { return _Free.length; }
		public function get inUseSize():int { return _InUse.length; }
		public function get startSize():int { return _StartSize; }
		public function get type():String { return _Type; }
	}
}