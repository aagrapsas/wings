package engine.misc 
{
	import engine.interfaces.IGameData;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IPoolConfig 
	{
		function execute( gameData:IGameData, poolManager:PoolManager ):void;
	}
}