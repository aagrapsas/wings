package  
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import starling.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.text.TextField;
	import flash.utils.getDefinitionByName;
	import game.ui.misc.UILoadingBar;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UnifiedPreloader extends MovieClip
	{
		[Embed(source = '../bin/swf/preloader.swf', symbol = "UIPreloaderBGSWF")]
		private var _BackgroundClass:Class;
		
		[Embed(source = '../bin/swf/preloader.swf', symbol = "UILoadingBarSWF")]
		private var _ProgressBarClass:Class;
		
		private var _ProgressBarMovieClip:MovieClip;
		
		private var _Tally:int = 10;
		
		private var _Statuses:Array = [ "Retrieving Galaxy Charts",
										"Loading Navigation AI",
										"Sequencing Ship Schematics",
										"Preparing Command Deck",
										"Tuning Communications",
										"Initializing Diplomatic Options",
										"Arming Weapon Systems",
										"Encrypting Network",
										"Validating Credentials",
										"Determining Coordinates",
										"Booting Combat AI",
										"Evaluating Combat Systems",
										"Synchronizing Sensor Nodes",
										"Connecting to WaveNet",
										"You're being watched...",
										"Enemies are everywhere.",
										"Tell no one.",
										"Hope is not lost.",
										"Requesting Military Access",
										"Rerouting Secure Connection",
										"Establishing Secure Protocol",
										"Tracing Enemy Locations",
										"Running DNA Validation",
										"Contacting Fleet Command" ];
										
		private static const TEXT_CHANGE_PERCENTAGE:int = 10;
		
		public function UnifiedPreloader() 
		{
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			this.addChild( new _BackgroundClass() );
			
			_ProgressBarMovieClip = new _ProgressBarClass();
			
			this.addChild( _ProgressBarMovieClip );
			
			_ProgressBarMovieClip.x = ( this.stage.stageWidth / 2 ) - ( _ProgressBarMovieClip.width / 2 );
			_ProgressBarMovieClip.y = ( this.stage.stageHeight / 2 ) - ( _ProgressBarMovieClip.height / 2 );
			
			_ProgressBarMovieClip.txtTwo.visible = true;
			_ProgressBarMovieClip.txtThree.visible = true;
			_ProgressBarMovieClip.txtOne.visible = true;
			
			_ProgressBarMovieClip.txtTwo.text = "";
			_ProgressBarMovieClip.txtThree.text = "";
			_ProgressBarMovieClip.txtOne.text = "";
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			trace(e.text);
		}
		
		private function progress(e:ProgressEvent):void 
		{
			var percent:Number = root.loaderInfo.bytesLoaded / root.loaderInfo.bytesTotal;
			
			_ProgressBarMovieClip.gotoAndStop( int( percent * 100 ) );
			
			var wholePercent:Number = percent * 100;
			
			if ( wholePercent > _Tally )
			{
				var index:int = int( Math.random() * _Statuses.length );
				var text:String = _Statuses[ index ];
				
				addLoaded( text );
				
				_Statuses.splice( index, 1 );
				
				_Tally += 10;
			}
		}
		
		private function addLoaded( text:String ):void
		{
			var textOne:String = _ProgressBarMovieClip.txtOne.text;
			var textTwo:String = _ProgressBarMovieClip.txtTwo.text;
			var textThree:String = _ProgressBarMovieClip.txtThree.text;
			
			if ( textOne )
			{
				_ProgressBarMovieClip.txtTwo.text = textOne;
				_ProgressBarMovieClip.txtTwo.visible = true;
			}
			
			if ( textTwo )
			{
				_ProgressBarMovieClip.txtThree.text = textTwo;
				_ProgressBarMovieClip.txtThree.visible = true;
			}
			
			_ProgressBarMovieClip.txtOne.text = text;
			_ProgressBarMovieClip.visible = true;
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO hide loader
			
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("Main") as Class;
			addChild(new mainClass() as DisplayObject);
		}
		
		/*
		[Embed(source = '../bin/swf/preloader.swf', symbol = "UILoadingBarSWF")]
		private var _ProgressBarClass:Class;
		
		[Embed(source = '../bin/swf/preloader.swf', symbol = "UIPreloaderBGSWF")]
		private var _BackgroundClass:Class;
		
		private var _ProgressBar:UILoadingBar;		
		private var _ProgressBarMovieClip:MovieClip;
		
		private var _Background:Bitmap;
		private var _BackgroundWidth:int;
		
		private var _Statuses:Array = [ "Retrieving Galaxy Charts",
										"Loading Navigation AI",
										"Sequencing Ship Schematics",
										"Preparing Command Deck",
										"Tuning Communications",
										"Initializing Diplomatic Options",
										"Arming Weapon Systems",
										"Encrypting Network",
										"Validating Credentials",
										"Determining Coordinates",
										"Booting Combat AI",
										"Evaluating Combat Systems",
										"Synchronizing Sensor Nodes",
										"Connecting to WaveNet",
										"You're being watched...",
										"Enemies are everywhere.",
										"Tell no one.",
										"Hope is not lost.",
										"Requesting Military Access",
										"Rerouting Secure Connection",
										"Establishing Secure Protocol",
										"Tracing Enemy Locations",
										"Running DNA Validation",
										"Contacting Fleet Command" ];
										
		private static const TEXT_CHANGE_PERCENTAGE:int = 10;
		
		public function UnifiedPreloader() 
		{						
			trace( this.currentFrame );
			
			// Set stage properties
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			_ProgressBarMovieClip = new _ProgressBarClass();
			_ProgressBar = new UILoadingBar( _ProgressBarMovieClip );
			
			_Background = new _BackgroundClass() as Bitmap;
			_BackgroundWidth = _Background.width;
			
			this.addChild( _Background );
			this.addChild( _ProgressBarMovieClip );
			
			_ProgressBarMovieClip.x = ( stage.stageWidth / 2 ) - ( 275 / 2 );
			_ProgressBarMovieClip.y = ( stage.stageHeight / 2 ) - ( _ProgressBar.clip.height / 2 );
			
			//addEventListener( Event.ENTER_FRAME, onEnterFrame );
			
			this.loaderInfo.addEventListener( ProgressEvent.PROGRESS, onProgress );
			this.addEventListener( Event.ENTER_FRAME, onEnterFrame );
		}
		
		private function onEnterFrame( e:Event):void
		{
			if ( framesLoaded == totalFrames )
			{
				this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
				this.loaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
				
				nextFrame();
				
				finishLoading();
			}
		}
		
		private function onProgress( e:ProgressEvent ):void
		{
			var percent:Number = root.loaderInfo.bytesLoaded / root.loaderInfo.bytesTotal;
			
			var wholePercent:Number = percent * 100;
			
			var dividedPercent:Number = wholePercent / TEXT_CHANGE_PERCENTAGE;
			var integerPercent:int = wholePercent / TEXT_CHANGE_PERCENTAGE;
			
			if ( dividedPercent == integerPercent )
			{
				var index:int = int( Math.random() * _Statuses.length );
				var text:String = _Statuses[ index ];
				
				_ProgressBar.addLoaded( text );
				
				_Statuses.splice( index, 1 );
			}
			
			_ProgressBar.setPercentage( percent );
			
			_ProgressBarMovieClip.x = ( stage.stageWidth / 2 ) - ( 275 / 2 );
			_ProgressBarMovieClip.y = ( stage.stageHeight / 2 ) - ( _ProgressBar.clip.height / 2 );
			
			var diffWidth:int = 0;
		
			if ( stage.stageWidth > _BackgroundWidth )
			{
				diffWidth = stage.stageWidth - _BackgroundWidth;
			}
			
			_Background.x = 0;
			_Background.y = 0;
			
			_Background.width = stage.stageWidth;
			_Background.height = stage.stageHeight + diffWidth;
		}
		
		private function finishLoading():void
		{			
			stop();
			
			var mainClass:Class = Class( getDefinitionByName( "Main" ) );
			
			var app:Object = new mainClass();
			
			addChild( app as DisplayObject );
		}
		*/
	}
}