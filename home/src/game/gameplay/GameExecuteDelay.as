package game.gameplay 
{
	import engine.interfaces.IGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class GameExecuteDelay implements IGameExecuteDelay
	{
		protected var _Delay:Number;
		protected var _LastExecution:Number = 0;
		private var _GameData:IGameData;
		
		public function GameExecuteDelay( gameData:IGameData, delay:Number  )
		{
			_GameData = gameData;
			_Delay = delay;
			_LastExecution = -delay;	// always start ready
		}
		
		public function canExecute():Boolean
		{
			var elapsedTime:Number = _GameData.Scene.GameTime - _LastExecution;
			
			return ( elapsedTime >= _Delay );
		}
		
		public function markExecution():void
		{
			_LastExecution = _GameData.Scene.GameTime;
		}
		
		public function get RefreshTimeRemaining():Number
		{
			var elapsedTime:Number = _GameData.Scene.GameTime - _LastExecution;
			var remainingTime:Number = Math.max( 0, _Delay - elapsedTime );
			
			return remainingTime;
		}
		
		public function get RefreshPercRemaining():Number
		{
			return 1 - Math.min( ( _GameData.Scene.GameTime - _LastExecution ) / _Delay, 1.0 );
		}
		
		public function get TotalRefreshTime():Number { return _Delay; }
	}
}