package game.gameplay.player 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import game.actors.SpaceShip;
	import game.ai.AIEvent;
	import game.controllers.AIController;
	import game.gameplay.waypoint.Waypoint;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class TargetSelector implements IDestroyable
	{
		private var _CurrentTarget:AIController;
		private var _GameData:HomeGameData;
		//private var _TargetIndicator:Waypoint;
		
		//private var _TargetIndicator:MovieClip;
		
		private var _IsTicking:Boolean = false;
		
		private var _OffscreenTargets:Vector.<AIController>;
		private var _InUseOffscreenIndicators:Vector.<OffscreenTargetIndicator>;
		private var _FreeOffscreenIndicatorsEnemy:Vector.<OffscreenTargetIndicator>;
		private var _FreeOffscreenIndicatorsFriend:Vector.<OffscreenTargetIndicator>;
		
		private var _IsOffscreenEnabled:Boolean = true;
		
		//private var _UI:UIPlayerTargetPanel;
		
		public function TargetSelector( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			_IsOffscreenEnabled = gameData.GameConfigData[ "hide_enemy_indicators" ] != "true";
			
			setupIndicator();
		}
		
		private function setupIndicator():void
		{
			_InUseOffscreenIndicators = new Vector.<OffscreenTargetIndicator>();
			_FreeOffscreenIndicatorsEnemy = new Vector.<OffscreenTargetIndicator>();
			_FreeOffscreenIndicatorsFriend = new Vector.<OffscreenTargetIndicator>();
			_OffscreenTargets = new Vector.<AIController>();
			
			//var targetIndicatorClass:Class = _GameData.AssetManager.getClass( "UITargetIndicatorSWF" );
			//_TargetIndicator = new targetIndicatorClass();
			
			_GameData.CurrentScenario.sortAI();
			
			//_UI = new UIPlayerTargetPanel( _GameData );
			//_GameData.Scene.UILayer.addChild( _UI );
		}
		
		public function update():void
		{
			if ( !_GameData.PlayerShip )
				return;
			
			if ( !_CurrentTarget )
			{
				if ( _GameData.CurrentScenario.EnemyAIControllers.length > 0 )
				{
					CycleTarget();
				}
			}
			
			if ( _IsOffscreenEnabled )
				updateOffscreenTargets();
		}
		
		private function updateOffscreenTargets():void
		{
			var staleAIs:Vector.<AIController> = new Vector.<AIController>;
			
			// Screen
			const width:int = _GameData.Scene.Display.stage.stageWidth;
			const height:int = _GameData.Scene.Display.stage.stageHeight;
			const halfWidth:int = width / 2;
			const halfHeight:int = height / 2;
			
			// World
			var left:int = -_GameData.ActiveCamera.X - halfWidth;
			var right:int = -_GameData.ActiveCamera.X + halfWidth;
			var top:int = _GameData.ActiveCamera.Y + halfHeight;
			var bottom:int = _GameData.ActiveCamera.Y - halfHeight;
			
			for each ( var potentialStale:AIController in _OffscreenTargets )
			{
				if ( potentialStale.Ship.ArmorHealth.isZero() || potentialStale.Ship.IsDead )	// DEAD
				{
					staleAIs.push( potentialStale );
				}
				else if ( potentialStale.Ship.X >= left && potentialStale.Ship.X <= right && potentialStale.Ship.Y <= top && potentialStale.Ship.Y >= bottom )
				{
					// Now on screen
					staleAIs.push( potentialStale );
				}
			}
			
			// Remove all stale controllers and their associated on-screen indicators
			for each ( var stale:AIController in staleAIs )
			{
				var indexOfStale:int = _OffscreenTargets.indexOf( stale );
				
				if ( indexOfStale > -1 )
				{
					_OffscreenTargets.splice( indexOfStale, 1 );
					_InUseOffscreenIndicators[ indexOfStale ].Display.alpha = 0;
					
					if ( _InUseOffscreenIndicators[ indexOfStale ].isFriend )
					{
						_FreeOffscreenIndicatorsFriend.push( _InUseOffscreenIndicators[ indexOfStale ] );
					}
					else
					{
						_FreeOffscreenIndicatorsEnemy.push( _InUseOffscreenIndicators[ indexOfStale ] );
					}
					
					_GameData.Scene.removeTickable( _InUseOffscreenIndicators[ indexOfStale ] );				
					_InUseOffscreenIndicators.splice( indexOfStale, 1 );
				}
			}
			
			// Add allAI's that are off screen to candidate list that aren't already there
			for each ( var candidate:AIController in _GameData.CurrentScenario.aiControllers )
			{
				if ( candidate.Ship.X < left ||
					 candidate.Ship.X > right ||
					 candidate.Ship.Y > top ||
					 candidate.Ship.Y < bottom )
				{
					var index:int = _OffscreenTargets.indexOf( candidate );
					
					// Only add those indicators that aren't already there!
					if ( index == -1 )
					{
						var indicator:OffscreenTargetIndicator;
						
						var isFriend:Boolean = _GameData.PlayerShip && _GameData.PlayerShip.Team == candidate.Ship.Team;
						
						if ( isFriend )
						{
							if ( _FreeOffscreenIndicatorsFriend.length > 0 )
							{
								indicator = _FreeOffscreenIndicatorsFriend.pop();
								indicator.setTarget( candidate.Ship );
							}
							else
							{
								indicator = new OffscreenTargetIndicator( _GameData, true, candidate.Ship );
								
								_GameData.Scene.addActor( indicator );
								indicator.Data = _GameData;
								indicator.initialize();
							}
						}
						else
						{
							if ( _FreeOffscreenIndicatorsEnemy.length > 0 )
							{
								indicator = _FreeOffscreenIndicatorsEnemy.pop();
								indicator.setTarget( candidate.Ship );
							}
							else
							{
								indicator = new OffscreenTargetIndicator( _GameData, false, candidate.Ship );
								
								_GameData.Scene.addActor( indicator );
								indicator.Data = _GameData;
								indicator.initialize();
							}
						}
						
						indicator.Display.alpha = 0.55;
						
						_GameData.Scene.addTickable( indicator );
						
						_OffscreenTargets.push( candidate );
						_InUseOffscreenIndicators.push( indicator );
					}
				}
			}
			
			var perfString:String = "";
			
			for each ( var valid:AIController in _OffscreenTargets )
			{
				perfString += valid.Ship.Info.Key + "\n";
			}
			
			//Debug.perfHud.setValue( "offscreen", perfString );
		}
		
		public function GetCurrentTarget():SpaceShip
		{
			if ( _CurrentTarget )
				return _CurrentTarget.Ship;
			
			return null;
		}
		
		private function onShipDied( e:AIEvent ):void
		{
			e.Owner.removeEventListener( AIEvent.AI_DEATH, onShipDied );
			
			//e.Owner.Ship.Display.removeChild( _TargetIndicator );
			
			_CurrentTarget = null;
		}
		
		public function CycleTarget():void
		{
			_GameData.CurrentScenario.sortAI();
			
			if ( _CurrentTarget )
			{
				_CurrentTarget.removeEventListener( AIEvent.AI_DEATH, onShipDied );
			}
				
			if ( _GameData.CurrentScenario.EnemyAIControllers.length > 0 )
			{
				_CurrentTarget = _GameData.CurrentScenario.EnemyAIControllers[ 0 ];
				_CurrentTarget.addEventListener( AIEvent.AI_DEATH, onShipDied );
			}
			
			_GameData.SoundSystem.playSound( _GameData.GameConfigData[ "target_select" ] );
			
			if ( _CurrentTarget )
			{
				//_UI.setTarget( _CurrentTarget.Ship );
				
				/*
				_TargetIndicator.width = _CurrentTarget.Ship.Display.cachedWidth + 10;
				_TargetIndicator.height = _CurrentTarget.Ship.Display.cachedHeight + 10;
				_TargetIndicator.x = -5;
				_TargetIndicator.y = -5;
				
				_CurrentTarget.Ship.Display.addChild( _TargetIndicator );
				*/
			}
		}
		
		public function destroy():void
		{						
			for each ( var indicator:OffscreenTargetIndicator in _InUseOffscreenIndicators )
			{
				_GameData.Scene.removeActor( indicator );
				_GameData.Scene.removeTickable( indicator );
			}
			
			for each ( var inactiveIndicator:OffscreenTargetIndicator in _FreeOffscreenIndicatorsEnemy )
			{
				_GameData.Scene.removeActor( inactiveIndicator );
			}
			
			for each ( var inactiveFriendIndicator:OffscreenTargetIndicator in _FreeOffscreenIndicatorsFriend )
			{
				_GameData.Scene.removeActor( inactiveFriendIndicator );
			}
			
			if ( _CurrentTarget )
			{
				_CurrentTarget.removeEventListener( AIEvent.AI_DEATH, onShipDied );
				
				// _CurrentTarget.Ship.Display.removeChild( _TargetIndicator );
			}
			
			//_TargetIndicator = null;
			
			//_GameData.Scene.UILayer.removeChild( _UI );
			
			//_UI.destroy();
		}
	}
}