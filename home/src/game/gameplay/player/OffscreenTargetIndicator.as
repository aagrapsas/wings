package game.gameplay.player 
{
	import engine.Actor;
	import engine.collision.CollisionInfo;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class OffscreenTargetIndicator extends Actor implements ITickable
	{
		private var _GameData:HomeGameData;
		private var _Target:SpaceShip;
		
		private static const DIST_FROM_EDGE:int = 32;
		
		private var _IsFriend:Boolean = false;
		
		private var _Dir:Point;
		
		public function OffscreenTargetIndicator( gameData:HomeGameData, isFriend:Boolean, target:SpaceShip ) 
		{
			_Dir = new Point;
			
			if ( isFriend )
			{
				var friendBitmap:Bitmap = gameData.AssetManager.getImage( "offscreen_indicator_friend" );
				setDrawingData( friendBitmap.bitmapData.clone() );
			}
			else
			{
				var enemyBitmap:Bitmap = gameData.AssetManager.getImage( "offscreen_indicator" );
				setDrawingData( enemyBitmap.bitmapData.clone() );
			}
			
			_IsFriend = isFriend;
			
			setTarget( target );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_GameData.PlayerShip || !_Target )
			{
				return;
			}
			
			// Screen
			const width:int = _GameData.Scene.Display.stage.stageWidth;
			const height:int = _GameData.Scene.Display.stage.stageHeight;
			const halfWidth:int = width / 2;
			const halfHeight:int = height / 2;
			
			var x:int = -_GameData.ActiveCamera.X;
			var y:int = _GameData.ActiveCamera.Y;
			
			// World
			var left:int = x - halfWidth;
			var right:int = x + halfWidth;
			var top:int = y + halfHeight;
			var bottom:int = y - halfHeight;			
			
			var intersection:CollisionInfo;
			
			// LEFT
			intersection = MathUtility.getLineIntersection( x, y, _Target.X, _Target.Y, left, top, left, bottom );
			
			var intersectX:int = 0;
			var intersectY:int = 0;
			
			if ( !intersection.DidCollide )
			{
				// RIGHT
				intersection = MathUtility.getLineIntersection( x, y, _Target.X, _Target.Y, right, top, right, bottom );
				
				if ( !intersection.DidCollide )
				{
					// TOP
					intersection = MathUtility.getLineIntersection( x, y, _Target.X, _Target.Y, left, top, right, top );
					
					if ( !intersection.DidCollide )
					{
						// BOTTOM
						intersection = MathUtility.getLineIntersection( x, y, _Target.X, _Target.Y, left, bottom, right, bottom );
					}
				}
			}
			
			if ( intersection && intersection.DidCollide )
			{	
				intersectX = intersection.X;
				intersectY = intersection.Y;
				
				_Dir.x =  Target.X - x;
				_Dir.y = _Target.Y - y;
				_Dir.normalize( 1 );
				
				this.X = intersectX - _Dir.x * DIST_FROM_EDGE;
				this.Y = intersectY - _Dir.y * DIST_FROM_EDGE;
				
				//trace( "Player: x: " + _GameData.PlayerShip.X + " y: " + _GameData.PlayerShip.Y );
				//trace( "Indica: x: " + this.X + " y: " + this.Y );
				
				//const dist:Number = Math.sqrt( ( _GameData.PlayerShip.X - this.X ) * ( _GameData.PlayerShip.X - this.X ) + ( _GameData.PlayerShip.Y - this.Y ) * ( _GameData.PlayerShip.Y - this.Y ) );
				
				//trace( "Distance: " + dist );
				
				this.faceToward( _Dir.x, _Dir.y );
			}
		}
		
		override public function set Data( value:IGameData ):void
		{
			super.Data = value;
			
			_GameData = HomeGameData( Data );
		}
		
		public function setTarget( value:SpaceShip ):void 
		{ 
			_Target = value;
		}
		
		public function get Target():SpaceShip { return _Target; }
		public function get isFriend():Boolean { return _IsFriend; }
		
		override public function destroy():void
		{
			super.destroy();
			
			_GameData = null;
		}
	}
}