package game.gameplay 
{
	import engine.Actor;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	//import game.gameplay.GameSystemIcon;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @TODO consider merging this with the Weapon class
	public class CombatSystem implements IDestroyable, ITickable
	{
		public var Key:String;
		public var Name:String;
		public var Type:String;
		public var HardPoint:int;
		public var HardPointLoc:Point;
		public var IsSubsystem:Boolean = false;	// true for weapons belonging to turretsfjkdlkjsdfjklfsd
		public var Owner:SpaceShip;
		//public var Icon:GameSystemIcon;
		
		public var ExecutionTimer:IGameExecuteDelay;
		
		public var Info:CombatSystemInfo;
		
		public function CombatSystem() 
		{
			
		}

		// Virtual
		public function deserialize( gameData:HomeGameData, xml:XML ):void
		{

		}
		
		// Virtual
		public function execute( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):void
		{
			return;
		}
		
		public function canExecute( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):Boolean { return true; }
		
		// Virtual
		public function tick( deltaTime:Number ):void
		{
			
		}

		public function destroy():void
		{
			Owner = null;
			
			/*
			if ( Icon )
				Icon.destroy();
				
			Icon = null;
			*/
		}
	}
}