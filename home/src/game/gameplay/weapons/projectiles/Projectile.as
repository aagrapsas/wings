package game.gameplay.weapons.projectiles 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.misc.GenericPool;
	import engine.misc.IPoolable;
	import engine.render.scene.SceneNode;
	import flash.display.Sprite;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.gameplay.weapons.DamageTypes;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Projectile extends Actor implements ITickable, IPoolable, ISceneNotifiable
	{		
		public var VelocityX:int;
		public var VelocityY:int;
		public var Instigator:SpaceShip;
		
		private var _Particle:String;
		
		private var _WeapInfo:CombatSystemInfo;
		private var _LifeTime:Number;
		private var _LifeDuration:Number;
		
		private var _Pool:GenericPool;
		private var _IsAlive:Boolean = false;
		
		public function Projectile() 
		{
			
		}
		
		public function setup( info:CombatSystemInfo, instigator:SpaceShip ):void
		{
			Instigator = instigator;
			_WeapInfo = info;
			
			_LifeDuration = Number( _WeapInfo.RawXML.@lifeTime );
		}
		
		public function resurrect():void
		{
			if ( _IsAlive )
				return;
				
			this.Display.reset();
			
			_LifeTime = 0;
			
			Data.Scene.addActor( this );
			Data.Scene.addTickable( this );
			
			_IsAlive = true;
		}
		
		public function suspend():void
		{
			if ( !_IsAlive )
				return;
			
			Data.Scene.removeTickable( this );			
			Data.Scene.removeActor( this );
			
			_IsAlive = false;
		}
		
		public function set pool( value:GenericPool ):void { _Pool = value; }
		
		// @TODO: account for any objects that happen to occur between the delta of X,Y changes (line check or multiple line checks)
		public function tick( deltaTime:Number ):void
		{
			const nextXPos:int = this.X + VelocityX * deltaTime;
			const nextYPos:int = this.Y + VelocityY * deltaTime;
			const bottomCheckX:int = this.X - this.Dir.x * ( this.Display.cachedWidth / 2 );
			const bottomCheckY:int = this.Y - this.Dir.y * ( this.Display.cachedHeight / 2);
			const topCheckX:int = nextXPos + this.Dir.x * ( this.Display.cachedWidth / 2);
			const topCheckY:int = nextYPos + this.Dir.y * ( this.Display.cachedHeight / 2);
			
			// @TODO: SUPER IMPORTANT --
			// should be a max lifespan
			
			// Project projectile's collision volume forward in time and decide if this movement will cause a collision
			var futureColliders:Vector.<SceneNode> = Data.Scene.rectCheck( nextXPos, nextYPos, Node.CollisionComponent.Width, Node.CollisionComponent.Height, Node.CollisionComponent.CollisionFlag );
			
			var shouldDestroy:Boolean = false;
			
			if ( futureColliders.length != 0 )
			{				
				for each ( var node:SceneNode in futureColliders )
				{
					var actor:SpaceShip = node.InternalObject as SpaceShip;
					
					if ( !actor )
						continue;
					
					// Do damage to all actors not on the projectile's team!					
					if ( actor && actor.Team != Instigator.Team && ( actor.Node.doesAccuratelyCollide( this.Node ) || actor.Node.CollisionComponent.doesLineAccuratelyIntersect( bottomCheckX, bottomCheckY, topCheckX, topCheckY, CollisionFlags.ACTORS ) ) )
					{
						//(Data as HomeGameData).ExplosionPool.explode( this.X, this.Y );
						actor.DoDamage( DamageTypes.PROJECTILE, _WeapInfo.Damage, Instigator, nextXPos, nextYPos );
						shouldDestroy = true;
						break;
					}
				}
			}
			
			_LifeTime += deltaTime;
			
			if ( _LifeTime >= _LifeDuration )
			{
				shouldDestroy = true;
			}
			
			if ( shouldDestroy && _IsAlive )
			{
				// Handle own life
				// return to pool
				_Pool.push( this );
			}
			else
			{
				this.X = nextXPos;
				this.Y = nextYPos;
			}
		}

		public function handleSceneTransition():void
		{
			if ( _IsAlive )
				_Pool.push( this );
		}

		override public function destroy():void
		{
			// Remove references within the scene
			suspend();
			
			super.destroy();
			
			Instigator = null;
			_WeapInfo = null;
		}
	}
}