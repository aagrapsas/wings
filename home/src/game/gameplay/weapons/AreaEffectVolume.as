package game.gameplay.weapons 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.interfaces.IGameData;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AreaEffectVolume implements ITickable, ISceneNotifiable
	{
		private var _WeaponInfo:CombatSystemInfo;
		private var _GameData:IGameData;
		private var _X:int;
		private var _Y:int;
		private var _Instigator:SpaceShip;
		
		private var _LifeTime:Number;
		
		private var _ParticleName:String;
		private var _Width:Number;
		private var _Height:Number;
		private var _LifeDuration:Number;
		
		public function AreaEffectVolume( instigator:SpaceShip, gameData:IGameData, weaponInfo:CombatSystemInfo, x:int, y:int ) 
		{
			_Instigator = instigator;
			_GameData = gameData;
			_WeaponInfo = weaponInfo;
			_X = x;
			_Y = y;
			_LifeTime = 0;
			
			//_ParticleName = String( _WeaponInfo.RawXML.@particle );
			_Width = Number( _WeaponInfo.RawXML.@width );
			_Height = Number( _WeaponInfo.RawXML.@height );
			_LifeDuration = Number( _WeaponInfo.RawXML.@duration );
			
			gameData.Scene.addTickable( this );
		}
		
		public function tick( deltaTime:Number ):void
		{
			var colliders:Vector.<SceneNode> = _GameData.Scene.rectCheck( _X, _Y, _Width, _Height, CollisionFlags.ACTORS );
			
			if ( colliders.length != 0 )
			{
				for each ( var node:SceneNode in colliders )
				{
					var actor:SpaceShip = node.InternalObject as SpaceShip;
					
					if ( actor && actor.Team != _Instigator.Team )
					{
						// @TODO need to do intersection check
						actor.DoDamage( DamageTypes.MISC, _WeaponInfo.Damage, _Instigator, _X, _Y );
					}
				}
			}
			
			_LifeTime += deltaTime;
			
			if ( _LifeTime >= _LifeDuration )
			{
				destroy();
			}
		}
		
		public function handleSceneTransition():void
		{
			destroy();
		}
	
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );	
			//_Particle.killParticle();
			
			//_Particle = null;
			_GameData = null;
			_Instigator = null;
			_WeaponInfo = null;
		}
	}
}