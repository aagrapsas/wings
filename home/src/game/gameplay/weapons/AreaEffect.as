package game.gameplay.weapons 
{
	import engine.Actor;
	import engine.interfaces.IGameData;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AreaEffect implements IWeaponType
	{
		private var _GameData:IGameData;
		private var _WeaponInfo:CombatSystemInfo;
		
		public function AreaEffect( gameData:IGameData, weaponInfo:CombatSystemInfo ) 
		{
			_GameData = gameData;
			_WeaponInfo = weaponInfo;
		}
		
		public function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void
		{
			var volume:AreaEffectVolume = new AreaEffectVolume( instigator, _GameData, _WeaponInfo, x1, y1 );
		}
		
		public function destroy():void
		{
			_GameData = null;
			_WeaponInfo = null;
		}
	}
}