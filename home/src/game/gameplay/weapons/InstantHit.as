package game.gameplay.weapons 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.interfaces.IGameData;
	import engine.render.scene.SceneNode;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class InstantHit implements IWeaponType
	{
		private var _GameData:IGameData;
		private var _WeaponInfo:CombatSystemInfo;
		private var _Particle:String;
		
		public function InstantHit( gameData:IGameData, weaponInfo:CombatSystemInfo )
		{
			_GameData = gameData;
			_WeaponInfo = weaponInfo;
			
			_Particle = String( weaponInfo.RawXML.@particle );
		}
		
		public function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void
		{			
			// for now, don't even do a line check for visibility, just get the actor at that point
			var nodes:Vector.<SceneNode> = _GameData.Scene.pointCheck( x2, y2, CollisionFlags.ACTORS );
			
			// @TODO: We'd want to do a line check for visibility here at some point with a sorted list of ships, and get the
			// first visible ship
			
			var target:SpaceShip;
			
			for each ( var node:SceneNode in nodes )
			{
				var actor:SpaceShip = node && node.InternalObject ? node.InternalObject as SpaceShip : null;
				
				if ( actor && actor.Team != instigator.Team )
				{
					target = actor;
					break;
				}
			}
			
			// Friendly fire is off for the demo
			if ( target )
			{
				// @TODO: Spawn FX -- need to do an intersection check here, a more accurate one
				target.DoDamage( DamageTypes.INSTANT, _WeaponInfo.Damage, instigator, target.X, target.Y );
				
				//WeaponParticleFactory.spawnParticle( _Particle, instigator.X, instigator.Y, actor.X, actor.Y, _GameData );
			}
		}
		
		public function destroy():void
		{
			_GameData = null;
			_WeaponInfo = null;
		}
	}
}