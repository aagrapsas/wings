package game.gameplay.weapons 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class RepairBay implements IWeaponType, ITickable, IDestroyable
	{	
		private var _GameData:IGameData;
		private var _WeaponInfo:CombatSystemInfo;
		private var _Owner:SpaceShip;
		
		private var _HealthPerSecond:Number = 0;
		private var _Radius:int = 0;
		
		public function RepairBay( owner:SpaceShip, gameData:IGameData, weaponInfo:CombatSystemInfo )
		{
			_GameData = gameData;
			_WeaponInfo = weaponInfo;
			_Owner = owner;
			
			initialize();
		}
		
		private function initialize():void
		{
			_GameData.Scene.addTickable( this );
			
			// Setup heal rate, etc. from XML
			var xml:XML = _WeaponInfo.RawXML;
			
			_Radius = int( xml.@radius );			
			_HealthPerSecond = Number( xml.@healthPerSecond );
		}
		
		public function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void
		{
			
		}
		
		public function tick( deltaTime:Number ):void
		{
			// Query scene, as this is passive
			var nearby:Vector.<SceneNode> = _GameData.Scene.rectCheck( _Owner.X, _Owner.Y, _Radius, _Radius, CollisionFlags.ACTORS );
			
			for each ( var node:SceneNode in nearby )
			{
				if ( !node.InternalObject )
				{
					continue;
				}
				
				var ship:SpaceShip = node.InternalObject as SpaceShip;
					
				if ( !ship || ship == _Owner )	// Don't heal the owner!
				{
					continue;
				}
				
				if ( ship.Team == _Owner.Team )
				{
					// Heal
					ship.ArmorHealth.heal( _HealthPerSecond * deltaTime );
				}
			}
		}
		
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );
			
			_GameData = null;
			_WeaponInfo = null;
			_Owner = null;
		}
	}
}