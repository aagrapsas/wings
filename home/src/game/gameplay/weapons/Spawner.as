package game.gameplay.weapons 
{
	import engine.interfaces.IGameData;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	import game.ai.IState;
	import game.ai.StateFactory;
	import game.controllers.AIController;
	import game.fx.HyperspaceFX;
	import game.fx.ShipFXTypes;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Spawner implements IWeaponType, ITickable, ISceneNotifiable
	{		
		private var _GameData:HomeGameData;
		private var _WeaponInfo:CombatSystemInfo;
		
		private var _AIs:Array;
		private var _Callbacks:int;
		
		private var _Duration:Number = 0;
		private var _TimeAccumulator:Number = 0;
		private var _IsActive:Boolean = false;
		
		public function Spawner( gameData:IGameData, weaponInfo:CombatSystemInfo ) 
		{
			_GameData = gameData as HomeGameData;
			_WeaponInfo = weaponInfo;
			
			_GameData.Scene.addTickable( this );
		}	
		
		public function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void
		{
			const team:uint = instigator.Team;
			_AIs = new Array;
			_Callbacks = 0;
			
			_IsActive = false;
			
			var relX:int = instigator.X;
			var relY:int = instigator.Y;
			
			if ( _GameData.PlayerShip )
			{
				relX = _GameData.PlayerShip.X;
				relY = _GameData.PlayerShip.Y;
			}
			
			var randPoint:Point = new Point( Math.random() * 100 - relX, Math.random() * 100 - relY );
			randPoint.normalize( 1 );
			
			const duration:Number = Number( _WeaponInfo.RawXML.@duration );
				
			_Duration = duration;
			_TimeAccumulator = 0;
			
			for each ( var xml:XML in _WeaponInfo.RawXML.reinforcement )
			{
				const shipType:String = String( xml.@shipType );
				const name:String = String( xml.@name );
				const x:int = int( xml.@x ) + instigator.X;
				const y:int = int( xml.@y ) + instigator.Y;
				const locomotion:String = String( xml.@locomotion );
				const rotation:int = int( xml.@rotation );
				
				var ship:SpaceShip = _GameData.Spawner.getShip( shipType );
				ship.Team = team;
				
				var ai:AIController = new AIController( ship, _GameData );
				var state:IState = StateFactory.getState( locomotion, ship, _GameData, ai );
				
				_AIs.push( { ai:ai, state:state } );
				
				_GameData.CurrentScenario.registerAI( name, ai );
				
				var fx:HyperspaceFX = new HyperspaceFX( _GameData as HomeGameData, ship, randPoint.x, randPoint.y );
				fx.jumpIn( x, y, onShipJumpedIn );
			}
		}
		
		private function onShipJumpedIn( ship:SpaceShip ):void
		{
			_Callbacks++;
			
			if ( _Callbacks == _AIs.length )
			{
				for each ( var obj:Object in _AIs )
				{
					var ai:AIController = obj.ai as AIController;
					var state:IState = obj.state as IState;
					
					ai.setLocomotionState( state );
					ai.setMainLocomotionState( state );
				}
				
				_IsActive = true;
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _IsActive )
			{
				_TimeAccumulator += deltaTime;
				
				if ( _TimeAccumulator >= _Duration )
				{
					freeShips();
				}
			}
		}
		
		private function freeShips():void
		{
			_IsActive = false;
			
			if ( _AIs.length == 0 )
			{
				return;
			}
			
			for each ( var obj:Object in _AIs )
			{
				var ai:AIController = obj.ai;
				
				if ( ai == null )
					continue;
					
				_GameData.CurrentScenario.removeAI( ai.Name );
					
				var jumpFX:HyperspaceFX = new HyperspaceFX( _GameData, ai.Ship, ai.Ship.Dir.x, ai.Ship.Dir.y );
				jumpFX.jumpOut( onShipsJumpedOut );
			}
			
			_TimeAccumulator = 0;
			_AIs.splice( 0, _AIs.length );	// Clear array
		}
		
		private function onShipsJumpedOut( ship:SpaceShip ):void
		{
			if ( ship.IsDead == false )
			{
				_GameData.Scene.removeActor( ship );
				ship.IsDead = true;
			}
		}
		
		public function handleSceneTransition():void
		{
			freeShips();
		}
		
		public function destroy():void
		{
			// freeShips();
			
			_GameData.Scene.removeTickable( this );
			
			_GameData = null;
			_WeaponInfo = null;
			
			if ( _AIs && _AIs.length > 0 )
			{
				_AIs.splice( 0, _AIs.length );
			}
		}
	}
}