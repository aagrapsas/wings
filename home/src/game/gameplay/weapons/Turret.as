package game.gameplay.weapons 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.misc.MathUtility;
	import engine.render.animation.Animation;
	import engine.render.Drawable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.catalogue.ShipComponentCatalogue;
	import game.debug.GameLogChannels;
	import game.gameplay.CombatSystem;
	import game.gameplay.GameExecuteDelay;
	import game.gameplay.GameSystemIcon;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Turret extends CombatSystem
	{
		private var _Turret:Drawable;	// physical representation attached to parent
		private var _FX:Sprite;
		
		private var _Weapons:Vector.<CombatSystem>;	// what weapons fire from this turret
		private var _GameData:HomeGameData;
		
		private var _WeaponInfo:CombatSystemInfo;	//	info detailing the turret and its weapons
		
		private var _Animations:Vector.<Animation>;	//	firing animations
		
		private var _WeaponType:IWeaponType;
		
		private var _LastFireAttempt:Number = 0;
		
		private const FACE_CENTER_AFTER:Number = 1.0;
		
		public function Turret( owner:SpaceShip, gameData:HomeGameData, weaponInfo:CombatSystemInfo, weaponType:IWeaponType, hardPoint:int ) 
		{
			// Allocation
			_Weapons = new Vector.<CombatSystem>;
			_Animations = new Vector.<Animation>;
			_Turret = new Drawable();
			_FX = new Sprite();
			
			// Assignment
			this.Owner = owner;
			_WeaponInfo = weaponInfo;
			_GameData = gameData;
			_WeaponType = weaponType;
			
			this.HardPoint = hardPoint;
			this.Info = weaponInfo;
			//this.Icon = new GameSystemIcon( weaponInfo.Icon, gameData );
			
			if ( Info.BurstDuration > 0 )
				this.ExecutionTimer = new WeaponExecuteBurst( gameData, Info.FireDelay, Info.BurstDuration, Info.BurstCooldown );
			else
				this.ExecutionTimer = new GameExecuteDelay( gameData, Info.FireDelay );
			
			initialize();
		}
		
		private function initialize():void
		{			
			// Setup drawable
			const memoryBitmap:Bitmap = _GameData.AssetManager.getImage( _WeaponInfo.RawXML.@asset ) as Bitmap;
			const data:BitmapData = memoryBitmap.bitmapData.clone();
			_Turret.RawData = data;
			_Turret.setCenter( int( _WeaponInfo.RawXML.@centerX ), int( _WeaponInfo.RawXML.@centerY ) );			
			
			this.HardPointLoc = this.Owner.Info.Chassis.HardPointLocations[this.HardPoint];
			
			_Turret.worldX = this.HardPointLoc.x;
			_Turret.worldY = -this.HardPointLoc.y;
			
			deserializeWeapons();
			
			// Handle death (for destruction)
			this.Owner.addEventListener( ShipEventTypes.SHIP_DESTROYED, onParentDestroyed );
			
			_GameData.Scene.addTickable( this );
			
			//this.Owner.Display.addChildren( _Turret, _HardPointLoc );
			//_GameData.Scene.addDrawable( _Turret );
			this.Owner.Display.addChild( _Turret.Display );
			_Turret.layer = this.Owner.ParentLayer;
		}
		
		private function deserializeWeapons():void
		{
			var xml:XML = this.Info.RawXML;
			
			const animationName:String = String( _WeaponInfo.RawXML.@fireAnimation );
			
			if ( animationName )
			{
				var anim:Animation = _GameData.AnimationSystem.getAnimation( animationName );
				
				if ( !anim && animationName )
				{
					Debug.errorLog( GameLogChannels.DATA, "Invalid animation name " + animationName + " for weapon " + this.Info.Name );
					return;
				}
				
				// Animation at tip of weapon
				anim.y = -anim.height;
				
				if ( xml.@animX != undefined && xml.@animY != undefined )
				{
					anim.x = int( xml.@animX );
					anim.y = int( xml.@animY );
				}
				
				anim.pause();
				
				_Turret.addChild( anim );
				
				_Animations.push( anim );
			}
			
			
		}
		
		override public function tick( deltaTime:Number ):void
		{		
			for each ( var animation:Animation in _Animations )
			{
				animation.tick( deltaTime );
			}
			
			if ( ( _GameData.Scene.GameTime - _LastFireAttempt ) >= FACE_CENTER_AFTER )
			{
				faceNeutral();
			}
			
			//this.Icon.setDisplayPercent( 1 - this.ExecutionTimer.RefreshPercRemaining );
		}
		
		private function faceNeutral():void
		{
			// Only calculate these values once (const)
			var angle:Number = MathUtility.getAngle( this.Owner.Dir.x, this.Owner.Dir.y, _Turret.normal.x, _Turret.normal.y );
			const clamp:Number = 5.0;
			
			// Clamp
			if ( Math.abs( angle ) > clamp )
			{
				angle = angle > 0 ? clamp : -clamp;
			}
			
			if ( angle != 0 )
				_Turret.rotate( angle );
		}
		
		// Pass throughs for interface
		override public function execute( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):void
		{ 			
			_LastFireAttempt = _GameData.Scene.GameTime;
			
			var turretWorldPosition:Point = new Point( this.HardPointLoc.x, this.HardPointLoc.y );
			turretWorldPosition = this.Owner.Display.transformPoint( turretWorldPosition );
			turretWorldPosition.y *= -1;
			
			if ( !canExecute( instigator, _Turret.normal, turretWorldPosition.x, turretWorldPosition.y, targetX, targetY ) )
			{
				// Didn't fire
				return;
			}
			
			// Fired
			_WeaponType.fire( instigator, _Turret.normal, turretWorldPosition.x, turretWorldPosition.y, targetX, targetY );
			
			_GameData.SoundSystem.playSound( _WeaponInfo.Audio["fireAudio"], 0, instigator );
			
			for each ( var anim:Animation in _Animations )
			{
				anim.playOnce( false );
			}
			
			var dir:Point = new Point( targetX - turretWorldPosition.x, targetY - turretWorldPosition.y );
			dir.normalize( 1 );
			
			const angle:Number = MathUtility.getAngle( dir.x, dir.y, _Turret.normal.x, _Turret.normal.y );
			_Turret.rotate( angle );
			
			this.ExecutionTimer.markExecution();
			
			return;
		}
		
		override public function canExecute( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):Boolean
		{
			return ( this.ExecutionTimer.canExecute() );
			
			/*
			//const deltaX:Number = ( originX - targetX );
			//const deltaY:Number = ( originY - targetY );
			//const distToTarget:Number = deltaX * deltaX + deltaY * deltaY;
			
			//if ( distToTarget > Info.RangeSquared )
			//	return false;
			
			return true;
			*/
		}
		
		public function get PercReloaded():Number { return ( _Weapons[ 0 ] as Weapon ).PercReloaded; }
		public function get WeapInfo():CombatSystemInfo { return ( _Weapons[ 0 ] as Weapon ).WeapInfo; }
		
		private function onParentDestroyed( e:ShipEvent ):void
		{
			destroy();
		}
		
		override public function destroy():void
		{		
			if ( this.Owner )
				this.Owner.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onParentDestroyed );
			
			// @TODO: make sure this doesn't get hit
			if ( !_GameData )
			{
				super.destroy();
				return;
			}
			
			_GameData.Scene.removeTickable( this );

			this.Owner.Display.removeChild( _Turret.Display );
			
			_WeaponInfo = null;
			
			for each ( var weapon:CombatSystem in _Weapons )
			{
				weapon.destroy();
			}
			
			_Weapons = null;	// should free all of these
			
			// DESTROY ANIMATIONS
			for each ( var animation:Animation in _Animations )
			{
				animation.destroy();
			}
			
			_GameData = null;
			
			super.destroy();
		}
	}
}