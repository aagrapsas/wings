package game.gameplay.weapons 
{
	import engine.debug.Debug;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.gameplay.CombatSystem;
	import game.gameplay.weapons.projectiles.ProjectileLauncher;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WeaponFactory 
	{
		public static function getWeapon( owner:SpaceShip, type:String, gameData:HomeGameData, weaponInfo:CombatSystemInfo, hardpoint:int, isSubSystem:Boolean ):CombatSystem
		{
			var weaponType:IWeaponType;
			
			switch ( type )
			{				
				case WeaponTypes.INSTANT_HIT:
					weaponType = new InstantHit( gameData, weaponInfo );
					break;// return new Weapon( owner, gameData, weaponInfo, , hardpoint, isSubSystem );
				case WeaponTypes.PROJECTILE:
					weaponType = new ProjectileLauncher( gameData, weaponInfo );
					break;// return new Weapon( owner, gameData, weaponInfo, , hardpoint, isSubSystem );
				case WeaponTypes.AREA_EFFECT:
					weaponType = new AreaEffect( gameData, weaponInfo );
					break; // return new Weapon( owner, gameData, weaponInfo, new , hardpoint, isSubSystem );
				case WeaponTypes.TRACKING:
					weaponType = new TrackingLauncher( gameData, weaponInfo );
					break; // return new Weapon( owner, gameData, weaponInfo, , hardpoint, isSubSystem );
				//case WeaponTypes.TURRET:
				//	return new Turret( owner, gameData, weaponInfo, hardpoint );
				case WeaponTypes.SPAWNER:
					weaponType = new Spawner( gameData, weaponInfo );
					break;// return new Weapon( owner, gameData, weaponInfo, , hardpoint, isSubSystem );
				case WeaponTypes.REPAIR:
					weaponType = new RepairBay( owner, gameData, weaponInfo );
			}
			
			if ( !weaponType )
			{
				Debug.errorLog( GameLogChannels.DATA, "Unable to deserialize weapon of type " + type );
				return null;
			}
			
			var combatSystem:CombatSystem;
			
			// @TODO: generalize this, turrets shouldn't be their own class anymore!
			if ( weaponInfo.IsTurret )
			{
				combatSystem = new Turret( owner, gameData, weaponInfo, weaponType, hardpoint );
			}
			else
			{
				combatSystem = new Weapon( owner, gameData, weaponInfo, weaponType, hardpoint, false );
			}
			
			return combatSystem;
		}
	}
}