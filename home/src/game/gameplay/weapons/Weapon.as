package game.gameplay.weapons 
{
	import engine.debug.Debug;
	import engine.misc.MathUtility;
	import engine.render.animation.Animation;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.gameplay.CombatSystem;
	import game.gameplay.GameExecuteDelay;
	import game.gameplay.GameSystemIcon;
	import game.HomeGameData;
	import starling.display.Image;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Weapon extends CombatSystem
	{
		private var _GameData:HomeGameData;
		private var _WeaponType:IWeaponType;
		private var _Animation:Animation;
		private var _Image:Image;
		
		public function Weapon( owner:SpaceShip, gameData:HomeGameData, weaponInfo:CombatSystemInfo, weaponType:IWeaponType, hardPoint:int, isSubSystem:Boolean ) 
		{
			_GameData = gameData;
			_WeaponType = weaponType;
			
			this.HardPoint = hardPoint;
			this.Info = weaponInfo;
			this.IsSubsystem = isSubSystem;
			this.Owner = owner;
			
			//if ( this.IsSubsystem == false )
			//	this.Icon = new GameSystemIcon( weaponInfo.Icon, gameData );
			
			if ( Info.BurstDuration > 0 )
				this.ExecutionTimer = new WeaponExecuteBurst( gameData, Info.FireDelay, Info.BurstDuration, Info.BurstCooldown );
			else
				this.ExecutionTimer = new GameExecuteDelay( gameData, Info.FireDelay );
				
			initialize();
		}
		
		private function initialize():void
		{
			_GameData.Scene.addTickable( this );
			
			if ( !this.Info.RawXML.@asset || this.Info.RawXML.@asset == undefined || this.Info.RawXML.@asset == "none" )
			{
				return;
			}
			
			_Image = _GameData.AssetManager.getImageCopy( this.Info.RawXML.@asset );
			
			this.HardPointLoc = this.Owner.Info.Chassis.HardPointLocations[ this.HardPoint ];
			
			this.Owner.addEventListener( ShipEventTypes.SHIP_DESTROYED, onParentDestroyed );
			
			_Image.pivotX = _Image.width / 2;
			_Image.pivotY = _Image.height / 2;
			
			_Image.x = this.HardPointLoc.x;
			_Image.y = this.HardPointLoc.y;
			
			this.Owner.Display.addChild( _Image );
			
			this.Owner.addEventListener( ShipEventTypes.SHIP_DESTROYED, onParentDestroyed );
			
			const animationName:String = this.Info.RawXML.@fireAnimation;
			
			if ( animationName )
			{
				_Animation = _GameData.AnimationSystem.getAnimation( animationName );
				
				if ( !_Animation && animationName )
				{
					Debug.errorLog( GameLogChannels.DATA, "Invalid animation name " + animationName + " for weapon " + this.Info.Name );
					return;
				}
				
				// Center x for animation
				_Animation.x = _Image.x;
				
				// Animation at tip of weapon
				_Animation.y = _Image.y - _Animation.height / 2;
				
				if ( this.Info.RawXML.@animX != undefined && this.Info.RawXML.@animY != undefined )
				{					
					// Offset
					_Animation.x += int( this.Info.RawXML.@animX );
					_Animation.y += int( this.Info.RawXML.@animY );
				}
				
				_Animation.pause();
				
				this.Owner.Display.addChild( _Animation );
			}			
		}
		
		private function onParentDestroyed( e:ShipEvent ):void
		{
			destroy();
		}
		
		override public function execute( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):void 
		{
			// Calculate place projectiles are actually firing from!
			
			if ( !this.IsSubsystem && _Image )
			{
				// Offset by half height, as we want the projectile to spawn at the barrel of the weapon
				var weaponWorldPosition:Point = new Point( this.HardPointLoc.x, this.HardPointLoc.y + _Image.height / 2 );
				weaponWorldPosition = this.Owner.Display.transformPoint( weaponWorldPosition );
				weaponWorldPosition.y *= -1;
				
				originX = weaponWorldPosition.x;
				originY = weaponWorldPosition.y;
			}
			
			if ( !canFire( instigator, facing, originX, originY, targetX, targetY ) )
				return;
				
			if (_Animation)
			{
				_Animation.playOnce( false );
			}
			
			_WeaponType.fire( instigator, facing, originX, originY, targetX, targetY );
			_GameData.SoundSystem.playSound( Info.Audio["fireAudio"], 0, instigator );
			this.ExecutionTimer.markExecution();
			
			return;
		}
		
		private function canFire( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):Boolean
		{
			return ( this.ExecutionTimer.canExecute() );
			
			/*
			const deltaX:Number = ( originX - targetX );
			const deltaY:Number = ( originY - targetY );
			const distToTarget:Number = deltaX * deltaX + deltaY * deltaY;
			
			if ( Info.RangeSquared > 0 && distToTarget > Info.RangeSquared )
				return false;
			
			if ( Info.FireAngle == 360 || Info.FireAngle == 0 || this.IsSubsystem )
				return true;
			
			var dir:Point = new Point( targetX - originX, targetY - originY );
			dir.normalize( 1 );
			const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, facing.x, facing.y ) );
			
			return ( angleToTarget <= Info.FireAngle );
			*/
		}
		
		override public function canExecute( instigator:SpaceShip, facing:Point, originX:int, originY:int, targetX:int, targetY:int ):Boolean
		{
			return canFire( instigator, facing, originX, originY, targetX, targetY );
		}
		
		override public function tick( deltaTime:Number ):void
		{
			if ( _Animation )
				_Animation.tick( deltaTime );
				
			// this.Icon.setDisplayPercent( 1 - this.ExecutionTimer.RefreshPercRemaining );
		}
		
		public function get PercReloaded():Number
		{
			return this.ExecutionTimer.RefreshPercRemaining;
		}		
		
		public function get WeapInfo():CombatSystemInfo { return Info; }
		
		override public function destroy():void
		{
			if ( this.Owner )
			{
				this.Owner.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onParentDestroyed );
				
				// Remove image of weapon from ship
				if ( _Image )
				{
					this.Owner.Display.removeChild( _Image );
				}
				
				if ( _Animation )
				{
					this.Owner.Display.removeChild( _Animation );
					_Animation.destroy();
				}
			}
			
			if ( _GameData )
			{
				_GameData.Scene.removeTickable( this );
			}
			
			if ( _WeaponType )
			{
				_WeaponType.destroy();
			}
			
			_GameData = null;
			_WeaponType = null;
			
			super.destroy();
		}
	}
}