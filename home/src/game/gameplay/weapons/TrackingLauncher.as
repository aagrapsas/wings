package game.gameplay.weapons 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class TrackingLauncher implements IWeaponType
	{		
		private var _GameData:IGameData;
		private var _WeaponInfo:CombatSystemInfo;
		private var _Particle:String;
		private var _Speed:Number;
		
		public function TrackingLauncher( gameData:IGameData, weaponInfo:CombatSystemInfo ) 
		{
			_GameData = gameData;
			_WeaponInfo = weaponInfo;
			
			_Particle = String( weaponInfo.RawXML.@particle );
			_Speed = Number( weaponInfo.RawXML.@speed );
		}	
		
		public function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void
		{
			var nodes:Vector.<SceneNode> = _GameData.Scene.pointCheck( x2, y2, CollisionFlags.ACTORS );
			
			var target:SpaceShip;
			
			for each ( var node:SceneNode in nodes )
			{
				var actor:SpaceShip = node && node.InternalObject ? node.InternalObject as SpaceShip : null;
				
				if ( actor && actor.Team != instigator.Team )
				{
					target = actor;
					break;
				}
			}
			
			var dir:Point = new Point( x2 - x1, y2 - y1 );
			dir.normalize( 1 );
			
			var tracker:Tracker = new Tracker( _WeaponInfo );
			const memDisplay:Bitmap = _GameData.AssetManager.getImage( _Particle ) as Bitmap;
			tracker.setDrawingData( memDisplay.bitmapData.clone() );
			tracker.Node = new SceneNode( new RectCollider( 0, 0, tracker.Display.width, tracker.Display.height, CollisionFlags.ACTORS ) );
			tracker.Instigator = instigator;
			
			if ( target )
			{
				const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, instigator.Dir.x, instigator.Dir.y ) );
				
				if ( _WeaponInfo.FireAngle > 0 && _WeaponInfo.FireAngle < 360 && angleToTarget > _WeaponInfo.FireAngle )
				{
					tracker.Target = null;
				}
				else
				{
					tracker.Target = target;
				}
			}
			
			tracker.Data = _GameData;
			tracker.initialize();
			tracker.faceToward( instigator.Dir.x, instigator.Dir.y );
			
			tracker.X = x1;
			tracker.Y = y1;
			
			tracker.VelocityX = instigator.Dir.x * instigator.Speed + dir.x * _Speed;
			tracker.VelocityY = instigator.Dir.y * instigator.Speed + dir.y * _Speed;
		}
		
		public function destroy():void
		{
			
		}
	}
}