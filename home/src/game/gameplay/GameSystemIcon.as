package game.gameplay 
{
	import engine.interfaces.IDestroyable;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.text.TextField;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class GameSystemIcon extends Sprite implements IDestroyable
	{
		public var Icon:String;
		
		private var _GameData:HomeGameData;		
		private var _Mask:Sprite;
		
		private var _Background:Bitmap;
		// private var _Gray:Bitmap;
		private var _Ready:Bitmap;
		private var _Icon:Bitmap;
		
		private var _Number:TextField;
		
		public function GameSystemIcon( icon:String, gameData:HomeGameData ) 
		{
			_GameData = gameData;
			Icon = icon;
			
			initialize();
		}
		
		private function initialize():void
		{
			_Background = _GameData.AssetManager.getImageCopy( "abilities_blue" );
			_Ready = _GameData.AssetManager.getImageCopy( "abilities_ready" );
			_Icon = _GameData.AssetManager.getImageCopy( Icon );
			//_Gray = _GameData.AssetManager.getImageCopy( "abilities_gray" );
			
			_Mask = new Sprite;
			_Mask.graphics.beginFill( 0x000000, 40 );
			_Mask.graphics.drawRect( 0, 0, _Background.width, _Background.height );
			_Mask.graphics.endFill();
			
			//_Gray.mask = _Mask;
			_Background.mask = _Mask;

			this.addChild( _Ready );
			this.addChild( _Background );
			//this.addChild( _Gray );
			this.addChild( _Mask );
			this.addChild( _Icon );
			
			_Icon.x = ( _Background.width / 2 ) - ( _Icon.width / 2 );
			_Icon.y = ( _Background.height / 2 ) - ( _Icon.height / 2 );
			
			_Mask.y = -_Background.height;
			
			_Number = new TextField();
			_Number.embedFonts = true;
			_Number.antiAliasType = AntiAliasType.ADVANCED;
			_Number.selectable = false;
			_Number.defaultTextFormat = new TextFormat( FontSystem.MYRIAD_PRO, 18, 0x0099CC );
			_Number.width = 20;
			_Number.height = 20;
			_Number.x = _Ready.width - _Number.width;
			_Number.y = 0;
			
			_Number.text = "0";
			
			this.addChild( _Number );
		}
		
		public function setFireNumber( number:int ):void
		{
			_Number.text = String( number );
		}
		
		public function setDisplayPercent( percent:Number ):void
		{
			_Mask.y = percent * -_Background.height;
		}
		
		public function destroy():void
		{
			
		}
	}
}