package game.gameplay.scoring 
{
	import game.actors.SpaceShip;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IScoreSystem 
	{
		function getDeathPoints( ship:SpaceShip, bonus:int, bonusType:int ):int;
		function getFinalScore():int;
	}
}