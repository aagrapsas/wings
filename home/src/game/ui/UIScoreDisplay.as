package game.ui 
{
	import engine.interfaces.IDestroyable;
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIScoreDisplay extends Sprite implements IDestroyable
	{
		private var _Text:TextField;
		private var _TextFormat:TextFormat;
		
		private var _GameData:HomeGameData;
		
		private var _ScoreToAdd:int;
		private var _DisplayedScore:int;
		
		private static const SCORE_RATE_PER_SECOND:int = 250;
		
		private static const WIDTH:int = 100;
		
		
		public function UIScoreDisplay( gameData:HomeGameData ) 
		{
			// Assignment
			_GameData = gameData;
			
			// Allocation
			
			initialize();
		}
		
		private function initialize():void
		{
			_Text = new TextField();
			
			this.addChild( _Text );
			
			this.x = _GameData.Scene.GameStage.stageWidth - WIDTH;
			
			_Text.antiAliasType = AntiAliasType.ADVANCED;
			_Text.width = WIDTH;
			_Text.height = 30;
			
			_TextFormat = new TextFormat();
			
			_TextFormat.align = "right";
			_TextFormat.size = "18";
			_TextFormat.bold = true;
			_TextFormat.color = 0xCCFF33;
			
			_Text.defaultTextFormat = _TextFormat;
			
			_Text.text = getPadded( 0 );
		}
		
		public function addScore( score:int ):void
		{
			_ScoreToAdd += score;
		}
		
		private function getPadded( value:int ):String
		{
			var padding:int = 6;
			
			var converted:String = value.toString();
			
			var len:int = converted.length;
			
			if ( len >= padding )
			{
				return converted;
			}
			
			var difference:int = padding - len;
			
			for ( var i:int = 0; i < difference; i++ )
			{
				converted = "0" + converted;
			}
			
			return converted;
		}
		
		public function update( deltaTime:Number ):void
		{
			if ( _ScoreToAdd > 0 )
			{
				var increment:int = _ScoreToAdd * deltaTime;
				
				if ( increment == 0 )
					increment = _ScoreToAdd;
				
				_ScoreToAdd -= increment;
				
				_DisplayedScore += increment;
				
				_Text.text = getPadded( _DisplayedScore );
			}
			
			this.x = _GameData.Scene.GameStage.stageWidth - 100;
		}
		
		public function destroy():void
		{
			
		}
	}
}