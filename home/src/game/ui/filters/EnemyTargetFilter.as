package game.ui.filters 
{
	import fl.motion.AdjustColor;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.Matrix;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EnemyTargetFilter
	{
		public static function getFilter():ColorMatrixFilter
		{
			var adjuster:AdjustColor = new AdjustColor();
			adjuster.brightness = 0;
			adjuster.contrast = 0;
			adjuster.hue = 70;
			adjuster.saturation = 33;
			
			var matrix:ColorMatrixFilter = new ColorMatrixFilter( adjuster.CalculateFinalFlatArray() );

			return matrix;
		}
	}
}