package game.ui.hud 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.HomeGameData;
	import game.ui.filters.PlayerIndicatorFilter;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIPlayerPanel extends Sprite implements ITickable, IDestroyable
	{
		
		private var _GameData:HomeGameData;
		
		private var _Clip:MovieClip;
		
		// From clip
		private var _ScoreText:TextField;
		private var _ShieldText:TextField;
		private var _ArmorText:TextField;
		private var _SalvageText:TextField;
		private var _FBCText:TextField;
		private var _Shields:MovieClip;
		private var _Armor:MovieClip;
		
		private var _LevelInfo:TextField;
		private var _LevelInfoValue:TextField;
		private var _CachedInfo:String;
		private var _CachedInfoValue:String;
		
		private var _ScoreToAdd:int;
		private var _DisplayedScore:int;
		
		private var _SalvageToAdd:int;
		private var _DisplayedSalvage:int;
		
		private var _HasBeenBuilt:Boolean = false;
		
		
		public function UIPlayerPanel( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			if ( _GameData.PlayerShip )
			{
				initialize();
			}
			else
			{
				this.visible = false;
			}
		}
		
		private function initialize():void
		{
			var scorePanelClass:Class = _GameData.AssetManager.getClass( "UIPlayerPanelSWF" );
			
			_Clip = new scorePanelClass();
			
			// Add to display
			this.addChild( _Clip );
			
			// Assignment from clip
			_ScoreText = _Clip.txtScore;
			_ShieldText = _Clip.txtShields;
			_ArmorText = _Clip.txtArmor;
			_SalvageText = _Clip.txtSalvage;
			_FBCText = _Clip.txtFBC;
			_Shields = _Clip.shields;
			_Armor = _Clip.armor;
			
			_LevelInfo = _Clip.txtLevelInfo;
			_LevelInfoValue = _Clip.txtLevelInfoValue;
			
			_LevelInfo.visible = false;
			_LevelInfoValue.visible = false;
			
			// Initialization of clip elements
			_Shields.gotoAndStop( 100 ); // full
			_Armor.gotoAndStop( 100 );
			
			// Hide shield values if no shields exist
			if ( _GameData.PlayerShip.Info.Shield == null )
			{
				// Hide shield display
				_Clip.txtShieldsDisplay.visible = false;
				_Shields.visible = false;
				_ShieldText.visible = false;
				
				// Move Salvage & FBC display up
				_Clip.txtSalvageDisplay.y = _Clip.txtShieldsDisplay.y + 3;
				_Clip.txtFBCDisplay.y =_Clip.txtSalvageDisplay.y;
				_SalvageText.y = _Clip.txtSalvageDisplay.y;
				_FBCText.y = _Clip.txtFBCDisplay.y;
			}
			
			// Setup ship silhouette
			var bitmap:Bitmap = _GameData.AssetManager.getImage( _GameData.PlayerShip.Info.Chassis.ShipWorldAsset );
			
			var bitmapCopy:Bitmap = new Bitmap( bitmap.bitmapData.clone(), "auto", true );
			
			bitmapCopy.filters = [ PlayerIndicatorFilter.getFilter() ];
			
			bitmapCopy.transform.matrix.translate( -bitmapCopy.width / 2, -bitmapCopy.height / 2 );
			bitmapCopy.rotation = 90;
			bitmapCopy.transform.matrix.translate( bitmapCopy.width / 2, bitmapCopy.height / 2 );
			
			const maxWidth:int = 65;
			const maxHeight:int = _Clip.height - 10;
			
			const scaleX:Number = Math.min( Math.max( maxWidth / bitmapCopy.width, 0 ), 1 );
			const scaleY:Number = Math.min( Math.max( maxHeight / bitmapCopy.height, 0 ), 1 );
			
			const scale:Number = scaleX < scaleY ? scaleX : scaleY;
			
			bitmapCopy.scaleX = scale;
			bitmapCopy.scaleY = scale;
			
			_Clip.addChild( bitmapCopy );
			
			bitmapCopy.y = ( _Clip.height / 2 ) - ( bitmapCopy.height / 2 );
			bitmapCopy.x = maxWidth;	// When rotated, axis point is top right
			
			updateHealth(); // Force update
			
			_ScoreText.text = getPadded( 0 );
			
			_HasBeenBuilt = true;
			
			this.visible = true;
			
			_FBCText.text = getPadded( _GameData.ClientData.fbc );
			_SalvageText.text = getPadded( _GameData.ClientData.salvage );
			
			if ( _CachedInfo )
			{
				setLevelInfo( _CachedInfo );
			}
			
			if ( _CachedInfoValue )
			{
				setLevelInfoValue( _CachedInfoValue );
			}
		}
		
		public function setLevelInfo( text:String ):void
		{
			if ( _HasBeenBuilt == false )
			{
				_CachedInfo = text;
				return;
			}
			
			_LevelInfo.visible = true;
			_LevelInfoValue.visible = true;
			_LevelInfo.text = text;
		}
		
		public function setLevelInfoValue( text:String ):void
		{
			if ( _HasBeenBuilt == false )
			{
				_CachedInfoValue = text;
				return;
			}
			
			if ( _LevelInfo.text != text )
			{
				_LevelInfoValue.text = text;
			}
		}
		
		public function addScore( score:int ):void
		{
			_ScoreToAdd += score;
		}
		
		public function addSalvage( salvage:int ):void
		{
			_SalvageToAdd += salvage;
		}
		
		private function getPadded( value:int ):String
		{
			var padding:int = 6;
			
			var converted:String = value.toString();
			
			var len:int = converted.length;
			
			if ( len >= padding )
			{
				return converted;
			}
			
			var difference:int = padding - len;
			
			for ( var i:int = 0; i < difference; i++ )
			{
				converted = "0" + converted;
			}
			
			return converted;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_HasBeenBuilt )
			{
				if ( !_GameData.PlayerShip )
				{
					return;
				}
				
				initialize();
			}
			
			if ( _ScoreToAdd > 0 )
			{
				var increment:int = _ScoreToAdd * deltaTime;
				
				if ( increment == 0 )
					increment = _ScoreToAdd;
				
				_ScoreToAdd -= increment;
				
				_DisplayedScore += increment;
				
				_ScoreText.text = getPadded( _DisplayedScore );
			}
			
			if ( _SalvageToAdd > 0 )
			{
				var salvageIncrement:int = _SalvageToAdd * deltaTime;
				
				if ( salvageIncrement == 0 )
				{
					salvageIncrement = _SalvageToAdd;
				}
				
				_SalvageToAdd -= salvageIncrement;
				
				_DisplayedSalvage += salvageIncrement;
				
				_SalvageText.text = getPadded( _DisplayedSalvage );
			}
			
			this.x = _GameData.Scene.GameStage.stageWidth - this.width;
			
			updateHealth();
		}
		
		private function updateHealth():void
		{
			if ( !_GameData.PlayerShip )
			{
				return;
			}
			
			const healthPercentage:Number = Math.min( _GameData.PlayerShip.ArmorHealth.current / _GameData.PlayerShip.Info.Armor.FullHitPoints, 1 );
			
			_Armor.gotoAndStop( int( 100 * healthPercentage ) );
			
			_ArmorText.text = _GameData.PlayerShip.ArmorHealth.current + "/" + _GameData.PlayerShip.Info.Armor.FullHitPoints;
			
			if ( _Shields.visible )
			{
				const shieldPercentage:Number = Math.min( _GameData.PlayerShip.ShieldHealth.current / _GameData.PlayerShip.Info.Shield.MaxShields, 1 );
				
				_Shields.gotoAndStop( int( 100 * shieldPercentage ) );
				
				_ShieldText.text = _GameData.PlayerShip.ShieldHealth.current + "/" + _GameData.PlayerShip.Info.Shield.MaxShields;
			}
		}
		
		public function setFBC( value:int ):void
		{
			_FBCText.text = getPadded( value );
		}
		
		public function destroy():void
		{

		}
	}
}