package game.ui.hud 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.EngineEvents;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	import game.ui.filters.EnemyTargetFilter;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIPlayerTargetPanel extends Sprite implements ITickable, IDestroyable
	{
		private var _GameData:HomeGameData;
		
		private var _Clip:MovieClip;
		
		// Clip members
		private var _ShipName:TextField;
		private var _ArmorText:TextField;
		private var _ShieldText:TextField;
		private var _Armor:MovieClip;
		private var _Shields:MovieClip;
		
		
		private var _Target:SpaceShip;
		private var _ShipDisplay:Bitmap;
		
		public function UIPlayerTargetPanel( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			var targetPanelClass:Class = _GameData.AssetManager.getClass( "UITargetPanelSWF" );
			
			_Clip = new targetPanelClass();
			
			this.addChild( _Clip );
			
			_ShipName = _Clip.txtName;
			_ArmorText = _Clip.txtArmor;
			_ShieldText = _Clip.txtShields;
			
			_Armor = _Clip.armor;
			_Shields = _Clip.shields;
			
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onResize );
			
			_GameData.Scene.addTickable( this );
			
			onResize( null );
			
			this.visible = false;
		}
		
		public function setTarget( target:SpaceShip ):void
		{
			// Break down any old target
			if ( _Target )
			{
				breakdownTarget();
			}
			
			_Target = target;
			
			if ( _Target.Info.Shield )
			{
				_Clip.shields.visible = true;
				_Clip.txtShields.visible = true;
				_Clip.txtShieldsDisplay.visible = true;
			}
			else
			{
				_Clip.shields.visible = false;
				_Clip.txtShields.visible = false;
				_Clip.txtShieldsDisplay.visible = false;
			}
			
			var bitmapData:BitmapData = _GameData.AssetManager.getImage( target.Info.Chassis.ShipWorldAsset ).bitmapData.clone();
			
			_ShipDisplay = new Bitmap( bitmapData, "auto", true );
			_ShipDisplay.filters = [ EnemyTargetFilter.getFilter() ];
			
			_ShipDisplay.rotation = 90;
			
			const scaleX:Number = Math.min( Math.max( _Clip.targetArea.width / _ShipDisplay.width, 0 ), 1 );
			const scaleY:Number = Math.min( Math.max( _Clip.targetArea.height / _ShipDisplay.height, 0 ), 1 );
			
			const scale:Number = scaleX < scaleY ? scaleX : scaleY;
			
			_ShipDisplay.scaleX = scale;
			_ShipDisplay.scaleY = scale;
			
			_Clip.addChild( _ShipDisplay );
			
			// When rotated, axis point is top right
			_ShipDisplay.x = _ShipDisplay.width + _Clip.targetArea.x + ( ( _Clip.targetArea.width / 2 ) - ( _ShipDisplay.width / 2 ) );
			_ShipDisplay.y = _Clip.targetArea.y + ( ( _Clip.targetArea.height / 2 ) - ( _ShipDisplay.height / 2 ) );
			
			_ShipName.text = target.Info.Name;
			
			_Target.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			this.visible = true;
			
			updateHealth();	// Force update of display
		}
		
		private function breakdownTarget():void
		{
			if ( !_Target )
			{
				return;
			}
			
			if ( _ShipDisplay )
			{
				_Clip.removeChild( _ShipDisplay );
				_ShipDisplay = null;
			}
			
			_Target.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_Target = null;
			
			this.visible = false;
		}
		
		public function tick( deltaTime:Number ):void
		{
			updateHealth();
		}
		
		private function updateHealth():void
		{
			if ( !_Target )
			{
				return;
			}
			
			const healthPercentage:Number = Math.min( _Target.ArmorHealth.current / _Target.Info.Armor.FullHitPoints, 1 );
			
			_Armor.gotoAndStop( int( 100 * healthPercentage ) );
			
			_ArmorText.text = _Target.ArmorHealth.current + "/" + _Target.Info.Armor.FullHitPoints;
			
			if ( _Shields.visible )
			{
				const shieldPercentage:Number = Math.min( _Target.ShieldHealth.current / _Target.Info.Shield.MaxShields, 1 );
				
				_Shields.gotoAndStop( int( 100 * shieldPercentage ) );
				
				_ShieldText.text = _Target.ShieldHealth.current + "/" + _Target.Info.Shield.MaxShields;
			}
		}
		
		private function onShipDestroyed( e:ShipEvent ):void
		{
			breakdownTarget();
		}
		
		private function onResize( data:Object ):void
		{
			// Bottom reight
			this.y = _GameData.Scene.GameStage.stageHeight - this.height;
			this.x = _GameData.Scene.GameStage.stageWidth - this.width;
		}
		
		public function destroy():void
		{
			if ( _Target )
			{
				breakdownTarget();
			}
			
			_GameData.Scene.removeTickable( this );
			
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onResize );
		}
	}
}