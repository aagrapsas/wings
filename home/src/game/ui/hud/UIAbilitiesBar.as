package game.ui.hud 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.EngineEvents;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import game.actors.SpaceShip;
	import game.gameplay.CombatSystem;
	import game.gameplay.GameSystemIcon;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIAbilitiesBar extends Sprite implements ITickable, IDestroyable
	{
		private var _Icons:Vector.<GameSystemIcon>;
		private var _EmptyIcons:Vector.<Bitmap>;
		private var _GameData:HomeGameData;
		private var _Ship:SpaceShip;
		
		private const PADDING:int = 16;
		
		public function UIAbilitiesBar( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_Icons = new Vector.<GameSystemIcon>();
			_EmptyIcons = new Vector.<Bitmap>();
			
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			var background:Bitmap = _GameData.AssetManager.getImage( "abilities_backplate" );
			background.alpha = 0.25;
			
			this.addChild( background );
			
			_GameData.Scene.UILayer.addChild( this );
			
			// Add any other icon			
			var runningX:int = 35;
			
			for ( var i:int = 0; i < 11; i++ )
			{
				var empty:Bitmap = new Bitmap( _GameData.AssetManager.getImage( "abilities_gray" ).bitmapData.clone(), "auto", true );
				
				this.addChild( empty );
				
				empty.x = runningX;
				empty.y = 6;
				empty.alpha = 0.25;
				
				runningX += empty.width + 5;
			}
			
			align();
		}
		
		public function addIcon( icon:GameSystemIcon ):void
		{
			_Icons.push( icon );
			
			this.addChild( icon );
		}
		
		public function setup( ship:SpaceShip ):void
		{
			_Ship = ship;
			
			var runningX:int = 35;
			
			// Add combat system icons
			for each ( var combatSystem:CombatSystem in _Ship.CombatSystems )
			{
				var icon:GameSystemIcon = combatSystem.Icon;
				
				if ( !icon )
				{
					continue;
				}
				
				addIcon( icon );
				
				icon.x = runningX;
				icon.y = 6;
				
				runningX += icon.width + 5;
			}
			
			align();
		}
		
		public function tick( deltaTime:Number ):void
		{

		}
		
		private function onWindowResize( data:Object ):void
		{
			align();
		}
		
		private function align():void
		{
			this.scaleX = 0.65;
			this.scaleY = 0.65;
			
			this.x = -5;
			this.y = _GameData.Scene.GameStage.stageHeight - 30 - PADDING;
		}
		
		public function destroy():void
		{
			_GameData.Scene.UILayer.removeChild( this );
			
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onWindowResize );
		}
	}
}