package game.ui.misc 
{
	import engine.misc.EngineEvents;
	import engine.ui.IPopup;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.external.Kongregate;
	import game.HomeGameData;
	import game.misc.FontSystem;
	import game.scenarios.Scenario;
	//import game.ui.menu.UIMenuContainer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIWinFailPopup implements IPopup
	{
		private var _GameData:HomeGameData;
		private var _Display:Sprite;
		
		// Fields
		private var _Title:TextField;
		private var _Score:TextField;
		private var _TimeBonus:TextField;
		private var _WarshipDamage:TextField;
		private var _FinalScore:TextField;
		
		private var _ScoreValue:TextField;
		private var _TimeBonusValue:TextField;
		private var _WarshipDamageValue:TextField;
		private var _FinalScoreValue:TextField;
		
		//private var _RetryButton:UIGenericButton;
		//private var _MainMenuButton:UIGenericButton;
		
		public function UIWinFailPopup( gameData:HomeGameData, score:int, timeBonus:int, warshipPercent:Number = 0, bonus:int = 0 ) 
		{
			_GameData = gameData;
			
			initialize( score, timeBonus, warshipPercent, bonus );
		}
		
		private function initialize( score:int, timeBonus:int, warshipPercent:Number, bonus:int ):void
		{
			// Pause game
			_GameData.Scene.IsPaused = true;
			
			_Display = new Sprite();
			
			var background:Bitmap = _GameData.AssetManager.getImage( "UIGenericPopupImg" );
			
			_Display.addChild( background );
			
			_Title = new TextField();
			_Title.embedFonts = true;
			_Title.antiAliasType = AntiAliasType.ADVANCED;
			_Title.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 17, 0xFFFF33, null, null, null, null, null, TextFormatAlign.CENTER );
			_Title.width = 211;
			_Title.height = 24;
			_Title.selectable = false;
			_Title.x = 175;
			_Title.y = 15;
			
			var titleGlow:GlowFilter = new GlowFilter();
			titleGlow.blurX = 8;
			titleGlow.blurY = 8;
			titleGlow.strength = 0.80;
			titleGlow.color = 0xFFFF33;
			titleGlow.quality = BitmapFilterQuality.LOW;
			
			_Title.filters = [ titleGlow ];
			
			_Title.text = "Mission Report";
			
			_Display.addChild( _Title );
			
			var defaultGlow:GlowFilter = new GlowFilter();
			defaultGlow.blurX = 5;
			defaultGlow.blurY = 5;
			defaultGlow.strength = 0.80;
			defaultGlow.color = 0xFFFFFF;
			defaultGlow.quality = BitmapFilterQuality.LOW;
			
			// Score
			_Score = getText( "SCORE:", 90, 20, 16, 0xFFFFFF, TextFormatAlign.LEFT );
			_Score.x = 64;
			_Score.y = 60;
			_Score.filters = [ defaultGlow ];
			
			_ScoreValue = getText( String( score ), 200, 20, 16, 0xFFFFFF, TextFormatAlign.LEFT );
			_ScoreValue.x = 300;
			_ScoreValue.y = 60;
			_ScoreValue.filters = [ defaultGlow ];
			
			// Time bonus
			_TimeBonus = getText( "TIME BONUS:", 180, 20, 16, 0xFFFFFF, TextFormatAlign.LEFT );
			_TimeBonus.x = 64;
			_TimeBonus.y = 100;
			_TimeBonus.filters = [ defaultGlow ];
			
			_TimeBonusValue = getText( String( timeBonus ), 200, 20, 16, 0xFFFFFF, TextFormatAlign.LEFT );
			_TimeBonusValue.x = 300;
			_TimeBonusValue.y = 100;
			_TimeBonusValue.filters = [ defaultGlow ];
			
			// Warship
			_WarshipDamage = getText( "WARSHIP DAMAGE:", 250, 20, 16, 0xFFFFFF, TextFormatAlign.LEFT );
			_WarshipDamage.x = 64;
			_WarshipDamage.y = 140;
			_WarshipDamage.filters = [ defaultGlow ];
			
			var redGlow:GlowFilter = new GlowFilter();
			redGlow.blurX = 5;
			redGlow.blurY = 5;
			redGlow.strength = 0.80;
			redGlow.color = 0xFF0033;
			redGlow.quality = BitmapFilterQuality.LOW;
			
			_WarshipDamageValue = getText( String( warshipPercent * 100 ) + "%", 200, 30, 25, 0xFF0033, TextFormatAlign.LEFT );
			_WarshipDamageValue.x = 300;
			_WarshipDamageValue.y = 132;
			_WarshipDamageValue.filters = [ redGlow ];
			
			if ( _WarshipDamageValue.text.length > 4 )
			{
				_WarshipDamageValue.text = _WarshipDamageValue.text.substr( 0, 2 ) + "%";
			}
			
			if ( warshipPercent == 0 )
			{
				_WarshipDamage.visible = false;
				_WarshipDamageValue.visible = false;
			}
			
			// Final score
			_FinalScore = getText( "FINAL SCORE:", 300, 30, 25, 0xFFFFFF, TextFormatAlign.CENTER );
			_FinalScore.x = ( _Display.width / 2 ) - ( _FinalScore.width / 2 );
			_FinalScore.y = 200;
			_FinalScore.filters = [ defaultGlow ];
			
			_FinalScoreValue = getText( String( timeBonus + score + bonus ), 300, 20, 16, 0xFFFFFF, TextFormatAlign.CENTER );
			_FinalScoreValue.x = ( _Display.width / 2 ) - ( _FinalScoreValue.width / 2 );
			_FinalScoreValue.y = 235;
			_FinalScoreValue.filters = [ defaultGlow ];
			
			// Post Kongregate events
			if ( Kongregate.isAvailable ) 
			{
				Kongregate.api.stats.submit( "HighScore", ( timeBonus + score + bonus ) );
				
				if ( _GameData.PlayerShip == null || _GameData.PlayerShip.ArmorHealth.current <= 0 )
				{
					Kongregate.api.stats.submit( "DeathCount", 1 );
				}
			}
			
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			setupButtons();
			
			align();
		}
		
		private function getText( text:String, width:int, height:int, size:int, color:uint, alignment:String ):TextField
		{
			var field:TextField = new TextField();
			field.embedFonts = true;
			field.antiAliasType = AntiAliasType.ADVANCED;
			field.selectable = false;
			field.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, size, color, null, null, null, null, null, alignment );
			field.text = text;
			field.width = width;
			field.height = height;
			
			_Display.addChild( field );
			
			return field;
		}
		
		private function setupButtons():void
		{
			_RetryButton = new UIGenericButton( "Retry", _GameData, onRetryClicked );
			_RetryButton.x = 280;
			_RetryButton.y = _Display.height - _RetryButton.height - 40;
			
			_Display.addChild( _RetryButton );
			
			_MainMenuButton = new UIGenericButton( "Menu", _GameData, onMenuClicked );
			_MainMenuButton.x = 90;
			_MainMenuButton.y = _Display.height - _MainMenuButton.height - 40;
			
			_Display.addChild( _MainMenuButton );
		}
		
		private function onRetryClicked( button:UIGenericButton ):void
		{
			_GameData.Scene.IsPaused = false;
			
			var scenarioName:String = _GameData.CurrentScenario.name;
			
			var xml:XML = _GameData.AssetManager.getXML( scenarioName );
			
			_GameData.CurrentScenario.breakDown();
			_GameData.CurrentScenario.destroy();
			
			_GameData.CurrentScenario = new Scenario( _GameData );
			_GameData.CurrentScenario.deserialize( xml );
			_GameData.CurrentScenario.setup();
			
			_GameData.Scene.clearPopup();
		}
		
		private function onMenuClicked( button:UIGenericButton ):void
		{
			_GameData.Scene.IsPaused = false;
			
			_GameData.CurrentScenario.breakDown();
			_GameData.CurrentScenario.destroy();
			
			//_GameData.Scene.addTickable( new UIMenuContainer( _GameData ) );
			
			_GameData.Scene.clearPopup();
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		private function onWindowResize( data:Object ):void
		{
			align();
		}
		
		private function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;

			// Center pop up
			_Display.x = ( width / 2 ) - ( _Display.width / 2 );
			_Display.y = ( height / 2 ) - ( _Display.height / 2);
		}
		
		public function get display():Sprite { return _Display; }
		public function get name():String { return "WinLose"; }
		
		public function destroy():void
		{
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			_RetryButton.destroy();
			_MainMenuButton.destroy();
		}
	}
}