package game.ui.misc 
{
	import engine.misc.EngineEvents;
	import engine.ui.IPopup;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIGenericPopupSlim implements IPopup
	{		
		private var _GameData:HomeGameData;
		private var _Display:Sprite;
		
		private var _Title:TextField;
		private var _OneLiner:TextField;
		
		private var _ButtonOneCallback:Function;
		private var _ButtonTwoCallback:Function;
		
		private var _ButtonOne:UIGenericButton;
		private var _ButtonTwo:UIGenericButton;
		
		private var _Exit:TextField;
		private var _ExitContainer:Sprite;
		
		public function UIGenericPopupSlim( gameData:HomeGameData, title:String, oneLiner:String, buttonOne:String, buttonTwo:String, buttonOneCallback:Function, buttonTwoCallback:Function, shouldShowX:Boolean = true, optionalSubFont:String = null ) 
		{
			_GameData = gameData;
			
			_ButtonOneCallback = buttonOneCallback;
			_ButtonTwoCallback = buttonTwoCallback;
			
			initialize( title, oneLiner, buttonOne, buttonTwo, shouldShowX, optionalSubFont );
		}
		
		private function initialize( title:String, oneLiner:String, buttonOne:String, buttonTwo:String, shouldShowX:Boolean, optionalSubFont:String ):void
		{
			_Display = new Sprite();
			
			var background:Bitmap = _GameData.AssetManager.getImage( "UIGenericPopupSmallImg" );
			
			_Display.addChild( background );
			
			// Title
			_Title = getText( title, FontSystem.PIRULEN, 211, 24, 17, 0xFFFF33, TextFormatAlign.CENTER );
			_Title.x = 175;
			_Title.y = 10;
			
			var titleGlow:GlowFilter = new GlowFilter();
			titleGlow.blurX = 8;
			titleGlow.blurY = 8;
			titleGlow.strength = 0.80;
			titleGlow.color = 0xFFFF33;
			titleGlow.quality = BitmapFilterQuality.LOW;
			
			_Title.filters = [ titleGlow ];
			
			// One liner
			var oneLinerFont:String = optionalSubFont ? optionalSubFont : FontSystem.MYRIAD_PRO;
			
			_OneLiner = getText( oneLiner, oneLinerFont, 438, 30, 25, 0xFFFFFF, TextFormatAlign.CENTER );
			_OneLiner.x = 63;
			_OneLiner.y = 100;
			
			// Buttons
			if ( buttonOne )
			{
				_ButtonOne = new UIGenericButton( buttonOne, _GameData, onButtonOneClicked );
				_Display.addChild( _ButtonOne );
			}
			
			if ( shouldShowX )
			{
				_Exit = getText( "X", FontSystem.MYRIAD_PRO, 27, 28, 20, 0xFFFFFF, TextFormatAlign.CENTER );
				_Exit.mouseEnabled = false;
				
				_ExitContainer = new Sprite();
				_ExitContainer.mouseEnabled = true;
				_ExitContainer.buttonMode = true;
				_ExitContainer.useHandCursor = true;
				
				_ExitContainer.x = 474;
				_ExitContainer.y = 39;
				
				_ExitContainer.addChild( _Exit );
				
				_Display.addChild( _ExitContainer );
				
				_ExitContainer.addEventListener( MouseEvent.CLICK, onExitClicked );
			}
			
			if ( buttonTwo )
			{
				_ButtonTwo = new UIGenericButton( buttonTwo, _GameData, onButtonTwoClicked );
				
				_Display.addChild( _ButtonTwo );
				
				_ButtonOne.x = 90;
				_ButtonTwo.x = 280;
				
				_ButtonOne.y = 150;
				_ButtonTwo.y = 150;
			}
			else if ( buttonOne )
			{
				_ButtonOne.x = 196;
				_ButtonOne.y = 150;
			}
			
			_GameData.Scene.IsPaused = true;
			
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			align();
		}
		
		private function onButtonOneClicked( button:UIGenericButton ):void
		{
			if ( _ButtonOneCallback != null )
			{
				_ButtonOneCallback.apply();
			}
			
			_GameData.Scene.IsPaused = false;
			
			_GameData.Scene.clearPopup();
		}
		
		private function onButtonTwoClicked( button:UIGenericButton ):void
		{
			if ( _ButtonTwoCallback != null )
			{
				_ButtonTwoCallback.apply();
			}
			
			_GameData.Scene.IsPaused = false;
			
			_GameData.Scene.clearPopup();
		}
		
		private function onExitClicked( e:MouseEvent ):void
		{
			_GameData.Scene.IsPaused = false;
			
			_GameData.Scene.clearPopup();
		}
		
		private function getText( text:String, font:String, width:int, height:int, size:int, color:uint, alignment:String ):TextField
		{
			var field:TextField = new TextField();
			field.embedFonts = true;
			field.antiAliasType = AntiAliasType.ADVANCED;
			field.selectable = false;
			field.defaultTextFormat = new TextFormat( font, size, color, null, null, null, null, null, alignment );
			field.text = text;
			field.width = width;
			field.height = height;
			
			_Display.addChild( field );
			
			return field;
		}
		
		private function onWindowResize( data:Object ):void
		{
			align();
		}
		
		private function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;

			// Center pop up
			_Display.x = ( width / 2 ) - ( _Display.width / 2 );
			_Display.y = ( height / 2 ) - ( _Display.height / 2);
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function get display():Sprite { return _Display; }
		public function get name():String { return _Title.text; }
		
		public function destroy():void
		{
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			if ( _ButtonOne )
			{
				_ButtonOne.destroy();
			}
			
			if ( _ButtonTwo )
			{
				_ButtonTwo.destroy();
			}
			
			if ( _ExitContainer )
			{
				_ExitContainer.removeEventListener( MouseEvent.CLICK, onExitClicked );
			}
		}
	}
}