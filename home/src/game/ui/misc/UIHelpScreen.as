package game.ui.misc 
{
	import engine.misc.EngineEvents;
	import engine.ui.IPopup;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIHelpScreen implements IPopup
	{
		private var _GameData:HomeGameData;
		private var _Display:Sprite;
		
		private var _Title:TextField;
		private var _Subtitle:TextField;
		private var _Body:TextField;
		private var _ClickAnywhere:TextField;
		
		private var _IsFromMenu:Boolean = false;
		private var _CloseCallback:Function = null;
		
		public function UIHelpScreen( gameData:HomeGameData, isFromMenu:Boolean = false, closeCallback:Function = null ) 
		{
			_GameData = gameData;
			_IsFromMenu = isFromMenu;
			_CloseCallback = closeCallback;
			
			initialize();
		}
	
		private function initialize():void
		{
			_Display = new Sprite();
			
			var background:Bitmap = _GameData.AssetManager.getImage( "UIGenericPopupImg" );
			
			_Display.addChild( background );
			
			_Title = new TextField();
			_Title.embedFonts = true;
			_Title.antiAliasType = AntiAliasType.ADVANCED;
			_Title.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 17, 0xFFFF33, null, null, null, null, null, TextFormatAlign.CENTER );
			_Title.width = 211;
			_Title.height = 24;
			_Title.selectable = false;
			_Title.x = 175;
			_Title.y = 15;
			
			var titleGlow:GlowFilter = new GlowFilter();
			titleGlow.blurX = 8;
			titleGlow.blurY = 8;
			titleGlow.strength = 0.80;
			titleGlow.color = 0xFFFF33;
			titleGlow.quality = BitmapFilterQuality.LOW;
			
			_Title.filters = [ titleGlow ];
			
			_Display.addChild( _Title );
			
			_Subtitle = new TextField();
			_Subtitle.embedFonts = true;
			_Subtitle.antiAliasType = AntiAliasType.ADVANCED;
			_Subtitle.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 16, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.LEFT );
			_Subtitle.selectable = false;
			_Subtitle.width = 450;
			_Subtitle.height = 34;
			_Subtitle.x = 205;
			_Subtitle.y = 60;
			
			var subtitleGlow:GlowFilter = new GlowFilter();
			subtitleGlow.blurX = 8;
			subtitleGlow.blurY = 8;
			subtitleGlow.strength = 0.80;
			subtitleGlow.color = 0xFFFFFF;
			subtitleGlow.quality = BitmapFilterQuality.LOW;
			
			_Subtitle.filters = [ subtitleGlow ];
			
			_Display.addChild( _Subtitle );
			
			_Body = new TextField();
			_Body.embedFonts = true;
			_Body.antiAliasType = AntiAliasType.ADVANCED;
			_Body.defaultTextFormat = new TextFormat( FontSystem.MYRIAD_PRO, 16, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.LEFT );
			_Body.width = 300;
			_Body.height = 221;
			_Body.selectable = false;
			_Body.multiline = true;
			_Body.wordWrap = true;
			_Body.x = 135;
			_Body.y = 106;
			
			_Display.addChild( _Body );
			
			_ClickAnywhere = new TextField();
			_ClickAnywhere.embedFonts = true;
			_ClickAnywhere.antiAliasType = AntiAliasType.ADVANCED;
			_ClickAnywhere.defaultTextFormat = new TextFormat( FontSystem.AVANT_GARDE, 20, 0xFFFFFF );
			_ClickAnywhere.selectable = false;
			_ClickAnywhere.x = 160;
			_ClickAnywhere.width = 250;
			_ClickAnywhere.height = 30;
			_ClickAnywhere.y = _Display.height - _ClickAnywhere.height - 50;
			
			if ( _IsFromMenu == false )
			{
				_ClickAnywhere.text = "Click anywhere to begin";
			}
			else
			{
				_ClickAnywhere.text = "Click anywhere to continue";
			}
			
			_ClickAnywhere.width = _ClickAnywhere.textWidth + 5;
			
			_ClickAnywhere.filters = [ subtitleGlow ];
			
			_Display.addChild( _ClickAnywhere );
			
			_Title.text = "Help";
			_Subtitle.text = "How to Play";
			_Body.text = "Arrow Up: Accelerate\n"
			_Body.appendText("Arrow Left: Turn Left\n");				
			_Body.appendText("Arrow Right: Turn Right\n");
			_Body.appendText("Arrow Down: Rotate Counter to Velocity\n");
			_Body.appendText("Space/1: Fire Guns\n");
			_Body.appendText("Control/2: Fire Missiles\n");
			_Body.appendText("3 & 4: Special Abilities (if equipped)\n");
			_Body.appendText("Tab: Target Nearest Enemy\n");
			_Body.appendText("X/Q: Boost\n");
			_Body.appendText("P: Pause Game");
			
			_Body.width = _Body.textWidth + 5;
			
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			align();
			
			_GameData.Scene.IsPaused = true;
			
			_GameData.Scene.GameStage.addEventListener( MouseEvent.CLICK, onMouseClick );
		}
		
		private function onMouseClick( e:MouseEvent ):void
		{
			if ( _CloseCallback != null )
			{
				_CloseCallback.apply();
			}
			
			_GameData.Scene.IsPaused = false;
			
			_GameData.Scene.clearPopup();
		}
		
		private function onWindowResize( data:Object ):void
		{
			align();
		}
		
		private function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;

			// Center pop up
			_Display.x = ( width / 2 ) - ( 550 / 2 );
			_Display.y = ( height / 2 ) - ( _Display.height / 2);
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function get display():Sprite { return _Display; }
		public function get name():String { return "Help"; }
		
		public function destroy():void
		{
			_GameData.Scene.GameStage.removeEventListener( MouseEvent.CLICK, onMouseClick );
			
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			_CloseCallback = null;
		}
	}
}