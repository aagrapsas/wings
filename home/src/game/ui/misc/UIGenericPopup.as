package game.ui.misc 
{
	import engine.misc.EngineEvents;
	import engine.ui.IPopup;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIGenericPopup implements IPopup
	{
		private var _GameData:HomeGameData;
		private var _Display:Sprite;
		
		private var _Title:TextField;
		private var _Subtitle:TextField;
		private var _Body:TextField;
		
		private var _Exit:TextField;
		private var _ExitContainer:Sprite;
		
		private var _TitleText:String;
		private var _SubtitleText:String;
		private var _BodyText:String;
		
		private var _ExitCallback:Function;
		
		private var _ShouldPause:Boolean = false;
		
		private var _Button:UIGenericButton;
		private var _ButtonText:String;
		private var _ButtonCallback:Function;
		
		public function UIGenericPopup( gameData:HomeGameData, title:String, subtitle:String, body:String, buttonText:String, buttonCallback:Function, exitCallback:Function, shouldPause:Boolean = false )
		{
			_GameData = gameData;
			
			_TitleText = title;
			_SubtitleText = subtitle;
			_BodyText = body;
			_ExitCallback = exitCallback;
			
			_ShouldPause = shouldPause;
			
			_ButtonText = buttonText;
			_ButtonCallback = buttonCallback;
			
			initialize();
		}
		
		private function initialize():void
		{
			_Display = new Sprite();
			
			var background:Bitmap = _GameData.AssetManager.getImage( "UIGenericPopupImg" );
			
			_Display.addChild( background );
			
			_Title = new TextField();
			_Title.embedFonts = true;
			_Title.antiAliasType = AntiAliasType.ADVANCED;
			_Title.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 17, 0xFFFF33, null, null, null, null, null, TextFormatAlign.CENTER );
			_Title.width = 211;
			_Title.height = 24;
			_Title.selectable = false;
			_Title.x = 175;
			_Title.y = 15;
			
			var titleGlow:GlowFilter = new GlowFilter();
			titleGlow.blurX = 8;
			titleGlow.blurY = 8;
			titleGlow.strength = 0.80;
			titleGlow.color = 0xFFFF33;
			titleGlow.quality = BitmapFilterQuality.LOW;
			
			_Title.filters = [ titleGlow ];
			
			_Display.addChild( _Title );
			
			_Subtitle = new TextField();
			_Subtitle.embedFonts = true;
			_Subtitle.antiAliasType = AntiAliasType.ADVANCED;
			_Subtitle.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 26, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.LEFT );
			_Subtitle.selectable = false;
			_Subtitle.width = 450;
			_Subtitle.height = 34;
			_Subtitle.x = 64;
			_Subtitle.y = 60;
			
			var subtitleGlow:GlowFilter = new GlowFilter();
			subtitleGlow.blurX = 8;
			subtitleGlow.blurY = 8;
			subtitleGlow.strength = 0.80;
			subtitleGlow.color = 0xFFFFFF;
			subtitleGlow.quality = BitmapFilterQuality.LOW;
			
			_Subtitle.filters = [ subtitleGlow ];
			
			_Display.addChild( _Subtitle );
			
			_Body = new TextField();
			_Body.embedFonts = true;
			_Body.antiAliasType = AntiAliasType.ADVANCED;
			_Body.defaultTextFormat = new TextFormat( FontSystem.MYRIAD_PRO, 25, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.LEFT );
			_Body.width = 438;
			_Body.height = 221;
			_Body.selectable = false;
			_Body.multiline = true;
			_Body.wordWrap = true;
			_Body.x = 63;
			_Body.y = 106;
			
			_Display.addChild( _Body );
			
			_Title.text = _TitleText;
			_Subtitle.text = _SubtitleText;
			_Body.text = _BodyText;
			
			// Exit button
			
			_Exit = new TextField();
			_Exit.embedFonts = true;
			_Exit.antiAliasType = AntiAliasType.ADVANCED;
			_Exit.defaultTextFormat = new TextFormat( FontSystem.MYRIAD_PRO, 20, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			_Exit.width = 27;
			_Exit.height = 28;
			_Exit.selectable = false;
			
			_Exit.text = "X";
			_Exit.mouseEnabled = false;
			
			_ExitContainer = new Sprite();
			_ExitContainer.mouseEnabled = true;
			_ExitContainer.buttonMode = true;
			_ExitContainer.useHandCursor = true;
			
			_ExitContainer.x = 474;
			_ExitContainer.y = 39;
			
			_ExitContainer.addChild( _Exit );
			
			_Display.addChild( _ExitContainer );
			
			_ExitContainer.addEventListener( MouseEvent.CLICK, onExitClicked );
			
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			_Button = new UIGenericButton( _ButtonText, _GameData, onButtonClicked );
			_Button.x = ( _Display.width / 2 ) - ( _Button.width / 2 );
			_Button.y = _Display.height - _Button.height - 50;
			
			_Display.addChild( _Button );
			
			align();
			
			if ( _ShouldPause )
			{
				_GameData.Scene.IsPaused = true;
			}
		}
		
		private function onButtonClicked( button:UIGenericButton ):void
		{
			if ( _ButtonCallback != null )
			{
				_ButtonCallback.apply();
			}
			
			if ( _ShouldPause )
			{
				_GameData.Scene.IsPaused = false;
			}
			
			_GameData.Scene.clearPopup();
		}
		
		private function onWindowResize( data:Object ):void
		{
			align();
		}
		
		private function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;

			// Center pop up
			_Display.x = ( width / 2 ) - ( _Display.width / 2 );
			_Display.y = ( height / 2 ) - ( _Display.height / 2);
		}
		
		private function onExitClicked( e:MouseEvent ):void
		{
			if ( _ExitCallback != null )
			{
				_ExitCallback.apply();
			}
			
			if ( _ShouldPause )
			{
				_GameData.Scene.IsPaused = false;
			}
			
			_GameData.Scene.clearPopup();
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function get display():Sprite { return _Display; }
		public function get name():String { return _TitleText; }
		
		public function destroy():void
		{
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			_Button.destroy();
			
			_ExitContainer.removeEventListener( MouseEvent.CLICK, onExitClicked );
		}
	}
}