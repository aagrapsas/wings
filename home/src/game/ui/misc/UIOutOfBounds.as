package game.ui.misc 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIOutOfBounds extends Sprite
	{
		private var _GameData:HomeGameData;
		private var _Clip:MovieClip;
		
		// On clip
		private var _Arrow:MovieClip;
		private var _DestroyText:TextField;
		
		public function UIOutOfBounds( gameData:HomeGameData ) 
		{
			// Assignment
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			// Allocation
			_Clip = new ( _GameData.AssetManager.getClass( "UIOutOfBoundsSWF" ) );
			
			this.addChild( _Clip );
			
			// Assignment
			_Arrow = _Clip.arrow;
			_DestroyText = _Clip.txtDestroy;
			
			// Properties
			this.mouseEnabled = false;
			this.mouseChildren = false;
		}
		
		public function rotateArrow( degrees:Number ):void
		{
			_Arrow.rotation = degrees;
		}
		
		public function setTime( seconds:int ):void
		{
			var secondsText:String = seconds == 1 ? "second" : "seconds";
			
			_DestroyText.text = "You will be destroyed in " + seconds + " " + secondsText;
		}
	}
}