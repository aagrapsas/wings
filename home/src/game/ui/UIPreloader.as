package game.ui 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import game.HomeGameData;
	import game.misc.FontSystem;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIPreloader extends Sprite implements ITickable, IDestroyable
	{	
		public static const ASSET_SYSTEM:String = "assets";
		public static const ANIMATION_SYSTEM:String = "animations";
		public static const AUDIO_SYSTEM:String = "audio";
		public static const SWF_SYSTEM:String = "swfs";
		public static const DONE:String = "done";
		
		private var _GameData:HomeGameData;
		private var _Stage:Stage;
		
		private var _CurrentlyLoading:String;
		private var _PreviousText:String;
		
		private var _TextField:TextField;

		private var _Extracting:String;
		private var _ExtractingPercentage:int = 80;
		
		public function UIPreloader( gameData:HomeGameData, stage:Stage ) 
		{
			_GameData = gameData;
			_Stage = stage;
			
			_GameData.Scene.addTickable( this );
			
			_Stage.addChild( this );
			
			_TextField = new TextField( stage.stageWidth, stage.stageHeight, "LOADING!", FontSystem.PIRULEN_FONT, 18, 0x000000, true );
			
			this.addChild( _TextField );
			
			_TextField.vAlign = VAlign.CENTER;
			_TextField.hAlign = HAlign.CENTER;
		}
		
		public function setLoading( type:String ):void
		{
			_CurrentlyLoading = type;
		}
		
		public function tick( deltaTime:Number ):void
		{
			var text:String = "";
			var percentage:int = 0;
			
			switch( _CurrentlyLoading )
			{
				case ANIMATION_SYSTEM:
					text = "Loading animations";
					percentage = 10;
					break;
				case AUDIO_SYSTEM:
					text = "Loading audio";
					percentage = 20;
					break;
				case ASSET_SYSTEM:
					text = "Loading " + _GameData.AssetManager.getLoadingAssetNames();
					
					percentage = 60;
					
					if ( text == "Loading " )
					{
						text = _Extracting ? "Extracting " + _Extracting : "Starting extraction...";
						percentage = Math.min( _ExtractingPercentage, 95 );
						_ExtractingPercentage += 2;
					}
					
					break;
				case SWF_SYSTEM:
					text = "Loading game swfs";
					percentage = 95;
					break;
				case DONE:
					text = "Loading game!";
					percentage = 100;
					// _Stage.addEventListener( KeyboardEvent.KEY_DOWN, onEnterKeyDown );
					break;
			}
			
			if ( text != _PreviousText )
			{
				_TextField.text = text;
			}
			
			if ( _CurrentlyLoading == DONE )
			{
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
		}
		
		public function extractionListener( asset:String ):void
		{
			_Extracting = asset;
		}
		
		public function destroy():void
		{
			_Stage.removeChild( this );
			_GameData.Scene.removeTickable( this );
			
			_GameData = null;
			_Stage = null;
		}
	}
}