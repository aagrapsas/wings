package game.ui.menu 
{
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Dictionary;
	import game.actors.SpaceShipInfo;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIHangar extends UIMenuPanel
	{
		// Text
		protected var _MainText:TextField;
		
		// Ship buttons
		private var _ShipButtons:Vector.<UIMenuButton>;
		private var _ShipInfo:Dictionary;
		private var _ShipPanels:Dictionary;
		
		private var _BackButton:UIMenuButton;
		
		private var _ActivePanel:UIShipHighlights;
		
		private var _WhiteDivider:Bitmap;
		private var _LeftFade:Bitmap;
		
		public function UIHangar() 
		{
			
		}
		
		override public function initialize( gameData:HomeGameData, container:UIMenuContainer ):void
		{
			super.initialize( gameData, container );
			
			_ShipButtons = new Vector.<UIMenuButton>();
			_ShipInfo = new Dictionary();
			_ShipPanels = new Dictionary();
			
			_WhiteDivider = _GameData.AssetManager.getBitmapFromClass( "UIMenuWhiteLineSWF" );
			
			this.addChild( _WhiteDivider );
			
			_MainText = new TextField();
			_MainText.embedFonts = true;
			_MainText.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 14, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.RIGHT );
			_MainText.selectable = false;
			_MainText.text = "Ships in Hangar";
			_MainText.width = _WhiteDivider.width;
			_MainText.height = 20;
			
			this.addChild( _MainText );
			
			_LeftFade = _GameData.AssetManager.getBitmapFromClass( "UIMenuVertShadowSWF" );
			
			this.addChild( _LeftFade );
			
			var buttonClass:Class = _GameData.AssetManager.getClass( "UIMenuButtonSWF" );
			
			var shipInfo:Vector.<SpaceShipInfo> = _GameData.ClientData.ships;
			
			for each ( var ship:SpaceShipInfo in shipInfo )
			{
				// Populate buttons
				var button:UIMenuButton = new UIMenuButton( _GameData, ship.Name, new buttonClass() as MovieClip, onShipClicked );
				button.registerRollOverCallback( onShipRollOver );
				
				_ShipInfo[ ship.Name ] = ship;
				
				_ShipButtons.push( button );
				
				this.addChild( button );
				
				// Populate panels
				var panel:UIShipHighlights = new UIShipHighlights( _GameData, ship );
				
				_ShipPanels[ ship.Name ] = panel;
			}
			
			_BackButton = new UIMenuButton( _GameData, "Back", new buttonClass(), onBackClicked, "UI_Generic_Cancel" );
			this.addChild( _BackButton );
			
			
			if ( _ShipButtons.length > 0 )
			{
				onShipRollOver( _ShipButtons[ 0 ] );
			}
			
			align();
		}
		
		protected function onShipClicked( button:UIMenuButton ):void
		{
			// Dig into that ship
			_Container.transition( new UIShipCustomizationOptions( _ShipInfo[ button.buttonName ] ) );
		}
		
		private function onShipRollOver( button:UIMenuButton ):void
		{
			var panel:UIShipHighlights = _ShipPanels[ button.buttonName ];
			
			if ( !panel )
			{
				Debug.errorLog( GameLogChannels.DATA, "Rollover failed to find type " + button.buttonMode );
				return;
			}
			
			if ( _ActivePanel == panel )
			{
				return;
			}
			
			if ( _ActivePanel )
			{
				this.removeChild( _ActivePanel );
			}
			
			_ActivePanel = panel;
			
			this.addChild( _ActivePanel );
			
			align();
		}
		
		private function onBackClicked( button:UIMenuButton ):void
		{
			// Go back to main menu
			_Container.transition( new UIMainMenu() );
		}
		
		override public function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;
			
			if ( _ActivePanel )
			{
				_ActivePanel.x = right - _ActivePanel.width - 40; // some buffer
				_ActivePanel.y = top;
			}
			
			_MainText.x = left + 10;
			_MainText.y = top + 77;
			
			_WhiteDivider.x = left + 10;
			_WhiteDivider.y = top + 99;
			
			_LeftFade.height = height;
			_LeftFade.x = left + 254;
			_LeftFade.y = top;
			
			var runningY:int = top + 101;
			
			for each ( var button:UIMenuButton in _ShipButtons )
			{
				button.x = left + 10;
				button.y = runningY;
				
				runningY += 20;
			}
			
			_BackButton.x = left + 10;
			_BackButton.y = runningY;
		}
		
		override public function tick( deltaTime:Number ):void
		{
			for each ( var button:UIMenuButton in _ShipButtons )
			{
				button.tick( deltaTime );
			}
			
			if ( _BackButton )
			{
				_BackButton.tick( deltaTime );
			}
		}
		
		override public function destroy():void
		{
			_BackButton.destroy();
			
			for each ( var button:UIMenuButton in _ShipButtons )
			{
				button.destroy();
			}
		}
	}
}