package game.ui.menu 
{
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShipInfo;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIShipHighlights extends Sprite
	{
		private var _GameData:HomeGameData;
		
		// Title
		private var _Title:TextField;
		private var _WhiteDivider:Bitmap;
		
		private var _Ship:SpaceShipInfo
		
		// Fields
		private var _Armor:UIShipInfoField;
		private var _Shields:UIShipInfoField;
		private var _Speed:UIShipInfoField;
		private var _Damage:UIShipInfoField;
		private var _Acceleration:UIShipInfoField;
		private var _TurnRate:UIShipInfoField;
		private var _Tonnage:UIShipInfoField;
		
		public function UIShipHighlights( gameData:HomeGameData, ship:SpaceShipInfo ) 
		{
			_Ship = ship;
			_GameData = gameData;
			
			initialize();
		}	
		
		private function initialize():void
		{
			// Upper divider
			_WhiteDivider = _GameData.AssetManager.getBitmapFromClass( "UIMenuWhiteLineSWF" );
			_WhiteDivider.x = 0;
			_WhiteDivider.y = 80;
			
			// Performance text
			_Title = new TextField();
			_Title.embedFonts = true;
			_Title.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 14, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			_Title.selectable = false;
			
			_Title.text = _Ship.Name.toLocaleUpperCase() + " PERFORMANCE";
			_Title.width = _Title.textWidth + 5;
			_Title.height = 20;
			_Title.x = 0;
			_Title.y = 60;
			
			this.addChild( _Title );
			
			this.addChild( _WhiteDivider );
			
			// Setup fields
			_Armor = new UIShipInfoField( _GameData );
			_Shields = new UIShipInfoField( _GameData );
			_Speed = new UIShipInfoField( _GameData );
			_Damage = new UIShipInfoField( _GameData );
			_Acceleration = new UIShipInfoField( _GameData );
			_TurnRate = new UIShipInfoField( _GameData );
			_Tonnage = new UIShipInfoField( _GameData );
			
			_Armor.setField( "Hit Points" );
			_Shields.setField( "Shields" );
			_Speed.setField( "Top Speed" );
			_Damage.setField( "Offensive" );
			_Acceleration.setField( "Acceleration" );
			_TurnRate.setField( "Turn Rate" );
			_Tonnage.setField( "Tonnage" );
			
			const baseLeft:int = 0;
			const baseTop:int = 90;
			const verticalSpace:int = 20;
			const horizontalSpace:int = 40;
			
			var leftFields:Vector.<UIShipInfoField> = new Vector.<UIShipInfoField>();
			var rightFields:Vector.<UIShipInfoField> = new Vector.<UIShipInfoField>();
			
			leftFields.push( _Damage );
			leftFields.push( _Armor );
			leftFields.push( _Shields );
			
			if ( _Ship.Chassis.MaxTonnage )
			{
				leftFields.push( _Tonnage );
			}
			
			rightFields.push( _Speed );
			rightFields.push( _Acceleration );
			rightFields.push( _TurnRate );
			
			var runningY:int = baseTop;
			
			for each ( var leftField:UIShipInfoField in leftFields )
			{
				leftField.x = baseLeft;
				leftField.y = runningY;
				
				// Actually add to clip
				this.addChild( leftField );
				
				runningY += leftField.height + verticalSpace;
			}
			
			runningY = baseTop;
			
			for each ( var rightField:UIShipInfoField in rightFields )
			{
				rightField.x = baseLeft + rightField.width + horizontalSpace;
				rightField.y = runningY;
				
				this.addChild( rightField );
				
				runningY += rightField.height + verticalSpace;
			}
			
			// Setup values
			const armor:int = Math.min( ( _Ship.Armor.FullHitPoints / 2000 ) * 100, 100 );
			_Armor.setProgress( armor );
			_Armor.setValue( _Ship.Armor.FullHitPoints );
			
			const shields:int = _Ship.Shield ? Math.min( ( _Ship.Shield.MaxShields / 1000 ) * 100, 100 ) : 1;
			_Shields.setProgress( shields );
			_Shields.setValue( _Ship.Shield ? _Ship.Shield.MaxShields : 0 );
			
			var combinedDPS:Number = 0;
			
			for each ( var combatSystem:String in _Ship.CombatSystems )
			{
				var system:CombatSystemInfo = _GameData.ShipCompCatalog.getCombatSystem( combatSystem );
				
				if ( system == null )
				{
					continue;
				}
				
				combinedDPS += system.DPS;
			}
			
			const damage:int = Math.min( combinedDPS / ( _GameData.ShipCompCatalog.getMostHardpoints() * 45 ), 1.0 ) * 100;
			_Damage.setProgress( damage );
			_Damage.setValue( damage );
			
			const speed:int = Math.min( ( _Ship.Engine.MaxSpeed / 400 ) * 100, 100 );
			_Speed.setProgress( speed );
			_Speed.setValue( _Ship.Engine.MaxSpeed );
			
			const acceleration:int =  Math.min( ( _Ship.Engine.MaxAcceleration / 400 ) * 100, 100 );
			_Acceleration.setProgress( acceleration );
			_Acceleration.setValue( _Ship.Engine.MaxAcceleration );
			
			const turnRate:int = Math.min( ( _Ship.Engine.MaxTurnSpeed / 5 ) * 100, 100 );
			_TurnRate.setProgress( turnRate );
			_TurnRate.setValue( _Ship.Engine.MaxTurnSpeed );
			
			const tonnage:int = _GameData.ShipCompCatalog.getShipInfoTonnage( _Ship );
			const maxTonnage:int = _Ship.Chassis.MaxTonnage;
			const percentTonnage:Number = Math.min( tonnage / maxTonnage, 1 ) * 100;
			
			_Tonnage.setProgress( percentTonnage );
			_Tonnage.setValueStr( String( tonnage ) + "/" + String( maxTonnage) );
		}
	}
}