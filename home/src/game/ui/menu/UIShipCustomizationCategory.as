package game.ui.menu 
{
	import engine.debug.Debug;
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.actors.components.ArmorInfo;
	import game.actors.components.ChassisMap;
	import game.actors.components.CombatSystemInfo;
	import game.actors.components.EngineInfo;
	import game.actors.components.EvasionInfo;
	import game.actors.components.SensorInfo;
	import game.actors.components.ShieldInfo;
	import game.actors.components.ShipComponentInfo;
	import game.actors.components.ShipComponentTypes;
	import game.actors.SpaceShipInfo;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.misc.FontSystem;
	import game.ui.misc.UIGenericPopup;
	import game.ui.misc.UIGenericPopupSlim;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIShipCustomizationCategory extends UIMenuPanel
	{
		private var _Ship:SpaceShipInfo;
		private var _Type:String;
		
		private var _ActivePanel:UIShipHighlights;
		
		private var _MainText:TextField;
		private var _WhiteDivider:Bitmap;
		private var _LeftFade:Bitmap;
		
		private var _Options:Array;
		private var _Buttons:Vector.<UIMenuButton>;
		private var _BackButton:UIMenuButton;
		private var _EmptyButton:UIMenuButton;
		
		private var _ComponentInfo:UIComponentInfo;
		
		public var isHardpointSelector:Boolean = false;
		public var hardPoint:int = -1;
		
		// Buttons
		
		public function UIShipCustomizationCategory( ship:SpaceShipInfo, type:String ) 
		{
			_Ship = ship;
			_Type = type;
		}
		
		override public function initialize( gameData:HomeGameData, container:UIMenuContainer ):void
		{
			super.initialize( gameData, container );
			
			_Buttons = new Vector.<UIMenuButton>();
			
			setupDisplay();
			
			if ( isHardpointSelector )
			{
				buildHardpointSelector();
			}
			else
			{
				buildSystemSelector();
			}
			
			align();
		}
		
		private function setupDisplay():void
		{
			_WhiteDivider = _GameData.AssetManager.getBitmapFromClass( "UIMenuWhiteLineSWF" );
			
			this.addChild( _WhiteDivider );
			
			_ActivePanel = new UIShipHighlights( _GameData, _Ship );
			this.addChild( _ActivePanel );
			
			_MainText = new TextField();
			_MainText.embedFonts = true;
			_MainText.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 14, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.RIGHT );
			_MainText.selectable = false;
			_MainText.text = _Ship.Name + " " + _Type;
			_MainText.width = _WhiteDivider.width;
			_MainText.height = 20;
			
			if ( isHardpointSelector )
			{
				_MainText.text = _Ship.Name + " Hardpoints";
			}
			
			this.addChild( _MainText );
			
			_LeftFade = _GameData.AssetManager.getBitmapFromClass( "UIMenuVertShadowSWF" );
			
			this.addChild( _LeftFade );
		}
		
		private function buildHardpointSelector():void
		{
			var numHardpoints:int = _Ship.Chassis.HardPointLocations.length;
			
			var buttonClass:Class = _GameData.AssetManager.getClass( "UIMenuButtonSWF" );
			
			// Setup buttons
			for ( var i:int = 0; i < numHardpoints; i++ )
			{
				var name:String = "";
				
				var weaponKey:String = _Ship.CombatSystems[ i.toString() ];
				
				if ( !weaponKey )
				{
					name = ( i + 1 ) + ": EMPTY";
				}
				else
				{
					var weapon:CombatSystemInfo = _GameData.ShipCompCatalog.getCombatSystem( weaponKey );
					
					name = ( i + 1 ) + ": " + weapon.Name;
				}
				
				var button:UIMenuButton = new UIMenuButton( _GameData, name, new buttonClass(), onButtonClicked );
				
				_Buttons.push( button );
				
				this.addChild( button );
			}
			
			_BackButton = new UIMenuButton( _GameData, "Back", new buttonClass(), onBackClicked, "UI_Generic_Cancel" );
			
			this.addChild( _BackButton );
		}
		
		private function buildSystemSelector():void
		{
			var map:ChassisMap = _GameData.ShipCompCatalog.getChassisMap( _Ship.Chassis.Name.toLocaleLowerCase() );
			
			if ( !map )
			{
				Debug.errorLog( GameLogChannels.ERROR, "Unable to find map for chassis " + _Ship.Chassis.Key );
				return;
			}
			
			_Options = map.lookUp[ _Type ];
			
			var buttonClass:Class = _GameData.AssetManager.getClass( "UIMenuButtonSWF" );
			
			_EmptyButton = new UIMenuButton( _GameData, "EMPTY", new buttonClass(), onEmptyClicked );
		
			if ( _Type == "combat" || _Type == "shield" )
			{
				this.addChild( _EmptyButton );
				
				_Buttons.push( _EmptyButton );
			}
			
			for each ( var option:ShipComponentInfo in _Options )
			{
				// Check if this component is valid for this chassis
				/*
				if ( option.Restrictions != null && option.Restrictions.length > 0 && option.Restrictions.indexOf( _Ship.Chassis.Key ) == -1 )
				{
					continue;
				}
				*/
				
				var button:UIMenuButton = new UIMenuButton( _GameData, option.Name, new buttonClass(), onButtonClicked );
				button.registerRollOverCallback( onOverButton );
				
				this.addChild( button );
				
				_Buttons.push( button );
			}
			
			_BackButton = new UIMenuButton( _GameData, "Back", new buttonClass(), onBackClicked, "UI_Generic_Cancel" );
			
			this.addChild( _BackButton );
			
			_ComponentInfo = new UIComponentInfo();
			_ComponentInfo.setInfo( _Options[ 0 ] );
			
			this.addChild( _ComponentInfo );
		}
		
		private function onButtonClicked( button:UIMenuButton ):void
		{
			// find index
			var index:int = _Buttons.indexOf( button );
			
			if ( isHardpointSelector == false && ( _Type == "shield" || _Type == "combat" ) )
			{
				// Decrement index, because we have an "empty" at the head of the list
				index--;
			}
			
			if ( isHardpointSelector )
			{
				// Drill down
				var panel:UIShipCustomizationCategory = new UIShipCustomizationCategory( _Ship, ShipComponentTypes.COMBAT_SYSTEM );
				panel.hardPoint = index;
				
				_Container.transition( panel );
			}
			else
			{
				var newComponent:ShipComponentInfo = _Options[ index ];
				
				if ( isTonnageAllowed( newComponent ) )
				{
					setShipComponent( newComponent );
				
					onBackClicked( null );
				}
				else
				{
					var popup:UIGenericPopupSlim = new UIGenericPopupSlim( _GameData, "Over Limit", "Not enough space! Over tonnage limit!", "OK", null, null, null );
					
					_GameData.Scene.addPopup( popup );
				}
			}
		}
		
		private function onEmptyClicked( button:UIMenuButton ):void
		{
			clearShipComponent();
			
			onBackClicked( null );
		}
		
		private function onOverButton( button:UIMenuButton ):void
		{
			var index:int = _Buttons.indexOf( button );
			
			if ( isHardpointSelector == false && ( _Type == "shield" || _Type == "combat" ) )
			{
				// Decrement index, because we have an "empty" at the head of the list
				index--;
			}
			
			if ( index > _Options.length )
			{
				return;
			}
			
			var component:ShipComponentInfo = _Options[ index ];
			
			_ComponentInfo.setInfo( component );
			
			updateShipHighlights( component );
		}
		
		private function updateShipHighlights( component:ShipComponentInfo ):void
		{
			var infoCopy:SpaceShipInfo = _Ship.copy();
			
			switch ( _Type )
			{
				case ShipComponentTypes.ARMOR_COMPONENT:
					infoCopy.Armor = component as ArmorInfo;
					break;
				case ShipComponentTypes.ENGINE_COMPONENT:
					infoCopy.Engine = component as EngineInfo;
					break;
				case ShipComponentTypes.EVASION_COMPONENT:
					infoCopy.Evasion = component as EvasionInfo;
					break;
				case ShipComponentTypes.SENSOR_COMPONENT:
					infoCopy.Sensor = component as SensorInfo;
					break;
				case ShipComponentTypes.SHIELD_COMPONENT:
					infoCopy.Shield = component as ShieldInfo;
					break;
				case ShipComponentTypes.COMBAT_SYSTEM:
					infoCopy.CombatSystems[ hardPoint ] = component.Key;
					break;
			}
			
			this.removeChild( _ActivePanel );
			
			_ActivePanel = new UIShipHighlights( _GameData, infoCopy );
			
			this.addChild( _ActivePanel );
			
			align();
		}
		
		private function onBackClicked( button:UIMenuButton ):void
		{
			if ( hardPoint > - 1 )	// drilled down into a specific hardpoint on the chassis, return to hardpoint selector
			{
				var panel:UIShipCustomizationCategory = new UIShipCustomizationCategory( _Ship, ShipComponentTypes.COMBAT_SYSTEM );
				panel.isHardpointSelector = true;
				
				_Container.transition( panel );
			}
			else
			{
				_Container.transition( new UIShipCustomizationOptions( _Ship ) );
			}
		}
		
		private function isTonnageAllowed( potentialComponent:ShipComponentInfo ):Boolean
		{
			var oldComponent:ShipComponentInfo = null;
			
			switch ( _Type )
			{
				case ShipComponentTypes.ARMOR_COMPONENT:
					oldComponent = _Ship.Armor;
					break;
				case ShipComponentTypes.ENGINE_COMPONENT:
					oldComponent = _Ship.Engine;
					break;
				case ShipComponentTypes.EVASION_COMPONENT:
					oldComponent = _Ship.Evasion;
					break;
				case ShipComponentTypes.SENSOR_COMPONENT:
					oldComponent = _Ship.Sensor;
					break;
				case ShipComponentTypes.SHIELD_COMPONENT:
					oldComponent = _Ship.Shield;
					break;
				case ShipComponentTypes.COMBAT_SYSTEM:
					oldComponent = _GameData.ShipCompCatalog.getCombatSystem( _Ship.CombatSystems[ hardPoint ] );
					break;
			}
			
			var shipTonnageAsIs:int = _GameData.ShipCompCatalog.getShipInfoTonnage( _Ship );
			var shipTonnageWithComponent:int = shipTonnageAsIs;
			
			if ( oldComponent != null )
			{
				shipTonnageWithComponent -= oldComponent.Tonnage;
			}
			
			shipTonnageWithComponent += potentialComponent.Tonnage;
			
			return shipTonnageWithComponent <= _Ship.Chassis.MaxTonnage;
		}
		
		private function clearShipComponent() : void
		{
			switch ( _Type )
			{
				case ShipComponentTypes.ARMOR_COMPONENT:
					_Ship.Armor = null;
					break;
				case ShipComponentTypes.ENGINE_COMPONENT:
					_Ship.Engine = null;
					break;
				case ShipComponentTypes.EVASION_COMPONENT:
					_Ship.Evasion = null;
					break;
				case ShipComponentTypes.SENSOR_COMPONENT:
					_Ship.Sensor = null;
					break;
				case ShipComponentTypes.SHIELD_COMPONENT:
					_Ship.Shield = null;
					break;
				case ShipComponentTypes.COMBAT_SYSTEM:
					_Ship.CombatSystems[ hardPoint ] = null;
					break;
			}
			
			_GameData.ClientData.saveData();
		}
		
		private function setShipComponent( component:ShipComponentInfo ):void
		{
			switch ( _Type )
			{
				case ShipComponentTypes.ARMOR_COMPONENT:
					_Ship.Armor = component as ArmorInfo;
					break;
				case ShipComponentTypes.ENGINE_COMPONENT:
					_Ship.Engine = component as EngineInfo;
					break;
				case ShipComponentTypes.EVASION_COMPONENT:
					_Ship.Evasion = component as EvasionInfo;
					break;
				case ShipComponentTypes.SENSOR_COMPONENT:
					_Ship.Sensor = component as SensorInfo;
					break;
				case ShipComponentTypes.SHIELD_COMPONENT:
					_Ship.Shield = component as ShieldInfo;
					break;
				case ShipComponentTypes.COMBAT_SYSTEM:
					_Ship.CombatSystems[ hardPoint ] = component.Key;
					break;
			}
			
			// Store decision locally
			_GameData.ClientData.saveData();
		}
		
		override public function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;
			
			if ( _ActivePanel )
			{
				_ActivePanel.x = right - _ActivePanel.width - 40; // some buffer
				_ActivePanel.y = top;
			}
			
			_MainText.x = left + 10;
			_MainText.y = top + 77;
			
			_WhiteDivider.x = left + 10;
			_WhiteDivider.y = top + 99;
			
			_LeftFade.height = height;
			_LeftFade.x = left + 254;
			_LeftFade.y = top;
			
			var runningY:int = top + 101;
			
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.x = left + 10;
				button.y = runningY;
				
				runningY += 20;
			}
			
			_BackButton.y = runningY;
			_BackButton.x = left + 10;
			
			if ( _ComponentInfo )
			{
				_ComponentInfo.x = left;
				_ComponentInfo.y = _BackButton.y + 40;
			}
		}
		
		override public function tick( deltaTime:Number ):void
		{
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.tick( deltaTime );
			}
			
			if ( _BackButton )
			{
				_BackButton.tick( deltaTime );
			}
		}
		
		override public function destroy():void
		{
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.destroy();
			}
			
			_BackButton.destroy();
		}
	}
}