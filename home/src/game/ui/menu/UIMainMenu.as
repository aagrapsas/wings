package game.ui.menu 
{
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.EngineEvents;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Dictionary;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.misc.FontSystem;
	import game.scenarios.Scenario;
	import game.ui.misc.UIHelpScreen;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIMainMenu extends UIMenuPanel
	{		
		// Text
		private var _MainMenuText:TextField;
		private var _GameName:TextField;
		private var _InfoText:TextField;
		
		private var _ShadowsOfSiren:TextField;
		
		// Buttons
		private var _Buttons:Vector.<UIMenuButton>;
		private var _HangarButton:UIMenuButton;
		private var _HelpButton:UIMenuButton;
		
		private var _ButtonToLevel:Dictionary;
		private var _ButtonToDescription:Dictionary;
		
		// Visuals
		private var _WhiteDivider:Bitmap;
		private var _LeftFade:Bitmap;
		
		public function UIMainMenu() 
		{
			
		}
		
		override public function initialize( gameData:HomeGameData, container:UIMenuContainer ):void
		{
			super.initialize( gameData, container );
			
			// Allocation
			_Buttons = new Vector.<UIMenuButton>();
			_ButtonToLevel = new Dictionary();
			_ButtonToDescription = new Dictionary();

			gameData.ClientData.activeShip = null;
			
			// UI
			setupVisuals();
			setupButtons();
			setupMainMenuText();
			setupInfoText();
			setupSOSBanner();
			
			align();
		}
		
		private function setupSOSBanner():void
		{
			_ShadowsOfSiren = new TextField();
			_ShadowsOfSiren.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 26, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			_ShadowsOfSiren.selectable = false;
			_ShadowsOfSiren.embedFonts = true;
			_ShadowsOfSiren.antiAliasType = AntiAliasType.ADVANCED;
			_ShadowsOfSiren.width = 480;
			_ShadowsOfSiren.height = 40;
			
			var effect:GlowFilter = new GlowFilter();
			effect.blurX = 8;
			effect.blurY = 8;
			effect.strength = 0.98;
			effect.quality = BitmapFilterQuality.LOW;
			effect.color = 0xFFFFFF;
			
			_ShadowsOfSiren.filters = [ effect ];
			
			_ShadowsOfSiren.text = "Shadows of Siren";
			
			this.addChild( _ShadowsOfSiren );
		}
		
		private function setupVisuals():void
		{
			_WhiteDivider = _GameData.AssetManager.getBitmapFromClass( "UIMenuWhiteLineSWF" );
			
			this.addChild( _WhiteDivider );
			
			_LeftFade = _GameData.AssetManager.getBitmapFromClass( "UIMenuVertShadowSWF" );
			
			this.addChild( _LeftFade );
		}
		
		private function setupButtons():void
		{
			var configXML:XML = _GameData.AssetManager.getXML( "game_config" );
			
			var buttonClass:Class = _GameData.AssetManager.getClass( "UIMenuButtonSWF" );
			
			for each ( var levelXML:XML in configXML.levels.level )
			{
				var key:String = levelXML.@key;
				var name:String = levelXML.@name;
				var description:String = levelXML.@description;
				
				var button:UIMenuButton = new UIMenuButton( _GameData, name, new buttonClass() as MovieClip, onButtonClicked );
				
				_ButtonToLevel[ name ] = key;
				_ButtonToDescription[ name ] = description;
				
				button.registerRollOverCallback( onOverButton );
				
				_Buttons.push( button );
				
				this.addChild( button );
			}
			
			_HangarButton = new UIMenuButton( _GameData, "Hangar", new buttonClass() as MovieClip, onHangarClicked );
			_HangarButton.registerRollOverCallback( onOverHangar );
			
			_Buttons.push( _HangarButton );
			this.addChild( _HangarButton );
			
			_HelpButton = new UIMenuButton( _GameData, "How to Play", new buttonClass() as MovieClip, onHelpClicked );
			_HelpButton.registerRollOverCallback( onOverHelp );
			
			_Buttons.push( _HelpButton );
			this.addChild( _HelpButton );
		}
		
		private function onButtonClicked( button:UIMenuButton ):void
		{
			if ( _GameData.Scene.ActivePopup != null )
			{
				return;
			}
			
			var level:String = _ButtonToLevel[ button.buttonName ];
			
			_Container.transition( new UIMenuSelectShipToPlay( level ) );
		}
		
		private function onHelpClicked( button:UIMenuButton ):void
		{
			if ( _GameData.Scene.ActivePopup != null )
			{
				return;
			}
			
			var help:UIHelpScreen = new UIHelpScreen( _GameData, true, onHelpCleared );
			
			_GameData.Scene.addPopup( help );
			
			_ShadowsOfSiren.visible = false;
			_InfoText.visible = false;
			
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.setEnabled( false );
			}
		}
		
		private function onHelpCleared():void
		{
			_ShadowsOfSiren.visible = true;
			_InfoText.visible = true;
			
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.setEnabled( true );
			}
		}
		
		private function onHangarClicked( button:UIMenuButton ):void
		{
			if ( _GameData.Scene.ActivePopup != null )
			{
				return;
			}
			
			_Container.transition( new UIHangar() );
		}
		
		private function onOverHangar( button:UIMenuButton ):void
		{
			_InfoText.text = _GameData.GameConfigData[ "hangar_text" ];
		}
		
		private function onOverHelp( button:UIMenuButton ):void
		{
			_InfoText.text = "Learn how to play Shadows of Siren";
		}
		
		private function onOverButton( button:UIMenuButton ):void
		{
			var description:String = _ButtonToDescription[ button.buttonName ];
			
			if ( !description )
			{
				return;
			}
			
			_InfoText.text = description;
		}
		
		private function setupMainMenuText():void
		{
			_MainMenuText = new TextField();
			_MainMenuText.embedFonts = true;
			
			var textFormat:TextFormat = new TextFormat( FontSystem.PIRULEN, 14, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.RIGHT );
			
			_MainMenuText.defaultTextFormat = textFormat;
			_MainMenuText.selectable = false;
			_MainMenuText.text = "Main Menu";
			_MainMenuText.width = 154;
			_MainMenuText.height = 20;
			
			this.addChild( _MainMenuText );
		}
		
		private function setupInfoText():void
		{
			_InfoText = new TextField();
			_InfoText.antiAliasType = AntiAliasType.ADVANCED;
			_InfoText.embedFonts = true;
			
			var textFormat:TextFormat = new TextFormat( FontSystem.MYRIAD_PRO, 14, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			
			_InfoText.defaultTextFormat = textFormat;
			_InfoText.selectable = false;
			_InfoText.multiline = true;
			_InfoText.wordWrap = true;
			_InfoText.text = _GameData.GameConfigData[ "menu_start_text" ];
			_InfoText.width = 214;
			_InfoText.height = 150;
			
			this.addChild( _InfoText );
		}
		
		override public function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;

			_MainMenuText.x = left + 94;
			_MainMenuText.y = top + 77;
			
			_InfoText.x = left + 30;
			_InfoText.y = top + 260;
			
			_WhiteDivider.x = left + 10;
			_WhiteDivider.y = top + 99;
			
			_LeftFade.height = height;
			_LeftFade.x = left + 254;
			_LeftFade.y = top;
			
			var runningY:int = top + 101;
			
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.x = left + 10;
				button.y = runningY;
				
				runningY += 20;
			}
			
			_ShadowsOfSiren.x = _LeftFade.x + _LeftFade.width + ( ( width - _LeftFade.x - _LeftFade.width ) / 2 ) - ( _ShadowsOfSiren.width / 2 );
			_ShadowsOfSiren.y = ( height / 2 ) - ( _ShadowsOfSiren.height / 2 );
		}
		
		override public function tick( deltaTime:Number ):void
		{
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.tick( deltaTime );
			}
		}
		
		override public function destroy():void
		{
			for each ( var button:UIMenuButton in _Buttons )
			{
				button.destroy();
			}
		}
	}
}