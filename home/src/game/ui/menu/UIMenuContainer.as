package game.ui.menu 
{
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.EngineEvents;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIMenuContainer extends Sprite implements ITickable, IDestroyable
	{
		private var _GameData:HomeGameData;
		
		private var _ActivePanel:UIMenuPanel;
		
		// Main panel characteristics
		private var _Asteroids:Vector.<Bitmap>;
		private var _AsteroidSpeeds:Vector.<Number>;
		
		private var _Debris:Vector.<Bitmap>;
		private var _DebrisSpeeds:Vector.<Number>;
		private var _DebrisLaunchAccumulator:Number = 0;
		private var _NextDebrisLaunch:Number = 0;
		
		private const ASTEROID_SPEED:Number = 0.10;
		
		private const MIN_DEBRIS_LAUNCH_TIME:Number = 2.0;
		private const MAX_DEBRIS_LAUNCH_TIME:Number = 15.0;
		
		private const MIN_DEBRIS_SPEED:Number = 0.20;
		private const MAX_DEBRIS_SPEED:Number = 0.60;
		
		// Layers
		private var _ForegroundLayer:Sprite;
		private var _BackgroundLayer:Sprite;
		
		// Bitmaps
		private var _Planet:Bitmap;
		
		private var _TopFade:Bitmap;
		
		private var _Background:Bitmap;
		
		// Cached data
		private var _BackgroundDimensions:Point;
		private var _PlanetDimensions:Point;
		
		// Preview text
		private var _PreviewText:TextField;
		
		// Left fill
		private var _LeftFill:Sprite;
		
		private var _Music:String;
		
		public function UIMenuContainer( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_ForegroundLayer = new Sprite();
			_BackgroundLayer = new Sprite();
			
			_Debris = new Vector.<Bitmap>();
			_DebrisSpeeds = new Vector.<Number>();
			
			_Asteroids = new Vector.<Bitmap>();
			_AsteroidSpeeds = new Vector.<Number>();
			
			this.addChild( _BackgroundLayer );
			this.addChild( _ForegroundLayer );
			
			// Audio
			_Music = _GameData.GameConfigData[ "menu_music" ];
			
			if ( _Music )
			{
				_GameData.SoundSystem.playSound( _Music );
			}
			
			// Setup events
			_GameData.GameEvents.register( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			// Add to scene
			_GameData.Scene.UILayer.addChild( this );
			
			setupBackground();
			
			setupPreviewText();
			
			loadMainMenu();
			
			align();
		}
		
		private function setupBackground():void
		{
			// Background image
			_Background = _GameData.AssetManager.getBitmapFromClass( "UIMenuBGSWF" );
			
			_BackgroundLayer.addChild( _Background );
			
			_BackgroundDimensions = new Point();
			_BackgroundDimensions.x = _Background.width;
			_BackgroundDimensions.y = _Background.height;
			
			// Asteroid belt ( for background )
			_Asteroids.push( _GameData.AssetManager.getBitmapFromClass( "UIAsteroidBeltSWF" ) );
			_Asteroids.push( _GameData.AssetManager.getBitmapFromClass( "UIAsteroidBeltSWF" ) );
			_Asteroids.push( _GameData.AssetManager.getBitmapFromClass( "UIAsteroidBeltSWF" ) );
			// _Asteroids.push( _GameData.AssetManager.getBitmapFromClass( "UIAsteroidBeltSWF" ) );
			
			for each ( var asteroid:Bitmap in _Asteroids )
			{
				_BackgroundLayer.addChild( asteroid );
				
				asteroid.scaleX = 0.99;
				asteroid.scaleY = 0.99;
			}
			
			const right:int = _GameData.Scene.GameStage.stageWidth;
			
			_Asteroids[0].x = right - _Asteroids[0].width;
			_Asteroids[1].x = _Asteroids[0].x + _Asteroids[1].width - 100;
			_Asteroids[2].x = _Asteroids[1].x + _Asteroids[2].width - 100;
			
			// Planet
			_Planet = _GameData.AssetManager.getBitmapFromClass( "UIMenuPlanetSWF" );
			
			_BackgroundLayer.addChild( _Planet );
			
			_TopFade = _GameData.AssetManager.getBitmapFromClass( "UIMenuTopLeftFadeSWF" );
			
			_BackgroundLayer.addChild( _TopFade );
			
			// Debris ( for foreground )
			var debris1:Bitmap = _GameData.AssetManager.getBitmapFromClass( "UIDebris1SWF" );
			var debris2:Bitmap = _GameData.AssetManager.getBitmapFromClass( "UIDebris2SWF" );
			var debris3:Bitmap = _GameData.AssetManager.getBitmapFromClass( "UIDebris3SWF" );
			var debris4:Bitmap = _GameData.AssetManager.getBitmapFromClass( "UIDebris4SWF" );
			
			_BackgroundLayer.addChild( debris1 );
			_BackgroundLayer.addChild( debris2 );
			_BackgroundLayer.addChild( debris3 );
			_BackgroundLayer.addChild( debris4 );
			
			debris1.visible = false;
			debris2.visible = false;
			debris3.visible = false;
			debris4.visible = false;
			
			_Debris.push( debris1 );
			_Debris.push( debris2 );
			_Debris.push( debris3 );
			_Debris.push( debris4 );
			
			for each ( var debris:Bitmap in _Debris )
			{
				_DebrisSpeeds.push( MIN_DEBRIS_SPEED );
				debris.scaleX = 0.99;
				debris.scaleY = 0.99;
			}
			
			_LeftFill = new Sprite();
			_LeftFill.graphics.beginFill( 0x000000, 0.50 );
			_LeftFill.graphics.drawRect( 0, 0, 254, 600 );
			_LeftFill.graphics.endFill();
			
			_BackgroundLayer.addChild( _LeftFill );
		}
		
		private function setupPreviewText():void
		{
			_PreviewText = new TextField();
			_PreviewText.embedFonts = true;
			_PreviewText.antiAliasType = AntiAliasType.ADVANCED;
			_PreviewText.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 16, 0xFFFFFF );
			_PreviewText.selectable = false;
			_PreviewText.text = "Preview Build";
			
			_PreviewText.width = _PreviewText.textWidth + 20;
			_PreviewText.height = 20;
			
			_BackgroundLayer.addChild( _PreviewText );
		}
		
		private function loadMainMenu():void
		{
			transition( new UIMainMenu() );
		}
		
		private function onWindowResize( data:Object ):void
		{
			align();
		}
		
		private function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;

			var diffWidth:int = 0;
			
			if ( width > _BackgroundDimensions.x )
			{
				diffWidth = width - _BackgroundDimensions.x;
			}
			
			_Background.x = left;
			_Background.width = width;
			_Background.y = top;
			_Background.height = height + diffWidth;
			
			_Planet.x = right - _Planet.width;
			_Planet.y = left;
			
			_TopFade.x = left;
			_TopFade.y = top;
			
			if ( _ActivePanel )
			{
				_ActivePanel.align();
			}
			
			_PreviewText.x = ( width / 2 ) - ( _PreviewText.width / 2 );
			_PreviewText.y = bottom - _PreviewText.height;
			
			_LeftFill.height = height;
			
			_LeftFill.x = left;
			_LeftFill.y = top;
		}
		
		public function transition( panel:UIMenuPanel ):void
		{
			if ( _ActivePanel )
			{
				_ForegroundLayer.removeChild( _ActivePanel );
				
				_ActivePanel.destroy();
			}
			
			panel.initialize( _GameData, this );
			
			_ActivePanel = panel;
			
			_ForegroundLayer.addChild( panel );
			
			panel.show();
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _ActivePanel )
			{
				_ActivePanel.tick( deltaTime );
			}
			
			for ( var i:int = 0; i < _Debris.length; i++ )
			{
				var debris:Bitmap = _Debris[ i ];
				
				if ( debris.visible )
				{
					debris.x -= _DebrisSpeeds[ i ];
				}
			}
			
			collectDebris();
			
			_DebrisLaunchAccumulator += deltaTime;
			
			if ( _DebrisLaunchAccumulator >= _NextDebrisLaunch )
			{
				_DebrisLaunchAccumulator = 0;
				
				launchDebris();
			}
			
			updateAsteroids();
		}
		
		private function launchDebris():void
		{
			_NextDebrisLaunch = MIN_DEBRIS_LAUNCH_TIME + Math.random() * ( MAX_DEBRIS_LAUNCH_TIME - MIN_DEBRIS_LAUNCH_TIME );
			
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;
			
			var free:Vector.<Bitmap> = new Vector.<Bitmap>();
			
			for each ( var debris:Bitmap in _Debris )
			{
				if ( debris.visible == false )
				{
					free.push( debris );
				}
			}
			
			if ( free.length == 0 )
			{
				return;
			}
			
			var ranIndex:int = Math.random() * free.length;
			
			var freeDebris:Bitmap = free[ ranIndex ];
			
			var actualIndex:int = _Debris.indexOf( freeDebris );
			
			_DebrisSpeeds[ actualIndex ] = MIN_DEBRIS_SPEED + ( Math.random() * ( MAX_DEBRIS_SPEED - MIN_DEBRIS_SPEED ) );
			
			// Debug.debugLog( GameLogChannels.DEBUG, "DEBRIS SPEED: " + _DebrisSpeeds[ ranIndex ] );
			
			freeDebris.visible = true;
			
			freeDebris.x = right;
			
			var randomPosition:int = Math.random() * ( bottom - freeDebris.height );
			
			freeDebris.y = randomPosition;
		}
		
		private function collectDebris():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;
			
			for each ( var debris:Bitmap in _Debris )
			{
				if ( debris.visible )
				{
					if ( ( debris.x + debris.width ) < left )
					{
						debris.visible = false;
					}
				}
			}
		}
		
		private function updateAsteroids():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;
			
			for each ( var asteroid:Bitmap in _Asteroids )
			{
				asteroid.x -= ASTEROID_SPEED;
				
				if ( ( asteroid.x + asteroid.width ) < left )
				{
					asteroid.x = right;
				}
			}
		}
		
		public function destroy():void
		{
			if ( _ActivePanel )
			{
				_ActivePanel.destroy();
			}
			
			if ( _Music )
			{
				_GameData.SoundSystem.stopSound( _Music );
			}
			
			_GameData.GameEvents.unregister( EngineEvents.WINDOW_RESIZE, onWindowResize );
			
			_GameData.Scene.UILayer.removeChild( this );
			
			_GameData.Scene.GameStage.focus = null;
		}
		
		public function get data():HomeGameData { return _GameData; }
	}
}
