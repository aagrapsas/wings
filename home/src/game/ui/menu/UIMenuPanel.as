package game.ui.menu 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Sprite;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIMenuPanel extends Sprite implements ITickable, IDestroyable
	{
		protected var _GameData:HomeGameData;
		protected var _Container:UIMenuContainer;
		
		public function UIMenuPanel() 
		{
			
		}
		
		public function initialize( gameData:HomeGameData, container:UIMenuContainer ):void
		{
			_GameData = gameData;
			_Container = container;
		}
		
		public function align():void
		{
			
		}
		
		public function show():void
		{
			
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}
}