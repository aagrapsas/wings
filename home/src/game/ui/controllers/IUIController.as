package game.ui.controllers 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IUIController extends IDestroyable, ITickable
	{
		function click():void;
		function clear():void;
		function get nodeHandling():SceneNode;
		function get isDisposing():Boolean;
		function get isDisposable():Boolean;
	}
}