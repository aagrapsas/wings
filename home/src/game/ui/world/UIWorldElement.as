package game.ui.world 
{
	import engine.render.scene.SceneNode;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// UI element that's in the world (should be added to a world UI layer)
	public class UIWorldElement 
	{
		protected var _Container:Sprite;
		protected var _Collision:SceneNode;
		
		public function UIWorldElement()
		{
			
		}
		
		public function get x():Number { return _Container.x; }
		public function set x( value:Number ):void { _Container.x = value; }
		
		public function get y():Number { return -_Container.y; }
		public function set y( value:Number ):void { _Container.y = -value; }
		
		public function get container():Sprite { return _Container; }
		public function get collision():SceneNode { return _Collision; }
	}
}