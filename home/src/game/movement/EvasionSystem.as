package game.movement 
{
	import engine.debug.Debug;
	import engine.input.InputCodes;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import starling.events.KeyboardEvent;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EvasionSystem implements IDestroyable, ITickable
	{
		private var _Ship:SpaceShip;
		private var _GameData:HomeGameData;
		private var _MovementSystem:SpaceMomentumMovement;
		
		// Cooldown
		private var _CooldownStart:Number = 0;
		private var _CooldownDuration:Number = 1;
		
		// Keys
		private var _BurstLeftKey:uint;
		private var _BurstRightKey:uint;
		private var _StopKey:uint;
		private var _BurstForwardKey:uint;
		private var _BurstForwardSecondKey:uint;
		
		// Velocity
		private var _VelocityMultiplier:Number = 9;
		private var _VelocityMultiplied:Number = 300;
		private var _StopDampener:Number = 0.90;
		
		private var _BurstVelX:Number = 0;
		private var _BurstVelY:Number = 0;
		
		private var _Deceleration:Number = 200;
		
		private var _EvadeAudio:String;
		
		public function EvasionSystem( gameData:HomeGameData, ship:SpaceShip, movementSystem:SpaceMomentumMovement ) 
		{
			_Ship = ship;
			_GameData = gameData;
			_MovementSystem = movementSystem;
			
			if ( ship.Info.Evasion != null )
			{
				initialize();
			}
		}
		
		private function initialize():void
		{
			_GameData.Scene.GameStage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			
			_BurstForwardKey = _GameData.InputMap[ "evade_burst" ];
			_BurstForwardSecondKey = _GameData.InputMap[ "evade_burst_secondary" ];
			_BurstLeftKey = _GameData.InputMap[ "evade_left" ];
			_BurstRightKey = _GameData.InputMap[ "evade_right" ];
			_StopKey = _GameData.InputMap[ "evade_stop" ];
			
			_EvadeAudio = _Ship.Info.Engine.Audio[ "evasion" ];
			
			// Setup variables
			_VelocityMultiplier = _Ship.Info.Evasion.velocityMultiplier;
			_StopDampener = _Ship.Info.Evasion.stopDampener;
			_Deceleration = _Ship.Info.Evasion.deceleration;
			_CooldownDuration = _Ship.Info.Evasion.cooldown;
			_VelocityMultiplied = _Ship.Info.Engine.MaxSpeed * _Ship.Info.Evasion.velocityMultiplier;
		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{			
			if ( _GameData.IsInputEnabled == false )
			{
				return;
			}
			
			var timestamp:Number = _GameData.Scene.GameTime;	// Seconds
			
			// Do nothing while cooling down
			if ( _CooldownStart > 0 && ( timestamp - _CooldownStart ) < _CooldownDuration )
			{
				return;
			}
			
			var didBurst:Boolean = false;
			
			var perpX:Number = 0;
			var perpY:Number = 0;
			
			if ( e.keyCode == _BurstForwardKey || e.keyCode == _BurstForwardSecondKey )
			{
				addPulse( _Ship.Dir.x * _VelocityMultiplied, _Ship.Dir.y * _VelocityMultiplied );
				
				didBurst = true;
			}
			else if ( e.keyCode == _BurstLeftKey )
			{				
				if ( _Ship.Dir.y > 0 )
				{
					perpX = -_Ship.Dir.y;   // 270 degree perp clockwise
					perpY = _Ship.Dir.x;
				}
				else
				{
					perpX = _Ship.Dir.y;    // 90 degree perp clockwise
					perpY = -_Ship.Dir.x;
				}
				
				addPulse( perpX * _VelocityMultiplied, perpY * _VelocityMultiplied );
				
				didBurst = true;
			}
			else if ( e.keyCode == _BurstRightKey )
			{
				if ( _Ship.Dir.y > 0 )
				{
					perpX = _Ship.Dir.y;    // 90 degree perp clockwise
					perpY = -_Ship.Dir.x;
				}
				else
				{
					perpX = -_Ship.Dir.y;   // 270 degree perp clockwise
					perpY = _Ship.Dir.x;
				}
                                        
				addPulse( perpX * _VelocityMultiplied, perpY * _VelocityMultiplied );
				
				didBurst = true;
			}
			else if ( e.keyCode == _StopKey )
			{
				// addPulse( -( _Ship.VelocityX + _BurstVelX ) * _StopDampener, -( _Ship.VelocityY + _BurstVelY ) * _StopDampener );
				_Ship.VelocityX *= _StopDampener;
				_Ship.VelocityY *= _StopDampener;
				_BurstVelX *= _StopDampener;
				_BurstVelY *= _StopDampener;
				
				didBurst = true;
			}
			
			if ( didBurst )
			{
				// Start cooldown
				_CooldownStart = timestamp;
				
				if ( _EvadeAudio )
				{
					_GameData.SoundSystem.playSound( _EvadeAudio );
				}
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			var velocityNormal:Point = new Point( _BurstVelX, _BurstVelY );
			velocityNormal.normalize( 1 );
			
			// Decay velocity
			if ( velocityNormal.x > 0 )
			{
				_BurstVelX = Math.max( 0, _BurstVelX - ( deltaTime * _Deceleration * velocityNormal.x ) );
			}
			else if ( velocityNormal.x < 0 )
			{
				_BurstVelX = Math.min( 0, _BurstVelX + ( deltaTime * _Deceleration * -velocityNormal.x ) );
			}
			
			if ( velocityNormal.y > 0 )
			{
				_BurstVelY = Math.max( 0, _BurstVelY - ( deltaTime * _Deceleration * velocityNormal.y ) );
			}
			else if ( velocityNormal.y < 0 )
			{
				_BurstVelY = Math.min( 0, _BurstVelY + ( deltaTime * _Deceleration * -velocityNormal.y ) );
			}
		}
		
		public function addPulse( velX:Number, velY:Number ):void
		{
			// Debug.debugLog( GameLogChannels.ACTOR, "Adding impulse of x: " + velX + " y: " + velY );
			
			_BurstVelX += velX;
			_BurstVelY += velY;
		}
		
		public function destroy():void
		{
			_GameData.Scene.GameStage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
		}
		
		public function get velocityX():Number { return _BurstVelX; }
		public function get velocityY():Number { return _BurstVelY; }
	}
}