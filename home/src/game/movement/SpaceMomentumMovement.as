package game.movement 
{
	import engine.debug.Debug;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SpaceMomentumMovement implements ITickable
	{
		private var _GameData:IGameData;
		private var _Ship:SpaceShip;
		private var _MoveDir:Point;
		
		private var _RotAccel:Number = 0;
		
		private var _Acceleration:Number = 0;
		
		private var _IsRotating:Boolean = false;
		private var _IsMoving:Boolean = false;
		
		public function SpaceMomentumMovement( ship:SpaceShip, gameData:IGameData ) 
		{
			_Ship = ship;
			_GameData = gameData;
			
			_MoveDir = new Point;
		}
		
		public function tick( deltaTime:Number ):void
		{
			var velocityNormal:Point = new Point( _Ship.VelocityX, _Ship.VelocityY );
			velocityNormal.normalize( 1 );
			
			// Decelerate
			if ( !_IsMoving )
			{
				if ( _Ship.VelocityX > 0 )
				{
					// Make velocity components decay proportionately to avoid sliding using velocity normal. NB ship direction might be a better choice here
					_Ship.VelocityX = Math.max( 0.0, _Ship.VelocityX - _Ship.Info.Engine.Deceleration * deltaTime * ( velocityNormal.x ) );
				}
				else if ( _Ship.VelocityX < 0 )
				{
					_Ship.VelocityX = Math.min( 0.0, _Ship.VelocityX + _Ship.Info.Engine.Deceleration * deltaTime * ( -velocityNormal.x ) );
				}
				
				if ( _Ship.VelocityY > 0 )
				{
					_Ship.VelocityY = Math.max( 0.0, _Ship.VelocityY - _Ship.Info.Engine.Deceleration * deltaTime  * ( velocityNormal.y ) );
				}
				else if ( _Ship.VelocityY < 0 )
				{
					_Ship.VelocityY = Math.min( 0.0, _Ship.VelocityY + _Ship.Info.Engine.Deceleration * deltaTime * ( -velocityNormal.y ) );
				}
				
				_Acceleration = Math.max( 0.0, _Acceleration - _Ship.Info.Engine.AccelerationStep * deltaTime );
			}
			
			if ( !_IsRotating )
			{
				if ( _RotAccel != 0 )
				{
					if ( _RotAccel > 0 )
					{
						_RotAccel = Math.max( 0.0, _RotAccel - _Ship.Info.Engine.TurnAcceleration * deltaTime );
					}
					else
					{
						_RotAccel = Math.min( 0.0, _RotAccel + _Ship.Info.Engine.TurnAcceleration * deltaTime );
					}
					
					if ( _RotAccel != 0 )
						_Ship.rotate( _RotAccel );
				}
			}
			
			/*
			if ( _Acceleration > 0 )
				trace( "Acceleration: " + _Acceleration );
				
			if ( _Ship.VelocityX != 0 || _Ship.VelocityY != 0 )
			{
				trace( "Velocity: (x: " + _Ship.VelocityX + ", y: " + _Ship.VelocityY + ")" );
			}
			*/
			
			_IsRotating = false;
			_IsMoving = false;
		}
		
		public function move( x:Number, y:Number, deltaTime:Number ):void
		{
			_IsMoving = true;
			
			_Acceleration = Math.min( _Ship.Info.Engine.MaxAcceleration, _Acceleration + _Ship.Info.Engine.AccelerationStep * deltaTime );
			
			_Ship.VelocityX += x * _Acceleration * deltaTime;
			_Ship.VelocityY += y * _Acceleration * deltaTime;
			
			//trace( "x: " + x + " y: " + y + " VelocityX: " + _Ship.VelocityX + " Y: " + _Ship.VelocityY );
			
			// Clamp
			const currentMagSquared:Number = _Ship.VelocityX * _Ship.VelocityX + _Ship.VelocityY * _Ship.VelocityY;
			const maxMagSquared:Number = _Ship.Info.Engine.MaxSpeed * _Ship.Info.Engine.MaxSpeed;
			
			if ( currentMagSquared > maxMagSquared )
			{
				var velocityNormal:Point = new Point( _Ship.VelocityX, _Ship.VelocityY );
				velocityNormal.normalize( 1 );
				
				_Ship.VelocityX = velocityNormal.x * _Ship.Info.Engine.MaxSpeed;
				_Ship.VelocityY = velocityNormal.y * _Ship.Info.Engine.MaxSpeed;
			}
		}
		
		public function addPulse( pulseX:Number, pulseY:Number ):void
		{
			// Debug.debugLog( GameLogChannels.DEBUG, "Adding pulse x: " + pulseX + " y: " + pulseY );
			
			_Ship.VelocityX += pulseX;
			_Ship.VelocityY += pulseY;
		}
		
		public function rotate( isLeft:Boolean, deltaTime:Number ):void
		{
			_IsRotating = true;
			
			_RotAccel += isLeft ? -_Ship.Info.Engine.TurnAcceleration * deltaTime : _Ship.Info.Engine.TurnAcceleration * deltaTime;
			
			if ( Math.abs( _RotAccel ) > _Ship.Info.Engine.MaxTurnSpeed )
			{
				_RotAccel = _RotAccel > 0 ? _Ship.Info.Engine.MaxTurnSpeed : -_Ship.Info.Engine.MaxTurnSpeed;
			}
			
			_Ship.rotate( _RotAccel );
		}
	}
}