package game.misc 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class FontSystem 
	{		
		[ Embed ( source = "../../../lib/fonts/PIRULEN.TTF", fontName = "Pirulen Font", mimeType = "application/x-font", fontWeight = "normal", unicodeRange="U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", advancedAntiAliasing="true", embedAsCFF="false" ) ]
		public static const PIRULEN_FONT:String;
		
		[ Embed ( source = "../../../lib/fonts/AVANTGARDEITCBYBT-MEDIUM.OTF", fontName = "Avant Garde Font", mimeType = "application/x-font", fontWeight = "normal", unicodeRange="U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", advancedAntiAliasing="true", embedAsCFF="false" ) ]
		public static const AVANT_GARDE_FONT:String;
		
		[ Embed ( source = "../../../lib/fonts/MYRIADPRO-REGULAR.OTF", fontName = "Myriad Pro Font", mimeType = "application/x-font", fontWeight = "normal", unicodeRange="U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", advancedAntiAliasing="true", embedAsCFF="false" ) ]
		public static const MYRIAD_PRO_FONT:String;
		
		public static const AVANT_GARDE:String = "Avant Garde Font";
		public static const PIRULEN:String = "Pirulen Font";
		public static const MYRIAD_PRO:String = "Myriad Pro Font";
	}
}