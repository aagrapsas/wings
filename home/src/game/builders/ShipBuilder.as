package game.builders 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.interfaces.IGameData;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import game.actors.components.CombatSystemInfo;
	import game.actors.HealthSystem;
	import game.actors.SpaceShip;
	import game.actors.SpaceShipInfo;
	import game.debug.GameLogChannels;
	import game.fx.ThrusterFXPulse;
	import game.gameplay.CombatSystem;
	import game.gameplay.weapons.WeaponFactory;
	import game.HomeGameData;
	import game.misc.HomePoolConfig;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipBuilder implements IShipBuilder
	{
		private var _Ship:SpaceShip;
		
		// @TODO: add a pool for AI
		
		public function ShipBuilder() 
		{
			
		}
		
		public function setupShipInfo( shipInfo:SpaceShipInfo ):void
		{
			_Ship = new SpaceShip;
			_Ship.Info = shipInfo;
			
			_Ship.ArmorHealth = new HealthSystem( shipInfo.Armor.FullHitPoints, shipInfo.Armor.ArmorRegenPerSecond, shipInfo.Armor.ArmorRegenDelay, 0 );
			
			if ( _Ship.Info.Shield )
			{
				_Ship.ShieldHealth = new HealthSystem( shipInfo.Shield.MaxShields, shipInfo.Shield.ShieldRegenPerSecond, shipInfo.Shield.ShieldRegenDelay, shipInfo.Shield.ShieldRegenZeroDelay );
			}
		}
		
		public function registerShip( gameData:IGameData ):void
		{
			// Draw proper bitmap
			var memoryBitmap:Bitmap = gameData.AssetManager.getImage( _Ship.Info.Chassis.ShipWorldAsset ) as Bitmap;
			var shipData:BitmapData = memoryBitmap.bitmapData.clone();
			_Ship.setDrawingData( shipData );
			
			// Create scene node
			_Ship.Node = new SceneNode( new RectCollider( 0, 0, _Ship.Display.width, _Ship.Display.height, CollisionFlags.ACTORS ) );
			
			// Register actor with scene
			gameData.Scene.addActor( _Ship );
			
			// Register game data
			_Ship.Data = gameData;
			
			// Build ship world info
			
			if ( (gameData as HomeGameData).GameConfigData[ "show_names" ] == "true" )
			{
				//var info:UIShipInfo = gameData.Pools.getPool( HomePoolConfig.WORLD_SHIP_INFO ).pop() as UIShipInfo;
				//info.setup( _Ship );
			}
		}
		
		public function buildWeapons( gameData:HomeGameData ):void
		{
			var weapons:Dictionary = _Ship.Info.CombatSystems;
			
			if ( weapons )
			{
				for ( var hardpoint:String in weapons )
				{
					const info:CombatSystemInfo = gameData.ShipCompCatalog.getCombatSystem( weapons[ hardpoint ] );
					
					if ( !info )
					{
						Debug.errorLog( GameLogChannels.DATA, "Could not build ship! Invalid weapon info for ship " + _Ship.Info.Key + " and weapon " + weapons[ hardpoint ] );
						return;
					}
					
					const weap:CombatSystem = WeaponFactory.getWeapon( _Ship, info.SystemType, gameData, info, int( hardpoint ), false );
					
					if ( !weap )
					{
						Debug.errorLog( GameLogChannels.DATA, "Could not build ship! Weapon type " + info.SystemType + " doesn't exist" );
						return;
					}
					
					_Ship.CombatSystems.push( weap );
					
					// Setup drawing
					
				}
			}
		}
		
		public function setupLocation( x:int, y:int ):void
		{
			_Ship.X = x;
			_Ship.Y = y;
		}
		
		public function setupFX():void
		{						
			//var thruster:ThrusterFX = new ThrusterFX( _Ship.Info.Chassis.ThrusterLocations );
			var thruster:ThrusterFXPulse = new ThrusterFXPulse( _Ship, _Ship.Data as HomeGameData, _Ship.Info.Chassis.ExhaustAsset, _Ship.Info.Chassis.ThrusterLocations );
			_Ship.Thruster = thruster;
		}
		
		public function initializeInternal():void
		{
			_Ship.initialize();
		}
		
		public function get Ship():SpaceShip { return _Ship; }
	}
}