package game.builders 
{
	import engine.debug.Debug;
	import flash.utils.Dictionary;
	import game.actors.SpaceShip;
	import game.actors.SpaceShipInfo;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipSpawner 
	{
		private var _Builder:IShipBuilder;
		private var _WeaponInfos:Dictionary;
		private var _GameData:HomeGameData;
		
		public function ShipSpawner( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			_WeaponInfos = new Dictionary;

			initialize();
		}
		
		private function initialize():void
		{
			
		}
		
		public function getShipFromInfo( info:SpaceShipInfo ):SpaceShip
		{
			_Builder.setupShipInfo( info );
			_Builder.registerShip( _GameData );
			_Builder.buildWeapons( _GameData );
			_Builder.setupLocation( 0, 0 );
			_Builder.setupFX();
			_Builder.initializeInternal();
			
			return _Builder.Ship;
		}
		
		public function getShip( type:String ):SpaceShip
		{
			const shipInfo:SpaceShipInfo = _GameData.ShipCompCatalog.getShipTemplate( type );
			
			if ( !shipInfo )
			{
				Debug.errorLog( GameLogChannels.DATA, "Could not build ship of type " + type + " it doesn't exist!" );
				return null;
			}
			
			var copy:SpaceShipInfo = shipInfo.copy();
			
			_Builder.setupShipInfo( shipInfo );
			_Builder.registerShip( _GameData );
			_Builder.buildWeapons( _GameData );
			_Builder.setupLocation( 0, 0 );
			_Builder.setupFX();
			_Builder.initializeInternal();
			
			return _Builder.Ship;
		}
		
		// Accessors
		public function get Builder():IShipBuilder { return _Builder; }
		
		// Mutators
		public function set Builder( value:IShipBuilder ):void { _Builder = value; }
	}
}

/*
// process weapons first
			var weaponsXML:XML = _GameData.AssetManager.getXML( "weapons" );
			
			for each ( var weaponXML:XML in weaponsXML.weapon )
			{
				const name:String = String( weaponXML.@name );
				const type:String = String( weaponXML.@type );
				//const deviation:Number = Number( weaponXML.@deviation );
				//const accuracy:Number = Number( weaponXML.@accuracy );
				const fireDelay:Number = Number( weaponXML.@fireDelay );
				const damage:Number = Number( weaponXML.@damage );
				//const particle:String = String( weaponXML.@particle );
				//const speed:Number = weaponXML.@speed != undefined ? Number( weaponXML.@speed ) : 0;
				const shouldUsePool:Boolean = weaponXML.@useAssetPool != undefined && String( weaponXML.@useAssetPool ) == "true";
				const poolSize:int = weaponXML.@poolSize != undefined ? int( weaponXML.@poolSize ) : 0;
				const angle:Number = weaponXML.@fireAngle;
				//const lifeTime:Number = weaponXML.@lifeTime != undefined ? Number( weaponXML.@lifeTime ) : 0;
				const range:int = weaponXML.@range != undefined ? int( weaponXML.@range ) : int.MAX_VALUE;
				//const width:Number = weaponXML.@width != undefined ? Number( weaponXML.@width ) : 0;
				//const height:Number = weaponXML.@height != undefined ? Number( weaponXML.@height ) : 0;
				const trigger:String = String( weaponXML.@trigger );
				//const turnSpeed:Number = weaponXML.@turnSpeed != undefined ? Number( weaponXML.@turnSpeed ) : 0;
				const icon:String = String( weaponXML.@icon );
				const fireAudio:String = String( weaponXML.@fireAudio );
				const doesOverheat:Boolean = String( weaponXML.@doesOverheat ) == "true" ? true : false;
				const burstDuration:Number = Number( weaponXML.@burstDuration );
				const burstCooldown:Number = Number( weaponXML.@burstCooldown );
				
				const subWeapon:String = weaponXML.@system != undefined ? String( weaponXML.@system ) : null;
				var subWeaponInfo:CombatSystemInfo;
				
				if ( subWeapon != null && subWeapon != "" )
				{
					subWeaponInfo = _WeaponInfos[ subWeapon ];
				}
				
				// not yet implemented
				//const weaponAsset:String = String( weaponXML.@asset );
				const weaponInfo:CombatSystemInfo = new WeaponInfo( type, fireDelay, damage, angle, trigger, icon, range, weaponXML, subWeaponInfo, fireAudio, doesOverheat, burstDuration, burstCooldown );
				
				// @TODO: will want to increase to max size required
				if ( shouldUsePool && poolSize > 0 )
				{
					HomeGameData( _GameData ).WeapProjectilePool.createPool( String( weaponInfo.XMLDescription.@particle ), _GameData, poolSize );
				}
				
				_WeaponInfos[ name ] = weaponInfo;
			}
			*/