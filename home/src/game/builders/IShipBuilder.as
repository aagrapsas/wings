package game.builders 
{
	import engine.interfaces.IGameData;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.actors.SpaceShipInfo;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IShipBuilder 
	{
		function setupShipInfo( shipInfo:SpaceShipInfo ):void;
		function registerShip( gameData:IGameData ):void;
		function buildWeapons( gameData:HomeGameData ):void
		function setupLocation( x:int, y:int ):void;
		function setupFX():void;
		function initializeInternal():void;
		function get Ship():SpaceShip;
	}
}