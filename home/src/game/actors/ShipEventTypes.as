package game.actors 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipEventTypes 
	{
		public static const SHIP_DESTROYED:String = "destroyed";
		public static const SHIP_DAMAGED:String = "damaged";
		public static const SHIP_HEAL:String = "heal";
	}
}