package game.actors 
{
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import game.actors.components.ArmorInfo;
	import game.actors.components.ChassisInfo;
	import game.actors.components.CombatSystemInfo;
	import game.actors.components.EngineInfo;
	import game.actors.components.EvasionInfo;
	import game.actors.components.SensorInfo;
	import game.actors.components.ShieldInfo;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SpaceShipInfo 
	{
		public var Key:String;
		public var Name:String;
		
		public var Engine:EngineInfo;
		public var Chassis:ChassisInfo;
		public var Shield:ShieldInfo;
		public var Armor:ArmorInfo;
		public var Sensor:SensorInfo;
		public var Evasion:EvasionInfo;
		public var ControllerType:String;
		
		// @TODO consider moving these
		public var DestructionAsset:String;
		public var DestructionShake:String;
		public var DestructionFlash:String;
		public var MinorHitAsset:String;
		
		public var CombatSystems:Dictionary;
		
		// @DEMO
		public var DifficultyLevel:int;
		
		public function SpaceShipInfo( key:String, name:String, chassis:ChassisInfo, engine:EngineInfo, shield:ShieldInfo, armor:ArmorInfo, sensor:SensorInfo, controllerType:String, destructionAsset:String, destructionShake:String, destructionFlash:String, minorHitAsset:String, difficultyLevel:int, evasion:EvasionInfo, combatSystems:Dictionary )  
		{
			Key = key;
			Name = name;
			Chassis = chassis;
			Engine = engine;
			Shield = shield;
			Armor = armor;
			Sensor = sensor;
			ControllerType = controllerType;
			DestructionAsset = destructionAsset;
			DestructionShake = destructionShake;
			DestructionFlash = DestructionFlash;
			MinorHitAsset = minorHitAsset;
			DifficultyLevel = difficultyLevel;
			Evasion = evasion;
			CombatSystems = combatSystems;
		}
		
		public function copy():SpaceShipInfo
		{			
			var combatSystemsCopy:Dictionary = new Dictionary();
			
			for ( var key:String in CombatSystems )
			{
				combatSystemsCopy[ key ] = CombatSystems[ key ];
			}
			
			var chassisCopy:ChassisInfo = new ChassisInfo();
			var engineCopy:EngineInfo = new EngineInfo();
			var shieldCopy:ShieldInfo = new ShieldInfo();
			var armorCopy:ArmorInfo = new ArmorInfo();
			var sensorCopy:SensorInfo = new SensorInfo();
			var evasionCopy:EvasionInfo = new EvasionInfo();
			
			chassisCopy.deserialize( Chassis.RawXML );
			engineCopy.deserialize( Engine.RawXML );
			
			if ( Shield )
				shieldCopy.deserialize( Shield.RawXML );
			else
				shieldCopy = null;
				
			armorCopy.deserialize( Armor.RawXML );
			sensorCopy.deserialize( Sensor.RawXML );
			
			if ( Evasion )
				evasionCopy.deserialize( Evasion.RawXML );
			else
				evasionCopy = null;
			
			return new SpaceShipInfo( Key, Name, chassisCopy, engineCopy, shieldCopy, armorCopy, sensorCopy, ControllerType, DestructionAsset, DestructionShake, DestructionFlash, MinorHitAsset, DifficultyLevel, evasionCopy, combatSystemsCopy );
		}
	}
}