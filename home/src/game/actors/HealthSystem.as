package game.actors 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class HealthSystem 
	{
		private var _Max:Number;
		private var _Current:Number;
		private var _RegenRate:Number;
		private var _RegenDelay:Number;
		private var _ZeroDelay:Number;
		
		public var type:int;
		
		public static const ARMOR_TYPE:int = 0;
		public static const SHIELD_TYPE:int = 0;
		
		private var _DamageRegenAccumulator:Number = 0;
		
		private var _IsInDamageCoolDown:Boolean = false;
		private var _IsInZeroCoolDown:Boolean = false;
		
		public function HealthSystem( max:Number, regenRate:Number, regenDelay:Number, zeroDelay:Number = 0 ) 
		{
			_Max = max;
			_Current = max;
			
			_RegenDelay = regenDelay;
			_RegenRate = regenRate;
		}
		
		// Returns non-zero when more damage was done than was absorbed (shield zero'd, for example)
		public function doDamage( amount:int ):int
		{			
			var remainder:int = amount - _Current;
			
			_Current = Math.max( _Current - amount, 0 );
			
			_DamageRegenAccumulator = 0;
			_IsInDamageCoolDown = true;
			
			if ( _Current == 0 && _ZeroDelay > 0 )
			{
				_IsInZeroCoolDown = true;
			}
			
			if ( remainder > 0 )
				return remainder;
			
			return 0;
		}
		
		public function isZero():Boolean
		{
			return ( _Current <= 0 );
		}
		
		public function heal( amount:Number ):void
		{
			_Current = Math.min( _Current + amount, _Max );
		}
		
		public function update( deltaTime:Number ): void
		{
			if ( _IsInDamageCoolDown == false && _IsInZeroCoolDown == false )
			{
				var heal:Number = deltaTime * _RegenRate;
				
				_Current = Math.min( _Current + heal, _Max );
			}
			else if ( _IsInZeroCoolDown )
			{
				_DamageRegenAccumulator += deltaTime;
				
				if ( _DamageRegenAccumulator >= _ZeroDelay )
				{
					_IsInZeroCoolDown = false;
					_IsInDamageCoolDown = false;
					_DamageRegenAccumulator = 0;
				}
			}
			else if ( _IsInDamageCoolDown )
			{
				_DamageRegenAccumulator += deltaTime;
				
				if ( _DamageRegenAccumulator >= _RegenDelay )
				{
					_IsInDamageCoolDown = false;
					_DamageRegenAccumulator = 0;
				}
			}
		}
		
		public function get current():int { return _Current; }
	}
}