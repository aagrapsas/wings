package game.actors 
{
	import engine.Actor;
	import engine.interfaces.IGameData;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import game.fx.FXUtilities;
	import game.fx.ShipFXPlayer;
	import game.fx.ThrusterFXPulse;
	import game.gameplay.CombatSystem;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SpaceShip extends Actor
	{
		public var Info:SpaceShipInfo;
		
		public var VelocityX:Number  = 0;
		public var VelocityY:Number = 0;
		public var Speed:uint;
		
		public var ArmorHealth:HealthSystem;
		public var ShieldHealth:HealthSystem;
		
		public var Team:uint;
		public var CanTakeDamage:Boolean = true;
		public var CombatSystems:Vector.<CombatSystem>;
		
		public var DamageCallback:Function;
		
		public var IsDead:Boolean = false;
		
		public var Thruster:ThrusterFXPulse;
		
		private var _AggroDistanceSquared:int;
		private var _AggroDistance:int;
		private var _HomeData:HomeGameData;
		
		private var _FXPlayer:ShipFXPlayer;
		
		public function SpaceShip() 
		{
			CombatSystems = new Vector.<CombatSystem>();
		}
		
		// @TODO consider adding owner
		
		/**
		 * Damages a ship and spawns corresponding vfx and sfx and dispatches damage and death notifications
		 * @param	amount	amount of damage done
		 * @param	x x-location damage occurred at, this is where fx will spawn
		 * @param	y y-location damage occurred at, this is where fx will spawn
		 * @param	instigator object doing the damage
		 */
		public function DoDamage( damageType:String, amount:int, instigator:Actor, x:int, y:int ):void
		{		
			if ( ArmorHealth.isZero() )
				return;
				
			if ( amount > 0 )
			{
				this.dispatchEvent( new ShipEvent( ShipEventTypes.SHIP_DAMAGED, this, null ) );
			}
				
			if ( ShieldHealth && !ShieldHealth.isZero() )
			{
				if ( amount > 0 )
				{
					HandleShieldDamage( x, y, amount );
				}
				
				if ( CanTakeDamage )
				{
					amount = ShieldHealth.doDamage( amount );
				}
			}
				
			if ( CanTakeDamage )
			{
				ArmorHealth.doDamage( amount );
				
				if ( DamageCallback != null )
					DamageCallback.apply( null, [ amount, instigator ] );
				
				_HomeData.SoundSystem.playSound( Info.Chassis.Audio[ "ship_hit" ] );
			}
			
			//_HomeData.ExplosionPool.explode( this.X, this.Y );
			
			if ( ArmorHealth.isZero() )
			{
				// _HomeData.ExplosionPool.explode( this.X, this.Y, true );
				_HomeData.SoundSystem.playSound( Info.Chassis.Audio[ "ship_destroyed" ] );
				IsDead = true;
				
				HandleDeath( instigator );
			}
			else
			{
				if ( amount > 0 )
				{
					HandleHullDamage( damageType, x, y, amount );
				}
			}
		}
		
		// @TODO: consider moving all of this just to the AttackState?
		public function get AggroDistanceSquared():int
		{
			// Cached after first call, this could be optimized even further, though; but, meh
			if ( _AggroDistanceSquared > 0 )
				return _AggroDistanceSquared;
				
			_AggroDistanceSquared = this.Info.Sensor.MaxRange * this.Info.Sensor.MaxRange;
			
			return _AggroDistanceSquared;
		}
		
		public function get AggroDistance():int
		{
			if ( _AggroDistance > 0 )
				return _AggroDistance;
				
			_AggroDistance = this.Info.Sensor.MaxRange;
			
			return _AggroDistance;
		}
		
		private function HandleDeath( instigator:Actor ):void
		{
			this.dispatchEvent( new ShipEvent( ShipEventTypes.SHIP_DESTROYED, this, null ) );
			
			_HomeData.GameEvents.dispatchEvent( ShipEventTypes.SHIP_DESTROYED, { destroyed : this, destroyer : instigator } );
			
			//_HomeData.ExplosionPool.uberFinaleExplosion( this );
			_HomeData.WorldFX.createFX( this.Info.DestructionAsset, this.X, this.Y );
			_HomeData.WorldFX.screenFlash( this.Info.DestructionFlash, this );
			_HomeData.WorldFX.screenShake( this.Info.DestructionShake, this );
			
			Data.Scene.removeActor( this );
			
			destroy();
		}
		
		private function HandleHullDamage( damageType:String, x:int, y:int, amount:int ):void
		{			
			FXUtilities.playExplosion( damageType, this, x, y, _HomeData, amount );
		}
		
		private function HandleShieldDamage( x:int, y:int, amount:int ):void
		{
			// No longer doing world-space
			//FXUtilities.playShieldExplosion( x, y, _HomeData, amount, this.Info.Shield.MaxShields );
			
			// Play in local space
			_FXPlayer.playFX( "shield", x, y, amount, this.Info.Shield.MaxShields );
		}
		
		override public function set Data( value:IGameData ):void
		{
			super.Data = value;
			
			_HomeData = value as HomeGameData;
			
			_FXPlayer = new ShipFXPlayer( this, _HomeData );
		}
		
		override public function destroy():void 
		{
			for each ( var gameSystem:CombatSystem in CombatSystems )
			{
				gameSystem.destroy();
			}
			
			this.Thruster.destroy();
			
			_FXPlayer.destroy();
			
			super.destroy();
		}
	}
}