package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShieldInfo extends ShipComponentInfo
	{
		public var MaxShields:uint;
		public var ShieldRegenPerSecond:Number;
		public var ShieldRegenDelay:Number;
		public var ShieldRegenZeroDelay:Number;
		public var CompatibleChassis:Vector.<String>;
		
		public function ShieldInfo() 
		{
			this.Type = ShipComponentTypes.SHIELD_COMPONENT;
		}
		
		override public function deserialize( xml:XML ):void 
		{
			MaxShields = uint( xml.@maxShields );
			ShieldRegenPerSecond = Number( xml.@shieldRegenPerSecond );
			ShieldRegenDelay = Number( xml.@shieldRegenDelay );
			ShieldRegenZeroDelay = Number( xml.@shieldRegenZeroDelay );
			
			super.deserialize( xml );
		}
	}
}