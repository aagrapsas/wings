package game.actors.components 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ChassisInfo extends ShipComponentInfo
	{
		public var Category:String;
		public var ShipWorldAsset:String;
		public var ThrusterLocations:Vector.<Point>;
		public var HardPointLocations:Vector.<Point>;
		public var ExhaustAsset:String;
		public var ExhaustScale:Number;
		public var MaxTonnage:int;
		public var isHuman:Boolean = true;
		
		public function ChassisInfo() 
		{
			this.Type = ShipComponentTypes.CHASSIS_COMPONENT;
			
			// Allocation
			ThrusterLocations = new Vector.<Point>;
			HardPointLocations = new Vector.<Point>;
		}
		
		override public function deserialize( xml:XML ):void 
		{
			Category = String( xml.@category );
			ShipWorldAsset = String( xml.@shipWorldAsset );
			ExhaustAsset = String( xml.@exhaustAsset );
			ExhaustScale = Number( xml.@exhaustScale );
			MaxTonnage = int( xml.@maxTonnage );
			
			isHuman = xml.@team == undefined || String( xml.@team ) == "human";
			
			// @TODO: figure out best way to handle hardpoints
			
			// Deserialize thruster locations
			for each ( var thrusterXML:XML in xml.exhausts.exhaust )
			{
				ThrusterLocations.push( new Point( Number( thrusterXML.@x ), Number( thrusterXML.@y ) ) );
			}
			
			// TODO consider if we're going to have restrictions on hardpoints in the future
			for each ( var hardpointXML:XML in xml.hardpoints.hardpoint )
			{
				HardPointLocations.push( new Point( Number( hardpointXML.@x ), Number( hardpointXML.@y ) ) );
			}
			
			super.deserialize( xml );
		}
	}
}