package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipComponentTypes 
	{
		public static const ENGINE_COMPONENT:String = "engine";
		public static const SHIELD_COMPONENT:String = "shield";
		public static const SENSOR_COMPONENT:String = "sensor";
		public static const CHASSIS_COMPONENT:String = "chassis";
		public static const ARMOR_COMPONENT:String = "armor";
		public static const COMBAT_SYSTEM:String = "combat";
		public static const PLATFORM:String = "platform";
		public static const EVASION_COMPONENT:String = "evasion";
	}
}