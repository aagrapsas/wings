package game.actors.components 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ChassisMap 
	{
		public var armor:Array;
		public var combatSystems:Array;
		public var engines:Array;
		public var sensors:Array;
		public var shields:Array;
		public var evasion:Array;
		
		public var key:String;
		
		public var lookUp:Dictionary;
		
		public function ChassisMap( key:String ) 
		{
			this.key = key;
			
			armor = new Array();
			combatSystems = new Array();
			engines = new Array();
			sensors = new Array();
			shields = new Array();
			evasion = new Array();
			
			lookUp = new Dictionary();
			
			lookUp[ ShipComponentTypes.ARMOR_COMPONENT ] = armor;
			lookUp[ ShipComponentTypes.COMBAT_SYSTEM ] = combatSystems;
			lookUp[ ShipComponentTypes.ENGINE_COMPONENT ] = engines;
			lookUp[ ShipComponentTypes.SENSOR_COMPONENT ] = sensors;
			lookUp[ ShipComponentTypes.SHIELD_COMPONENT ] = shields;
			lookUp[ ShipComponentTypes.EVASION_COMPONENT ] = evasion;
		}
	}
}