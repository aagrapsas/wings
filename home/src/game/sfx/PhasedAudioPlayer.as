package game.sfx 
{
	import engine.Actor;
	import engine.audio.SoundObject;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.media.SoundChannel;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PhasedAudioPlayer implements ITickable, IDestroyable
	{
		private var _GameData:IGameData;
		
		private var _StartChannel:SoundObject;
		private var _StartSound:String;
		private var _StartAccumulator:Number = 0;
		
		private var _LoopChannel:SoundObject;
		private var _LoopSound:String;
		private var _IsLoopPlaying:Boolean = false;
		
		private var _OutChannel:SoundObject;
		private var _OutSound:String;		
		
		private var _Attenuator:Actor;
		
		public function PhasedAudioPlayer( gameData:IGameData, startSound:String, loopSound:String, outSound:String, attenuator:Actor = null ) 
		{
			_GameData = gameData;
			_StartSound = startSound;
			_LoopSound = loopSound;
			_OutSound = outSound;
			_Attenuator = attenuator;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( deltaTime > 0 )
			{
				if ( _OutChannel )
				{
					_OutChannel.stop();
					_OutChannel = null;
				}
				
				if ( !_IsLoopPlaying && !_StartChannel )
				{
					_StartChannel = _GameData.SoundSystem.playSound( _StartSound, _StartAccumulator, _Attenuator );
					//trace( "Starting to play at " + _StartAccumulator );
				}
				
				if ( _StartChannel )
				{
					_StartAccumulator = Math.min( _StartAccumulator + deltaTime, _StartChannel.Length );
					
					//trace( "Len: " + _StartChannel.Length + " current: " + _StartChannel.Current + " accum: " + _StartAccumulator );
					
					if ( _StartChannel.IsBlendFinished )
					{
						_IsLoopPlaying = true;
						_StartChannel = null;
						
						_LoopChannel = _GameData.SoundSystem.playSound( _LoopSound, 0, _Attenuator );
						//trace( "Starting loop" );
					}
				}
			}
			else
			{
				if ( ( _StartChannel || _LoopChannel ) && !_OutChannel )
				{
					_OutChannel = _GameData.SoundSystem.playSound( _OutSound, 0, _Attenuator );
					//trace( "Starting out" );
				}
				
				if ( _StartChannel )
				{
					_StartChannel.stop();
					_StartChannel = null;
					//trace( "Stopping start" );
				}
				
				if ( _LoopChannel )
				{
					_LoopChannel.stop();
					_LoopChannel = null;
					//trace( "Stopping loop" );
				}
				
				_IsLoopPlaying = false;
				
				if ( _OutChannel && _OutChannel.IsFinished )
				{
					_OutChannel = null;
					//trace( "Stopping out" );
				}
			}
		}
		
		public function destroy():void
		{
			
		}
	}
}