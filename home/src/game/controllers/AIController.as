package game.controllers 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.render.scene.SceneNode;
	import flash.display.Graphics;
	import starling.events.EventDispatcher;
	import flash.utils.Dictionary;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.ai.AIEvent;
	import game.ai.IState;
	import game.ai.IStateMachine;
	import game.ai.StateData;
	import game.ai.StateFactory;
	import game.ai.StateTypes;
	import game.HomeGameData;
	import game.sfx.PhasedAudioPlayer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AIController extends EventDispatcher implements IController, IStateMachine
	{
		private var _AggroDistance:uint = 400;
		private var _GameData:HomeGameData;
		private var _SpaceShip:SpaceShip;
		
		private var _CurrentState:IState;
		private var _MainLocomotionState:IState;
		private var _CurrentLocomotionState:IState;
		
		private var _StateData:StateData;
		
		private var _IsLogicPaused:Boolean = false;
		
		// Dictionary to prevent the newing of states repeatedly
		private var _CachedStates:Dictionary;
		
		private var _ThrusterAudio:PhasedAudioPlayer;
		
		public var Squad:String;
		public var Name:String;
		
		public function AIController( spaceShip:SpaceShip, gameData:HomeGameData ) 
		{
			_SpaceShip = spaceShip;
			_GameData = gameData;
			
			_CachedStates = new Dictionary;
			_StateData = new StateData;
			
			_ThrusterAudio = new PhasedAudioPlayer( _GameData, spaceShip.Info.Engine.Audio[ "engine_accelerate" ], spaceShip.Info.Engine.Audio[ "engine_loop" ], spaceShip.Info.Engine.Audio[ "engine_release" ], spaceShip );
			
			initialize();
		}
		
		private function initialize():void
		{
			// Setup idle as default state
			_CachedStates[ StateTypes.IDLE_STATE ] = StateFactory.getState( StateTypes.IDLE_STATE, _SpaceShip, _GameData, this );
			_CurrentState = _CachedStates[ StateTypes.IDLE_STATE ];
			
			// Register with scene (may want to move this to factory!)
			_GameData.Scene.addTickable( this );
			
			_SpaceShip.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
		}
		
		private function onShipDestroyed( e:ShipEvent ):void
		{			
			this.dispatchEvent( new AIEvent( AIEvent.AI_DEATH, this ) );
			
			destroy();
		}
		
		// This really should just be done with a state machine
		public function tick( deltaTime:Number ):void
		{
			if ( _IsLogicPaused )
				return;
			
			if ( _CurrentState )
			{
				_CurrentState.updateState( deltaTime );
			}
			
			if ( _CurrentLocomotionState )
			{
				_CurrentLocomotionState.updateState( deltaTime );
			}
				
			if ( _SpaceShip.VelocityX != 0 || _SpaceShip.VelocityY != 0 )
			{
				_ThrusterAudio.tick( deltaTime );
				
				_SpaceShip.Thruster.update( true );
			}
			else
			{
				_ThrusterAudio.tick( -deltaTime );
				
				_SpaceShip.Thruster.update( false );
			}
			
			_SpaceShip.ArmorHealth.update( deltaTime );
			
			if ( _SpaceShip.ShieldHealth )
			{
				_SpaceShip.ShieldHealth.update( deltaTime );
			}
		}
		
		public function goToState( state:String ):void
		{
			if ( _CurrentState )
			{
				_CurrentState.leaveState();
			}
			
			var newState:IState;
			
			if ( _CachedStates[ state ] != null )
			{
				newState = _CachedStates[ state ];
			}
			else
			{
				newState = StateFactory.getState( state, _SpaceShip, _GameData, this );
				_CachedStates[ state ] = newState;
			}
			
			newState.enterState();
			_CurrentState = newState;
		}
		
		public function setupShip( ship:SpaceShip ):void
		{
			if ( _SpaceShip )
			{
				_SpaceShip.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			}
			
			_SpaceShip = ship;
			
			_SpaceShip.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
		}
		
		public function goToExplicitState( name:String, state:IState ):void
		{
			_CachedStates[ name ] = state;
			
			goToState( name );
		}
		
		public function setLocomotionState( state:IState ):void
		{
			_CurrentLocomotionState = state;
		}
		
		public function setMainLocomotionState( state:IState ):void
		{
			_MainLocomotionState = state;
		}
		
		public function get SharedData():StateData { return _StateData; }
		public function get Ship():SpaceShip { return _SpaceShip; }
		public function get MainLocomotionState():IState { return _MainLocomotionState; }
		public function get IsLogicPaused():Boolean { return _IsLogicPaused; }
		
		public function set IsLogicPaused( value:Boolean ):void { _IsLogicPaused = value; }
		
		public function destroy():void
		{
			if ( _SpaceShip )
				_SpaceShip.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_GameData.Scene.removeTickable( this );
			
			this.dispatchEvent( new AIEvent( AIEvent.AI_DEATH, this ) );
		}
	}
}