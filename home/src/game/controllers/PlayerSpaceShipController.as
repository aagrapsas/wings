package game.controllers 
{
	import engine.Actor;
	import starling.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.media.SoundChannel;
	import flash.utils.Dictionary;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.fx.IShipFX;
	import game.gameplay.CombatSystem;
	import game.gameplay.player.TargetSelector;
	import game.HomeGameData;
	import game.movement.CameraLagMovement;
	import game.movement.EvasionSystem;
	import game.movement.SpaceMomentumMovement;
	import game.sfx.PhasedAudioPlayer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PlayerSpaceShipController implements IController
	{
		private var _KeysDown:Dictionary;
		private var _PlayerShip:SpaceShip;
		private var _GameData:HomeGameData;
		
		private var _MovementSystem:SpaceMomentumMovement;
		private var _CameraMovementSystem:CameraLagMovement;
		private var _EvasionSystem:EvasionSystem;
		
		private var _TargetSelector:TargetSelector
		//private var _WeaponSelector:ActiveSelector;
		//private var _AbilitiesUI:UIAbilitiesBar;
	
		private var _CurrentFX:Vector.<IShipFX>;
		
		private var _MoveForwardKey:uint;
		private var _RotateCounterVelocityKey:uint;
		private var _TurnLeftKey:uint;
		private var _TurnRightKey:uint;
		
		private var _FirePrimaryKey:uint;
		private var _FirePrimaryKeyAlt:uint;
		private var _FireSecondaryKey:uint;
		private var _FireSecondaryKeyAlt:uint;
		private var _FireSpecialOne:uint;
		private var _FireSpecialTwo:uint;
		
		private var _PrimaryBankWeapons:Vector.<CombatSystem>;
		private var _SecondaryBankWeapons:Vector.<CombatSystem>;
		private var _SpecialOneWeapon:CombatSystem;
		private var _SpecialTwoWeapon:CombatSystem;
		
		private var _ThrusterAudio:PhasedAudioPlayer;
		
		private var _ThrusterChannel:SoundChannel;
		private var _ThrusterAudioLength:Number;
		private var _ThrusterAudioAccumulated:Number = 0;
		private var _ThrusterLoopChannel:SoundChannel;
		private var _ThrusterIsPlayingLoop:Boolean = false;
		
		private var _ShouldCycleWeapon:Boolean = false;
		
		public function PlayerSpaceShipController( playerShip:SpaceShip, gameData:HomeGameData ) 
		{	
			_PlayerShip = playerShip;
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_CurrentFX = new Vector.<IShipFX>;
			
			_MovementSystem = new SpaceMomentumMovement( _PlayerShip, _GameData );
			_CameraMovementSystem = new CameraLagMovement( _PlayerShip, _GameData );
			_TargetSelector = new TargetSelector( _GameData as HomeGameData );
			//_AbilitiesUI = new UIAbilitiesBar( _GameData );
			_EvasionSystem = new EvasionSystem( _GameData, _PlayerShip, _MovementSystem );

			_PrimaryBankWeapons = new Vector.<CombatSystem>();
			_SecondaryBankWeapons = new Vector.<CombatSystem>();
			
			_KeysDown = new Dictionary();
			_GameData.Scene.GameStage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			_GameData.Scene.GameStage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			
			_GameData.Scene.GameStage.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			_GameData.Scene.GameStage.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			_GameData.Scene.GameStage.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			
			//_AbilitiesUI.setup( _PlayerShip );
			
			_GameData.Scene.addTickable( this );
			
			_PlayerShip.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_MoveForwardKey = _GameData.InputMap[ "move_forward" ];
			_RotateCounterVelocityKey = _GameData.InputMap[ "reverse_velocity" ];
			_TurnLeftKey = _GameData.InputMap[ "turn_left" ];
			_TurnRightKey = _GameData.InputMap[ "turn_right" ];
			
			_FirePrimaryKey = _GameData.InputMap[ "fire_primary" ];
			_FireSecondaryKey = _GameData.InputMap[ "fire_secondary" ];
			_FirePrimaryKeyAlt = _GameData.InputMap[ "fire_primary_alt" ];
			_FireSecondaryKeyAlt = _GameData.InputMap[ "fire_secondary_alt" ];
			_FireSpecialOne = _GameData.InputMap[ "fire_special_1" ];
			_FireSpecialTwo = _GameData.InputMap[ "fire_special_2" ];
			
			_ThrusterAudio = new PhasedAudioPlayer( _GameData, _PlayerShip.Info.Engine.Audio[ "engine_accelerate" ], _PlayerShip.Info.Engine.Audio[ "engine_loop" ], _PlayerShip.Info.Engine.Audio[ "engine_release" ] );
			
			_PlayerShip.DamageCallback = onDamage;
			
			// Not the cleanest way to do this
			for each ( var combatSystem:CombatSystem in _PlayerShip.CombatSystems )
			{
				if ( combatSystem.Info.SystemType == "projectile" )
				{
					//combatSystem.Icon.setFireNumber( 1 );
					_PrimaryBankWeapons.push( combatSystem );
				}
				else if ( combatSystem.Info.SystemType == "tracking" )
				{
					//combatSystem.Icon.setFireNumber( 2 );
					_SecondaryBankWeapons.push( combatSystem );
				}
				else if ( !_SpecialOneWeapon )
				{
					//combatSystem.Icon.setFireNumber( 3 );
					
					_SpecialOneWeapon = combatSystem;
				}
				else if ( !_SpecialTwoWeapon )
				{
					//combatSystem.Icon.setFireNumber( 4 );
					
					_SpecialTwoWeapon = combatSystem;
				}
			}
		}
		
		private function onShipDestroyed( e:ShipEvent ):void
		{
			_PlayerShip.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			HomeGameData( _GameData ).PlayerShip = null;
			HomeGameData( _GameData ).PlayerController = null;
			destroy();
		}
		
		public function tick( deltaTime:Number ):void 
		{
			var isInput:Boolean = false;
			
			_PlayerShip.ArmorHealth.update( deltaTime );
			
			if ( _PlayerShip.ShieldHealth )
			{
				_PlayerShip.ShieldHealth.update( deltaTime );
			}
			
			// Rotation
			if ( _GameData.IsInputEnabled )
			{
				if ( _KeysDown[ _TurnLeftKey ] )
				{
					_MovementSystem.rotate( true, deltaTime );
				}
				else if ( _KeysDown[ _TurnRightKey ] )
				{
					_MovementSystem.rotate( false, deltaTime );
				}
			}
			
			if ( _GameData.IsInputEnabled && _KeysDown[ _MoveForwardKey ] )
			{	
				_MovementSystem.move( _PlayerShip.Dir.x, _PlayerShip.Dir.y, deltaTime );
				isInput = true;
			}
			else if ( _GameData.IsInputEnabled && _KeysDown[ _RotateCounterVelocityKey ] )
			{		
				if ( _PlayerShip.VelocityX != 0 || _PlayerShip.VelocityY != 0 )
				{
					var velocityDir:Point = new Point( _PlayerShip.VelocityX, _PlayerShip.VelocityY );
					velocityDir.normalize( 1 );
					
					// Flip
					velocityDir.x *= -1;
					velocityDir.y *= -1;	
					
					// Rotate towards
					_PlayerShip.faceToward( velocityDir.x, velocityDir.y, _PlayerShip.Info.Engine.MaxTurnSpeed );
				}
			}
			
			_MovementSystem.tick( deltaTime );
			_EvasionSystem.tick( deltaTime );
			
			// Apply velocity to position and move camera accordingly
			if ( _PlayerShip.VelocityX != 0 || _PlayerShip.VelocityY != 0 || _EvasionSystem.velocityX != 0 || _EvasionSystem.velocityY != 0 )
			{
				_PlayerShip.X += ( _PlayerShip.VelocityX + _EvasionSystem.velocityX ) * deltaTime;
				_PlayerShip.Y += ( _PlayerShip.VelocityY + _EvasionSystem.velocityY ) * deltaTime;
				
				// Debug.debugLog( GameLogChannels.ACTOR, "Ship velx: " + _PlayerShip.VelocityX + " ship vely: " + _PlayerShip.VelocityY );
			}
			
			_CameraMovementSystem.tick( deltaTime );
			
			_TargetSelector.update();
			
			if ( _PlayerShip.Thruster )
			{
				if ( isInput )
				{					
					_ThrusterAudio.tick( deltaTime );
					_PlayerShip.Thruster.update( true );
				}
				else
				{
					_ThrusterAudio.tick( -deltaTime );
					_PlayerShip.Thruster.update( false );
				}
			}
			
			/*
			_GameData.DebugSystem.PerfHUD.setValue( "Ship", "( x: " + _PlayerShip.X + " y: " + _PlayerShip.Y + ")" );
			_GameData.DebugSystem.PerfHUD.setValue( "Ship Vel", "( x: " + _PlayerShip.VelocityX + " y: " + _PlayerShip.VelocityY + " )" );
			_GameData.DebugSystem.PerfHUD.setValue( "Ship Health", _PlayerShip.ArmorHealth.current );
			_GameData.DebugSystem.PerfHUD.setValue( "Ship Shields", _PlayerShip.ShieldHealth ? _PlayerShip.ShieldHealth.current : 0 );
			_GameData.DebugSystem.PerfHUD.setValue( "Ship frame", "( x:" + ( _PlayerShip.VelocityX * deltaTime  ) + " y: " + ( _PlayerShip.VelocityY * deltaTime ) + " )" );
			_GameData.DebugSystem.PerfHUD.setValue( "Ship dir", "( x: " + _PlayerShip.Dir.x + " y: " + _PlayerShip.Dir.y + " )" );
			*/
			
			var target:SpaceShip = _TargetSelector.GetCurrentTarget();
			
			/*
			if ( target )
			{
				_GameData.DebugSystem.PerfHUD.setValue( "TARGET health", target.ArmorHealth.current );
				
				if ( target.ShieldHealth )
				{
					_GameData.DebugSystem.PerfHUD.setValue( "TARGET shields", target.ShieldHealth.current );
				}
			}
			
			const speed:Number = Math.sqrt( _PlayerShip.VelocityY * _PlayerShip.VelocityY + _PlayerShip.VelocityX * _PlayerShip.VelocityX );
			
			_GameData.DebugSystem.PerfHUD.setValue( "Ship Speed", speed );
			*/
			
			if ( _GameData.IsInputEnabled )
			{
				var fireDirX:int = 0;
				var fireDirY:int = 0;
				
				if ( target )
				{
					fireDirX = target.X;
					fireDirY = target.Y;
				}
				else
				{
					fireDirX = _PlayerShip.X + _PlayerShip.Dir.x * 100;
					fireDirY = _PlayerShip.Y + _PlayerShip.Dir.y * 100;
				}
				
				if ( _KeysDown[ _FirePrimaryKey ] || _KeysDown[ _FirePrimaryKeyAlt ] )
				{
					for each ( var primarySystem:CombatSystem in _PrimaryBankWeapons )
					{
						primarySystem.execute( _PlayerShip, _PlayerShip.Dir, _PlayerShip.X, _PlayerShip.Y, fireDirX, fireDirY );
					}
				}
				
				if ( ( _KeysDown[ _FireSecondaryKey ] || _KeysDown[ _FireSecondaryKeyAlt ] ) )
				{
					for each ( var secondarySystem:CombatSystem in _SecondaryBankWeapons )
					{
						secondarySystem.execute( _PlayerShip, _PlayerShip.Dir, _PlayerShip.X, _PlayerShip.Y, fireDirX, fireDirY );
					}
				}
				
				if ( _KeysDown[ _FireSpecialOne ] && _SpecialOneWeapon )
				{
					_SpecialOneWeapon.execute( _PlayerShip, _PlayerShip.Dir, _PlayerShip.X, _PlayerShip.Y, fireDirX, fireDirY );
				}
				
				if ( _KeysDown[ _FireSpecialTwo ] && _SpecialTwoWeapon )
				{
					_SpecialTwoWeapon.execute( _PlayerShip, _PlayerShip.Dir, _PlayerShip.X, _PlayerShip.Y, fireDirX, fireDirY );
				}
				
				if ( _KeysDown[ _GameData.InputMap[ "fire_weapon" ] ] )	// space down
				{
					if ( _KeysDown[ _GameData.InputMap[ "cycle_target" ] ] )	// tab down
					{
						_ShouldCycleWeapon = true;
					}
				}
			}
		}
		
		private function onDamage( amount:int, instigator:Actor ):void
		{
			var dir:Point;
			
			if ( instigator )
			{
				dir = new Point;
				dir.x = instigator.X - _PlayerShip.X ;
				dir.y = instigator.Y - _PlayerShip.Y;
				dir.normalize( 1 );
			}
			
			const maxPercent:Number = _GameData.GameConfigData[ "hit_reaction_max_reaction_percent" ];
			const maxHealth:int = _PlayerShip.Info.Armor.FullHitPoints + ( _PlayerShip.Info.Shield ? _PlayerShip.Info.Shield.MaxShields : 0 );
			
			const damagePercent:Number = amount / maxHealth;
			const scalePercent:Number = damagePercent / maxPercent;
			
			const reactionMag:Number = _GameData.GameConfigData[ "hit_reaction_mag" ];
			const reactionDur:Number = _GameData.GameConfigData[ "hit_reaction_dur" ];
			
			const scaledReactionMag:Number = Math.min( reactionMag * scalePercent, reactionMag );
			const scaledReactionDuration:Number = Math.min( reactionDur * scalePercent, reactionDur );
			
			_GameData.ActiveCamera.shake( scaledReactionMag, scaledReactionDuration, dir );
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			_KeysDown[ e.keyCode ] = true;
		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{
			_KeysDown[ e.keyCode ] = false;
			
			if ( _GameData.IsInputEnabled )
			{
				if ( e.keyCode == _GameData.InputMap[ "cycle_target" ] )
				{
					_TargetSelector.CycleTarget();
				}
			}
		}
		
		private function onMouseDown( e:MouseEvent ):void
		{
			_KeysDown[ "mouse" ] = true;
		}
		
		private function onMouseUp( e:MouseEvent ):void
		{
			_KeysDown[ "mouse" ] = false;
		}
		
		private function onMouseMove( e:MouseEvent ):void
		{
			_KeysDown[ "mouse_x" ] = e.stageX;
			_KeysDown[ "mouse_y" ] = e.stageY;
		}
		
		public function setHUDVisibility( value:Boolean ):void
		{
			// _WeaponSelector.visible = value;
		}
		
		public function get CameraMovementSystem():CameraLagMovement { return _CameraMovementSystem; }
		
		public function destroy():void 
		{
			//_AbilitiesUI.destroy();
			
			_TargetSelector.destroy();
			
			_EvasionSystem.destroy();
			
			_TargetSelector = null;
			_EvasionSystem = null;
			
			_GameData.Scene.removeTickable( this );
			_GameData.Scene.GameStage.removeEventListener( MouseEvent.CLICK, onMouseDown );
			_GameData.Scene.GameStage.removeEventListener( MouseEvent.CLICK, onMouseUp );
			_GameData.Scene.GameStage.removeEventListener( MouseEvent.CLICK, onMouseMove );
			_PlayerShip.DamageCallback = null;
			_GameData.Scene.GameStage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			_GameData.Scene.GameStage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
		}
	}
}