package game.controllers 
{
	import engine.interfaces.ISceneNotifiable;
	import starling.events.KeyboardEvent;
	import flash.utils.Dictionary;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class RoamingCameraController implements IController, ISceneNotifiable
	{
		private var _GameData:HomeGameData;
		
		private var _KeysDown:Dictionary;
		
		private var _VelocityX:Number = 0;
		private var _VelocityY:Number = 0;
		
		private var _MoveForwardKey:uint;
		private var _TurnLeftKey:uint;
		private var _TurnRightKey:uint;
		private var _MoveBackwardKey:uint;
		
		private var _VelocityStep:Number = 1.0;
		private var _VelocityCap:Number = 10.0;
		
		public function RoamingCameraController( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_KeysDown = new Dictionary();
			
			_MoveForwardKey = _GameData.InputMap[ "move_forward" ];
			_MoveBackwardKey = _GameData.InputMap[ "reverse_velocity" ];
			_TurnLeftKey = _GameData.InputMap[ "turn_left" ];
			_TurnRightKey = _GameData.InputMap[ "turn_right" ];
			
			_GameData.Scene.GameStage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true );
			_GameData.Scene.GameStage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp, false, 0, true );
			
			_GameData.Scene.addTickable( this );
			
			_VelocityCap = _GameData.GameConfigData[ "roaming_max_speed" ];
			_VelocityStep = _GameData.GameConfigData[ "roaming_acceleration" ];
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			_KeysDown[ e.keyCode ] = true;
		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{
			_KeysDown[ e.keyCode ] = false;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _KeysDown[ _MoveForwardKey ] )
			{
				_VelocityY = Math.min( _VelocityY + _VelocityStep, _VelocityCap );
			}
			else if ( _KeysDown[ _MoveBackwardKey ] )
			{
				_VelocityY = Math.max( _VelocityY - _VelocityStep, -_VelocityCap );
			}
			else
			{
				_VelocityY = _VelocityY > 0 ? Math.max( _VelocityY - _VelocityStep, 0 ) : Math.min( _VelocityY + _VelocityStep, 0 );
			}
			
			if ( _KeysDown[ _TurnLeftKey ] )
			{
				_VelocityX = Math.min( _VelocityX + _VelocityStep, _VelocityCap );
			}
			else if ( _KeysDown[ _TurnRightKey ] )
			{
				_VelocityX = Math.max( _VelocityX - _VelocityStep, -_VelocityCap );
			}
			else
			{
				_VelocityX = _VelocityX > 0 ? Math.max( _VelocityX - _VelocityStep, 0 ) : Math.min( _VelocityX + _VelocityStep, 0 );
			}
			
			if ( _VelocityX != 0 || _VelocityY != 0 )
			{
				_GameData.ActiveCamera.scrollXY( _VelocityX * deltaTime, _VelocityY * deltaTime );
			}
		}

		public function handleSceneTransition():void
		{
			destroy();
		}
		
		public function playFX( name:String, data:Object, doneCallback:Function ):void
		{
			
		}
		
		public function destroy():void
		{
			if ( !_GameData )
				return;
			
			_GameData.Scene.removeTickable( this );
			_GameData = null;
		}
	}
}