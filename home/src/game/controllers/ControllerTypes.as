package game.controllers 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ControllerTypes 
	{
		public static const PLAYER_CONTROLLER:String = "player";
		public static const AI_CONTROLLER:String = "ai";
	}
}