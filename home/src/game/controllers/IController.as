package game.controllers 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IController extends ITickable, IDestroyable
	{
		//function playFX( name:String, data:Object, doneCallback:Function ):void;
	}
}