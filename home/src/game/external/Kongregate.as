package game.external 
{
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Stage;
	import starling.events.Event;
	import flash.net.URLRequest;
	import flash.system.Security;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Kongregate 
	{		
		private static var _KongLoader:Loader;
		public static var api:*;
		
		public static function setup( stage:Stage ):void
		{
			var flashVars:Object = LoaderInfo( stage.root.loaderInfo ).parameters;
			
			var kongregateAPIPath:String = flashVars.kongregate_api_path || "http://www.kongregate.com/flash/API_AS3_Local.swf";
			
			Security.allowDomain( kongregateAPIPath );
			
			var request:URLRequest = new URLRequest( kongregateAPIPath );
			
			_KongLoader = new Loader();
			_KongLoader.addEventListener( Event.COMPLETE, onLoadComplete );
			
			_KongLoader.load( request );
		}
		
		private static function onLoadComplete( e:Event ):void
		{
			api = e.target.content;
			
			// Connect to actualy Kong services
			api.services.connect();
		}
		
		public static function get isAvailable():Boolean { return api != null; }
	}
}