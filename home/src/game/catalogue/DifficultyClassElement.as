package game.catalogue 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DifficultyClassElement 
	{
		public var Key:String;
		public var Cost:int;
		
		public function DifficultyClassElement( key:String, cost:int ) 
		{
			Key = key;
			Cost = cost;
		}
	}
}