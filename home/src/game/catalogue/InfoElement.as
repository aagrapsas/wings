package game.catalogue 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	/**
	 * Class deserialized from additional_config.xml
	 */
	public class InfoElement 
	{
		public var target : String;
		public var type : String;
		public var softCost : int;
		public var hardCost : int;
		public var description : String;
		
		public function InfoElement() 
		{
			
		}
		
		public function deserialize( xml:XML ):void
		{
			target = String( xml.@key );
			type = String( xml.@type );
			softCost = int( xml.@softCost );
			hardCost = int( xml.@hardCost );
			description = String( xml.@description );
		}
	}
}