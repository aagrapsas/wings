package game.catalogue 
{
	import engine.debug.Debug;
	import flash.utils.Dictionary;
	import game.actors.components.ArmorInfo;
	import game.actors.components.ChassisInfo;
	import game.actors.components.ChassisMap;
	import game.actors.components.CombatSystemInfo;
	import game.actors.components.EngineInfo;
	import game.actors.components.EvasionInfo;
	import game.actors.components.PlatformInfo;
	import game.actors.components.SensorInfo;
	import game.actors.components.ShieldInfo;
	import game.actors.components.ShipComponentInfo;
	import game.actors.SpaceShipInfo;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	 /**
	  * Handles deserialization and retrieval of ship components and ship templates
	  */
	public class ShipComponentCatalogue 
	{
		private var _GameData:HomeGameData
		private var _ShipTemplates:Dictionary;
		private var _ActiveComponents:Dictionary;
		private var _ShieldComponents:Dictionary;
		private var _SensorsComponent:Dictionary;
		private var _ChassisComponents:Dictionary;
		private var _EngineChomponents:Dictionary;
		private var _ArmorComponents:Dictionary;
		private var _SoundSets:Dictionary;
		private var _CombatSystemTemplates:Dictionary; // dictionary of dictionaries mapping hardpoint (int) to weapon type (string)
		private var _CombatSystems:Dictionary;
		private var _Platforms:Dictionary;	// maps ship to a platform info
		private var _EvasionComponents:Dictionary;
		
		// @TODO: implement
		private var _InactiveComponents:Dictionary
		
		private var _ChassisMapping:Dictionary;	// key (Fighter) --> all components a fighter can have
		
		private var _HighestDPS:Number = 0;
		private var _MostHardpoints:Number = 0;
		
		public function ShipComponentCatalogue( gameData:HomeGameData ) 
		{
			// Assignment
			_GameData = gameData;
			
			// Allocation
			_ShipTemplates = new Dictionary();
			_ActiveComponents = new Dictionary();
			_ShieldComponents = new Dictionary();
			_SensorsComponent = new Dictionary();
			_ChassisComponents = new Dictionary();
			_EngineChomponents = new Dictionary();
			_SoundSets = new Dictionary();
			_ArmorComponents = new Dictionary();
			_InactiveComponents = new Dictionary();
			_CombatSystems = new Dictionary();
			_CombatSystemTemplates = new Dictionary();
			_Platforms = new Dictionary();
			_EvasionComponents = new Dictionary();
			
			_ChassisMapping = new Dictionary();
			
			deserialize();
		}
		
		private function processChassis( component:ShipComponentInfo ):void
		{
			// @HACK
			// THIS IS GOING TO BE SLOW! SHOULD HAVE A TAG SYSTEME IN DATA
			
			var lowerCompare:String = component.Key.toLocaleLowerCase();
			
			for ( var key:String in _ChassisMapping )
			{			
				if ( lowerCompare.search( key ) != -1 )
				{
					var map:ChassisMap = _ChassisMapping[ key ];
					
					if ( map )
					{
						map.lookUp[ component.Type ].push( component );
					}
				}
			}
		}
		
		private function setupAudio( xml:XML, component:ShipComponentInfo ):void
		{
			if ( xml.@soundSet == undefined )
				return;
			
			var key:String = xml.@soundSet;
			
			if ( key == null || key == "" )
				return;
				
			var audio:Dictionary = _SoundSets[ key ];
			
			if ( audio == null )
			{
				Debug.errorLog( GameLogChannels.DATA, "Invalid sound bank " + key + " for component " + component.Name );
				return;
			}
				
			component.Audio = audio;
		}
		
		private function deserialize():void
		{
			var component:ShipComponentInfo;
			
			// Sound sets (everything else relies on these)
			xml = _GameData.AssetManager.getXML( "sound_sets" );
			
			for each ( var setXML:XML in xml.set )
			{
				var soundSet:Dictionary = new Dictionary();
				
				for each ( var soundXML:XML in setXML.sound )
				{
					soundSet[ String( soundXML.@type ) ] = String( soundXML.@source );
				}
				
				_SoundSets[ String( setXML.@key ) ] = soundSet;
			}
			
			var xml:XML = _GameData.AssetManager.getXML( "chassis_components" );
			
			// Chassis
			for each ( var chassisXML:XML in xml.chassis )
			{
				component = new ChassisInfo();
				component.deserialize( chassisXML );
				
				setupAudio( chassisXML, component );
				
				_ChassisMapping[ component.Name.toLocaleLowerCase() ] = new ChassisMap( component.Name.toLocaleLowerCase() );
				
				_ChassisComponents[ component.Key ] = component;
			}
			
			// Engines
			xml = _GameData.AssetManager.getXML( "engine_components" );
			
			for each ( var engineXML:XML in xml.engine )
			{
				component = new EngineInfo();
				component.deserialize( engineXML );
				
				setupAudio( engineXML, component );
				processChassis( component );
				
				_EngineChomponents[ component.Key ] = component;
			}
			
			// Shields
			xml = _GameData.AssetManager.getXML( "shield_components" );
			
			for each ( var shieldXML:XML in xml.shields )
			{
				component = new ShieldInfo();
				component.deserialize( shieldXML );
				
				setupAudio( shieldXML, component );
				processChassis( component );
				
				_ShieldComponents[ component.Key ] = component;
			}
			
			// Sensors
			xml = _GameData.AssetManager.getXML( "sensor_components" );
			
			for each ( var sensorXML:XML in xml.sensor )
			{
				component = new SensorInfo();
				component.deserialize( sensorXML );
				
				setupAudio( sensorXML, component );
				processChassis( component );
				
				_SensorsComponent[ component.Key ] = component;
			}
			
			// Armor
			xml = _GameData.AssetManager.getXML( "armor_components" );
			
			for each ( var armorXML:XML in xml.armor )
			{
				component = new ArmorInfo();
				component.deserialize( armorXML );
				
				setupAudio( armorXML, component );
				processChassis( component );
				
				_ArmorComponents[ component.Key ] = component;
			}
			
			// Combat systems
			xml = _GameData.AssetManager.getXML( "flat_weapons" ); // TODO consider renaming this xml to combat systems (they're active/passive)
			
			for each ( var combatXML:XML in xml.weapon )
			{
				var weaponInfo:CombatSystemInfo = new CombatSystemInfo();	// TODO this system needs to be more generic, too!
				weaponInfo.deserialize( combatXML );
				
				setupAudio( combatXML, weaponInfo );
				processChassis( weaponInfo );
				
				_CombatSystems[ weaponInfo.Key ] = weaponInfo;
				
				if ( weaponInfo.DPS > _HighestDPS )
				{
					_HighestDPS = weaponInfo.DPS;
				}
			}
			
			xml = _GameData.AssetManager.getXML( "special_weapons" ); // TODO consider renaming this xml to combat systems (they're active/passive)
			
			for each ( var specialCombatXML:XML in xml.weapon )
			{
				var specialWeaponInfo:CombatSystemInfo = new CombatSystemInfo();	// TODO this system needs to be more generic, too!
				specialWeaponInfo.deserialize( specialCombatXML );
				
				setupAudio( specialCombatXML, specialWeaponInfo );
				processChassis( specialWeaponInfo );
				
				_CombatSystems[ specialWeaponInfo.Key ] = specialWeaponInfo;
			}
			
			// Evasion components
			xml = _GameData.AssetManager.getXML( "evasion_components" );
			
			for each ( var evasionXML:XML in xml.evasion )
			{
				var evasionInfo:EvasionInfo = new EvasionInfo();
				evasionInfo.deserialize( evasionXML );
				processChassis( component );
				
				_EvasionComponents[ evasionInfo.Key ] = evasionInfo;
			}
			
			// Ship templates
			xml = _GameData.AssetManager.getXML( "ship_templates" );
			
			for each ( var templateXML:XML in xml.template )
			{
				const name:String = String( templateXML.@name );
				const key:String = String( templateXML.@key );
				const engine:String = String( templateXML.@engine );
				const chassis:String = String( templateXML.@chassis );
				const shield:String = String( templateXML.@shield );
				const sensor:String = String( templateXML.@sensor );
				const armor:String = String( templateXML.@armor );
				const controller:String = String( templateXML.@controller );
				const destructionAsset:String = String( templateXML.@destructionAsset );
				const destructionShake:String = String( templateXML.@destructionShake );
				const destructionFlash:String = String( templateXML.@destructionFlash );
				const minorHitAsset:String = String( templateXML.@minorHitAsset );
				const difficultyLevel:int = int( templateXML.@difficultyLevel );
				const evasion:String = String( templateXML.@evasion );
				
				if ( !name || !engine || !chassis || !armor || !controller )
				{
					Debug.errorLog( GameLogChannels.DATA, "Invalid ship template for " + name + " check names of components" );
					continue;
				}
				
				const chassisComp:ChassisInfo = _ChassisComponents[ chassis ];
				const shieldComp:ShieldInfo = _ShieldComponents[ shield ];
				const engineComp:EngineInfo = _EngineChomponents[ engine ];
				const armorComp:ArmorInfo = _ArmorComponents[ armor ];
				const sensorComp:SensorInfo = _SensorsComponent[ sensor ];
				const evasionComp:EvasionInfo = _EvasionComponents[ evasion ];
				
				if ( !chassisComp || !engineComp || !armorComp || !sensorComp )
				{
					Debug.errorLog( GameLogChannels.DATA, "Invalid ship template for " + name + " could not find one of the component values. Check to make sure they all exist." );
					continue;
				}
				
				if ( evasion && !evasionComp )
				{
					Debug.errorLog( GameLogChannels.DATA, "Invalid evasion system " + evasion + " for template " + name );
					continue;
				}
				
				var combatSystems:Dictionary = new Dictionary();
				var combatSystemsCost:int = 0;
				
				// Combat system templates
				for each ( var xmlAttributes:XML in templateXML.attributes() )
				{
					const attName:String = xmlAttributes.name();	// active0
					const className:String = attName.substr( 0, 6 );
					const numeric:int = int( attName.substr( 6 ) );
					
					if ( className == "active" )
					{
						combatSystems[ numeric ] = String( xmlAttributes );
						combatSystemsCost = _GameData.InfoCatalog.getCost( String( xmlAttributes.@name ) );
					}
					
					if ( numeric > _MostHardpoints )
					{
						_MostHardpoints = numeric;
					}
				}
				
				var info:SpaceShipInfo = new SpaceShipInfo( key, name, chassisComp, engineComp, shieldComp, armorComp, sensorComp, controller, destructionAsset, destructionShake, destructionFlash, minorHitAsset, difficultyLevel, evasionComp, combatSystems );
				
				_ShipTemplates[ key ] = info;
				_CombatSystemTemplates[ key ] = combatSystems;
				
				const templateCost:int = _GameData.InfoCatalog.getShipInfoCost( info );
				const totalCost:int = templateCost + combatSystemsCost;
				
				_GameData.InfoCatalog.setTemplateCost( key, totalCost );
			}
			
			// Platforms
			xml = _GameData.AssetManager.getXML( "platform_config" );
			
			for each ( var platformXML:XML in xml.platform )
			{
				var platform:PlatformInfo = new PlatformInfo();
				platform.deserialize( platformXML );
				
				_Platforms[ platform.ShipKey ] = platform;
			}
		}
		
		public function getShipInfoTonnage( ship:SpaceShipInfo ):int
		{
			var tonnage:int = 0;
			
			tonnage += ship.Armor.Tonnage;
			tonnage += ship.Engine.Tonnage;

			if ( ship.Evasion )
				tonnage += ship.Evasion.Tonnage;
				
			tonnage += ship.Sensor.Tonnage;
			
			if ( ship.Shield )
				tonnage += ship.Shield.Tonnage;
			
			for each ( var combatSystem:String in ship.CombatSystems )
			{
				var systemInfo:CombatSystemInfo = getCombatSystem( combatSystem );
				
				if ( systemInfo == null )
				{
					continue;
				}
				
				tonnage += systemInfo.Tonnage;
			}
			
			return tonnage;
		}
		
		public function getMostHardpoints():Number
		{
			return _MostHardpoints;
		}
		
		public function getChassisMap( key:String ):ChassisMap
		{
			return _ChassisMapping[ key ];
		}
		
		public function getEngine( key:String ):EngineInfo
		{
			return _EngineChomponents[ key ];
		}
		
		public function getShipTemplate( key:String ):SpaceShipInfo
		{
			return _ShipTemplates[ key ];
		}
		
		public function getShield( key:String ):ShieldInfo
		{
			return _ShieldComponents[ key ];
		}
		
		//public function getActiveComponent( key:String )
		
		public function getChassis( key:String ):ChassisInfo
		{
			return _ChassisComponents[ key ];
		}
		
		public function getSensor( key:String ):SensorInfo
		{
			return _SensorsComponent[ key ];
		}
		
		public function getArmor( key:String ):ArmorInfo
		{
			return _ArmorComponents[ key ];
		}
		
		public function getCombatSystemTemplates( key:String ):Dictionary
		{
			return _CombatSystemTemplates[ key ];
		}
		
		public function getCombatSystem( key:String ):CombatSystemInfo
		{
			return _CombatSystems[ key ];
		}
		
		public function getPlatform( shipKey:String ):PlatformInfo
		{
			return _Platforms[ shipKey ];
		}
		
		public function getEvasion( key:String ):EvasionInfo
		{
			return _EvasionComponents[ key ];
		}
	}
}