package game.data 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class StaticData 
	{
		private var _LevelMap:Dictionary;	// maps levels to expected player wealth
		
		public function GameData() 
		{
			
		}
		
		public function deserialize( xml:XML ):void
		{
			
		}
		
		public function get LevelMap:Dictionary { return _LevelMap; }
	}
}