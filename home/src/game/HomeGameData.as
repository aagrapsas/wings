package game 
{
	import engine.assets.AdvancedAssetManager;
	import engine.assets.AnimationManager;
	import engine.audio.SoundManager;
	import engine.interfaces.IGameData;
	import engine.misc.EngineProperties;
	import engine.misc.EventBus;
	import engine.misc.LocalStorage;
	import engine.misc.PoolManager;
	import engine.misc.XMLValidatorManager;
	import engine.render.Camera;
	import engine.render.scene.SceneManager;
	import flash.utils.Dictionary;
	import game.actors.SpaceShip;
	import game.builders.ShipSpawner;
	import game.catalogue.InfoCatalogue;
	import game.catalogue.ShipComponentCatalogue;
	import game.controllers.PlayerSpaceShipController;
	import game.data.PlayerData;
	import game.fx.WorldFXManager;
	import game.gameplay.waypoint.Waypoint;
	import game.gameplay.waypoint.WaypointDir;
	import game.scenarios.GameMode;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class HomeGameData implements IGameData
	{
		// IGameData
		private var _AssetSystem:AdvancedAssetManager;
		//private var _DebugSystem:DebugManager;
		private var _Scene:SceneManager;
		private var _ActiveCamera:Camera;
		private var _AllowInput:Boolean;
		private var _SoundManager:SoundManager;
		private var _InputMap:Dictionary;
		private var _AnimationManager:AnimationManager;
		private var _Pools:PoolManager;
		private var _EngineProperties:EngineProperties;
		private var _EngineEvents:EventBus;
		
		// HomeGame
		public var CurrentScenario:Scenario;
		public var Spawner:ShipSpawner
		public var PlayerShip:SpaceShip;
		public var PlayerController:PlayerSpaceShipController;
		public var GameConfigData:Dictionary;
		public var ShipCompCatalog:ShipComponentCatalogue;
		public var InfoCatalog:InfoCatalogue;
		public var CurrentGameMode:GameMode;
		public var ClientData:PlayerData;
		public var XMLValidators:XMLValidatorManager;
		public var WorldFX:WorldFXManager;
		public var LocalDatabase:LocalStorage;
		
		public var GameEvents:EventBus;
		
		// Waypoint data
		public var PlayerWaypoint:Waypoint;			// @TODO: Consider how to encapsulate this better
		public var PlayerWaypointDir:WaypointDir;
		
		public function HomeGameData() 
		{
			_AllowInput = true;
		}
		
		// IGameData
		public function get AssetManager():AdvancedAssetManager { return _AssetSystem; }
		public function set AssetManager( value:AdvancedAssetManager ):void { _AssetSystem = value; }
		
		public function get SoundSystem():SoundManager { return _SoundManager; }
		public function set SoundSystem( value:SoundManager ):void { _SoundManager = value; }
		
		public function get Scene():SceneManager { return _Scene; }
		public function set Scene( value:SceneManager ):void { _Scene = value; }
		
		public function get ActiveCamera():Camera { return _ActiveCamera; }
		public function set ActiveCamera( value:Camera ):void { _ActiveCamera = value; }
		
		public function get IsInputEnabled():Boolean { return _AllowInput; }
		public function set IsInputEnabled( value:Boolean ):void { _AllowInput = value; }
		
		public function get InputMap():Dictionary { return _InputMap; }
		public function set InputMap( value:Dictionary ):void { _InputMap = value; }
		
		public function get AnimationSystem():AnimationManager { return _AnimationManager; }
		public function set AnimationSystem( value:AnimationManager ):void { _AnimationManager = value; }
		
		public function get Pools():PoolManager { return _Pools; }
		public function set Pools( value:PoolManager ):void { _Pools = value; }
		
		public function get Properties():EngineProperties { return _EngineProperties; }
		public function set Properties( value:EngineProperties ):void { _EngineProperties = value; }
		
		public function get EngineEvents():EventBus { return _EngineEvents; }
		public function set EngineEvents( value:EventBus ):void { _EngineEvents = value; }
		// HomeGame
	}
}