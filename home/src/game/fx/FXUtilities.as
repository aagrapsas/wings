package game.fx 
{
	import engine.debug.Debug;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.gameplay.weapons.DamageTypes;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class FXUtilities 
	{
		public static function playExplosion( damageType:String, instigator:SpaceShip, x:int, y:int, gameData:HomeGameData, damage:int ):void
		{
			var asset:String = "";
			var scale:Number = 1.0;
			var rotation:Number = 0;
			
			const debris_threshold:Number = gameData.GameConfigData[ "debris_only_threshold" ];
			
			const percent:Number = ( damage / instigator.Info.Armor.FullHitPoints );
			
			if ( percent <= debris_threshold && damageType != DamageTypes.MISSILE )
			{
				x += 40;
				
				
				var dir:Point = new Point( x - instigator.X, y - instigator.Y );
				dir.normalize( 1 );
				
				rotation = MathUtility.getAngle( 0, 1, dir.x, dir.y ) * ( Math.PI / 180 );
				
				// Debug.debugLog( GameLogChannels.DEBUG, "Rotation: " + rotation );
				
				asset = "human_armor_debris";
				
				if ( instigator.Info.MinorHitAsset )
					asset = instigator.Info.MinorHitAsset;
			}
			else
			{
				const random:int = Math.random() * 2;
				const minExplosion:Number = gameData.GameConfigData[ "min_explosion_scale" ];
				const maxExplosionPercent:Number = gameData.GameConfigData[ "max_explosion_damage_percent" ];
				const scalePercent:Number = percent / maxExplosionPercent;
				
				scale = Math.max( Math.min( scalePercent, 1.0 ), minExplosion );
			
				if ( damageType == DamageTypes.MISSILE )
				{
					asset = "missile_explosion";
				}
				else if ( random == 0 )
				{
					asset = "single_explosion";
				}
				else
				{
					asset = "multi_explosion";
				}
			}
			
			gameData.WorldFX.createFX( asset, x, y, scale );
		}
		
		public static function playShieldExplosion( x:int, y:int, gameData:HomeGameData, damage:int, fullShield:int ):void
		{
			const name:String = "shield_explosion";
			
			const minExplosion:Number = gameData.GameConfigData[ "min_explosion_scale" ];
			const maxExplosionPercent:Number = gameData.GameConfigData[ "max_explosion_damage_percent" ];
			const percent:Number = ( damage / fullShield ) / maxExplosionPercent;
			
			const scale:Number = Math.max( Math.min( percent, 1.0 ), minExplosion );
			
			gameData.WorldFX.createFX( name, x, y, scale );
		}
	}
}