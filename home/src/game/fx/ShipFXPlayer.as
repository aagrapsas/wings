package game.fx 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import engine.render.animation.Animation;
	import engine.render.animation.IAnimated;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	import starling.display.DisplayObject;
	import starling.display.Image;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipFXPlayer implements ITickable, IDestroyable
	{
		private var _Ship:SpaceShip;
		private var _GameData:HomeGameData;
		private var _ActiveFX:Vector.<IAnimated>;
		private var _FXToFree:Vector.<IAnimated>;
		
		private var _CachedPoint:Point;
		
		private var _ShieldFX:Image;
		private var _ShieldFXAccumulator:Number = 0;
		private static const SHIELD_DELAY:Number = 0.50;
		
		public function ShipFXPlayer( ship:SpaceShip, gameData:HomeGameData ) 
		{
			_Ship = ship;
			_GameData = gameData;
			
			_Ship.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_GameData.Scene.addTickable( this );
			
			_ActiveFX = new Vector.<IAnimated>();
			_FXToFree = new Vector.<IAnimated>();
			
			_CachedPoint = new Point();
		}
		
		private function onShipDestroyed( e:ShipEvent ):void
		{
			_Ship.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
		}
		
		public function tick( deltaTime:Number ):void
		{
			for each ( var activeFX:IAnimated in _ActiveFX )
			{
				activeFX.tick( deltaTime );
			}
			
			for each ( var fxToFree:IAnimated in _FXToFree )
			{
				trace( "Removing FX" );
				_ActiveFX.splice( _ActiveFX.indexOf( fxToFree ), 1 );
			}
			
			_FXToFree.splice( 0, _FXToFree.length );
			
			if ( _ShieldFX && _ShieldFX.alpha > 0 )
			{
				_ShieldFXAccumulator += deltaTime;
				var alpha:Number = Math.min( _ShieldFXAccumulator / SHIELD_DELAY, 1.0 );
				
				_ShieldFX.alpha = MathUtility.easeOut( 1, 0, alpha );
			}
		}
		
		public function playFX( type:String, x:int, y:int, amount:int, max:int ):void
		{
			if ( type == "shield" )
			{
				if ( _ShieldFX == null )
				{
					var fxName:String = null;
					
					if ( _Ship.Info.Chassis.isHuman )
					{
						fxName = "human_shield_fx";
					}
					else
					{
						fxName = "alien_shield_fx";
					}
					
					_ShieldFX = _GameData.AssetManager.getImageCopy( fxName );
					
					var proportion:Number = _Ship.Display.cachedHeight > _Ship.Display.cachedWidth ? _Ship.Display.cachedHeight : _Ship.Display.cachedWidth;
					
					_ShieldFX.width = _Ship.Width + 20;// proportion + 20;
					_ShieldFX.height = _Ship.Height + 20;// proportion + 20;
					
					_Ship.Display.addChild( _ShieldFX );
					_ShieldFX.alpha = 1;
					_ShieldFXAccumulator = 0;
					
					_ShieldFX.x = ( _Ship.Display.cachedWidth / 2 ) - ( _ShieldFX.width / 2 );
					_ShieldFX.y = ( _Ship.Display.cachedHeight / 2 ) - ( _ShieldFX.height / 2 );
				}
				
				_ShieldFXAccumulator = 0;
				_ShieldFX.alpha = 1;
				
				const minExplosion:Number = _GameData.GameConfigData[ "min_shield_scale" ];
				const maxExplosionPercent:Number = _GameData.GameConfigData[ "max_explosion_damage_percent" ];
				const percent:Number = ( amount / max ) / maxExplosionPercent;
			
				const scale:Number = Math.max( Math.min( percent, 1.0 ), minExplosion );
				
				if ( _Ship.Info.Chassis.isHuman )
				{
					playShieldRipple( x, y, scale, "human_shield_ripple" );
				}
				else
				{
					playShieldRipple( x, y, scale, "alien_shield_ripple" );
				}
			}
		}
		
		private function playShieldRipple( x:int, y:int, scale:Number, type:String ):void
		{
			var fx:ShieldRippleFX = _GameData.Pools.getPool( type ).pop() as ShieldRippleFX;
			
			if ( !fx )
			{
				Debug.debugLog( EngineLogChannels.ASSETS, "Unable to allocate new ShieldRippleFX" );
				return;
			}
			
			const left:Number = _Ship.X + 
			
			fx.playOnce( true );
			
			fx.registerFinishCallback( freeFX );
			
			_ActiveFX.push( fx );
			
			// Convert from world-space to ship-space
			const offsetX:Number = _Ship.X - x + ( _Ship.Display.cachedWidth / 2 );
			const offsetY:Number = -( _Ship.Y - y - ( _Ship.Display.cachedHeight / 2 ) );
			
			fx.scaleX = scale;
			fx.scaleY = scale;
			
			_CachedPoint.x = offsetX - ( ( fx.width * scale ) / 2 );
			_CachedPoint.y = offsetY - ( ( fx.height * scale ) / 2 );
			
			_CachedPoint = _Ship.Display.matrix.transformPoint( _CachedPoint );
			
			fx.x = _CachedPoint.x;
			fx.y = _CachedPoint.y;
			
			trace( "x: " + fx.x + " y " + fx.y );
			
			var dir:Point = new Point( ( _Ship.Display.cachedWidth / 2 ) - fx.x, ( _Ship.Display.cachedHeight / 2 ) - fx.y );
			dir.normalize( 1 );
			
			// determine rotation
			var rotation:Number = MathUtility.getAngle( 0, 1, dir.x, dir.y );
			
			fx.rotation = rotation;
			
			//trace( "Hit at: " + x + "/" + y + " anim at " + anim.x + " / " + anim.y );
			
			_Ship.Display.addChild( fx );
			
			trace( "ADDING FX" );
		}
		
		private function freeFX( anim:IAnimated ):void
		{			
			_FXToFree.push( anim );
			
			if ( _Ship )
			{
				_Ship.Display.removeChild( anim as DisplayObject );
			}
			
			anim.destroy();	// destroy also returns the animation back to the pool
		}
		
		public function destroy():void
		{
			if ( _Ship )
			{
				_Ship.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
				
				// Remove any waiting removal
				for each ( var animRem:IAnimated in _FXToFree )
				{
					_Ship.Display.removeChild( animRem as DisplayObject );
					
					_ActiveFX.splice( _ActiveFX.indexOf( animRem ), 1 );
				}
			
				// Remove others
				for each ( var anim:IAnimated in _ActiveFX )
				{
					_Ship.Display.removeChild( anim as DisplayObject );
					anim.destroy();
				}
			}
			
			_ActiveFX = null;
			_FXToFree = null;
			
			_Ship = null;
			
			_GameData.Scene.removeTickable( this );
			
			_GameData = null;
		}
	}
}