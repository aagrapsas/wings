package game.fx 
{
	import engine.misc.GenericPool;
	import engine.misc.MathUtility;
	import engine.render.animation.IAnimated;
	import game.HomeGameData;
	import starling.display.Image;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShieldRippleFX extends Sprite implements IAnimated
	{
		private var _Image:Image;
		private var _Pool:GenericPool;
		
		private var _IsAlive:Boolean = false;
		
		private var _Accumulator:Number = 0;
		
		private var _GameData:HomeGameData;
		
		private var _Callback:Function;
		
		private static const START_SCALE:Number = 0.5;
		private static const END_SCALE:Number = 1.0;
		
		private static const DURATION:Number = 0.75;
		
		public function ShieldRippleFX( gameData:HomeGameData, asset:String ) 
		{
			_GameData = gameData;
			
			_Image = _GameData.AssetManager.getImageCopy( asset );
			
			this.addChild( _Image );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _IsAlive == false )
			{
				return;
			}
			
			_Accumulator += deltaTime;
			
			var alpha:Number = Math.min( _Accumulator / DURATION, 1.0 );
			
			_Image.alpha = MathUtility.easeOut( 1, 0, alpha );
			_Image.scaleX = MathUtility.easeOut( START_SCALE, END_SCALE, alpha );
			_Image.scaleY = MathUtility.easeOut( START_SCALE, END_SCALE, alpha );
			
			if ( _Image.alpha == 0 )
			{
				_Callback.apply( null, [this] );
				
				_Pool.push( this );
			}
		}
		
		public function registerFinishCallback( callback:Function ):void
		{
			_Callback = callback;
		}
		
		public function playOnce( shouldInterrupt:Boolean ):void
		{
			_Accumulator = 0;
		}
		
		// Poolable
		public function resurrect():void
		{
			if ( _IsAlive )
			{
				return;
			}
			
			_Image.scaleX = START_SCALE;
			_Image.scaleY = START_SCALE;
			_Image.alpha = 1;
			_Accumulator = 0;
			
			_GameData.Scene.addTickable( this );
			
			_IsAlive = true;
		}
		
		public function suspend():void
		{
			if ( !_IsAlive )
			{
				return;
			}
			
			_Callback = null;
			
			_GameData.Scene.removeTickable( this );
			
			_IsAlive = false;
		}
		
		public function set pool( value:GenericPool ):void
		{
			_Pool = value;
		}
		
		public function destroy():void
		{
			suspend();
		}
	}
}