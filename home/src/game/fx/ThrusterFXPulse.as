package game.fx 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ThrusterFXPulse extends Sprite implements ITickable, IDestroyable
	{		
		private var _GameData:HomeGameData;
		private var _Parent:SpaceShip;
		
		private var _AlphaStep:Number = 0.08;
		private var _DecelStep:Number = 0.12;

		private var _IsAccelerating:Boolean = false;
		
		public function ThrusterFXPulse( parent:SpaceShip, gameData:HomeGameData, asset:String, offsets:Vector.<Point> ) 
		{
			_Parent = parent;
			_GameData = gameData;
			
			if ( asset )
				build( asset, offsets );
		}
		
		private function build( asset:String, offsets:Vector.<Point> ):void
		{		
			_GameData.Scene.addTickable( this );
			
			const bitmap:Bitmap = _GameData.AssetManager.getImage( asset ) as Bitmap;
			const scale:Number = _Parent.Info.Chassis.ExhaustScale > 0 ? _Parent.Info.Chassis.ExhaustScale : 1;
			
			var texture:Texture = Texture.fromBitmap( bitmap );
			
			for each ( var offsetDraw:Point in offsets )
			{
				var image:Image = new Image( texture );
				
				image.scaleX = scale;
				image.scaleY = scale;
				
				image.x = offsetDraw.x;
				image.y = offsetDraw.y;
				
				this.addChild( image );
			}
			
			_Parent.Display.addChild( this );
			
			this.alpha = 0;
		}
		
		public function update( isAccelerating:Boolean ):void
		{
			_IsAccelerating = isAccelerating;
		}
		
		public function tick( deltaTime:Number ):void
		{			
			if ( _IsAccelerating )
			{
				this.alpha = Math.min( this.alpha + _AlphaStep, 1 );
			}
			else
			{
				this.alpha = Math.max( this.alpha - _DecelStep, 0 );
			}
		}
		
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );
		}
	}
}