package game.fx 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.events.VideoEvent;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class HyperspaceFX implements ITickable, ISceneNotifiable, IDestroyable
	{
		private var _TargetX:Number;
		private var _TargetY:Number;
		private var _StartX:Number;
		private var _StartY:Number;
		private var _Effected:SpaceShip;
		private var _GameData:HomeGameData;
		private var _DirX:Number;
		private var _DirY:Number;
		
		private var _TimeAccumulator:Number = 0;
		
		private const TRANSITION_TIME:Number = 2;
		private const SPAWN_DISTANCE:int = 2000;
		
		private var _IsJumpingIn:Boolean = false;
		
		private var _Callback:Function;
		
		public function HyperspaceFX( gameData:HomeGameData, effected:SpaceShip, dirX:Number, dirY:Number )
		{
			_GameData = gameData;
			_Effected = effected;
			
			_DirX = dirX;
			_DirY = dirY;
			
			initialize();
		}
		
		private function initialize():void
		{
			_GameData.Scene.addTickable( this );
			
			_Effected.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDeath );
			
			_Effected.faceToward( _DirX, _DirY );
		}
		
		public function jumpIn( targetX:Number, targetY:Number, callback:Function ):void
		{
			_TargetX = targetX;
			_TargetY = targetY;
			
			_StartX = _TargetX - ( _DirX * SPAWN_DISTANCE );
			_StartY = _TargetY - ( _DirY * SPAWN_DISTANCE );
			
			_Effected.X = _StartX;
			_Effected.Y = _StartY;
			
			_Callback = callback;
			
			_IsJumpingIn = true;
			
			_GameData.SoundSystem.playSound( "HyperspaceJumpSound" );
		}
		
		public function jumpOut( callback:Function ):void
		{
			_StartX = _Effected.X;
			_StartY = _Effected.Y;
			
			_TargetX = _StartX + ( _DirX * SPAWN_DISTANCE );
			_TargetY = _StartY + ( _DirY * SPAWN_DISTANCE );
			
			_Callback = callback;
			
			_IsJumpingIn = false;
			
			_GameData.SoundSystem.playSound( "HyperspaceJumpSound" );
		}
		
		public function handleSceneTransition():void
		{
			destroy();
		}
		
		private function onShipDeath( e:ShipEvent ):void
		{
			destroy();
		}
		
		public function tick( deltaTime:Number ):void
		{
			_TimeAccumulator += deltaTime;
			
			const alpha:Number = Math.min( _TimeAccumulator / TRANSITION_TIME, 1 );
			
			if ( _IsJumpingIn )
			{
				_Effected.X = MathUtility.easeInInt( _StartX, _TargetX, alpha );
				_Effected.Y = MathUtility.easeInInt( _StartY, _TargetY, alpha );
			}
			else
			{
				_Effected.X = MathUtility.easeOutInt( _StartX, _TargetX, alpha );
				_Effected.Y = MathUtility.easeOutInt( _StartY, _TargetY, alpha );
			}
			
			if ( alpha >= 1 )
			{
				if ( _Callback != null )
				{
					_Callback.apply( null, [ _Effected ] );
				}
				
				destroy();
			}
		}
		
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );
			
			_Effected.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDeath );
		}
	}
}