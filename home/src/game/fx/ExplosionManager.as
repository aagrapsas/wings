package game.fx 
{
	import engine.Actor;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneLayer;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// DEPRECATED
	 
	public class ExplosionManager implements ITickable, IDestroyable, ISceneNotifiable
	{
		private var _GameData:HomeGameData;
		
		private var _ExplosionData:Vector.<BitmapData>;
		private var _ExplosionParticleData:Vector.<BitmapData>;
		
		private var _ExplosionPool:Vector.<DamageFX>;
		private var _InUseExplosions:Vector.<DamageFX>;
		
		public function ExplosionManager( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			_ExplosionData = new Vector.<BitmapData>;
			_ExplosionParticleData = new Vector.<BitmapData>;
			_ExplosionPool = new Vector.<DamageFX>;
			_InUseExplosions = new Vector.<DamageFX>;
			
			gameData.Scene.addTickable( this );
			
			initialize();
		}	
		
		// @TODO: make this better
		private function initialize():void
		{
			// load related images
			for ( var i:int = 1; i < 7; i++ )
			{
				var expBitmap:Bitmap = _GameData.AssetManager.getImage( "explosion" + i ) as Bitmap;
				_ExplosionData.push( expBitmap.bitmapData );
			}
			
			for ( var j:int = 1; j < 6; j++ )
			{
				var partBitmap:Bitmap = _GameData.AssetManager.getImage( "exp-particle" + j ) as Bitmap;
				_ExplosionParticleData.push( partBitmap.bitmapData );
			}
			
			for ( var k:int = 0; k < 10; k++ )
			{
				createExplosion();
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			for each ( var explosion:DamageFX in _InUseExplosions )
			{
				explosion.tick( deltaTime );
			}
		}
		
		private function createExplosion():void
		{
			var explosion:DamageFX = new DamageFX( this, _ExplosionData, _ExplosionParticleData );
			
			_ExplosionPool.push( explosion );
		}
		
		public function explode( x:int, y:int, shouldShowDebris:Boolean = false ):void
		{
			if ( _ExplosionPool.length == 0 )
			{
				createExplosion();
			}
			
			var explosiveIndex:int = _ExplosionPool.length * Math.random();
			var explosion:DamageFX = _ExplosionPool[ explosiveIndex ];
			_ExplosionPool.splice( explosiveIndex, 1 );
			explosion.x = x;
			explosion.y = -y;
			
			//_GameData.Scene.Layers[ "objects" ].SceneDisplay.addChild( explosion );
						
			explosion.explode( shouldShowDebris );
			
			_InUseExplosions.push( explosion );
		}
		
		public function uberFinaleExplosion( actor:Actor ):void
		{
			var baseX:int = actor.X - ( actor.Dir.x * actor.Height / 2 );
			var baseY:int = actor.Y - ( actor.Dir.y * actor.Height / 2 );
			
			var amount:int = actor.Width * actor.Height / 2116;
			amount *= 3;
			
			// find perpendicular
			var perpX:int = -actor.Dir.x;
			var perpY:int = actor.Dir.y;
			
			for ( var i:int = 0; i < amount; i++ )
			{
				var centerOffset:int = Math.random() * actor.Height;
				var widthOffsetX:int = ( Math.random() > 0.5 ? -1 : 1 ) * Math.random() * actor.Width / 2;
				var widthOffsetY:int = ( Math.random() > 0.5 ? -1 : 1 ) * Math.random() * actor.Width / 2;
				
				explode( baseX + widthOffsetX + actor.Dir.x * centerOffset, baseY + widthOffsetY + actor.Dir.y * centerOffset, true );
			}
		}
		
		public function freeExplosion( explosion:DamageFX ):void
		{
			var index:int = _InUseExplosions.indexOf( explosion );
			
			if ( index > -1 )
			{
				_ExplosionPool.push( explosion );
				
				_InUseExplosions.splice( index, 1 );
				
				// Layer may not exist if we're in the middle of a transition
				var layer:SceneLayer = _GameData.Scene.Layers[ "objects" ];
				if ( layer && layer.SceneDisplay.contains( explosion ) )
					layer.SceneDisplay.removeChild( explosion );	// @TODO: we can keep these alpha'd out, possibly? would that be more effective?
				
				explosion.reset();
			}
		}

		public function handleSceneTransition():void
		{
			// Free all explosions immediately
			var toFree:Vector.<DamageFX> = new Vector.<DamageFX>();
			
			for each ( var fx:DamageFX in _InUseExplosions )
			{
				toFree.push( fx );
			}
			
			for each ( var fxFree:DamageFX in toFree )
			{
				freeExplosion( fxFree );
			}
		}
		
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );
			_ExplosionData = null;
			_ExplosionParticleData = null;
			_ExplosionPool = null;
			_GameData = null;
		}
	}
}