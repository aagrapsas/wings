package game.fx 
{
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.render.animation.Animation;
	import engine.render.CameraShakeInfo;
	import flash.utils.Dictionary;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WorldFXManager implements ITickable, IDestroyable
	{	
		private var _GameData:HomeGameData;
		
		private var _ActiveFX:Vector.<Animation>;
		
		private var _ScreenFlashes:Dictionary;
		private var _ScreenFlashDistances:Dictionary;
		private var _ScreenShakes:Dictionary;
		private var _ScreenShakeDistances:Dictionary;
		
		public function WorldFXManager( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			_GameData.Scene.addTickable( this );
			
			_ActiveFX = new Vector.<Animation>();
			_ScreenFlashes = new Dictionary();
			_ScreenShakes = new Dictionary();
			_ScreenShakeDistances = new Dictionary();
			_ScreenFlashDistances = new Dictionary();
			
			deserialize();
		}	
		
		private function deserialize():void
		{
			var xml:XML = _GameData.AssetManager.getXML( "ship_destruction_fx" );
			
			for each ( var xmlFlash:XML in xml.screen_flash )
			{
				_ScreenFlashes[ String( xmlFlash.@key ) ] = Number( xmlFlash.@duration );
				_ScreenFlashDistances[ String( xmlFlash.@key ) ] = int( xmlFlash.@distance );
			}
			
			for each ( var xmlShake:XML in xml.screen_shake )
			{
				const key:String = String( xmlShake.@key );
				const distance:int = int( xmlShake.@distance );
				
				var shakes:Vector.<CameraShakeInfo> = new Vector.<CameraShakeInfo>();
				
				for each ( var xmlShakeInfo:XML in xmlShake.shake )
				{
					const mag:int = int( xmlShakeInfo.@mag );
					const duration:Number = Number( xmlShakeInfo.@duration );
					const number:int = int( xmlShakeInfo.@number );
					
					shakes.push( new CameraShakeInfo( mag, duration, number ) );
				}
				
				_ScreenShakes[ key ] = shakes;
				_ScreenShakeDistances[ key ] = distance;
			}
		}
		
		public function createFX( type:String, x:int, y:int, scale:Number = 1.0, rotation:Number = 0 ):void
		{
			if ( !type )
				return;
			
			var anim:Animation = _GameData.Pools.getPool( type ).pop() as Animation;
			
			if ( !anim )	// happens if we've hit the pool's capacity
				return;
			
			anim.playOnce( true );
			
			// Setup listener for last frame
			anim.registerFinishCallback( freeFX );
			
			_ActiveFX.push( anim );
			
			anim.scaleX = scale;
			anim.scaleY = scale;
			
			anim.x = x;
			anim.y = -y;	// flipped y coord
			
			anim.rotation = rotation;
			
			_GameData.Scene.addWorldObject( anim );
		}
		
		public function screenShake( key:String, instigator:SpaceShip ):void
		{
			if ( !key || !_ScreenShakeDistances[ key ] || !_GameData.ActiveCamera )
				return;
			
			var maxDist:int = _ScreenShakeDistances[ key ];
			maxDist *= maxDist;
			
			const deltaX:int = _GameData.ActiveCamera.X - instigator.X;
			const deltaY:int = _GameData.ActiveCamera.Y - instigator.Y;
			
			const dist:int = deltaX * deltaX + deltaY * deltaY;
			
			if ( dist > maxDist )
				return;
				
			var shake:Vector.<CameraShakeInfo> = _ScreenShakes[ key ];
			
			if ( !shake )
			{
				Debug.errorLog( GameLogChannels.DATA, key + " is not a valid screen shake fx!" );
				return;
			}
			
			if ( _GameData.ActiveCamera )
			{
				_GameData.ActiveCamera.multiShake( shake );
			}
		}
		
		public function screenFlash( key:String, instigator:SpaceShip ):void
		{
			if ( !key || !_ScreenFlashDistances[ key ] || !_GameData.ActiveCamera )
				return;
				
			var maxDist:int = _ScreenFlashDistances[ key ];
			maxDist *= maxDist;
			
			const deltaX:int = _GameData.ActiveCamera.X - instigator.X;
			const deltaY:int = _GameData.ActiveCamera.Y - instigator.Y;
			
			const dist:int = deltaX * deltaX + deltaY * deltaY;
			
			if ( dist > maxDist )
				return;
				
			_GameData.Scene.Flash.flash( _ScreenFlashes[ key ] );
		}
		
		private function freeFX( anim:Animation ):void
		{
			_ActiveFX.splice( _ActiveFX.indexOf( anim ), 1 );
			
			_GameData.Scene.removeWorldObject( anim );
			
			anim.destroy();	// destroy also returns the animation back to the pool
		}
		
		public function tick( deltaTime:Number ):void
		{
			for each ( var activeFX:Animation in _ActiveFX )
			{
				activeFX.tick( deltaTime );
			}
		}
		
		/**
		 * Release references to all objects. Note: this does note free them to the pool! That must be done somewhere else!
		 */
		public function releaseAll():void
		{
			_ActiveFX = new Vector.<Animation>();
		}
		
		public function destroy():void
		{
			for each ( var anim:Animation in _ActiveFX )
			{
				anim.destroy();
			}
			
			_ActiveFX = null;
			
			_GameData.Scene.removeTickable( this );
			
			_GameData = null;
		}
	}
}