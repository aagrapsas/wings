package game.fx 
{
	import flash.display.Sprite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ThrusterFX extends Sprite
	{	
		private var _Thrusters:Vector.<ShipExhaust>;
		
		public function ThrusterFX( offsets:Vector.<Point> ) 
		{		
			initialize(  offsets );
		}
		
		private function initialize( offsets:Vector.<Point> ):void
		{
			_Thrusters = new Vector.<ShipExhaust>;
			
			for each ( var point:Point in offsets )
			{
				var thruster:ShipExhaust = new ShipExhaust;
				_Thrusters.push( thruster );
				this.addChild( thruster );
				thruster.x = point.x;
				thruster.y = point.y;
			}
		}
		
		public function speed( amount:Number ):void
		{
			for each ( var thruster:ShipExhaust in _Thrusters )
			{
				thruster.speed( amount );
			}
		}
		
		public function angle( amount:Number ):void
		{
			for each ( var thruster:ShipExhaust in _Thrusters )
			{
				thruster.angle( amount );
			}
		}
		
		public function off():void
		{
			for each ( var thruster:ShipExhaust in _Thrusters )
			{
				thruster.off();
			}
		}
	}
}

import com.greensock.TweenLite;
import flash.display.GradientType;
import flash.display.Sprite;
import flash.geom.Matrix;

// Helper class
class ShipExhaust extends Sprite
{
	private var _PlumeD:Sprite;
	private var _PlumeE:Sprite;
	private var _CurrentThrust:Number;
	
	private static const TIME_INCREMENT:Number = 0.15;
	private static const MAX_PLUME_HORIZONTAL:Number = 10.0;
	private static const MAX_PLUME_VERTICAL:Number = 10.0;
	
	private var _IsTransitioning:Boolean = false;
	
	public function ShipExhaust()
	{
		this.alpha = 0;
		
		buildPlume();
	}
	
	private function buildPlume():void
	{
		_PlumeD = new Sprite;
		_PlumeE = new Sprite;
		
		makeGradientCircle( _PlumeD, 10, 0x4993E6, 15, 10 );
		makeGradientCircle( _PlumeE, 5, 0xFFFFFF, 13, 9 );
		
		_PlumeE.y = 2;
		
		this.addChild( _PlumeD );
		this.addChild( _PlumeE );
	}
	
	private function makeGradientCircle( sprite:Sprite, radius:uint, color:uint, width:uint, height:uint ):void
	{
		var matrix:Matrix = new Matrix;
		matrix.createGradientBox( radius * 2, radius * 2, 0, -radius, -radius );
		sprite.graphics.beginGradientFill( GradientType.RADIAL, [color, color, color], [1, 0.75, 0], [100, 155, 255], matrix );
		sprite.graphics.drawCircle( 0, 0, radius );	
		sprite.width = width;
		sprite.height = height;
	}
	
	public function off():void
	{
		//trace( "CALLING OFF" );
		TweenLite.killTweensOf( this );
		TweenLite.to( this, 1, { overwrite: true, rotation: 0, alpha: 0, height: 5, width: 5 } );
		_IsTransitioning = false;
	}
	
	public function angle( degrees:Number ):void
	{
		TweenLite.to( this, 1, { overwrite: false, rotation: -degrees } );
	}
	
	public function speed( amount:Number ):void
	{
		amount *= 10;
		//trace( "Amount: " + amount + " alpha: " + this.alpha );
		if ( amount > 0 )
		{
			if ( !_IsTransitioning )
			{
				TweenLite.killTweensOf( this );
				//trace( "Telling to get appropriate alpha" );
				TweenLite.to( this, 1, { overwrite: false, alpha:1 } );
				_IsTransitioning = true;
			}
			
			TweenLite.to( this, TIME_INCREMENT, { overwrite: false, width:Math.min( MAX_PLUME_HORIZONTAL, amount ), height: Math.min( MAX_PLUME_VERTICAL, amount ) } );
		}
		else
		{
			off();
		}
		
		_CurrentThrust = amount;
	}
}