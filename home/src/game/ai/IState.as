package game.ai 
{
	import engine.interfaces.IDestroyable;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IState extends IDestroyable
	{
		function enterState():void;
		function updateState( deltaTime:Number ):void;
		function leaveState():void;
	}
}