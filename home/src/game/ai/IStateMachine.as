package game.ai 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IStateMachine 
	{
		function goToState( state:String ):void;
		function setLocomotionState( state:IState ):void;
		function get SharedData():StateData;
		function get MainLocomotionState():IState;
	}
}