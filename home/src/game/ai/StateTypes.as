package game.ai 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class StateTypes 
	{		
		public static const ATTACK_STATE:String = "attack";
		public static const IDLE_STATE:String = "idle";
		public static const FIGHTER_LOCOMOTION_STATE:String = "fighter_loco";
		public static const CAPITAL_LOCOMOTION_STATE:String = "capital_loco";
		public static const FOLLOW_STATE:String = "follow";
	}
}