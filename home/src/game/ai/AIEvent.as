package game.ai 
{
	import starling.events.Event;
	import game.controllers.AIController;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AIEvent extends Event
	{
		public static const AI_DEATH:String = "ai_death";
		public var Owner:AIController;		
		
		public function AIEvent( type:String, owner:AIController ) 
		{
			Owner = owner;
			super( type );
		}
	}
}