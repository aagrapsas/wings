package game.ai 
{
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class StateFactory 
	{		
		public static function getState( state:String, owner:SpaceShip, gameData:HomeGameData, stateMachine:IStateMachine ):IState
		{
			switch ( state )
			{
				case StateTypes.ATTACK_STATE:
					return new AttackState( owner, gameData, stateMachine );
				case StateTypes.IDLE_STATE:
					return new IdleState( owner, gameData, stateMachine );
				case StateTypes.FIGHTER_LOCOMOTION_STATE:
					return new FighterLocomotionState( owner, gameData, stateMachine );
				case StateTypes.CAPITAL_LOCOMOTION_STATE:
					return new CapitalShipLocomotionState( owner, gameData, stateMachine );
			}
			
			return null;
		}	
	}
}