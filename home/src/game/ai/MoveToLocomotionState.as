package game.ai 
{
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.controllers.AIController;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class MoveToLocomotionState implements IState
	{
		private var _Owner:SpaceShip;
		private var _GameData:IGameData;
		private var _StateMachine:IStateMachine;
		private var _MoveX:int;
		private var _MoveY:int;
		private var _ShouldStopOnEnemy:Boolean;
		
		private var _MaxSpeed:int = 0;
		
		private static const SLOW_DISTANCE:int = 20;
		
		public function MoveToLocomotionState( owner:SpaceShip, gameData:IGameData, stateMachine:IStateMachine, x:int, y:int, shouldStopOnEnemey:Boolean, maxSpeed:int = 0 ) 
		{
			_MoveX = x;
			_MoveY = y;
			
			_GameData = gameData;
			_StateMachine = stateMachine;
			_Owner = owner;
			
			_ShouldStopOnEnemy = shouldStopOnEnemey;
			
			if ( maxSpeed > 0 )
				_MaxSpeed = maxSpeed;
			else
				_MaxSpeed = owner.Info.Engine.MaxSpeed;
		}
		
		public function enterState():void
		{
			
		}
		
		public function updateState( deltaTime:Number ):void
		{
			var lastSeenTarget:SpaceShip = _StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ];
			
			// If there's a target, break out of moveTo
			if ( lastSeenTarget && _ShouldStopOnEnemy )
			{
				_StateMachine.setLocomotionState( _StateMachine.MainLocomotionState );
			}
			else	// move to destination
			{
				const distX:Number = ( _Owner.X - _MoveX );
				const distY:Number = ( _Owner.Y - _MoveY );
				
				// @TODO: Optimize this to not use actual distance if needed
				const distance:Number = Math.sqrt( distX * distX + distY * distY );
				
				if ( distance <= SLOW_DISTANCE )
				{
					//const magnitude:Number = Math.sqrt( _Owner.VelocityX * _Owner.VelocityX + _Owner.VelocityY * _Owner.VelocityY );
					//const slowAmount:Number = magnitude / distance;
					//_Owner.VelocityX = Math.max( _Owner.VelocityX > 0 ? _Owner.VelocityX - slowAmount : _Owner.VelocityX + slowAmount, 0 );
					//_Owner.VelocityY = Math.max( _Owner.VelocityY > 0 ? _Owner.VelocityY - slowAmount : _Owner.VelocityY + slowAmount, 0 );
					_Owner.VelocityX = 0;
					_Owner.VelocityY = 0;
					
					_Owner.X += _MaxSpeed * _Owner.Dir.x;
					_Owner.Y += _MaxSpeed * _Owner.Dir.y;
					
					const newDistX:Number = ( _Owner.X - _MoveX );
					const newDistY:Number = ( _Owner.Y - _MoveY );
					const newDistanceSquared:Number = newDistX * newDistX + newDistY * newDistY;
					const oldDistanceSquared:Number = distance * distance;
					
					if ( newDistanceSquared > oldDistanceSquared )
					{
						_Owner.X = _MoveX;
						_Owner.Y = _MoveY;
						
						_StateMachine.setLocomotionState( _StateMachine.MainLocomotionState );
					}
				}
				else
				{
					var dir:Point = new Point( _MoveX - _Owner.X, _MoveY - _Owner.Y );
					dir.normalize( 1 );
					
					const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, _Owner.Dir.x, _Owner.Dir.y ) );
					
					_Owner.faceToward( dir.x, dir.y, _Owner.Info.Engine.MaxTurnSpeed );
					
					if ( angleToTarget <= 15 )
					{
						_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration;
						_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration;
					}
				}
				
				const currentMagSquared:Number = _Owner.VelocityX * _Owner.VelocityX + _Owner.VelocityY * _Owner.VelocityY;
				const maxMagSquared:Number = _MaxSpeed * _MaxSpeed;
				
				// @TODO: I'm not sure this math actually works...
				const ratio:Number = currentMagSquared / maxMagSquared;
				
				if ( currentMagSquared > maxMagSquared )
				{
					var normalized:Point = new Point( _Owner.VelocityX, _Owner.VelocityY );
					normalized.normalize( 1 );
					_Owner.VelocityX = normalized.x * _MaxSpeed;
					_Owner.VelocityY = normalized.y * _MaxSpeed;
				}
				
				_Owner.X += _Owner.VelocityX;	// @TODO: should scale by deltaTime
				_Owner.Y += _Owner.VelocityY;
				
				//trace( "Owner -- " + ( _StateMachine as AIController ).Name + " Owner x: " + _Owner.X + " Owner y: " + _Owner.Y + " with vx: " + _Owner.VelocityX + " vy: " + _Owner.VelocityY );
				//trace( "Owner: " + ( _StateMachine as AIController ).Name + " moving to: " + _MoveX + " / " + _MoveY );
			}
		}
		
		public function leaveState():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}
}