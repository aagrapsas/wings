package game.scenarios.modes 
{
	import engine.debug.Debug;
	import engine.misc.RandomSeed;
	import game.actors.SpaceShip;
	import game.actors.SpaceShipInfo;
	import game.ai.IState;
	import game.ai.StateFactory;
	import game.catalogue.DifficultyClassElement;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.scenarios.GameMode;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WaveSpawner 
	{		
		public function WaveSpawner() 
		{

		}
		
		public function spawnWave( gameData:HomeGameData, seed:RandomSeed, minShipValue:int, minValue:int, maxValue:int ):Vector.<AIController>
		{
			var returnShips:Vector.<AIController> = new Vector.<AIController>();
			var randomWaveValue:int = minValue + seed.getNext() * ( maxValue - minValue );
			var runningX:int = x;
			var runningY:int = y;
			
			while ( randomWaveValue > 0 && randomWaveValue > minShipValue )
			{
				// Figure out which ship to spawn of what value
				const randomShipValue:int = minShipValue + ( randomWaveValue - minShipValue );
				const possibleShips:Vector.<DifficultyClassElement> = gameData.InfoCatalog.DifficultyClassCatalog.getSmallestClosestDifficulty( randomShipValue );
				const randomShip:int = seed.getNext() * possibleShips.length;
				const ship:String = possibleShips[ randomShip ].Key;
				
				// Get ship info
				const shipInfo:SpaceShipInfo = gameData.ShipCompCatalog.getShipTemplate( ship );
				
				// Out
				if ( shipInfo == null )
				{
					Debug.debugLog( GameLogChannels.ERROR, "WaveSpawner: attempted to select ship of type " + ship + " and catalogue returned null" );
					continue;
				}
				
				// Reduce cost for next spawn
				randomWaveValue -= possibleShips[ randomShip ].Cost;
				
				// Spawn ship
				var ship:SpaceShip = gameData.Spawner.getShip( ship );
				ship.Team = GameMode.ENEMY_TEAM;
				
				var controller:AIController = new AIController( ship, gameData );
				var state:IState = StateFactory.getState( ship.Info.ControllerType, ship, gameData, controller );
				
				controller.setLocomotionState( state );
				controller.setMainLocomotionState( state );
				
				returnShips.push( controller );
			}
			
			return returnShips;
		}
	}
}