package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	// @name flash
	// @description flashes the screen
	// @category camera
	// @isConditional false
	// @param_required duration:Number
	public class FlashScript extends Script
	{
		private var _Duration:Number;
		
		public function FlashScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Duration = Number( xml.@duration );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.Scene.Flash.flash( _Duration );
			
			return true;
		}
	}

}