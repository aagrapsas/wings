package game.scenarios.scripting 
{
	import flash.utils.Dictionary;
	import game.controllers.AIController;
	import game.HomeGameData;
	//import game.ui.menu.UIMenuContainer;
	//import game.ui.misc.UIWinFailPopup;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EndGameScript extends Script
	{
		private var _VictoryAI:String;
		private var _Bonus:int = 0;
		
		public function EndGameScript()
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_VictoryAI = String( xml.@victoryAI );
			
			{
			if ( xml.@bonus != undefined )
				_Bonus = int( xml.@bonus );
			}
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			/*
			var mainScore:int = gameData.CurrentScenario.scoringSystem.getMainScore();
			
			var scoreEventHistory:Dictionary = gameData.CurrentScenario.scoringSystem.scoreEventHistory;
			
			var timeBonus:int = 0;
			
			for each ( var amount:int in scoreEventHistory )
			{
				timeBonus += amount;
			}
			
			var aiBonusPercent:Number = 0;
			
			if ( _VictoryAI )
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _VictoryAI );
				
				if ( !ai || ai.Ship.IsDead || ai.Ship.ArmorHealth.current <= 0 )
				{
					aiBonusPercent = 1;
				}
				else
				{
					aiBonusPercent = 1 - ( ai.Ship.ArmorHealth.current / ai.Ship.Info.Armor.FullHitPoints );
				}
			}
			
			var popup:UIWinFailPopup = new UIWinFailPopup( gameData, mainScore, timeBonus, aiBonusPercent, _Bonus * aiBonusPercent );
			
			gameData.Scene.addPopup( popup );
			*/
			
			return true;
		}
	}
}