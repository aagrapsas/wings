package game.scenarios.scripting 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import flash.display.Bitmap;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.gameplay.waypoint.Waypoint;
	import game.gameplay.waypoint.WaypointDir;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	import starling.display.Image;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name setWaypoint
	// @description sets or moves the current waypoint
	// @category actor
	// @isConditional false
	// @param_optional target:String x:int y:int
	public class SetWaypointScript extends Script
	{
		private var _X:int;
		private var _Y:int;
		private var _Target:String;
		
		public function SetWaypointScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_X = xml.@x != undefined ? int( xml.@x ) : 0;
			_Y = xml.@y != undefined ? int( xml.@y ) : 0;
			_Target = xml.@target != undefined ? String( xml.@target ) : null;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			var ai:AIController;
			
			if ( _Target != null && _Target != "" )
			{
				ai = gameData.CurrentScenario.getAI( _Target );
				
				if ( !ai )
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "SetWayPointScript: AI " + _Target + " does not exist!" );
				}
			}
			
			if ( gameData.PlayerWaypoint && gameData.PlayerWaypointDir )
			{
				if ( ai )
				{
					gameData.PlayerWaypoint.Target = ai.Ship;
				}
				else
				{
					gameData.PlayerWaypoint.Target = null;
					gameData.PlayerWaypoint.X = _X;
					gameData.PlayerWaypoint.Y = _Y;
				}
			}
			else
			{
				gameData.PlayerWaypoint = new Waypoint;
				
				var image:Image = gameData.AssetManager.getImageCopy( "waypoint" );
				
				gameData.PlayerWaypoint.Display.addChild( image );
				gameData.PlayerWaypoint.Display.alpha = 0.55;
				gameData.Scene.addActor( gameData.PlayerWaypoint );
				gameData.PlayerWaypoint.Data = gameData;
				
				if ( ai )
				{
					gameData.PlayerWaypoint.Target = ai.Ship;
				}
				else
				{
					gameData.PlayerWaypoint.Target = null;
					gameData.PlayerWaypoint.X = _X;
					gameData.PlayerWaypoint.Y = _Y;
				}
				
				gameData.PlayerWaypoint.initialize();
				gameData.Scene.addTickable( gameData.PlayerWaypoint );
				
				gameData.PlayerWaypointDir = new WaypointDir;
				
				var waypointDirImage:Image = gameData.AssetManager.getImageCopy( "dir_indicator" );
				
				gameData.PlayerWaypointDir.Display.addChild( waypointDirImage );
				gameData.PlayerWaypointDir.Display.alpha = 0.55;
				gameData.Scene.addActor( gameData.PlayerWaypointDir );
				gameData.PlayerWaypointDir.Data = gameData;				
				gameData.PlayerWaypointDir.X = _X;
				gameData.PlayerWaypointDir.Y = _Y;
				gameData.PlayerWaypointDir.initialize();
				gameData.Scene.addTickable( gameData.PlayerWaypointDir );
			}
			
			return true;
		}
	}
}