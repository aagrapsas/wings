package game.scenarios.scripting 
{
	import engine.assets.Animatic;
	import flash.display.Sprite;
	import starling.events.KeyboardEvent;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name playAnimatic
	// @description plays the designated anmatic, can be used as a trigger
	// @category level
	// @isConditional true
	// @param_required loc:String duration:Number
	// @param_optional heightToWidth:Number fadeStart:Number
	public class PlayAnimaticScript extends Script
	{
		private var _Duration:Number;
		private var _Animatic:Animatic;
		private var _Location:String;
		private var _TimeStamp:Number = 0;
		private var _HeightToWidth:Number = 0;
		private var _ShouldSkip:Boolean = false;
		private var _AlphaStart:Number = 0;
		
		private var _BlackScreen:Sprite;
		
		private var _GameData:HomeGameData;
		
		public function PlayAnimaticScript() 
		{
			
		}	
		
		override public function deserialize( xml:XML ):void
		{
			_Location = String( xml.@loc );
			_Duration = Number( xml.@duration );
			_HeightToWidth = xml.@heightToWidth != undefined ? Number( xml.@heightToWidth ) : 1;
			_AlphaStart = xml.@fadeStart != undefined ? Number( xml.@fadeStart ) : 0;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			/*
			if ( _TimeStamp == 0 )
			{
				_TimeStamp = gameData.Scene.GameTime;
				
				// Start
				const isFullScreen:Boolean = gameData.Scene.IsFullScreen;
				var width:int = isFullScreen ? gameData.Scene.Display.stage.fullScreenWidth : gameData.Scene.Display.stage.stageWidth;
				var height:int = isFullScreen ? width * _HeightToWidth : width * _HeightToWidth;
				
				_GameData = gameData;
				gameData.Scene.Display.stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
				
				_Animatic = new Animatic( _Location, width, height, true );
				
				const x:int = isFullScreen ? ( width - 730 ) / 2 : 0;
				var y:int = isFullScreen ? ( height - 600 ) / 2: 0;
				
				_Animatic.x = -x;
				_Animatic.y = -y;
				
				_BlackScreen = new Sprite;
				_BlackScreen.graphics.beginFill( 0x000000, 1 );
				_BlackScreen.graphics.drawRect( 0, 0, gameData.Scene.Display.stage.fullScreenWidth, gameData.Scene.Display.stage.fullScreenHeight );
				_BlackScreen.graphics.endFill();
				
				_BlackScreen.x = -x;
				_BlackScreen.y = -( ( gameData.Scene.Display.stage.fullScreenHeight - 600 ) / 2 );
				
				gameData.Scene.Display.stage.addChild( _BlackScreen );				
				gameData.Scene.Display.stage.addChild( _Animatic );
			}
			
			const elapsed:Number = gameData.Scene.GameTime - _TimeStamp;
			
			if ( elapsed >= _AlphaStart )
			{
				const alphaDuration:Number = _Duration - _AlphaStart;
				const elapsedAlpha:Number = elapsed - _AlphaStart;
				const alpha:Number = 1 - elapsedAlpha / alphaDuration;
				
				_Animatic.alpha = Math.max( alpha, 0 );
				_BlackScreen.alpha = Math.max( alpha, 0 );
			}
			
			if ( elapsed >= _Duration || _ShouldSkip )
			{
				// Stop
				_Animatic.pause();
				gameData.Scene.Display.stage.removeChild( _BlackScreen );
				gameData.Scene.Display.stage.removeChild( _Animatic );
				
				return true;
			}
			
			return false;
			*/
			
			return true;
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			if ( e.keyCode == _GameData.InputMap[ "skip_animatic" ] )
			{
				_ShouldSkip = true;
				
				_GameData.Scene.Display.stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
				_GameData = null;
			}
		}
		
		override public function reset():void
		{
			super.reset();
			
			_TimeStamp = 0;
		}
	}
}