package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name setVisibility
	// @description sets an actor's visibility (actual image)
	// @category actor
	// @isConditional false
	// @param_required target:String value:Boolean
	public class SetVisibilityScript extends Script
	{
		private var _Target:String;
		private var _Value:Boolean = false;
		
		public function SetVisibilityScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_Value = String( xml.@value ) == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Target == "player" )
			{
				gameData.PlayerShip.Display.Display.visible = _Value;
			}
			else
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _Target );
				
				if ( ai )
				{
					ai.Ship.Display.Display.visible = _Value;
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "SetVisibilityScript: AI " + _Target + " doesn't exist" );
				}
			}
			
			return true;
		}
	}
}