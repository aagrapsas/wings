package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name setHUDVisibility
	// @description toggles hud visibility
	// @category level
	// @isConditional false
	// @param_required value:Boolean
	public class SetHUDVisibilityScript extends Script
	{
		private var _Value:Boolean;
		
		public function SetHUDVisibilityScript() 
		{
			
		}
	
		override public function deserialize( xml:XML ):void
		{
			_Value = String( xml.@value ) == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.PlayerController.setHUDVisibility( _Value );
			
			return true;
		}
	}
}