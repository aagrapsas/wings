package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import flash.utils.getQualifiedClassName;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Script 
	{
		public var Children:Vector.<Script>;
		
		private var rawXML:XML;
		
		public function Script() 
		{
			Children = new Vector.<Script>;
		}
		
		public function deserializeChildren( xml:XML ):void
		{	
			Debug.debugLog( GameLogChannels.SCRIPT, "Deserializing script: " + getQualifiedClassName( this ) );
			
			rawXML = xml;
			
			for each ( var subXML:XML in xml.* )
			{
				var script:Script = ScriptFactory.getScript( subXML.localName() );
				
				if ( !script )
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "Invalid script name " + subXML.localName() );
					continue;
				}
				
				script.deserializeChildren( subXML );
				script.deserialize( subXML );
				Children.push( script );
			}
		}
		
		public function isValid( gameData:HomeGameData ):Boolean
		{
			var name:String = rawXML.name().localName;
			
			return gameData.XMLValidators.isValidXML( name, rawXML );
		}
		
		public function deserialize( xml:XML ):void
		{
			
		}
		
		public function execute( gameData:HomeGameData ):Boolean
		{
			return true;
		}
		
		public function reset():void
		{
			for each ( var child:Script in Children )
			{
				child.reset();
			}
		}
	}
}