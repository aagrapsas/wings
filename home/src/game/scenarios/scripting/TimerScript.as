package game.scenarios.scripting 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name timer
	// @description triggers an event after an amount of time
	// @category trigger
	// @isConditional true
	// @param_required duration:Number
	public class TimerScript extends Script
	{
		private var _TimeTarget:Number;
		private var _Stamp:Number = 0;
		
		public function TimerScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_TimeTarget = Number( xml.@duration );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Stamp == 0 )
				_Stamp = gameData.Scene.GameTime;
			
			return ( gameData.Scene.GameTime - _Stamp >= _TimeTarget );
		}
		
		override public function reset():void
		{
			super.reset();
			
			_Stamp = 0;
		}
	}
}