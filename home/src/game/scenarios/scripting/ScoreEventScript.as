package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.debug.GameLogChannels;
	import game.gameplay.scoring.ScoreEvent;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ScoreEventScript extends Script
	{	
		private var _Key:String;
		private var _MaxAmount:int;
		private var _MinAmount:int;
		private var _MaxTime:Number;
		private var _MinTime:Number;
		private var _Delay:Number;
		private var _Score:int;
		
		private var _ShouldExecute:Boolean = false;
		
		public function ScoreEventScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Key = String( xml.@key );
			_MaxAmount = xml.@maxAmount != undefined ? int( xml.@maxAmount ) : 0;
			_MinAmount = xml.@minAmount != undefined ? int( xml.@minAmount ) : 0;
			_MaxTime = xml.@maxTime != undefined ? int( xml.@maxTime ) : 0;
			_MinTime = xml.@minTime != undefined ? int( xml.@minTime ) : 0;
			_Delay = xml.@delay != undefined ? int( xml.@delay ) : 0;
			_Score = xml.@score != undefined ? int( xml.@score ) : 0;
			
			_ShouldExecute = xml.@shouldExecute != undefined && String( xml.@shouldExecute ) == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			/*
			if ( !_Key )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "ScoreEventScript: no key provided!" );
				return true;
			}
			
			if ( _ShouldExecute && _MaxAmount == 0 && _MinAmount == 0 && _MaxTime == 0 && _MinTime == 0 && _Delay == 0 && _Score == 0 )
			{
				gameData.CurrentScenario.scoringSystem.triggerScoreEvent( _Key );
				return true;
			}
			
			var scoreEvent:ScoreEvent = new ScoreEvent( _Key, _MaxAmount, _MinAmount, _MaxTime, _MinTime, _Delay, _Score );
			
			gameData.CurrentScenario.scoringSystem.registerScoreEvent( scoreEvent );
			
			if ( _ShouldExecute )
			{
				gameData.CurrentScenario.scoringSystem.triggerScoreEvent( _Key );
			}
			*/
			
			return true;
		}
	}
}