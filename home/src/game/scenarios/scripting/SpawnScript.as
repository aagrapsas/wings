package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.actors.SpaceShip;
	import game.ai.AttackState;
	import game.ai.IState;
	import game.ai.StateFactory;
	import game.ai.StateTypes;
	import game.controllers.AIController;
	import game.controllers.IController;
	import game.controllers.PlayerSpaceShipController;
	import game.debug.GameLogChannels;
	import game.fx.HyperspaceFX;
	import game.HomeGameData;
	import game.misc.HomePoolConfig;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name spawn
	// @description spawns an actor
	// @category actor
	// @isConditional false
	// @param_required name:String shipType:String startX:int startY:int team:int
	// @param_optional squad:String controller:String locomotion:String startRot:Number target:String
	public class SpawnScript extends Script
	{		
		private var _Name:String;
		private var _ShipType:String;
		private var _Squad:String;
		private var _ControllerType:String;
		private var _LocomotionState:String;
		private var _StartX:int;
		private var _StartY:int;
		private var _Team:int;
		private var _StartRot:int;
		private var _Target:String;
		private var _ShouldHyperspace:Boolean = false;
		private var _ShouldShowName:Boolean = false;
		
		private var _IsLocationPlayerRelative:Boolean = false;
		
		public function SpawnScript() 
		{
			
		}	
		
		override public function deserialize( xml:XML ):void
		{
			_Name = String( xml.@name );
			_Squad = xml.@squad != undefined ? String( xml.@squad ) : null;
			_ControllerType = xml.@controller != undefined ? String( xml.@controller ) : null;
			_ShipType = String( xml.@shipType );
			_LocomotionState =  xml.@locomotion != undefined ? String( xml.@locomotion ) : null;
			_StartX = int( xml.@startX );
			_StartY = int( xml.@startY );
			_Team = int( xml.@team );
			_StartRot = xml.@startRot != undefined ? int( xml.@startRot ) : 0;
			_Target = String( xml.@target );
			
			_ShouldHyperspace = xml.@shouldHyperspace != undefined && String( xml.@shouldHyperspace ) == "true";
			_ShouldShowName = xml.@shouldShowName != undefined && String( xml.@shouldShowName ) == "true";
			
			_IsLocationPlayerRelative = xml.@isLocationPlayerRelative != undefined && ( String( xml.@isLocationPlayerRelative ) == "true" ) ? true : false;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			var ship:SpaceShip = null;
			
			if ( _ControllerType == "player" && gameData.ClientData.activeShip != null )
			{
				ship = gameData.Spawner.getShipFromInfo( gameData.ClientData.activeShip );
			}
			else
			{
				ship = gameData.Spawner.getShip( _ShipType );
			}
			
			if ( !ship )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "SpawnScript: unable to spawn ship of type " + _ShipType + ", it doesn't exist!" );
				return true;
			}
			
			if ( _IsLocationPlayerRelative && gameData.PlayerShip )
			{
				_StartX += gameData.PlayerShip.X;
				_StartY += gameData.PlayerShip.Y;
			}
			
			// Values shared across all ships
			ship.Team = _Team;
			ship.X = _StartX;
			ship.Y = _StartY;
			
			// Set any initial rotation
			if ( _StartRot != 0 )
			{
				ship.rotate( _StartRot )
			}
			
			// Handle player controller
			if ( _ControllerType == "player" )
			{
				gameData.PlayerShip = ship;
				
				var playerController:PlayerSpaceShipController = new PlayerSpaceShipController( ship, gameData );
				
				gameData.PlayerController = playerController;
			}
			else if ( _ControllerType == "ai" )	// Handle AI controller
			{
				var AI:AIController = new AIController( ship, gameData );
				
				// Set default locomotion
				if ( _LocomotionState != null && _LocomotionState != "" )
				{
					var state:IState = StateFactory.getState( _LocomotionState, ship, gameData, AI );
					AI.setLocomotionState( state );
					AI.setMainLocomotionState( state );
				}
				
				// Build squad, if needed
				if ( _Squad != null && _Squad != "" )
				{
					gameData.CurrentScenario.registerSquadMember( _Squad, AI );
				}
					
				// Register to AI list for script access
				if ( _Name )
				{
					gameData.CurrentScenario.registerAI( _Name, AI );
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "SpawnScript: spawning ship of type " + _ShipType + " without name! Please provide a unique name!" );
				}
				
				var target:SpaceShip;
				
				if ( _Target != null && _Target != "" )
				{
					if ( _Target == "player" )
					{
						target = gameData.PlayerShip;
					}
					else
					{
						var targetAI:AIController = gameData.CurrentScenario.getAI( _Target );
						target = targetAI.Ship;
					}
				}
				
				AI.goToExplicitState( StateTypes.ATTACK_STATE, new AttackState( AI.Ship, gameData, AI, target ) );
			}
			
			if ( _ShouldHyperspace )
			{
				var fx:HyperspaceFX = new HyperspaceFX( gameData, ship, ship.Dir.x, ship.Dir.y );
				fx.jumpIn( ship.X, ship.Y, null );
			}
			
			if ( _ShouldShowName )
			{
				//var info:UIShipInfo = gameData.Pools.getPool( HomePoolConfig.WORLD_SHIP_INFO ).pop() as UIShipInfo;
				//info.setup( ship, _Name );
			}
			
			return true;
		}
	}
}