package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ScriptFactory 
	{		
		public static function getScript( type:String ):Script 
		{
			switch( type )
			{
				case ScriptTypes.MOVE_TO:
					return new MoveToScript();
				case ScriptTypes.SPAWN:
					return new SpawnScript();
				case ScriptTypes.LOAD_SCENARIO:
					return new LoadScenarioScript();
				case ScriptTypes.TOUCH:
					return new TouchScript();
				case ScriptTypes.TIMER:
					return new TimerScript();
				case ScriptTypes.LOG:
					return new LogScript();
				case ScriptTypes.DAMAGE:
					return new DamageScript();
				case ScriptTypes.INDESTRUCTABLE:
					return new IndestructableScript();
				case ScriptTypes.KILL:
					return new KillScript();
				case ScriptTypes.SHOW_BLINDS:
					return new ShowBlindsScript();
				case ScriptTypes.SHOW_CAPTION:
					return new ShowCaptionScript();
				case ScriptTypes.ALLOW_INPUT:
					return new AllowInputScript();
				case ScriptTypes.PLAY_SOUND:
					return new PlaySoundScript();
				case ScriptTypes.STOP_SOUND:
					return new StopSoundScript();
				case ScriptTypes.DEATH:
					return new DeathScript();
				case ScriptTypes.SET_WAYPOINT:
					return new SetWaypointScript();
				case ScriptTypes.CLEAR_WAYPOINT:
					return new ClearWaypointScript();
				case ScriptTypes.LOAD_PARALLAX_LAYER:
					return new LoadParallaxLayerScript();
				case ScriptTypes.PLAY_HYPERSPACE:
					return new PlayHyperspaceFXScript();
				case ScriptTypes.SET_VISIBILITY:
					return new SetVisibilityScript();
				case ScriptTypes.FADE_TO_BLACK:
					return new FadeToBlackScript();
				case ScriptTypes.PAN_CAMERA:
					return new PanCameraScript;
				case ScriptTypes.SET_CONTROLLER:
					return new SetControllerScript();
				case ScriptTypes.PLAY_ANIMATIC:
					return new PlayAnimaticScript();
				case ScriptTypes.SET_HUD_VISIBILITY:
					return new SetHUDVisibilityScript();
				case ScriptTypes.ATTACH_ACTOR:
					return new AttachActorScript();
				case ScriptTypes.ATTACK_TARGET:
					return new AttackTargetScript();
				case ScriptTypes.FOLLOW_ACTOR:
					return new FollowActorScript();
				case ScriptTypes.CAMERA_SHAKE:
					return new CameraShakeScript();
				case ScriptTypes.FLASH:
					return new FlashScript();
				case ScriptTypes.LOOP:
					return new LoopScript();
				case ScriptTypes.WORLD_FX:
					return new WorldFXScript();
				case ScriptTypes.SCORE_EVENT:
					return new ScoreEventScript();
				case ScriptTypes.SHOW_POPUP:
					return new ShowPopupScript();
				case ScriptTypes.END_GAME:
					return new EndGameScript();
				case ScriptTypes.NOTIFY_WAVE:
					return new NotifyWaveScript();
				case ScriptTypes.SET_SCORING_TYPE:
					return new SetScoringTypeScript();
				case ScriptTypes.SHOW_HELP:
					return new ShowHelpScript();
			}
			
			return null;
		}	
	}
}