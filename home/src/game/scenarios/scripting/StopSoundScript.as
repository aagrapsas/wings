package game.scenarios.scripting 
{
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name stopSound
	// @description stops a sound
	// @category audio
	// @isConditional false
	// @param_required name:String
	// @param_optional fade:Number
	public class StopSoundScript extends Script
	{
		private var _Sound:String;
		private var _FadeDuration:Number;
		private var _FadeStart:Number = 0;
		
		private var _Channel:SoundChannel;
		private var _Transform:SoundTransform;
		
		public function StopSoundScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Sound = String( xml.@name );
			_FadeDuration = xml.@fade != undefined ? Number( xml.@fade ) : 0;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _FadeDuration != 0 )
			{
				if ( _FadeStart == 0 )
				{
					_FadeStart = gameData.Scene.GameTime;
				}
				
				var elapsedTime:Number = gameData.Scene.GameTime - _FadeStart;
				var alpha:Number = Math.max( 1 - elapsedTime / _FadeDuration, 0 );
				
				// @TODO: needs to account for volume not being 1 at maximum for all things!
				gameData.SoundSystem.getSound( _Sound ).setVolume( alpha );
				
				if ( alpha == 0 )
				{
					gameData.SoundSystem.stopSound( _Sound );
				}
				
				return alpha == 0;
			}
			else
			{
				gameData.SoundSystem.stopSound( _Sound );
			
				return true;
			}
		}
	}
}