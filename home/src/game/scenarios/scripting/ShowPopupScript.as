package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShowPopupScript extends Script
	{
		private var _Title:String;
		private var _Subtitle:String;
		private var _Body:String;
		private var _Button:String;
		private var _ShouldPause:Boolean = false;
		
		public function ShowPopupScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			if ( xml.@shouldPause != undefined )
			{
				_ShouldPause = String( xml.@shouldPause ) == "true";
			}
			
			_Title = String( xml.@title );
			_Subtitle = String( xml.@subtitle );
			_Body = String( xml.@body );
			_Button = String( xml.@button );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			//gameData.Scene.addPopup( new UIGenericPopup( gameData, _Title, _Subtitle, _Body, _Button, null, null, _ShouldPause ) );
			
			return true;
		}
	}
}