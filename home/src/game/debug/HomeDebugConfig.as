package game.debug 
{
	import engine.collision.CollisionInfo;
	import engine.debug.Debug;
	import engine.debug.DebugConfig;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import engine.render.animation.Animation;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.controllers.AIController;
	import game.fx.HyperspaceFX;
	import game.fx.WorldNumber;
	import game.HomeGameData;
	import game.misc.HomePoolConfig;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class HomeDebugConfig extends DebugConfig
	{
		private var _HomeGameData:HomeGameData;
		
		// For test stuff
		private var _Anim:Animation;	// used by testAnim
		
		public function HomeDebugConfig( gameData:HomeGameData ) 
		{
			super( gameData );
			
			_HomeGameData = gameData;
		}
		
		public override function initialize():void
		{
			//super.initialize();
			
			/*
			_GameData.DebugSystem.addDebugFunction( "testparticle", testParticle );
			_GameData.DebugSystem.addDebugFunction( "angle", angleBetween );
			_GameData.DebugSystem.addDebugFunction( "fade", fade );
			_GameData.DebugSystem.addDebugFunction( "lerp", lerp );
			_GameData.DebugSystem.addDebugFunction( "god", god );
			_GameData.DebugSystem.addDebugFunction( "spawndrone", spawnDrone );
			_GameData.DebugSystem.addDebugFunction( "intersect", intersect );
			_GameData.DebugSystem.addDebugFunction( "explode", explode );
			_GameData.DebugSystem.addDebugFunction( "shake", shake );
			_GameData.DebugSystem.addDebugFunction( "flash", flash );
			_GameData.DebugSystem.addDebugFunction( "load", load );
			_GameData.DebugSystem.addDebugFunction( "reset", reset );
			_GameData.DebugSystem.addDebugFunction( "testanim", testAnim );
			//_GameData.DebugSystem.addDebugFunction( "testswf", testSwf );
			_GameData.DebugSystem.addDebugFunction( "testnum", testNum );
			_GameData.DebugSystem.addDebugFunction( "reinforce", reinforce );
			_GameData.DebugSystem.addDebugFunction( "popup", popup );
			_GameData.DebugSystem.addDebugFunction( "menu", menu );
			_GameData.DebugSystem.addDebugFunction( "audio", audio );
			_GameData.DebugSystem.addDebugFunction( "help", help );
			_GameData.DebugSystem.addDebugFunction( "win", win );
			_GameData.DebugSystem.addDebugFunction( "small", small );
			*/
		}
		
		private function testParticle():void
		{
			const shipX:int = _HomeGameData.PlayerShip.X;
			const shipY:int = _HomeGameData.PlayerShip.Y;
			const shipFacing:Point = _HomeGameData.PlayerShip.Dir;
		}
		
		private function lerp( start:int, end:int, alpha:Number ):void
		{
			trace( "Lerp: " + MathUtility.lerpInt( start, end, alpha ) );
		}
		
		private function spawnDrone( x:int, y:int ):void
		{		
			var ship:SpaceShip = _HomeGameData.Spawner.getShip( "Fighter" );
			
			ship.Team = 200;
			ship.X = x;
			ship.Y = y;
			
			var ai:AIController = new AIController( ship, _HomeGameData );
		}
		
		public function angleBetween( x1:Number, y1:Number, x2:Number, y2:Number ):void
		{
			var angle:Number = Math.atan2( x1, y1 ) - Math.atan2( x2, y2 );
			
			trace( angle * 180 / Math.PI );
		}
		
		public function fade( dir:int, duration:Number ):void
		{
			_GameData.Scene.fadeToBlack( dir, duration );
		}
		
		public function god():void
		{
			var data:HomeGameData = _GameData as HomeGameData;
			
			data.PlayerShip.CanTakeDamage = false;
		}
		
		private function intersect( x1:int, y1:int, x2:int, y2:int, x3:int, y3:int, x4:int, y4:int ):void
		{
			var intersection:CollisionInfo = MathUtility.getLineIntersection( x1, y1, x2, y2, x3, y3, x4, y4 );
			
			if ( intersection.DidCollide )
			{
				trace( "Collided at x: " + intersection.X + " y: " + intersection.Y );
			}
			else
			{
				trace( "No collision." );
			}
		}
		
		private function explode( x:int, y:int ):void
		{
			var data:HomeGameData = _GameData as HomeGameData;
			
			// data.ExplosionPool.explode( x, y );
		}
		
		private function shake( max:int, duration:Number ):void
		{
			var data:HomeGameData = _GameData as HomeGameData;
			
			data.ActiveCamera.shake( max, duration, null );
		}
		
		private function flash( duration:Number ):void
		{
			_GameData.Scene.Flash.flash( duration );
		}
		
		private function load( name:String ):void
		{
			var xml:XML = _HomeGameData.AssetManager.getXML( name );
			
			if ( !xml )
			{
				Debug.debugLog( GameLogChannels.DATA, "Level " + name + " does not exist!" );
				return;
			}
			
			var menu:UIMenuContainer = null;
			
			for each ( var tickable:ITickable in _GameData.Scene.Tickables )
			{
				if ( tickable is UIMenuContainer )
				{
					menu = tickable as UIMenuContainer;
					break;
				}
			}
			
			if ( menu )
			{
				_HomeGameData.Scene.removeTickable( menu );
				
				menu.destroy();
			}
			
			if ( _HomeGameData.CurrentScenario )
			{
				_HomeGameData.CurrentScenario.breakDown();
				_HomeGameData.CurrentScenario.destroy();
			}
			
			_HomeGameData.CurrentScenario = new Scenario( _HomeGameData );
			_HomeGameData.CurrentScenario.deserialize( xml );
			_HomeGameData.CurrentScenario.setup();
		}
		
		private function reset():void
		{
			var name:String = _HomeGameData.CurrentScenario.name;
			
			load( name );
		}
		
		private function testAnim( anim:String ):void
		{
			var animation:Animation = _GameData.AnimationSystem.getAnimation( anim );
			
			if ( !animation )
			{
				Debug.debugLog( GameLogChannels.DATA, "Animation " + anim + " doesn't exist!" );
				return;
			}
			
			if ( _Anim )
			{
				//_GameData.DebugSystem.DebugSprite.removeChild( _Anim );
				_GameData.Scene.removeTickable( _Anim );
			}
			
			//_GameData.DebugSystem.DebugSprite.addChild( animation );
			_GameData.Scene.addTickable( animation );
			
			animation.x = _GameData.ActiveCamera.X;
			animation.y = _GameData.ActiveCamera.Y;
			
			_Anim = animation;
		}
		
		/*
		private function testSwf( name:String ):void
		{
			var type:Class = _GameData.AssetManager.swfManager.getClass( name );
			
			if ( !type )
			{
				Debug.debugLog( GameLogChannels.DATA, "Swf class " + name + " doesn't exist!" );
				return;
			}
			
			var obj:DisplayObject = new type() as DisplayObject;
			
			if ( !obj )
			{
				Debug.debugLog( GameLogChannels.DATA, "Unable to build object of type " + name );
				return;
			}
			
			_GameData.DebugSystem.DebugSprite.addChild( obj );
		}
		*/
		
		private function testNum():void
		{
			var num:int = Math.random() * 1000;
			
			var number:WorldNumber = _GameData.Pools.getPool( HomePoolConfig.WORLD_NUMBER ).pop() as WorldNumber;
			number.spawn( String( num ), _GameData.ActiveCamera.X, _GameData.ActiveCamera.Y );
		}
		
		private function reinforce():void
		{
			var data:HomeGameData = _GameData as HomeGameData;
			
			var ship:SpaceShip = data.Spawner.getShip( "Bomber" );
			ship.Team = data.PlayerShip.Team;
			
			_GameData.Scene.addActor( ship );
			
			var fx:HyperspaceFX = new HyperspaceFX( data, ship, 0, -1 );	// fly in from top
			fx.jumpIn( data.PlayerShip.X, data.PlayerShip.Y, null );
		}
		
		private function popup():void
		{
			//_GameData.Scene.addPopup( new UIGenericPopup( _HomeGameData, "Test Popup", "This is just a test!", "Welcome to a test that will test the testiness of testing things that are testy by testing!", "OK", null, null ) );
		}
		
		private function help():void
		{
			//_GameData.Scene.addPopup( new UIHelpScreen( _HomeGameData ) );
		}
		
		private function win():void
		{
			//_GameData.Scene.addPopup( new UIWinFailPopup( _HomeGameData, 700, 7000000, 0.80 ) );
		}
		
		private function menu():void
		{
			/*
			if ( _HomeGameData.CurrentScenario != null )
			{
				_HomeGameData.CurrentScenario.breakDown();
				_HomeGameData.CurrentScenario.destroy();
				_HomeGameData.Scene.addTickable( new UIMenuContainer( _HomeGameData ) );
			}
			*/
		}
		
		private function audio( name:String ):void
		{
			_GameData.SoundSystem.playSound( name, 0, null );
		}
		
		private function small():void
		{
			//_GameData.Scene.addPopup( new UIGenericPopupSlim( _HomeGameData, "Pause", "This game is paused.", "Unpause bitch", null, null, null ) );
		}
	}
}