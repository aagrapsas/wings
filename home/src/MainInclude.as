package  
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import starling.events.Event;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class MainInclude extends Sprite
	{
		CONFIG::ship
		{
			
			[Embed(source = "../bin/data/generated/animations_pack.swf", mimeType = "application/octet-stream")]
			private var animations_pack:Class;
			
			[Embed(source = "../bin/data/generated/audio.swf", mimeType = "application/octet-stream")]
			private var audio:Class;
			
			[Embed(source = "../bin/data/generated/environment_and_fx.swf", mimeType = "application/octet-stream")]
			private var environment_and_fx:Class;
			
			[Embed(source = "../bin/data/generated/icons.swf", mimeType = "application/octet-stream")]
			private var icons:Class;
			
			[Embed(source = "../bin/data/generated/manifest.xml", mimeType = "application/octet-stream")]
			private var manifest:Class;
			
			[Embed(source = "../bin/data/generated/ships.swf", mimeType = "application/octet-stream")]
			private var ships:Class;
			
			[Embed(source = "../bin/data/generated/userinterface.swf", mimeType = "application/octet-stream")]
			private var userinterface:Class;
			
			[Embed(source = "../bin/data/generated/weapons.swf", mimeType = "application/octet-stream")]
			private var weapons:Class;
			
			// FLA generated swfs
			[Embed(source = "../bin/swf/ingameui.swf", mimeType = "application/octet-stream")]
			private var ingameui:Class;
			
			[Embed(source = "../bin/swf/mainmenu.swf", mimeType = "application/octet-stream")]
			private var mainmenu:Class;
			
			// XML assets
			[Embed(source = "../bin/data/generated/xml.swf", mimeType = "application/octet-stream")]
			private var xml:Class;
			
			private var _PreloadAssets:Dictionary;
			
			private var _ConcreteClasses:Dictionary;
			private var _Loaders:Dictionary;
			private var _Manifest:XML;
			
			public function MainInclude() 
			{
				_Manifest = new XML( new manifest() );
				
				_ConcreteClasses = new Dictionary();
				_ConcreteClasses[ "animations_pack" ] = new animations_pack();
				_ConcreteClasses[ "audio" ] = new audio();
				_ConcreteClasses[ "environment_and_fx" ] = new environment_and_fx();
				_ConcreteClasses[ "icons" ] = new icons();
				_ConcreteClasses[ "ships" ] = new ships();
				_ConcreteClasses[ "userinterface" ] = new userinterface();
				_ConcreteClasses[ "weapons" ] = new weapons();
				_ConcreteClasses[ "ingameui" ] = new ingameui();
				_ConcreteClasses[ "mainmenu" ] = new mainmenu();
				_ConcreteClasses[ "xml" ] = new xml();
				
				_Loaders = new Dictionary();
				
				// Setup all loaders
				for ( var key:String in _ConcreteClasses )
				{
					var loader:Loader = new Loader();
					loader.loadBytes( _ConcreteClasses[ key ] );
					
					_Loaders[ key ] = loader;
				}
				
				this.addEventListener( Event.ENTER_FRAME, onEnterFrame );
			}
			
			private function onEnterFrame( e:Event ):void
			{
				for each ( var loader:Loader in _Loaders )
				{
					if ( loader.contentLoaderInfo != null )
					{
						deserialize( _Manifest );
						
						this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
						
						break;
					}
				}
			}
			
			private function deserialize( xml:XML ):void
			{
				_PreloadAssets = new Dictionary();
				
				for each ( var manifestXML:XML in xml.manifest )
				{
					const swfName:String = manifestXML.@name;
					
					var loader:Loader = _Loaders[ swfName ];
					
					for each ( var assetXML:XML in manifestXML.file )
					{
						const assetName:String = assetXML.@name;
						const domainName:String = swfName + "_" + assetName;
						const count:int = int( assetXML.@count );
						const type:String = assetXML.@type;
						
						var raw:Class = null;
						
						if ( type == "Animation" && count > 1 )
						{
							var bitmaps:Vector.<Bitmap> = new Vector.<Bitmap>();
							
							for ( var iAnimIndex:int = 0; iAnimIndex < count; iAnimIndex++ )
							{
								raw = loader.contentLoaderInfo.applicationDomain.getDefinition( domainName + iAnimIndex.toString() ) as Class;
								
								bitmaps.push( new raw() );
							}
							
							_PreloadAssets[ assetName ] = bitmaps;
						}
						else
						{
							raw = loader.contentLoaderInfo.applicationDomain.getDefinition( domainName ) as Class;
							
							if ( type == "XML" )
							{
								_PreloadAssets[ assetName ] = new XML( new raw() );						
								
								if ( assetName == "swf_manifest" )
								{
									loadExternalSWFs( _PreloadAssets[ assetName ] );
								}
							}
							else
							{
								_PreloadAssets[ assetName ] = new raw();
							}
						}
					}
				}
				
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
			
			private function loadExternalSWFs( xml:XML ):void
			{
				for each ( var swfXML:XML in xml.swf )
				{
					var loader:Loader = _Loaders[ String( swfXML.@name ) ];
					
					for each ( var exportXML:XML in swfXML.export )
					{
						var name:String = exportXML.@name;
						var domain:String = exportXML.@domain;
						
						var raw:Class = loader.contentLoaderInfo.applicationDomain.getDefinition( domain ) as Class;
						
						_PreloadAssets[ name ] = raw;
					}
				}
			}
			
			public function getAssets():Dictionary
			{
				return _PreloadAssets;
			}
		
		}
	}
}