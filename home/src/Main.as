package 
{
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.Dictionary;
	import game.Game;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Main extends Sprite 
	{
		private var _Starling:Starling;
		
		public function Main():void 
		{
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener( Event.DEACTIVATE, deactivate );
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			_Starling = new Starling( Game, this.stage );
			_Starling.start();
		}

		private function deactivate( e:Event ):void 
		{
			// auto-close
			NativeApplication.nativeApplication.exit();
		}
	}
	
}